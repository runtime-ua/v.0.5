<?php
namespace proto;

/**
 * Исключение выбрасываемое если у пользователя нет прав
 *
 * @author Mendel
 */
class ForbiddenException extends \Exception
{
}
