<?php
namespace proto;

/**
 * Абстрактный прототип для веб-контроллерров
 *
 * @author Mendel
 */
abstract class WebController extends \drycore\util\web\AbstractWebController
{
    /**
     * Оборачивать ли все действия в транзакции
     * @var boolean
     */
    protected $needTransaction = true;
    
    /**
     * Обернем наши действия в транзакцию открываемою в начале
     * и завершаемую после удачного выполнения действия
     */
    public function action()
    {
        if ($this->needTransaction) {
            s()->db->beginTransaction();
        }
        parent::action();
        if ($this->needTransaction) {
            s()->db->commit();
        }
    }
    
    /**
     * Если произошел редирект, то возврата в действие нет,
     * так что если у нас есть транзакция - завершим ее
     */
    protected function redirect()
    {
        if ($this->needTransaction) {
            s()->db->commit();
        }
        parent::redirect();
    }
}
