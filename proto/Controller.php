<?php
namespace proto;

/**
 * Абстрактный контроллер, родитель всех контроллеров,
 * как веб, так и консоли
 *
 * @author Mendel
 */
abstract class Controller extends \drycore\core\controller\AbstractController
{
}
