<?php
namespace proto;

/**
 * Абстрактный конфигурируемый сервис, такой как фабрики MVC и т.п.
 *
 * @author Mendel
 */
abstract class ConfigurableService extends \drycore\sys\service\AbstractConfigurableService
{
}
