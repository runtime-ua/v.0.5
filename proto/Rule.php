<?php
namespace proto;

/**
 * Абстрактное правило, являющееся родителем для всех правил
 *
 * @author Mendel
 */
abstract class Rule extends \drycore\sys\rule\AbstractRule
{
}
