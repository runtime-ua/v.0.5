<?php
namespace proto;

/**
 * Абстрактный сервис, являющийся родителем для всех сервисов
 *
 * @author Mendel
 */
abstract class Service extends \drycore\sys\service\AbstractService
{
}
