<?php
namespace proto;

/**
 * Базовый класс модели выбираемый если класс не указан
 *
 * @author Mendel
 */
class Model extends \drycore\sys\standartModel\StandartModel
{
}
