<?php
namespace proto;

/**
 * Абстрактная компонента, все классы фреймворка и приложений в идеале
 * наследуются от нее
 *
 * @author Mendel
 */
abstract class Component extends \drycore\sys\component\AbstractComponent
{
}
