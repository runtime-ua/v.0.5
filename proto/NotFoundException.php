<?php
namespace proto;

/**
 * Исключение выбрасываемое если что-то не найдено и перехватываемое под 404
 *
 * @author Mendel
 */
class NotFoundException extends \Exception
{
}
