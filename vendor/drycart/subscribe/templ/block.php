<?php $subscribe = model('subscribe'); ?>
<!-- section start -->
<!-- ================ -->
<section class="section pv-40 <?=$subscribeClass;?>"
         style="background-image:url('<?=$subscribeImg;?>');background-position:50% 77%;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action text-center">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <h2 class="title"><?=$_->subscribeTitle;?></h2>
                            <p><?=$_->subscribeText;?></p>
                            <div class="separator"></div>
                            <form class="form-inline margin-clear"
                                  method="post" action="<?=url(['subscribe:subscribe']);?>">
                                <div class="form-group has-feedback
                                    <?=showIf($subscribe->error('email'), 'has-warning');?>">
                                    <input
                                        type="email"
                                        name="subscribe[email]"
                                        class="form-control"
                                        placeholder="email"
                                        value="<?=$subscribe->email;?>">
                                    <i class="fa fa-envelope form-control-feedback"></i>
                                    <?php $err = $subscribe->error('email');
                                    if ($err) : ?>
                                    <span class="help-block  has-warning"><?=$err;?></span>
                                    <?php endif; ?>
                                </div>
                                <button type="submit"
                                        class="btn btn-gray-transparent btn-animated margin-clear">
                                    <?=$_->subscribeBtn;?> <i class="fa fa-send"></i>
                                </button>
                            <?= s()->xsrf->field(); ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>
<!-- section end -->