<?php
$subscribe = model('subscribe');
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?=$_->title;?></h3>
  </div>
  <div class="panel-body">
    <form method="post" action="<?=url(['subscribe:subscribe']);?>">
        <div class="input-group">
            <input type="email"
                name="subscribe[email]"
                class="form-control"
                placeholder="email@example.com"
                value="<?=$subscribe->email;?>">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit">
                        <i class="fa fa-fw fa-envelope"></i>
                        <?=$_->btn;?>
                    </button>
                </span>
        </div>
        <?php $err = $subscribe->error('email'); if ($err) : ?>
        <span class="help-block  has-warning"><?=$err;?></span>
        <?php endif; ?>
        <?= s()->xsrf->field(); ?>
    </form>
  </div>
</div>
