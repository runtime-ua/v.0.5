<article>
    <h1><?=$title;?></h1>
    <?=$article;?>
</article>
<form method="post">
    <?=view('drycore/util/form/form', ['model'=>$subscribe]);?>
    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <input type="submit" value="<?=$_->submit;?>" class="submit-button btn btn-default btn-block">
            <?= s()->xsrf->field(); ?>
        </div>
    </div>
</form>