<!-- Section Start -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
    <tr>
        <td>
            <table bgcolor="#fafafa" align="center" border="0" cellpadding="0" cellspacing="0" width="580" style="border-collapse: collapse;">
                <!-- Space -->
                <tr><td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td></tr>
                <tr>
                    <td width="100%" align="center" style="font-size: 28px; line-height: 34px; font-family:helvetica, Arial, sans-serif; color:#343434;">
                        Подписка на новости прошла удачно
                    </td>
                </tr>
                <!-- Space -->
                <tr><td style="font-size: 0; line-height: 0;" height="10">&nbsp;</td></tr>
            </table>
            <!-- First Row -->
            <table bgcolor="#fafafa" align="center" border="0" cellpadding="0" cellspacing="0" width="580" style="border-collapse: collapse;">
                <tr>
                    <td>
                        Вы подписались на рассылку новостей на <?=findModel('config@registry')->siteName;?>.<br>
                        При необходимости Вы можете 
                        <a href="<?=$unsubscribe;?>">отписаться</a>
                        перейдя по ссылке.
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <!-- Space -->
    <tr><td style="font-size: 0; line-height: 0;" height="20">&nbsp;</td></tr>
</table>
<!-- Section End -->
