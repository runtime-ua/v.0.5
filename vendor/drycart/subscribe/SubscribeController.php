<?php
namespace drycart\subscribe;

class SubscribeController extends \proto\WebController
{
    /**
     * Ключи - список экшенов страницы для которых нужно добавить
     * значения - данные для этих страниц
     * @var array
     */
    protected $installPages = [
        'subscribe'=>[],
        'unsubscribe'=>[],
    ];
    
    public function subscribeAction()
    {
        // **********************************************************************
        $model = s()->post->getModel('subscribe');
        if (!$model->isNew() and $model->isValid()) {
            $model->hash = md5($model->created.$model->ip.$model->email.rand());
            $model->save();
            s()->email->send(
                $model->email,
                $this->lang('subscribedSubj').findModel('config@registry')->siteName,
                ['unsubscribe'=>url(['drycart/subscribe/subscribe:unsubscribe','hash'=>$model->hash], true)],
                'drycart/subscribe/email/subscribe'
            );
            // выведем плашку что всё ок
            s()->alert->success($this->lang('subscribed'));
        }
        // Если в форме ошибка
        if (!$model->isNew() and !$model->isValid()) {
            // выведем плашку что ошибка заполнения формы
            s()->alert->warning($this->lang('formError'));
        }
        // **********************************************************************
        $model->scenario('form');
        $this->initShow();
        $this->showVar('subscribe', $model);
        $this->show();
    }
    
    public function unsubscribeAction()
    {
        $model = box('subscribe')->findOne(['hash'=>$this->param['hash']]);
        //
        if ($model) {
            $model->delete();
            // выведем плашку что всё ок
            s()->alert->warning($this->lang('unsubscribed'));
            s()->email->send(
                $model->email,
                $this->lang('unsubscribedSubj').findModel('config@registry')->siteName,
                [],
                'drycart/subscribe/email/unsubscribe'
            );
        } else {
            // выведем плашку что ошибка
            s()->alert->warning($this->lang('unsubscribeError'));
        }
        // **********************************************************************
        $this->initShow();
        $this->show();
    }
}
