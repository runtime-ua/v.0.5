<?php
if (!isset($size)) {
    $size = 6;
}
if (!isset($modelName)) {
    $modelName = 'drycart/feedback/feedback:form';
}
if (!isset($action)) {
    $action = ['drycart/feedback/feedback:default','back'=>true];
}
?>
<a name="<?=$id;?>"></a>
<div class="row" id="<?=$id;?>">
    <div class="col-sm-12">
        <h3><?=$name;?></h3>
        <?=view('drycore/util/form/formBlock', ['size'=>$size,'model'=>model($modelName), 'action'=>$action]);?>
    </div>
</div>
