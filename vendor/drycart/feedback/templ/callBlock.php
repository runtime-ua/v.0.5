<?php assetStart();?>
<style>
    .callBlock {
        padding-bottom: 10px;
        padding-top: 10px;
    }
</style>
<?php assetEnd('headStyle');?>
<a name="<?=$id;?>"></a>
<div class="well">
<div class="row callBlock" id="<?=$id;?>">
    <div class="col-sm-8">
        <span class="h2">Заказать звонок специалиста</span>
    </div>
    <div class="col-sm-4">
    <a href="#" class="btn btn-block btn-lg btn-success" data-toggle="modal"
       data-target="#<?=$id;?>_Modal">
        Заказ звонка
    </a>
    </div>
</div>
</div>    
    
<div class="modal fade" tabindex="-1" role="dialog"  id="<?=$id;?>_Modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title"><?=$_->title;?></h4>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <p><?=$_->desc;?></p>
                <form role="form"  class="form-horizontal" method="post"
                    action="<?=url(['drycart/feedback/call:default','back'=>true]);?>">
                    <?=view('drycore/util/form/form', [
                        'model'=>model('drycart/feedback/call:form'),
                        'noHelp'=>true]);?>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <input type="submit" value="<?=$_->submit;?>"
                                   class="btn btn-primary btn-block">
                            <?= s()->xsrf->field(); ?>
                        </div>
                    </div>
                </form>
                <hr>
                <?=view('drycart/contact/vcart', ['needMap'=>false]);?>
            </div>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
