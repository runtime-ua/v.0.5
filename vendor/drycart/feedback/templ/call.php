<div class="hidden-sm hidden-xs"
     style="position: fixed; bottom: 0px;right: 0px;z-index: 10000;">
    <a href="#"  data-toggle="modal" data-target="#callModal">
        <span class="fa-stack fa-4x">
          <i class="fa fa-circle fa-stack-2x" style="color: #0c0;opacity: 0.9;"></i>
          <i class="fa fa-phone fa-stack-1x fa-inverse fa-spin"></i>
        </span>
    </a>
</div>
<div class="modal fade" tabindex="-1" role="dialog"  id="callModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?=$_->title;?></h4>
        </div>
        <div class="modal-body">
            <div class="container-fluid">
                <p><?=$_->desc;?></p>
                <form role="form"  class="form-horizontal" method="post" action="<?=url(['drycart/feedback/call:default','back'=>true]);?>">
                    <?=view('drycore/util/form/form', ['model'=>model('drycart/feedback/call:form'),'noHelp'=>true]);?>
                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <input type="submit" value="<?=$_->submit;?>" class="btn btn-primary btn-block">
                            <?= s()->xsrf->field(); ?>
                        </div>
                    </div>
                </form>
                <hr>
                <?=view('drycart/contact/vcart', ['needMap'=>false]);?>
            </div>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->