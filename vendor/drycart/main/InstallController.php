<?php
namespace drycart\main;

class InstallController extends AbstractInstallController
{
    protected $controllers = [];
    protected $dropTables = [];
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
    
    public function defaultAction()
    {
        // Разлогинем от греха подальше
        s()->user->logout();
        $pageData = [
            'title' =>'Начнем установку :)',
            'article' =>'Начинаем установку. Пройдите все этапы до конца.'
        ];
        $next = 'db';
        $this->article($pageData, $next);
    }
    
    public function dbAction()
    {
        // Разлогинем от греха подальше
        s()->user->logout();
        $pageData = [
            'title' =>'Настройки базы данных',
            'article' =>'Заполните данные базы данных'
        ];
        $modelName = 'db@registry';
        $next = 'drop';
        $this->regModel($pageData, $modelName, $next);
    }

    public function dropAction()
    {
        // Разлогинем от греха подальше
        s()->user->logout();
        $pageData = [
            'title' =>'ОЧИСТКА ТЕКУЩЕЙ БАЗЫ!',
            'article' =>'Удаляем все таблицы.<h2>ОСТОРОЖНО!!!</h2>'
        ];
        $next = 'structure';
        //
        if ($this->confirm($pageData, $next)) {
            foreach ($this->dropTables as $table) {
                s()->db->go('DROP TABLE IF EXISTS `'.$table.'`;');
            }
            redirect('/?__action='.$next);
        }
    }
    
    public function structureAction()
    {
        // Разлогинем от греха подальше
        s()->user->logout();
        $pageData = [
            'title' =>'Структура',
            'article' =>'Cтруктура базы'
        ];
        $next = 'admin';
        //
        if ($this->confirm($pageData, $next)) {
            // Прям здесь, рядом....
            require_once 'structure.php';
            // выполним запросы
            foreach ($sql as $line) {
                s()->db->go($line);
            }
            redirect('/?__action='.$next);
        }
    }
    
    public function adminAction()
    {
        // Разлогинем от греха подальше
        s()->user->logout();
        $pageData = [
            'title' =>'Создаем администратора',
            'article' =>'Заполните данные будущего администратора'
        ];
        $next = 'addPages';
        //
        $this->registerUser($pageData, $next);
    }
    
    public function addPagesAction()
    {
        $pageData = [
            'title' =>'Инсталяция модулей',
            'article' =>'Инсталяция модулей и базовые страницы'
        ];
        $next = 'end';
        //
        if ($this->confirm($pageData, $next)) {
            // Выполним инсталы для всех указанных контроллеров
            foreach ($this->controllers as $controller) {
                startController([$controller.':install']);
            }
            // Выполним инсталы для всех модулей
            foreach (loadJson(RUNTIME_FOLDER.'/modules.json') as $module) {
                if (s()->controllerFactory->exist($module.'/install')) {
                    startController([$module.'/install:install']);
                }
            }
            redirect('/?__action='.$next);
        }
    }
    
//    public function emailAction() {
//        $pageData = [
//            'metatitle' =>'Настройки отправки почты',
//            'title' =>'Настройки отправки почты',
//            'article' =>'Заполните данные email'
//        ];
//        $modelName = 'email@registry';
//        $next = 'end';
//        //
//        $this->regModel($pageData, $modelName, $next);
//    }

    public function endAction()
    {
        // Грохнем маркер инсталяции
        $result = @unlink(RUNTIME_FOLDER.'/flag/install');
        if (!$result) {
            s()->alert->danger('Не удалось удалить маркер установки. Проверьте права.');
        }
        // Выведем нашу страницу
        $pageData = [
            'title' =>'Установка завершена!',
            'article' =>'Переходите скорее к <a href="/">сайту</a>'
        ];
        $this->article($pageData);
    }
}
