<?php
namespace drycart\main;

/**
 * Адский контроллер для выполнения произвольного кода.
 * Инструмент для отстрела себе ноги.
 * Не использовать если нет четкого понимания того что это такое.
 */
class EvalController extends \drycore\util\standartController\AdminOnepageController
{
    protected $boxName;
    
    public function __construct($config = null)
    {
        //
        parent::__construct($config);
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
    
    public function defaultAction()
    {
        // Если у нас не включен режим разработчика, то ну нафиг.
        // Не умираем потому что иногда при краше так можно вернуть содержимое формы
        if (!defined('DEVMODE')) {
            s()->alert->success($this->lang('warningAlert'));
        }

        //
        $model = s()->post->getModel($this->boxName);
        if (!$model->isNew() and $model->isValid()) {
            // выполним то что передали (ОСТОРОЖНО!!!!)
            ob_start();
            if (defined('DEVMODE')) {
                eval($model->eval);
            }
            $result = ob_get_contents();
            ob_end_clean();
            $this->showVar('result', $result);
            // выведем плашку что всё ок
            s()->alert->success($this->lang('successAlert'));
        }
        $this->initShow();
        $this->showVar('model', $model);
        $this->show();
    }
}
