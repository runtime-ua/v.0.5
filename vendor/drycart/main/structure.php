<?php
/**
 * Здесь мы создаем структуру базы, а уже в инстоллах добавляем данные
 * Так лучше, чтобы не мучаться с зависимостями. Потом сделаем миграции
 */
$sql = [];
// **************************************************************************************
// Создадим таблицу для страниц
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'article',
  `route` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `pageurl` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `folder` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ico` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `style` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created` int(11) NOT NULL,
  `updated` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `updatedBy` int(11) NOT NULL,
  `trash` int(1) NOT NULL,
  `manuf_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `oldprice` float NOT NULL,
  `topsale` int(1) NOT NULL,
  `action` int(1) NOT NULL,
  `newproduct` int(1) NOT NULL,
  `mainmenu` int(1) NOT NULL,
  `menu1` int(1) NOT NULL,
  `menu2` int(1) NOT NULL,
  `menu3` int(1) NOT NULL,
  `menu4` int(1) NOT NULL,
  `menu5` int(1) NOT NULL,
  `thumb` varchar(127) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(255) NOT NULL,
  `metatitle` varchar(255) NOT NULL,
  `breadtitle` varchar(128) NOT NULL,
  `menutitle` varchar(128) NOT NULL,
  `article` text NOT NULL,
  `preview` text NOT NULL,
  `metadesc` text NOT NULL,
  `needslider` int(11) NOT NULL,
  `sliderimg` varchar(255) COLLATE utf8_bin NOT NULL,
  `slidertitle` varchar(255) COLLATE utf8_bin NOT NULL,
  `slidertext` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
//$sql[] = "ALTER TABLE `page`
//  ADD PRIMARY KEY (`id`),
//  ADD KEY `sort` (`sort`);
//";
// **************************************************************************************
// Создадим таблицу фидбеков
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new` int(11) NOT NULL,
  `ip` varchar(64) NOT NULL,
  `created` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(128) NOT NULL,
  `subject` varchar(64) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
// **************************************************************************************
// Создадим таблицу для подписки
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `subscribe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` int(11) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
// **************************************************************************************
// Создадим таблицу для сессий
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sessid` varchar(255) NOT NULL,
  `updated` int(11) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`sessid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
//$sql[] = "ALTER TABLE `session`
//  ADD UNIQUE KEY `session_id` (`sessid`);
//";
// **************************************************************************************
// Создадим таблицу для пользователей
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passHash` varchar(256) NOT NULL,
  `created` int(11) NOT NULL,
  `avatar` varchar(256) NOT NULL,
  `folder` varchar(64) NOT NULL,
  `group` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
// **************************************************************************************
// Создадим таблицу для истории изменений
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `updateHistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modelName` varchar(255) NOT NULL,
  `model_id` int(11) NOT NULL,
  `modelMethod` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `action` varchar(64) NOT NULL,
  `transaction_id` varchar(64) NOT NULL,
  `created` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
// **************************************************************************************
// Создадим таблицы для заказов
// **************************************************************************************
$sql[] = "CREATE TABLE IF NOT EXISTS `drycart_cart_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `new` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_bin NOT NULL,
  `phone` varchar(128) COLLATE utf8_bin NOT NULL,
  `email` varchar(128) COLLATE utf8_bin NOT NULL,
  `city` varchar(128) COLLATE utf8_bin NOT NULL,
  `adress` varchar(255) COLLATE utf8_bin NOT NULL,
  `text` text COLLATE utf8_bin NOT NULL,
  `ip` varchar(64) COLLATE utf8_bin NOT NULL,
  `created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
";
$sql[] = "CREATE TABLE IF NOT EXISTS `drycart_cart_order_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `order_id` varchar(128) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
";
