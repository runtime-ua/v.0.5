<?php
namespace drycart\main;

class AbstractInstallController extends \proto\WebController
{
    protected $serverFolder = SERVER_UPLOADER_FOLDER;
    protected $siteFolder = SITE_UPLOADER_FOLDER;
    protected $needTransaction = false;
    protected $articleView = 'app/install/article';
    protected $actionView = 'app/install/action';
    protected $formView = 'app/install/form';
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
    
    protected function article($pageData, $next = null)
    {
        $this->viewName = $this->articleView;
        $this->initShow($pageData);
        if (!is_null($next)) {
            $this->showVar('next', [$this->selfType().':'.$next]);
        }
        $this->show();
    }
    
    protected function confirm($pageData, $next)
    {
        $this->viewName = $this->actionView;
        if (!isset($this->param['confirm'])) {
            $this->initShow($pageData);
            $this->showVar('next', [$this->selfType().':'.$next]);
            $this->showVar('confirm', [$this->selfType().':'.$this->action,'confirm'=>true]);
            $this->show();
            // По идее не вернемся, но на всякий случай :)
            return false;
        } else {
            return true;
        }
    }

    protected function regModel($pageData, $modelName, $next)
    {
        $this->viewName = $this->formView;
        $model = findModel($modelName);
        // Пробуем обновить данные из POST, если обновили и данные валидны, то сохраним
        if (s()->post->update($model) and $model->isValid()) {
            $model->save();
            redirect([$this->selfType().':'.$next]);
        }
        //
        $this->initShow($pageData);
        $this->showVar('model', $model);
        $this->show();
    }
    
    protected function registerUser($pageData, $next)
    {
        $this->viewName = $this->formView;
        $model = model('user:profile');
        $model->email = 'admin@example.com';
        if (s()->post->update($model) and $model->isValid()) {
            $model->scenario('default');
            $model->save();
            s()->user->login($model);
            redirect([$this->selfType().':'.$next]);
        }
        //
        $this->initShow($pageData);
        $this->showVar('model', $model);
        $this->show();
    }
    
    public function uploaderAction()
    {
        // Проверим что есть параметр папки и не содержит слешей
        $folder = $this->param['folder'];
        if (!$folder) {
            throw new \proto\ForbiddenException();
        }
        if (haveStr($folder, '/')) {
            throw new \proto\ForbiddenException();
        }
        //
        $serverFolder = HOME.'/files/uploads/';
        $siteFolder = '/files/uploads/';
        $selfUrl =  urlXsrf([$this->selfType().':uploader','folder'=>$folder], true);
        //
        $serverFolder = $this->serverFolder . $folder. '/';
        $siteFolder = $this->siteFolder . $folder. '/';
        //
        s()->drycore_util_uploader_uploader->config($serverFolder, $siteFolder, $selfUrl);
    }
}
