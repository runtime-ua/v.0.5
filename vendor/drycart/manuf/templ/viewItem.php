    <?php
    if (isset($model['thumb'])) {
        $articleSize= 9;
    } else {
        $articleSize= 12;
    }
    ?>
<div class="row">
    <?php if (isset($model['thumb'])) : ?>
    <div class="col-md-3 col-xs-12">
        <br>
        <a class="no-link no-color" href="<?=$model->pageurl;?>">
            <img src="<?=$model->imgVariant('thumb', 'small');?>"
                 class="img-thumbnail"  style="min-width: 100%">
        </a>
    </div>
    <?php endif; ?>
    <article class="col-md-<?=$articleSize;?> col-xs-12">
        <header>
            <h2><a class="no-link no-color" href="<?=$model->pageurl;?>"><?=$model->title;?></a></h2>
        </header>
        <div>
            <a class="no-link no-link" href="<?=$model->pageurl;?>">
                <p><?=$model->preview;?></p>
            </a>
        </div>
    </article>
</div>