<?php
$colNum = 12 / $itemNum; // 4
?>
  <div class="col-md-<?=$colNum;?>">
    <a class="thumbnail no-link no-color" href="<?=$model->pageurl;?>">
        <img alt="<?=$model->title;?>" src="<?=$model->imgVariant('thumb', 'thumbnail');?>" class="img-responsive">
    </a>
  </div>
