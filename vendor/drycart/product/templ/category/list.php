<?php
// дополнительное условие - только прямые потомки.
// Может быть уместным, может и не быть. Если категории одноуровневые, то по идее
// пофиг, но если они одноуровневые но с разными родителями, то надо убрать
$page = controller()->pageModel();
$box->where(['parent_id'=>$page->id]);
?>
<?=view('drystyle/basic/boxList', [
    'box'=>$box,
    'itemView'=>'drycart/product/category/viewItem',
    'title'=>$title,
    'article'=>$article
]);
