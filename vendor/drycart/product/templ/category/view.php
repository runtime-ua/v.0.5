<?php
// Пагинация только по товарам, ибо делать категорий столько что надо листать - моветон
// но если надо, то можно сделать наоборот :)
$products = $model->products;
$categorys = $model->categorys;
$pager = view('drycore/util/widget/pager', ['box'=>$products]);
?>
<h1><?=$title;?></h1>
<article>
    <?=$article;?>
</article>
<div class="row row-flex">
    <?=showModels('drycart/product/category/viewItem', $categorys->findAll());?>
</div>
<div class="row row-flex">
<?=showModels('drycart/product/productItem', $products->findAll());?>
</div>
<?=$pager->render()?>