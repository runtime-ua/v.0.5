<?php assetStart();?>
<style>
    .product-status-block {
        position: absolute;
        z-index: 1;
        top: 5px;
        right: 0px;
    }
    .product-status-block .item {
        color:#fff;
        margin-bottom: 5px;
        padding-left: 5px;
        padding-right: 5px;
        opacity: 0.9;
    }
    .product-status-block .newproduct {
        background-color: #090;
    }
    .product-status-block .action {
        background-color: #f00;
    }
    .product-status-block .topsale {
        background-color: #00f;
    }
</style>
<?php assetEnd('headStyle');?>
<?php if ($model->newproduct or $model->action or $model->topsale) :?>
<div class="product-status-block">
    <?php if ($model->newproduct) : ?>
    <div class="item newproduct"><b><?=$_->newproduct;?></b></div>
    <?php endif; ?>
    <?php if ($model->action) : ?>
    <div class="item action"><b><?=$_->action;?></b></div>
    <?php endif; ?>
    <?php if ($model->topsale) : ?>
    <div class="item topsale"><b><?=$_->topsale;?></b></div>
    <?php endif; ?>
</div>
<?php endif;?>
