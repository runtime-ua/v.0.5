<?php $itemColNum = 12 / $itemNum;?>
<?php assetStart();?>
<style>
    .productItem {
        margin-bottom: 15px;
        margin-top: 15px;
    }
    .productItem .itemWraper {
        border: 1px solid gray;
        padding: 10px;
        border-radius: 10px;
        height: 100%;
    }
    .productItem .imgBlock {
        height: 300px;
    }
    .productItem .imgBlock  img {
        display:block;
        margin: 0 auto;
    }
    .productItem .priceBlock {
        position: absolute;
        bottom: 10px;
    }
</style>
<?php assetEnd('headStyle');?>
<div class="col-md-<?=$itemColNum;?> col-sm-6 col-xs-12 productItem">
    <div class="itemWraper">
      <a href="<?=$model->pageurl;?>" class="no-link no-color">
        <div class="imgBlock">
            <?=view('drycart/product/product/status', ['model'=>$model]);?>
          <img src="<?=$model->imgVariant('thumb', 'small-crop-v');?>" alt="<?=$model->title?>"
               class="img-responsive">
        </div>
        <h4 class="text-center"><?=$model->title;?></h4>
        <?php if ($model->preview and $itemNeedPreview) :?>
        <p class="small"> <?=$model->preview;?></p>
        <?php endif;?>
      </a>
        <?php if ($itemNeedManuf and isset($model['manuf'])
                and isset($model->manuf['thumb'])
                and $model->manuf['thumb']) :?>
      <div class="manufBlock text-center">
        <a href="<?=$model->manuf->pageurl?>">
          <img alt="<?=$model->manuf->title?>"
               src="<?=$model->manuf->imgVariant('thumb', 'thumbnail')?>"
               class="img-responsive" style="max-height: 50px;"/>
        </a>
      </div>
        <?php endif;?>
        <?php if ($itemNeedPrice) :?>
      <br><br><br>
      <div class="priceBlock text-center">
        <?php if ($model->oldprice) :?>
          Старая цена:
          <span class="h5" style="color:#f55;">
              <s><?=s()->money->go($model->oldprice, $_->noprice);?></s>
          </span>
        <?php endif;?>
          <br>
          <div class="row">
            <div class="col-lg-6 pull-right">
                <?php $price = s()->money->go($model->price, $_->noprice);?>
              <h6><?=$price;?></h6>
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
                <?php if (s()->viewFactory->exist('drycart/cart/cart/smallAddForm')) :?>
                <?=view('drycart/cart/cart/smallAddForm', ['model'=>$model]);?>
                <?php endif;?>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->      
      </div>
        <?php endif;?>
    </div>
</div>