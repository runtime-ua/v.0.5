<?php assetStart();?>
<style>
.myZoom .myZoomOverlay {
  width:300px;
  height:200px;
  display:none;
  background-repeat:no-repeat;  
  position: absolute;
  z-index: 100;
}
.myZoom img {
float:left;
margin-right: 10px;
}
</style>
<?php assetEnd('headStyle');?>
<?php assetStart();?>
<script>
function zoomIn(event, myZoom) {
  var element = myZoom.getElementsByClassName("myZoomOverlay").item(0);
 var img = myZoom.getElementsByTagName("img").item(0);
   
  element.style.display = "inline-block";
  var backgroundImg = "url('"+ img.dataset.big+ "')";
  element.style.backgroundImage = backgroundImg;
  var posX = event.offsetX ? (event.offsetX) : event.pageX - img.offsetLeft;
  var posY = event.offsetY ? (event.offsetY) : event.pageY - img.offsetTop;
  element.style.backgroundPosition=(-posX*2)+"px "+(-posY*4)+"px";
}

function zoomOut(myZoom) {
  var element = myZoom.getElementsByClassName("myZoomOverlay").item(0);
  element.style.display = "none";
}
</script>
<?php assetEnd('headScript');?>


<?php
//$viewPicColNum = 4;
//$viewH1top = FALSE;
//$viewPriceFull = FALSE;
//
$textColNum = 12 - $viewPicColNum;
if ($viewPriceFull) {
    $priceClass = 'col-md-12';
} else {
    $priceClass = 'col-md-offset-'.$viewPicColNum.' col-md-'.$textColNum;
}
?>
<span itemscope itemtype="http://schema.org/Product">
<?php if ($viewH1top) : ?>
<h1 itemprop="name"><?=$model->title;?></h1>
<?php endif;?>
<div class="row">
    <div class="col-md-<?=$viewPicColNum;?>">
        <?=view('drycart/product/product/status', ['model'=>$model]);?>
        <div class="_thumbnail myZoom"  onmousemove="zoomIn(event, this)" onmouseout="zoomOut(this)">
            <img itemprop="image" alt="<?=$model->title?>"
                src="<?=$model->imgVariant('thumb', 'medium');?>"
                data-big="<?=$model->imgVariant('thumb', 'big');?>"
                class="img-responsive"/>
            <div class="myZoomOverlay"></div>  
        </div>
        <?php if ($viewNeedManuf and isset($model['manuf'])
                and isset($model->manuf['thumb'])
                and $model->manuf['thumb']) :?>
        <center>
            <a href="<?=$model->manuf->pageurl?>">
                <img alt="<?=$model->manuf->title?>"
                     src="<?=$model->manuf->imgVariant('thumb', 'thumbnail')?>"
                     class="img-responsive" style="max-height: 50px;"/>
            </a>
        </center>
        <?php endif;?>
    </div>
    <div class="col-md-<?=$textColNum;?>">
    <?php if (!$viewH1top) : ?>
        <h1 itemprop="name" style="margin-top: -5px;"><?=$model->title;?></h1>
    <?php endif;?>
        <span itemprop="description">
        <?=$model->article;?>
        </span>
    </div>
    <div class="<?=$priceClass;?>">
        <hr>
        <div style="padding-right: 10px; padding-left: 10px;">
            <div class="pull-left">
            <?php if ($model->oldprice) :?>
                <span class="h3" style="color:#f55;">
                    <s><?=s()->money->go($model->oldprice, $_->noprice);?></s>
                </span>
            <?php endif;?>
                <span class="h3"  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                    <?=s()->money->go($model->price, $_->noprice, true);?>
                </span>
            </div>
            <div class="pull-right">
            <?php if (s()->viewFactory->exist('drycart/cart/cart/bigAddForm')) :?>
                <?=view('drycart/cart/cart/bigAddForm', ['model'=>$model]);?>
            <?php endif;?>
            </div>
        </div>
    </div>
</div>
</span>