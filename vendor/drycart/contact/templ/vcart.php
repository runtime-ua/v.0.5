<?php assetStart();?>
<style>
.contact-row {
  position: relative;
}
.contact-block table {
    margin-bottom: 0;
}
.map-block {
  padding-left: 0px;
  position: absolute; 
  right:0;
  top:0;
  bottom:0;
}
.map-block iframe {
    width: 100%;
    height: 100%;
    border: 0;
}
</style>
<?php assetEnd('headStyle');?>
<?php
if (!isset($info)) {
    $info = findModel('contact@registry');
}
if (!$info->mapUrl) {
    $needMap = false;
}
if (!isset($needMap)) {
    $needMap = true;
}
if ($needMap) {
    $contactSize = 6;
} else {
    $contactSize = 12;
}
?>
<div class="row contact-row">
    <div class="col-sm-<?=$contactSize;?> contact-block">
        <table class="table table-bordered" itemscope itemtype="http://schema.org/Organization">
            <tr>
                <th colspan="2" itemprop="name">
                    <center><?=$info->name;?></center>
                </th>
            </tr>
            <tr>
                <th style="min-width: 100px;">
                    <i class="fa fa-fw fa-globe"></i>&nbsp;<?=$_->adress;?>
                </th>
                <td itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    <span itemprop="addressCountry"><?=$info->country;?></span>,
                    <span itemprop="postalCode"><?=$info->postalCode;?></span>,
                    <span itemprop="addressLocality"><?=$info->city;?></span>,
                    <span itemprop="streetAddress"><?=$info->adress;?></span>
                </td>
            </tr>
            <?php if ($info->time) : ?>
            <tr>
                <th>
                    <i class="fa fa-clock-o"></i>&nbsp;<?=$_->time;?>
                </th>
                <td>
                    <?=$info->time;?>
                </td>
            </tr>
            <?php endif;?>
            <tr>
                <th>
                    <i class="fa fa-phone"></i>&nbsp;<?=$_->phone;?>
                </th>
                <td itemprop="telephone">
                    <?=$info->phone;?>
                </td>
            </tr>
            <tr>
                <th>
                    <i class="fa fa-fw fa-envelope"></i>&nbsp;<?=$_->email;?>
                </th>
                <td itemprop="email">
                    <?=$info->emailText;?>
                </td>
            </tr>
        </table>
    </div>
    <?php if ($needMap) : ?>
    <div class="col-sm-6 hidden-xs map-block">
        <iframe
            src="<?=$info->mapUrl;?>"
            allowfullscreen></iframe>
    </div>
    <?php endif;?>
</div>
