<?php
$info = findModel('contact@registry');
?>
<p class="navbar-text navbar-right small" style="padding-right: 10px;">
    <?php if ($info->vkName) : ?>
    <a class="no-link" target="_blank" href="https://vk.com/<?=$info->vkName;?>">
        <span class="fa-stack">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    <?php endif;?>
    <?php if ($info->facebookName) : ?>
    <a class="no-link" target="_blank" href="https://facebook.com/<?=$info->facebookName;?>">
        <span class="fa-stack">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    <?php endif;?>
    <?php if ($info->twitterName) : ?>
        <a class="no-link" target="_blank" href="https://twitter.com/<?=$info->twitterName;?>">
            <span class="fa-stack">
              <i class="fa fa-circle fa-stack-2x"></i>
              <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
            </span>
        </a>
    <?php endif;?>
    <?php if ($info->instagramName) : ?>
    <a class="no-link" target="_blank" href="https://instagram.com/<?=$info->instagramName;?>/">
        <span class="fa-stack">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    <?php endif;?>
    <?php if ($info->okName) : ?>
    <a class="no-link" target="_blank" href="https://ok.ru/<?=$info->okName;?>">
        <span class="fa-stack">
          <i class="fa fa-circle fa-stack-2x"></i>
          <i class="fa fa-odnoklassniki fa-stack-1x fa-inverse"></i>
        </span>
    </a>
    <?php endif;?>
</p>
