<?php $info = findModel('contact@registry');?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?=$_->title;?></h3>
  </div>
  <div class="panel-body">
    <center>
    <ul class="list-inline text-muted">
        <?php if ($info->vkName) : ?>
        <li>
            <a class="no-link" target="_blank" href="https://vk.com/<?=$info->vkName;?>">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-vk fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </li>
        <?php endif;?>
        <?php if ($info->facebookName) : ?>
        <li>
            <a class="no-link" target="_blank" href="https://facebook.com/<?=$info->facebookName;?>">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </li>
        <?php endif;?>
        <?php if ($info->twitterName) : ?>
        <li>
            <a class="no-link" target="_blank" href="https://twitter.com/<?=$info->twitterName;?>">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </li>
        <?php endif;?>
        
        <?php if ($info->instagramName) : ?>
        <li>
            <a class="no-link" target="_blank" href="https://instagram.com/<?=$info->instagramName;?>/">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </li>
        <?php endif;?>
        <?php if ($info->okName) : ?>
        <li>
            <a class="no-link" target="_blank" href="https://ok.ru/<?=$info->okName;?>">
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-odnoklassniki fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </li>
        <?php endif;?>
        
    </ul>
    </center>
  </div>
</div>
