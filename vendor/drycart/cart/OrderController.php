<?php
namespace drycart\cart;

class OrderController extends \drycore\util\standartController\FormController
{
    protected $parentProductBoxName = 'drycart/cart/cart';
    protected $selfProductBoxName = 'drycart/cart/order/product';
    protected $redirect = false;
    
    public function parentProductBox()
    {
        return box($this->parentProductBoxName);
    }

    // Основной блок - действия с валидными данными
    protected function go($model)
    {
        // Если надо сохранять
        if ($this->save) {
            $model->save();
            // Перекопируем все товары из корзины в заказ
            foreach ($this->parentProductBox()->findAll() as $cartProduct) {
                $orderProduct = model($this->selfProductBoxName);
                $orderProduct->order_id = $model->id;
                $orderProduct->product_id = $cartProduct->product_id;
                $orderProduct->count = $cartProduct->count;
                $orderProduct->save();
            }
            // Удалим корзину
            $this->parentProductBox()->delete(true);
            // Попробуем отправить письмо админу
            $this->tryAdminEmail($model);
            if ($this->redirect) {
                redirect($this->redirect);
            }
        }
    }
}
