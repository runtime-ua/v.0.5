<form class="form-inline" action="<?=url(['drycart/cart/cart:create']);?>" method="post">
  <input type="hidden" name="drycart_cart_cart[product_id]" value="<?=$model->id;?>">
  <div class="input-group">
    <input type="text" name="drycart_cart_cart[count]" class="form-control input-sm" value="1">
    <span class="input-group-btn">
        <button type="submit"  class="btn btn-default btn-sm">
            <i class="fa fa-shopping-cart"></i>
        </button>
    </span>
  </div>
  <?= s()->xsrf->field(); ?>
</form>
