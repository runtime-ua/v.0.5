<?php assetStart();?>
<style>
    .cart-menu {
        margin-top: 0px;
        margin-right: -1px;
        padding: 15px;
    }
    .cart-menu-toggle {
          margin-right: 0px;
    }
    .skin-flatly .cart-menu, 
    .skin-darkly .cart-menu {
        margin-top: -1px;
    }
    .skin-yeti .cart-menu {
        color: #fff;
    }
    .skin-yeti .cart-menu-toggle {
          margin-top: 6px;
    }
</style>
<?php assetEnd('headStyle');?>
<?php if ($count) : ?>
    <button
        type="button"
        class="btn btn-default navbar-btn dropdown-toggle navbar-right cart-menu-toggle"
        data-toggle="dropdown"
    >
        <i class="fa fa-fw fa-shopping-cart fa-lg"></i> 
        <span class="badge label-default"><?=$count;?></span>
    </button>
    <div class="dropdown-menu dropdown-menu-right cart-menu">
            <table class="table cart-menu-table">
                <thead>
                    <tr>
                        <th><?=$cart->lang('count');?></th>
                        <th><?=$cart->lang('product');?></th>
                        <th><?=$cart->lang('price');?></th>
                        <th><?=$cart->lang('amount');?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($cart->findAll() as $line) : ?>                    
                    <tr>
                        <td class="quantity"><?=$line->count;?> x</td>
                        <td class="product">
                            <a href="<?=$line->product->pageurl;?>">
                                <?=$line->product->title;?>
                            </a>
                        </td>
                        <td class="amount"><?=s()->money->go($line->product->price, $_->noPrice);?></td>
                        <td class="amount"><?=s()->money->go($line->product->price * $line->count, '----');?></td>
                    </tr>
                <?php endforeach;?>                    
                    <tr>
                        <td class="total-quantity" colspan="2">
                            <?=$_->all.' '.$count.' '.$_->units;?>
                        </td>
                        <td class="total-amount" colspan="2"><?=s()->money->go($amount, '---');?></td>
                    </tr>
                </tbody>
            </table>
            <div class="btn-group pull-right">  
                <a href="<?=url(['drycart/cart/order']);?>" class="btn btn-default btn-sm"><?=$_->toOrder;?></a>
                <a href="<?=url(['drycart/cart/cart']);?>" class="btn btn-primary btn-sm"><?=$_->toCart;?></a>
            </div>
    </div>
<?php endif;?>
