<?php if ($count) : ?>
<table class="table cart table-hover table-colored dark">
    <thead>
        <tr>
            <th><?=$cart->lang('product');?></th>
            <th><?=$cart->lang('count');?></th>
            <th><?=$cart->lang('price');?></th>
            <th class="amount"><?=$cart->lang('amount');?></th>
            <th></th>
        </tr>
    </thead>
    <tbody class="dark-bg">
<?php foreach ($cart->findAll() as $line) : ?> 
        <tr>
            <td class="product"><a href="<?=$line->product->pageurl;?>">
                    <?=$line->product->title;?>
                </a>
            </td>
            <td class="quantity" style="width: 100px">
                <form
                    class="form-inline pull-right"
                    action="<?=urlXsrf(['drycart/cart/cart:update','id'=>$line->id]);?>"
                    method="post"
                >
                    <div class="form-group">
                        <input type="text"
                               name="drycart_cart_cart[count]"
                               id="count" class="form-control input-sm"
                               value="<?=$line->count;?>" style="width: 40px;"
                        >
                    </div>                                                
                    <button type="submit"  class="margin-clear btn btn-primary btn-sm">
                       <i class="fa fa-refresh"></i> 
                    </button>
                    <?= s()->xsrf->field(); ?>
                </form>
            </td>
            <td class="price"><?=s()->money->go($line->product->price, $_->noPrice);?></td>
            <td class="amount"><?=s()->money->go($line->product->price * $line->count, '---');?></td>
            <td class="remove">
                <a class="btn btn-sm btn-danger" href="<?=urlXsrf(['drycart/cart/cart:delete','id'=>$line->id]);?>">
                      <i class="fa fa-trash fa-lg"></i> 
                </a></td>
        </tr>
<?php endforeach;?>                    
        <tr>
            <td class="total-quantity" colspan="4">
                <?=$_->all.' '.$count.' '.$_->units;?>
            </td>
            <td class="total-amount"><?=s()->money->go($amount, '---');?></td>
        </tr>
    </tbody>
</table>
<?php endif;?>
<?php if (!$count) : ?>
<div class="alert alert-info">
    <?=$_->noProduct;?>
</div>
<?php endif;?>
