<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$title;?></h3>
        <?php if (controller()->viewActions) : ?>
            <div class="box-tools">
            <?php foreach (controller()->viewActions as $block) : ?>
                <div class="btn-group">
                    <?php foreach ($block as $action) : ?>
                        <?=view($action.'Action', ['model'=>$model,'controllerName'=>controller()->selfType()]);?>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="box-body">
        <?=view('drycore/util/widget/model', ['model'=>$model,'scenario'=>controller()->viewScenario]);?>
        <h2><?=$_->productList;?></h2>
        <?=view('drycart/cart/order/productList', ['cart'=>$model->products]);?>
    </div>
</div>
