<article>
    <h1><?=$title;?></h1>
    <?=$article;?>
</article>
<?php if (!$model->isBoxed()) : ?>
<?=view('drycart/cart/order/productList', ['cart'=>controller()->parentProductBox()]);?>
<h3><?=$_->cartTitle;?></h3>
<form class="form-horizontal" role="form" method="post">
    <?=view('drycore/util/form/form', ['model'=>$model,'noHelp'=>true]);?>
    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <input type="submit" value="<?=$_->submit;?>" class="submit-button btn btn-default btn-block">
            <?= s()->xsrf->field(); ?>
        </div>
    </div>
</form>
<?php endif;?>
