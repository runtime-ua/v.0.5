<?php if ($count) : ?>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th><?=$cart->lang('product');?></th>
                <th><?=$cart->lang('count');?></th>
                <th><?=$cart->lang('price');?></th>
                <th class="amount"><?=$cart->lang('amount');?></th>
            </tr>
        </thead>
        <tbody class="dark-bg">
        <?php foreach ($cart->findAll() as $line) : ?>                    
            <tr>
                <td class="product">
                    <a href="<?=$line->product->pageurl;?>">
                        <?=$line->product->title;?>
                    </a>
                </td>
                <td class="quantity"><?=$line->count.' '.$_->units;?></td>
                <td class="price"><?=s()->money->go($line->product->price, $_->noPrice);?></td>
                <td class="amount"><?=s()->money->go($line->product->price * $line->count, '---');?></td>
            </tr>
        <?php endforeach;?>                    
            <tr>
                <td class="total-quantity" colspan="3">
                    <?=$_->all.' '.$count.' '.$_->units;?>
                </td>
                <td class="total-amount"><?=s()->money->go($amount, '---');?></td>
            </tr>
        </tbody>
    </table>
<?php endif;?>
<?php if (!$count) : ?>
<div class="alert alert-info">
    <?=$_->noProduct;?>
</div>
<?php endif;?>
