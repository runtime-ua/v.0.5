<?php
namespace drycart\cart;

/**
 * Список товаров с колвом и ценами
 */
class ProductListView extends \proto\View
{
    public function init()
    {
        if (!isset($this->fields['cart'])) {
            $this->fields['cart'] = box('drycart/cart/cart');
        }
        $cart = $this->fields['cart'];
        if (!$cart) {
            $cart = [];
        }
        //
        $amount = $count = 0;
        foreach ($cart->findAll() as $line) {
            $count = $count + $line->count;
            $amount = $amount + $line->count * $line->product->price;
        }
        $this->fields['count'] = $count;
        $this->fields['amount'] = $amount;
        parent::init();
    }
}
