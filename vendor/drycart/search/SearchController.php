<?php
namespace drycart\search;

class SearchController extends \drycore\util\standartController\AbstractModelController
{
    public function defaultAction()
    {
        $search = s()->get->getModel('drycart/search/search');
        //
        $box = $this->box();
        if (!$search->isNew()) {
            $box->where([
                '(`article` LIKE :q) OR (`title` LIKE :q)',
                'q'=> '%'.$search->q.'%'
            ]);
        }
        // **********************************************************************
        $this->initShow();
        $this->showVar('q', $search->q);
        $this->showVar('box', $box);
        $this->show();
    }
}
