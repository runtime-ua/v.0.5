<?php if (isset($where)) {
    $box->where($where);
}?>
<h1><?=$title;?></h1>
<form role="search"  method="get" action="<?=url(['drycart/search/search']);?>">
    <div class="form-group">
        <div class="input-group">
            <input type="text" name="drycart_search_search[q]" class="form-control" placeholder="<?=$_->search;?>">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>
</form>
<article>
    <?=$model->article;?>
</article>
<?php $pager = view('drycore/util/widget/pager', ['box'=>$box])?>
<?=showModels('drystyle/basic/article/viewItem', $box->findAll())?>
<?=$pager->render()?>
