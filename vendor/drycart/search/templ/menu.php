<form class="navbar-form navbar-right hidden-md hidden-sm" role="search"  method="get" action="<?=url(['drycart/search/search']);?>">
    <div class="form-group">
        <div class="input-group">
            <input type="text" name="drycart_search_search[q]" class="form-control" placeholder="<?=$_->search;?>">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-search"></i> 
                </button>
            </span>
        </div>
    </div>
</form>
<a class="btn navbar-btn visible-md visible-sm navbar-right" href="<?=url(['drycart/search/search']);?>">
    <i class="fa fa-search"></i>
</a>
