<div class="panel panel-default">
  <div class="panel-body">
    <form class="hidden-md hidden-sm" role="search"  method="get" action="<?=url(['drycart/search/search']);?>">
        <div class="form-group">
            <div class="input-group">
                <input type="text" name="drycart_search_search[q]" class="form-control" placeholder="<?=$_->search;?>">
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-default">
                        <i class="fa fa-search"></i> 
                    </button>
                </span>
            </div>
        </div>
    </form>
  </div>
</div>
