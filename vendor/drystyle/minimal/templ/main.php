<!DOCTYPE html>
<html lang="<?=s()->locale->shortLang;?>">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?=showIf(isset($metatitle), $metatitle);?></title>
		<meta name="description" content="<?=showIf(isset($metadesc), $metadesc);?>">
        <?php $skin = 'default';?>
        <?php asset('headCss', '/files/css/'.$skin.'.first.min.css');?>
        <?php asset('footerCss', '/files/css/'.$skin.'.last.min.css');?>
        <?php asset('mainCss', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');?>
        <?php asset('mainCss', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');?>
        <?php asset('mainJs', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js');?>
        <?php asset('mainJs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');?>
        <?php foreach (asset('mainCss') as $css) :?>
           <link href="<?=$css;?>" rel="stylesheet" type="text/css">
        <?php endforeach;?>
        <?php foreach (asset('headCss') as $css) :?>
           <link href="<?=$css;?>" rel="stylesheet" type="text/css">
        <?php endforeach;?>
        <?php foreach (asset('headJs') as $css) :?>
            <script src="<?=$js;?>"></script>
        <?php endforeach;?>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?=show(asset('headStyle'));?>
        <?=show(asset('headScript'));?>
    </head>
    <body>
        <br>
        <section class="container">
            <?=$content;?>
        </section>
        <?php foreach (asset('mainJs') as $js) :?>
        <script src="<?=$js;?>"></script>
        <?php endforeach;?>
        <?php foreach (asset('footerCss') as $css) :?>
        <link href="<?=$css;?>" rel="stylesheet" type="text/css">
        <?php endforeach;?>
        <?php foreach (asset('footerJs') as $js) :?>
        <script src="<?=$js;?>"></script>
        <?php endforeach;?>
        <?=show(asset('footerStyle'));?>
        <?=show(asset('footerScript'));?>
    </body>
</html>
