<h2><?=$title;?></h2>
<p><?=$article;?>
<a class="btn btn-default" href="<?=url(['auth:login']);?>">
    <i class="fa fa-user fa-lg"></i>
</a>
</p>
<?php if (defined('DEVMODE')) : ?>
<?=view('drystyle/minimal/error/exception', ['exception'=>$exception]);?>
<?php endif;?>
