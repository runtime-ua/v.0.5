<article>
    <h1><?=$title;?></h1>
    <p>
        <?=$article;?>
    </p>
    <a class="btn btn-lg btn-danger btn-block" href="<?=url($confirm);?>">Выполнить действие</a>
    <?php if (isset($next)) : ?>
    <a class="btn btn-lg btn-default btn-block" href="<?=url($next);?>">Пропустить</a>
    <?php endif;?>
</article>

