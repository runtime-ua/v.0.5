<article>
    <h1><?=$title;?></h1>
    <p>
        <?=$article;?>
    </p>
    <?php if (isset($next)) : ?>
    <a class="btn btn-lg btn-default btn-block" href="<?=url($next);?>">Перейти к следующему этапу</a>
    <?php endif;?>
</article>

