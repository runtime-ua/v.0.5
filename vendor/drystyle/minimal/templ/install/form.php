<article>
    <h1><?=$title;?></h1>
    <p>
        <?=$article;?>
    </p>
</article>
<?=view('drycore/util/form/formTab', ['model'=>$model,'actions'=>[['drystyle/admin/actions/save']]]);?>
<?php if ($model->needUploader()) : ?>
<?=view('drycore/util/uploader/uploader', [
    'url'=>[controller()->controller.':uploader','folder='.$model->folder],
    'addEditor'=>true
]);?>
<?php endif; ?>
