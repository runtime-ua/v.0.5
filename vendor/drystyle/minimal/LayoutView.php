<?php
namespace drystyle\minimal;

/**
 * Description of LayoutView
 *
 * @author Maks
 */
class LayoutView extends AbstractLayoutView
{
    public function init()
    {
        $this->fields['content'] = showViews($this->fields['structure']);
        parent::init();
    }
}
