<?php
namespace drystyle\minimal;

/**
 * Description of LayoutView
 *
 * @author Maks
 */
class AbstractLayoutView extends \proto\View
{
    public function init()
    {
        if (!isset($this->fields['metatitle']) and isset($this->fields['title'])) {
            $this->fields['metatitle'] = $this->fields['title'];
        }
        if (!isset($this->fields['metatitle'])) {
            $this->fields['metatitle'] = '';
        }
        if (!isset($this->fields['metadesc'])) {
            $this->fields['metadesc'] = '';
        }
        //
        $canonical = $_SERVER['REQUEST_URI'];
        if (haveStr($canonical, '?')) {
            list($canonical,) = explode('?', $canonical, 2);
        }
        $this->fields['canonical'] = $canonical;
        parent::init();
    }
}
