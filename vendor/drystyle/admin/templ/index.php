<?php
$structure = [
    [
        'size'=>12,
        'blocks'=>[
            [
                'view'=>'drystyle/admin/widget/countInfo',
                'size'=>6,
                'data'=>[
                    'ico'=>'fa fa-envelope',
                    'icoStyle'=>'yellow',
                    'title'=>'Новых обращений',
                    'boxName' =>'drycart/feedback/feedback',
                    'where'=> ['new'=>true],
                    'url'=>['drycart/feedback/feedback/admin'],
                ]
            ],
            [
                'view'=>'drystyle/admin/widget/countInfo',
                'size'=>6,
                'data'=>[
                    'ico'=>'fa fa-envelope',
                    'icoStyle'=>'yellow',
                    'title'=>'Всего страниц',
                    'boxName' =>'#sitePage',
                    'url'=>['drycart/main/all'],
                ]
            ],
        ]
    ],
    [
        'size'=>12,
        'blocks'=>[
            [
                'view'=>'drystyle/admin/widget/datedList',
                'size'=>4,
                'data'=>[
                   'boxName'=>'drycart/search/searchPage',
                   'timestampField'=>'created',
                   'userField'=>'creator',
                   'title'=>'Новые добавления',
                   'limit'=>5,
                   'order'=>'!created'
                ]
            ],
            [
                'view'=>'drystyle/admin/widget/datedList',
                'size'=>4,
                'data'=>[
                   'boxName'=>'drycart/search/searchPage',
                   'timestampField'=>'updated',
                   'userField'=>'updater',
                   'title'=>'Свежие изменения',
                   'limit'=>5,
                   'order'=>'!updated'
                ]
            ],
            [
                'view'=>'drystyle/admin/widget/userList',
                'size'=>4,
                'data'=>[
                ]
            ]
        ]
    ]
];
?>
<div class="row">
    <?php foreach ($structure as $line) :?>
    <div class="col-md-<?=$line['size'];?>">
        <div class="row">
        <?php foreach ($line['blocks'] as $block) :?>
            <div class="col-md-<?=$block['size'];?>">
                <?=view($block['view'], $block['data']);?>
            </div>
        <?php endforeach;?>
        </div>
    </div>
    <?php endforeach;?>
</div>
