<div class="row">
    <?php if (isset($result)) :?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?=$result;?>
            </div>
        </div>
    </div>
    <?php endif;?>
    <div class="col-lg-12">
        <?=view('drycore/util/form/formTab', [
            'model'=>$model,
            'actions'=> controller()->updateActions,
            'btnText'=>$_->submit
        ]);?>
    </div>
</div>
