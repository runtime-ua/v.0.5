<section class="content-header">
    <?php foreach ($alerts as $alert) : ?>
    <div class="alert alert-<?=$alert['type'];?> alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?=$alert['content'];?>
    </div>
    <?php endforeach;?>
    <?php if (defined('DEVMODE') or defined('DEBUG')) :?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?=$_->devmode;?>
    </div>
    <?php endif;?>
</section>
