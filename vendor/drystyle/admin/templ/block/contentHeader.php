<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><?=$_->adminHeader;?></h1>
    <?php if ($parents or $currentTitle) :?>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-home"></i></a></li>
        <?php foreach ($parents as $line) : ?>
        <li><a href="<?=$line->pageurl;?>"><?=$line->breadtitle;?></a></li>
        <?php endforeach;?>
        <li class="active"><?=$currentTitle;?></li>
    </ol>
    <?php endif;?>
</section>
