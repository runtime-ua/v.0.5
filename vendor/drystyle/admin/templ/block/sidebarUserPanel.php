<?php
$user = s()->user->current();
?>
    <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="<?=$user->avatar;?>" class="img-circle">
        </div>
        <div class="pull-left info">
          <p><?=$user->login;?></p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
