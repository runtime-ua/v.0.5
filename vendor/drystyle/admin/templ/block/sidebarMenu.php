<?php
if (!isset($menu)) {
    $menu = s()->drycore_util_widget_menuHelper->go(box('drystyle/admin/menu')->findAll());
}
?>
<!-- Sidebar Menu -->
      <ul class="sidebar-menu">
        <li class="header"><?=$_->title;?></li>
        <?php foreach ($beforeMenuWhere as $line) : ?>
        <?php $line = box('#basicPage')->findOne($line);?>
            <li>
              <a href="<?=$line['pageurl'];?>">
                <i class="fa fa-<?=showIf($line['ico'], $line['ico'])?><?=showIf(!$line['ico'], 'link')?>"></i>
                <span><?=$line['menutitle'];?></span>
              </a>
            </li>
        <?php endforeach;?>
        <?php foreach ($menu as $line) : ?>
            <?php if (!isset($line['menu'])) :?>
                <li <?=showIf(isset($line['active']), ' class="active"');?>>
                    <a href="<?=$line['url'];?>">
                      <i class="fa fa-<?=showIf($line['ico'], $line['ico'])?><?=showIf(!$line['ico'], 'link')?>"></i>
                      <span><?=$line['ancor'];?></span>
                    </a>
                </li>
            <?php endif;?>
            <?php if (isset($line['menu'])) :?>
                <li class="treeview<?=showIf(isset($line['active']), ' active');?>">
                    <a href="<?=$line['url'];?>">
                      <i class="fa fa-<?=showIf($line['ico'], $line['ico'])?><?=showIf(!$line['ico'], 'link')?>"></i>
                      <span><?=$line['ancor'];?></span>
                      <i class="fa fa-angle-left pull-right"></i>
                    </a>
                  <ul class="treeview-menu">
                    <?php foreach ($line['menu'] as $subline) : ?>
                    <li>
                     <a href="<?=url($subline['url']);?>">
                        <?php
                        if (!$subline['ico']) {
                            $subline['ico'] = 'circle-o';
                        }
                        ?>
                      <i class="fa fa-<?=$subline['ico'];?>"></i>
                      <span><?=$subline['ancor'];?></span>
                     </a>
                    </li>
                    <?php endforeach;?>
                  </ul>
                </li>
            <?php endif;?>
        <?php endforeach;?>
      </ul>
      <!-- /.sidebar-menu -->
