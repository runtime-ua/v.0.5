<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header with-border">
                <h3><?=$title;?></h3>
            </div>
            <div class="box-body">
                <?php if (!is_null($parentData)) :?>
                <a class="btn btn-lg btn-primary col-md-12" href="<?=urlXsrf($parentData['url']);?>">
                    <?=$parentData['ancor'];?>
                </a>
                <?php endif;?>
                <?php foreach ($childsData as $child) :?>
                <a class="btn btn-lg btn-default col-md-4" href="<?=urlXsrf($child['url']);?>">
                    <?=$child['ancor'];?>
                </a>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    <?php if ($article) : ?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?=$article;?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
