<div class="row">
    <div class="col-lg-12">
        <h3><?=$title.': '.$model->selfName();?></h3>
        <?=view('drycore/util/form/formTab', ['model'=>$model,'actions'=>  controller()->updateActions]);?>
    </div>
    <?php if ($article) : ?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?=$article;?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
<?php if ($model->needUploader()) : ?>
<?=view('drycore/util/uploader/uploader', ['folder'=>$model->folder,'addEditor'=>true]);?>
<?php endif; ?>
