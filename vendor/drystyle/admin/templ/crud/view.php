<div class="row">
    <div class="col-lg-12 table-responsive">
        <div class="box">
            <div class="box-header with-border">
                <h3><?=$title.': '.$model->selfName();?></h3>
                <?php if (controller()->viewActions) : ?>
                    <div class="box-tools">
                    <?php foreach (controller()->viewActions as $block) : ?>
                        <div class="btn-group">
                            <?php foreach ($block as $action) : ?>
                                <?=view($action.'Action', [
                                    'model'=>$model,
                                    'controllerName'=>controller()->selfType()]);?>
                            <?php endforeach; ?>
                        </div>
                    <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="box-body table-responsive">
                <?=view('drycore/util/widget/model', ['model'=>$model]);?>
            </div>
        </div>        
    </div>
    <?php if ($article) : ?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?=$article;?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
