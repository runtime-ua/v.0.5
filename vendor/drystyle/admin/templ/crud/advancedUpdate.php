<div class="row">
    <div class="col-lg-8">
        <?=view('drycore/util/form/formTab', ['model'=>$model,'actions'=>  controller()->updateActions]);?>
    </div>
    <div class="col-lg-4">
        <div class="box">
            <div class="box-body">
                <h4><?=$model->title();?> <small><?=$model->selfName();?></small></h4>
                <?=view('drycore/util/widget/model', ['model'=>$model,'scenario'=> 'advancedInfo']);?>
            </div>
        </div>
        <?php
        $historyBox = box('drycore/util/history/updateHistory')->
            limit(10)->
            where([
                'modelName'=>$model->selfType(),
                'model_id'=>$model->id
            ]);
        ?>
        <?=view('drycore/util/history/historyWidget', ['box'=>$historyBox]);?>
    </div>
    <?php if ($article) : ?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?=$article;?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
<?php if ($model->needUploader()) : ?>
<?=view('drycore/util/uploader/uploader', ['folder'=>$model->folder,'addEditor'=>true]);?>
<?php endif; ?>
