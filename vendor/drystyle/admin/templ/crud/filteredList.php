<div class="row">
    <div class="col-lg-3">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title"><?=controller()->lang('folders');?></h3>
            </div>
            <div class="box-body no-padding">
                <?=view('drycore/util/widget/filters', ['box'=>$box,
                    'filters'=>controller()->filters,
                    'defaultFilter'=>controller()->defaultFilter
                ]);?>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-lg-9 table-responsive">
        <div class="box">
            <div class="box-header with-border">
                <?=view('drycore/util/form/searchForm', ['box'=>$box]);?>
                <h3 class="box-title"><?=$title;?></h3>
                <div class="box-tools">
                    <?=view('drycore/util/widget/orderForm', ['box'=>$box]);?>
               </div>
            </div>
            <div class="box-body table-responsive">
            <?php
                $pager = view(
                    'drycore/util/widget/pager',
                    ['box'=>$box,'cssClass'=>'pagination pagination-sm no-margin pull-right']
                );
            ?>
            <?=view('drycore/util/widget/selectedTable', [
                'defaultAction'=>'view',
                'box'=>$box,
                'actions'=>  controller()->listActions,
                'topActions'=>  controller()->listTopActions]);
            ?>
            </div>
            <div class="box-footer clearfix">
                <?=$pager->render();?>
            </div>
        </div>        
    </div>
    <?php if ($article) : ?>
    <div class="col-lg-12">
        <div class="box">
            <div class="box-body">
                <?=$article;?>
            </div>
        </div>
    </div>
    <?php endif;?>
</div>
