<div class="box box-danger">
    <div class="box-header with-border">
      <h3 class="box-title">Кто онлайн</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body no-padding">
      <ul class="users-list clearfix">
        <?php foreach (box('drycore/util/session')->findAll() as $line) :?>
        <?php
        $user = $line->user;
        if (!$user) {
            $user = model('user');
        }
        ?>
        <li>
            <img src="<?=$user->avatar;?>" alt="User Image">
          <a class="users-list-name" href="#"><?=$user->login;?></a>
            <?php if ((time() - $line->updated) > 5*60) :?>
          <span class="users-list-date"><?=$line->pretty('updated');?></span>
            <?php endif;?>
            <?php if ((time() - $line->updated) <= 5*60) :?>
          <span class="users-list-date"><i class="fa fa-circle text-success"></i></span>
            <?php endif;?>
        </li>
        <?php endforeach;?>
      </ul>
      <!-- /.users-list -->
    </div>
</div>
