<?php
if (!isset($url)) {
    $url = false;
}
if (!isset($title)) {
    $title = $_->title;
}
if (!isset($style)) {
    $style = false;
}
if (!isset($icoStyle)) {
    $icoStyle = false;
}
if (!isset($desc)) {
    $desc = $_->desc;
}
if (!isset($progress)) {
    $progress = false;
}
//
if (!isset($count1)) {
    if (!isset($box1)) {
        $box1 = box($boxName1);
    }
    if (isset($where1)) {
        $box1->where($where1);
    }
    $count1 = $box1->count();
}
//
if (!isset($count1)) {
    if (!isset($box2)) {
        $box2 = box($boxName2);
    }
    if (isset($where2)) {
        $box2->where($where2);
    }
    $count2 = $box2->count();
}
// completed отношение/выполнено (первое на второе)
// plus прирост (первое минус второе делить на второе)
// minus убыль/остаток (второе минус первое делить на второе)
//
if (!isset($type)) {
    $type = '';
}
switch ($type) {
    case 'plus':
        $top = $count1 - $count2;
        $bottom = $count2;
        break;
    case 'minus':
        $top = $count2 - $count1;
        $bottom = $count2;
        break;
    case 'completed':
    default:
        $top = $count1;
        $bottom = $count2;
        break;
}
if ($bottom != 0) {
    $progress = (int) (100*$top / $bottom);
}
//
if (isset($unitName)) {
    $value = $count1.'/'.$count2.' '.$this->lang($unitName);
} else {
    $value = $count1;
}

?>
<?=view('drystyle/admin/widget/info', [
    'ico'=>$ico,
    'icoStyle'=>$icoStyle,
    'style'=>$style,
    'url'=>$url,
    'title'=>$title,
    'value'=>$value,
    'desc'=>$desc,
    'progress'=>$progress,
]);
