<?php
if (!isset($url)) {
    $url = false;
}
if (!isset($title)) {
    $title = $_->title;
}
if (!isset($style)) {
    $style = false;
}
if (!isset($icoStyle)) {
    $icoStyle = false;
}
//if(!isset($desc)) $desc = false;
//if(!isset($progress)) $progress = false;
//
if (!isset($box)) {
    $box = box($boxName);
}
if (isset($where)) {
    $box->where($where);
}
$value = $box->count();
if (isset($unitName)) {
    $value = $this->lang($unitName);
}
?>
<?=view('drystyle/admin/widget/info', [
    'ico'=>$ico,
    'icoStyle'=>$icoStyle,
    'style'=>$style,
    'url'=>$url,
    'title'=>$title,
    'value'=>$value,
]);
