<?php
$box = box($boxName);
if (isset($where)) {
    $box->where($where);
}
if (isset($limit)) {
    $box->limit($limit);
}
if (isset($order)) {
    $box->order($order);
}
if (!isset($title)) {
    $title = $_->title;
}
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$title;?></h3>
    </div>
    <div class="box-body">
        <ul class="products-list product-list-in-box">
        <?php foreach ($box->findAll() as $line) :?>
        <li class="item">
            <a href="<?=$line->pageurl;?>" class="h5">
                <?=$line->title();?>
                <small><?=$line->selfName();?></small>
            </a>
            <br>
            <?php if (isset($timestampField)) :?>
            <span class="small pull-left"><?=$line->dateTime($timestampField);?></span>
            <?php endif;?>
            <?php if (isset($userField)) :?>
            <span class="small pull-right"><?=$line->pretty($userField);?></span>
            <?php endif;?>
        </li>
        <?php endforeach;?>
      </ul>
    </div>
</div>
