<?php assetStart();?>
<script>
$(function () {
    $.each($(".direct-chat-messages"),function(index, value){
        value.scrollTop = value.scrollHeight;
    });
});
</script>
<?php assetEnd('footerScript');?>


<?php
$style = 'danger';
$title = 'Корпоративный чат';
$chatId = '';
$msgList = [];
$msg = new \stdClass();
$msg->createdBy = 2;
$msg->created = time() - rand(10, 5*24*60*60);
$msg->chatId = $chatId;
$msg->msg = "Текст сообщения которое нам прислали";
$msgList[] = $msg;
$msg = new \stdClass();
$msg->createdBy = 2;
$msg->created = time() - rand(10, 5*24*60*60);
$msg->msg = "Текст сообщения которое нам прислали";
$msgList[] = $msg;
$msg = new \stdClass();
$msg->createdBy = 1;
$msg->created = time() - rand(10, 5*24*60*60);
$msg->msg = "Текст сообщения которое нам прислали";
$msgList[] = $msg;
$msg = new \stdClass();
$msg->createdBy = 2;
$msg->created = time() - rand(10, 5*24*60*60);
$msg->msg = "Текст сообщения которое нам прислали";
$msgList[] = $msg;

?>
<div class="box box-<?=$style;?> direct-chat direct-chat-<?=$style;?>">
  <div class="box-header with-border">
    <h3 class="box-title"><?=$title;?></h3>
  </div><!-- /.box-header -->
  <div class="box-body">
    <!-- Conversations are loaded here -->
    <div class="direct-chat-messages">
        <?php foreach ($msgList as $msg) :?>
          <!-- Message -->
          <div class="direct-chat-msg <?=showIf($msg->createdBy == s()->user->current()->id, 'right');?>">
            <div class="direct-chat-info clearfix">
              <span class="direct-chat-name pull-left"><?=s()->user->current()->title();?></span>
              <span class="direct-chat-timestamp pull-right">23.12.2017 2:00</span>
            </div><!-- /.direct-chat-info -->
            <img class="direct-chat-img" src="<?=s()->user->current()->avatar;?>" alt="message user image">
            <!-- /.direct-chat-img -->
            <div class="direct-chat-text"><?=$msg->msg;?></div>
          </div>
          <!-- /.direct-chat-msg -->
        <?php endforeach;?>
  </div><!-- /.box-body -->
  <div class="box-footer">
    <div class="input-group">
      <input type="text" name="message" placeholder="..." class="form-control">
      <span class="input-group-btn">
          <button type="button" class="btn btn-danger btn-flat"><i class="fa fa-envelope"></i></button>
      </span>
    </div>
  </div><!-- /.box-footer-->
  </div>
</div>
