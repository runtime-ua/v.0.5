<?php
//$style = 'navy';
//$icoStyle = false;
//$ico = 'fa fa-comments-o';
//$title = 'Likes';
//$value = 12000;
//
//$progress = 75;
//$desc = 'Increase in 30 Days';

if (!isset($url)) {
    $url = false;
}
if (!isset($title)) {
    $title = $_->title;
}
if (!isset($style)) {
    $style = false;
}
if (!isset($icoStyle)) {
    $icoStyle = false;
}
if (!isset($desc)) {
    $desc = false;
}
if (!isset($progress)) {
    $progress = false;
}
//
$progressBar = $progress;
if ($progress > 100) {
    $progressBar = 100;
}
if ($progress < 0) {
    $progressBar = 0;
}
?>
<?php if ($url) :?>
<a href="<?=url($url);?>" class="h4">
<?php endif;?>
<div class="info-box<?=showIf($style, ' bg-'.$style);?>">
  <span class="info-box-icon<?=showIf($icoStyle, ' bg-'.$icoStyle);?>"><i class="<?=$ico;?>"></i></span>
  <div class="info-box-content">
    <span class="info-box-text"><?=$title;?></span>
    <span class="info-box-number"><?=$value;?></span>
    <?php if ($progress) :?>
    <div class="progress">
      <div class="progress-bar" style="width: <?=$progressBar;?>%"></div>
    </div>
    <?php endif;?>
    <?php if ($desc) :?>
    <span class="progress-description">
        <?=$progress;?>% <?=$desc;?>
    </span>
    <?php endif;?>
  </div><!-- /.info-box-content -->
</div><!-- /.info-box -->
<?php if ($url) :?>
</a>
<?php endif;?>
