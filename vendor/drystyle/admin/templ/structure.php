<div class="wrapper">
    <!-- Main Header -->
    <header class="main-header">
        <!-- Logo -->
        <a href="<?=url(['drycart/main/adminIndex:default'])?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?=$_->brandMini;?></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><?=$_->brand;?></span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"></a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php
                // выведем все дочерние вьювы от drystyle/admin/top/trayUnit
                $views = s()->viewFactory->factoryChildsList('drystyle/admin/top/trayUnit', false);
                foreach ($views as $viewName) {
                    echo view($viewName);
                }
                ?>
                <?=view('drystyle/admin/top/user'); ?>
                <!-- Control Sidebar Toggle Button -->
    <!--
                <li><a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a></li>
    -->
            </ul>
          </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <?=showViews($leftSidebarBlocks); ?>
        </section>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?=showViews($contentBlocks);?>
    </div>
    <?php //view('drystyle/admin/block/rightSidebar');?>    
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
            DRYcart dashboard
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2016 <a href="http://zzzlab.com">PE Runtime</a></strong>. All rights reserved. 
    </footer>
</div>
