<?php $user = s()->user->current();?>
<!-- User Account Menu -->
<li class="dropdown user user-menu">
    <!-- Menu Toggle Button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
      <!-- The user image in the navbar-->
      <img src="<?=$user->avatar;?>" class="user-image" alt="User Image">
      <!-- hidden-xs hides the username on small devices so only the image appears. -->
      <span class="hidden-xs"><?=$user->login;?></span>
    </a>
    <ul class="dropdown-menu">
      <!-- The user image in the menu -->
      <li class="user-header">
        <img src="<?=$user->avatar;?>" class="img-circle" alt="User Image">

        <p>
            <?=$user->login;?>
          <small><?=$_->logedBy;?> <b><?=$user->login;?></b></small>
        </p>
      </li>
      <!-- Menu Footer-->
      <li class="user-footer">
        <div class="pull-left">
          <a href="<?=url(['auth:profile']);?>" class="btn btn-default btn-flat"><?=$_->profile;?></a>
        </div>
        <div class="pull-right">
          <a href="<?=url(['auth:logout']);?>" class="btn btn-default btn-flat"><?=$_->logout;?></a>
        </div>
      </li>
    </ul>
</li>
