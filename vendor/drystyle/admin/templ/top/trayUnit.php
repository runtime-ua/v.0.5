<?php $count = box($boxName)->count($where);?>
<li>
    <a href="<?=url($url);?>">
        <i class="<?=$icoClass;?>"></i>
        <?php if ($count) :?>
        <span class="<?=$labelClass;?>"><?=$count;?></span>
        <?php endif; ?>
    </a>
</li>
