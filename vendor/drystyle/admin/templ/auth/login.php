<?php if (!isset($login)) {
    $login = model('drycart/main/login');
}?>
<div class="login-box">
  <div class="login-logo">
    <a href="<?=url(['drycart/main/adminIndex:default']);?>"><?=$_->brand;?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"><?=$_->title;?></p>
<?php if (isset($loginError) and $loginError) : ?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span></button>
    <?=$_->error;?>
</div>
<?php endif; ?>
    <form method="post" action="<?=url(['auth:login']);?>">
      <div class="form-group has-feedback">
        <input type="text" class="form-control"
               placeholder="<?=$login->lang('login');?>"
               value="<?=$login->login;?>"
               name="drycart_main_login[login]">
        <i class="fa fa-user form-control-feedback"></i>
<?php $err = $login->error('login');
if ($err) : ?>
<span class="help-block  has-warning"><?=$err;?></span>
<?php endif; ?>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control"
               placeholder="<?=$login->lang('password');?>"
               value="<?=$login->password;?>"
               name="drycart_main_login[password]">
        <i class="fa fa-lock form-control-feedback"></i>
<?php $err = $login->error('password');
if ($err) : ?>
<span class="help-block has-warning"><?=$err;?></span>
<?php endif; ?>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
<!--              
            <label>
              <input type="checkbox"> Запомнить меня
            </label>
-->
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?=$_->submit;?></button>
        </div>
        <!-- /.col -->
      </div>
        <?=s()->xsrf->field(); ?>
    </form>
<!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat">
        <i class="fa fa-facebook"></i> Sign in using Facebook
      </a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat">
        <i class="fa fa-google-plus"></i> Sign in using Google+
      </a>
    </div>

    <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a>
-->
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php if (defined('DEVMODE') and isset($exception)) : ?>
<?=view('drystyle/minimal/error/exception', ['exception'=>$exception]);?>
<?php endif;?>