<div class="row">
    <div class="col-lg-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title"><?=$title?></h3>
        </div>
        <div class="box-body">
            <form role="form" method="post" class="form-horizontal">
                <?=view('drycore/util/form/form', ['model'=>$model]);?>
                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3">
                        <input type="submit" value="<?=$_->save;?>" class="btn btn-primary btn-block">
                        <?= s()->xsrf->field(); ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>
<?php if ($model->needUploader()) : ?>
<?=view('drycore/util/uploader/uploader', ['folder'=>$model->folder]);?>
<?php endif; ?>
