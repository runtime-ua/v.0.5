<div class="register-box">
  <div class="register-logo">
    <a href="<?=url(['drycart/main/adminIndex:default']);?>"><?=$_->brand;?></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg"><?=$_->title;?></p>
<?php if ($user->error()) : ?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
    <?=$_->error;?>
</div>
<?php endif; ?>

    <form method="post">
    <?= s()->xsrf->field(); ?>
      <div class="form-group has-feedback <?=showIf($user->error('login'), 'has-warning');?>">
        <input type="text"
               class="form-control" placeholder="<?=$user->lang('login');?>"
               name="user[login]"
               value="<?=$user->login;?>">
        <i class="fa fa-user form-control-feedback"></i>
<?php $err = $user->error('login');
if ($err) : ?>
<span class="help-block  has-warning"><?=$err;?></span>
<?php endif; ?>
      </div>
      <div class="form-group has-feedback <?=showIf($user->error('email'), 'has-warning');?>">
        <input type="email"
               class="form-control" placeholder="mail@example.com"
               name="user[email]" value="<?=$user->email;?>">
        <i class="fa fa-envelope form-control-feedback"></i>
<?php $err = $user->error('email');
if ($err) : ?>
<span class="help-block  has-warning"><?=$err;?></span>
<?php endif; ?>
      </div>
      <div class="form-group has-feedback <?=showIf($user->error('password'), 'has-warning');?>">
        <input type="password" class="form-control"
               placeholder="<?=$user->lang('password');?>"
               name="user[password]" value="<?=$user->password;?>">
        <i class="fa fa-lock form-control-feedback"></i>
<?php $err = $user->error('password');
if ($err) : ?>
<span class="help-block  has-warning"><?=$err;?></span>
<?php endif; ?>
      </div>
      <div class="form-group has-feedback <?=showIf($user->error('password2'), 'has-warning');?>">
        <input type="password"
               class="form-control" placeholder="<?=$user->lang('password2');?>"
               name="user[password2]" value="<?=$user->password2;?>">
        <i class="fa fa-lock form-control-feedback"></i>
<?php $err = $user->error('password2');
if ($err) : ?>
<span class="help-block  has-warning"><?=$err;?></span>
<?php endif; ?>
      </div>
      <div class="row">
<!--              
        <div class="col-xs-4">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div>
        </div>
-->
        <!-- /.col -->
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><?=$_->submit;?></button>
        </div>
        <!-- /.col -->
      </div>
    </form>
<!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat">
        <i class="fa fa-facebook"></i> Sign up using Facebook
      </a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat">
        <i class="fa fa-google-plus"></i> Sign up using Google+
      </a>
    </div>
    <a href="<?=url(['auth:login']);?>" class="text-center">Я уже зарегистрирован</a>
-->
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
