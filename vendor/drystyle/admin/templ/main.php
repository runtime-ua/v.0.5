<!DOCTYPE html>
<html lang="<?=s()->locale->shortLang;?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=showIf(isset($metatitle), $metatitle);?></title>
    <meta name="description" content="<?=showIf(isset($metadesc), $metadesc);?>">
    <link rel="shortcut icon" href="<?=$faviconImg;?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php asset('mainCss', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css');?>
    <?php asset('mainCss', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');?>
    <?php asset('mainCss', '/files/myadmin/css/AdminLTE.min.css');?>
    <?php asset('mainCss', '/files/myadmin/css/_all-skins.min.css');?>
    <?php asset('mainJs', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js');?>
    <?php asset('mainJs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');?>
    <?php asset('mainJs', '/files/myadmin/js/app.min.js');?>
    <?php foreach (asset('mainCss') as $css) :?>
       <link href="<?=$css;?>" rel="stylesheet" type="text/css">
    <?php endforeach;?>
    <?php foreach (asset('headCss') as $css) :?>
       <link href="<?=$css;?>" rel="stylesheet" type="text/css">
    <?php endforeach;?>
    <?php foreach (asset('headJs') as $css) :?>
        <script src="<?=$js;?>"></script>
    <?php endforeach;?>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?=show(asset('headStyle'));?>
    <?=show(asset('headScript'));?>
</head>
<!-- LAYOUT OPTIONS: fixed, layout-boxed, layout-top-nav, sidebar-collapse, sidebar-mini-->
<body class="hold-transition skin-<?=$skinName;?> <?=$bodyClass;?>">
    <?=view($structureName);?>
    <?php foreach (asset('mainJs') as $js) :?>
    <script src="<?=$js;?>"></script>
    <?php endforeach;?>
    <?php foreach (asset('footerCss') as $css) :?>
    <link href="<?=$css;?>" rel="stylesheet" type="text/css">
    <?php endforeach;?>
    <?php foreach (asset('footerJs') as $js) :?>
    <script src="<?=$js;?>"></script>
    <?php endforeach;?>
    <?=show(asset('footerStyle'));?>
    <?=show(asset('footerScript'));?>
</body>
</html>
