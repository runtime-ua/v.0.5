<?php
$menu = s()->drycore_util_widget_menuHelper->go($box->findAll());
$controllerName = controller()->selfType();
?>
<div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header with-border">
                <?=view('drycore/util/form/searchForm', ['box'=>$box]);?>
                <h3 class="box-title"><?=$title;?></h3>
                <div class="box-tools">
                    <?=view('drycore/util/widget/orderForm', ['box'=>$box]);?>
               </div>
            </div>
            <div class="box-body">
                <?php foreach ($menu as $line) : ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                        <?php foreach (controller()->listActions as $block) : ?>
                            <div class="btn-group">
                                <?php foreach ($block as $action) : ?>
                                    <?=view($action.'Action', ['model'=>$line,'controllerName'=>$controllerName]);?>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                        <?=$line['ancor'];?>
                    </h3>
                  </div>
                    <?php if (isset($line['menu'])) :?>
                      <div class="panel-body">
                          <ul class="list-group col-xs-offset-2">
                            <?php foreach ($line['menu'] as $subline) : ?>
                            <li class="list-group-item">
                                <?php foreach (controller()->listActions as $block) : ?>
                                    <div class="btn-group">
                                        <?php foreach ($block as $action) : ?>
                                            <?=view($action.'Action', [
                                                'model'=>$subline,
                                                'controllerName'=>$controllerName
                                            ]);?>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endforeach; ?>
                                <?=$subline['ancor'];?>
                            </li>
                            <?php endforeach;?>
                          </ul>
                      </div>
                    <?php endif;?>
                </div>            
                <?php endforeach;?>
            </div>
        </div>        
    </div>
    <div class="col-md-6">
        <div class="box">
            <div class="box-body">
                <?=$article;?>
            </div>
        </div>
    </div>
</div>
