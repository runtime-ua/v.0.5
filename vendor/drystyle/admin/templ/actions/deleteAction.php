<a class="btn btn-danger btn-xs"
   href="<?=urlXsrf([$controllerName.':delete','id'=>$model->id]);?>"
   onClick="return window.confirm('<?=$_->confirm;?>');">
    <i class="fa fa-fw fa-trash"></i>
</a>                    
