<h1><?=$title;?></h1>
<article>
    <?=$article;?>
</article>
<?php $pager = view('drycore/util/widget/pager', ['box'=>$box])?>
<div class="row row-flex">
<?=showModels($itemView, $box->findAll())?>
</div>
<?=$pager->render();?>
