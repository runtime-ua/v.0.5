<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if (isset($subj)) :?>
        <title><?=$subj;?></title>
<?php endif;?>
    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>
                    <?=showViews($before);?>
                    <?=$content;?>
                    <?=showViews($after);?>
                </td>
            </tr>
        </table>
    </body>
</html>