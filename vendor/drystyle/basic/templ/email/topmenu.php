<!-- Header Start -->
<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
    <tr>
        <td style="padding:15px 0 0 0;">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="580"
                   style="border-collapse: collapse;">
                <tr>
                    <td>
                        <table align="left" border="0" cellpadding="0" cellspacing="0" width="370"
                               style="border-collapse: collapse;">
                            <tr>
                                <td  height="75" style="text-align: left; vertical-align: middle;">
                                    <a href="<?=url('/', true);?>"
                                       style="font-family:helvetica, Arial, sans-serif; color: #333333;
                                       font-size: 16px; text-decoration: none;">
                                        <b><?=findModel('config@registry')->siteName;?></b>
                                    </a>
                                </td>
                                <td  height="75" style="text-align: right; vertical-align: middle;">
                                <?php foreach ($menu as $line) : ?>
                                    <a href="<?=url($line['url'], true);?>"
                                       style="font-family:helvetica, Arial, sans-serif; color: #333333;
                                       font-size: 16px; text-decoration: none;">
                                        <?=$line['name'];?>
                                    </a> &nbsp;&nbsp;
                                <?php endforeach;?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- Header End -->
