<?php $info = findModel('contact@registry'); ?>
<div class="col-md-12 well well-sm" itemscope itemtype="http://schema.org/Organization">
    <small>
        <span class="pull-left">
            <i class="fa fa-fw fa-globe fa-lg"></i>
            <span itemprop="name"><?=$info->name;?></span>,
            <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <span itemprop="addressCountry"><?=$info->country;?></span>,
                <span itemprop="postalCode"><?=$info->postalCode;?></span>,
                <span itemprop="addressLocality"><?=$info->city;?></span>,
                <span itemprop="streetAddress"><?=$info->adress;?></span>
            </span>
        </span>
        <span class="pull-right">
            <span>
                <i class="fa fa-clock-o"></i> <?=$info->time;?>
            </span>
            <span itemprop="telephone">
                <i class="fa fa-phone"></i> <?=$info->phone;?>
            </span>
            <span itemprop="email">
                <i class="fa fa-envelope"></i> <?=$info->emailText;?>
            </span>
        </span>
    </small>
</div>
