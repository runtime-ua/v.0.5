<?php $info = findModel('contact@registry'); ?>
<div class="col-md-4 small" itemscope itemtype="http://schema.org/Organization">
    <span class="h3" itemprop="name"><?=$info->name;?></span>
    <br>
    <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
        <i class="fa fa-fw fa-globe fa-lg"></i>
        <span itemprop="addressCountry"><?=$info->country;?></span>,
        <span itemprop="postalCode"><?=$info->postalCode;?></span>,
        <span itemprop="addressLocality"><?=$info->city;?></span>,
        <br>
        <span itemprop="streetAddress"><?=$info->adress;?></span>
    </span>
    <br>
    <span>
        <i class="fa fa-clock-o"></i> <?=$info->time;?>
    </span>
    <br>
    <span itemprop="telephone">
        <i class="fa fa-phone"></i> <?=$info->phone;?>
    </span>
    <br>
    <span itemprop="email">
        <i class="fa fa-fw fa-envelope"></i> <?=$info->emailText;?>
    </span>
</div>
