<div class="col-lg-12 visible-lg text-center" style="padding: 10px;">
    <?php if (isset($title)) :?>
    <span class="h3"><?=$title;?></span><br>
    <?php endif;?>
    <?php foreach ($logos as $logo) :?>
        <img src="<?=$logo;?>" height="<?=$height;?>">
    <?php endforeach;?>
</div>
