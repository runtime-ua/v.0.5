<!DOCTYPE html>
<html lang="<?=s()->locale->shortLang;?>">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?=findModel('config@registry')->favicon;?>">
        <title><?=$metatitle;?></title>
		<meta name="description" content="<?=$metadesc;?>">
        <link href="<?=$canonical;?>" rel="canonical"/>
        <?php foreach (asset('mainCss') as $css) :?>
           <link href="<?=$css;?>" rel="stylesheet" type="text/css">
        <?php endforeach;?>
        <?php foreach (asset('headCss') as $css) :?>
           <link href="<?=$css;?>" rel="stylesheet" type="text/css">
        <?php endforeach;?>
        <?php foreach (asset('headJs') as $css) :?>
            <script src="<?=$js;?>"></script>
        <?php endforeach;?>
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?=show(asset('headStyle'));?>
        <?=show(asset('headScript'));?>
    </head>
    <body>
        <?=$content;?>
        <?php foreach (asset('mainJs') as $js) :?>
        <script src="<?=$js;?>"></script>
        <?php endforeach;?>
        <?php foreach (asset('footerCss') as $css) :?>
        <link href="<?=$css;?>" rel="stylesheet" type="text/css">
        <?php endforeach;?>
        <?php foreach (asset('footerJs') as $js) :?>
        <script src="<?=$js;?>"></script>
        <?php endforeach;?>
        <?=show(asset('footerStyle'));?>
        <?=show(asset('footerScript'));?>
    </body>
</html>
