<div class="col-sm-3">
    <a class="no-link no-color" href="<?=$model->pageurl;?>">
        <img src="<?=$model->imgVariant('thumb', 'small');?>" class="img-thumbnail hidden-xs"  style="_min-width: 100%">
        <div class="h4"><?=$model->title;?></div>
        <div style="min-height: 120px;"><?=$model->preview;?></div>
    </a>
</div>