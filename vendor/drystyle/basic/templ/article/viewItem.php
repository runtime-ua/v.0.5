<div class="container col-lg-12">
    <?php if (isset($model['thumb'])) : ?>
    <div class="col-md-3 col-xs-12">
        <br>
        <a href="<?=$model->pageurl;?>">
            <img src="<?=$model->imgVariant('thumb', 'mini');?>"  class="img-thumbnail"  style="min-width: 100%">
        </a>
    </div>
    <?php endif; ?>
    <?php
    if (isset($model['thumb'])) {
        $articleSize= 9;
    } else {
        $articleSize= 12;
    }
    ?>
    <article class="col-md-<?=$articleSize;?> col-xs-12">
        <header>
            <h2><a class="no-link no-color" href="<?=$model->pageurl;?>"><?=$model->title;?></a></h2>
            <?php if (isset($model['showDate'])) : ?>
            <i class="fa fa-calendar"></i>
            <span class="day"><?=s()->locale->asDate($model->showDate);?></span>
            <?php endif; ?>
        </header>
        <div>
            <a class="no-link no-color" href="<?=$model->pageurl;?>">
                <p><?=$model->preview;?></p>
                <div class="pull-right"><b><?=$_->more;?> >>></b></div>
            </a>
        </div>
    </article>
</div>
