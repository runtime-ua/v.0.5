<div class="row" style="padding-bottom: 5px;">
    <?php if (isset($model['thumb'])) : ?>
    <div class="col-md-4 hidden-sm hidden-xs">
        <a href="<?=$model->pageurl;?>">
            <img src="<?=$model->imgVariant('thumb', 'thumbnail');?>"  class="img-responsive">
        </a>
    </div>
    <?php endif; ?>
    <div class="col-md-<?=showIf(isset($model['thumb']), '8');?><?=showIf(!isset($model['thumb']), '12');?> col-xs-12">
        <h5>
            <a class="no-link" href="<?=$model->pageurl;?>">
                <?=$model->title;?>
            </a>
        </h5>
        <?php if (isset($model['showDate'])) : ?>
        <div class="small">
            <i class="fa fa-fw fa-calendar"></i>
            <?=s()->locale->asDate($model->showDate);?>
        </div>
        <?php endif; ?>
    </div>
</div>
