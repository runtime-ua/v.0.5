<a name="<?=$id;?>"></a>
<div class="row miniArticle" id="<?=$id;?>">
    <div class="col-sm-12">
        <?=$content;?>
    </div>
    <?php if ($needHr) :?>
    <div class="col-md-12"><hr></div>
    <?php endif;?>
</div>
