<?php assetStart();?>
<style>
    .miniArticle .thumb {
        width:100%;
    }
    .miniArticle div hr {
        margin-top: 10px;
        margin-bottom: 10px;
    }
    
    @media (min-width: 992px) {
        .miniArticle {
            position: relative;
        }
        .miniArticleContent {
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }
        
        .miniArticleContent.col-md-11 {left: 8.33%;}
        .miniArticleContent.col-md-10 {left: 16.66%;}
        .miniArticleContent.col-md-9 {left: 25%;}
        .miniArticleContent.col-md-8 {left: 33.33%;}
        .miniArticleContent.col-md-7 {left: 41.66%;}
        .miniArticleContent.col-md-6 {left: 50%;}
        .miniArticleContent.col-md-5 {left: 58.33%;}
        .miniArticleContent.col-md-4 {left: 66.67%;}
        .miniArticleContent.col-md-3 {left: 75%;}
        .miniArticleContent.col-md-2 {left: 83.33%;}
        .miniArticleContent.col-md-1 {left: 91.66%;}
        .miniArticleContent.col-md-pull-1,
        .miniArticleContent.col-md-pull-2,
        .miniArticleContent.col-md-pull-3,
        .miniArticleContent.col-md-pull-4,
        .miniArticleContent.col-md-pull-5,
        .miniArticleContent.col-md-pull-6,
        .miniArticleContent.col-md-pull-7,
        .miniArticleContent.col-md-pull-8,
        .miniArticleContent.col-md-pull-9,
        .miniArticleContent.col-md-pull-10,
        .miniArticleContent.col-md-pull-11 {
            left: 0;
        }    
    }    
</style>
<?php assetEnd('headStyle');?>
<a name="<?=$id;?>"></a>
<div class="row miniArticle" id="<?=$id;?>">
    <?php if ($thumb and $thumbSize) :?>
    <div class="col-md-<?=$thumbSize;?> <?=showIf($rightThumb, 'col-md-push-'.(12 - $thumbSize));?>">
        <img class="img-responsive thumb" src="<?=$thumb;?>" alt="">
    </div>
    <?php endif;?>
    <div class="miniArticleContent col-md-<?=(12 - $thumbSize);?> <?=showIf($rightThumb, 'col-md-pull-'.$thumbSize);?>">
        <?=$content;?>
    </div>
    <?php if ($needHr) :?>
    <div class="col-md-12"><hr></div>
    <?php endif;?>
</div>
