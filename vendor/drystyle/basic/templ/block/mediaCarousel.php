<?php
/**
 * $class - css-класс карусели
 * $id - id карусели
 * $num - колво блоков за раз
 */
?>
<?php assetStart();?>
<style>
    .media-carousel .carousel-inner .item
    {
        margin-right: 10px;
    }
    .media-carousel 
    {
      margin-bottom: 0;
      padding: 0px 40px 30px 40px;
      margin-top: 30px;
    }
    /* Previous button  */
    .media-carousel .carousel-control.left 
    {
      left: -12px;
      background-image: none;
      background: none repeat scroll 0 0 #222222;
      border: 4px solid #FFFFFF;
      border-radius: 23px 23px 23px 23px;
      height: 40px;
      width : 40px;
    }
    /* Next button  */
    .media-carousel .carousel-control.right 
    {
      right: -12px !important;
      background-image: none;
      background: none repeat scroll 0 0 #222222;
      border: 4px solid #FFFFFF;
      border-radius: 23px 23px 23px 23px;
      height: 40px;
      width : 40px;
    }
    /* Changes the position of the indicators */
    .media-carousel .carousel-indicators 
    {
      right: 50%;
      top: auto;
      bottom: 0px;
      margin-right: -19px;
    }
    /* Changes the colour of the indicators */
    .media-carousel .carousel-indicators li 
    {
      background: #c0c0c0;
    }
    .media-carousel .carousel-indicators .active 
    {
      background: #333333;
    }
</style>
<?php assetEnd('footerStyle');?>

<?php
$data = box($boxName)->limit($limit)->findAll($where);
?>
<?php if ($data) : ?>
    <?php if ($needTitle) : ?>
    <span class="h3 hidden-xs hidden-sm"><?=$_->title;?></span>
    <?php endif;?>
    <div class="carousel slide media-carousel hidden-xs hidden-sm <?=$class;?>"
         data-pause="true" data-interval="false" id="<?=$id;?>">
        <div class="carousel-inner">
            <?php foreach (array_chunk($data, $num) as $key => $line) : ?>
            <div class="item<?=showIf($key==0, ' active');?>">
                <div class="row">
                <?php
                foreach ($line as $model) {
                    $data = $itemConfig;
                    $data['model'] = $model;
                    echo view($itemViewName, $data);
                }
                ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <a data-slide="prev" href="#<?=$id;?>" class="left carousel-control">‹</a>
        <a data-slide="next" href="#<?=$id;?>" class="right carousel-control">›</a>
    </div>
<?php endif;?>
