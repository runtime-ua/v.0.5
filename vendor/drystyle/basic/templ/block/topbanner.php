<?php $img = s()->drycore_util_image_convertor->getVersionUrl($img, 'big');?>
<?php assetStart();?>
<style>
    .topbanner,
    .topslider,
    .fill {
        padding-top: 50px;
        min-height: <?=$height;?>px;
        background-image: url("<?=$img;?>");
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
        background-repeat: no-repeat; 
        background-attachment: scroll;
        background-position: center center;
        position: inherit;
        text-align: center;
    }
    .topbanner.full,
    .fullheight {
        height: 50vh;
        max-height: 250px;
    }
    @media all and (min-width: 992px) {
        .fullheight {
            height: 100vh;
            max-height: 100vh;
        }
    }

    /*Вертикальная центровка*/
    @media all and (min-width: 992px) {
        .topbanner {
            position: relative;    
        }
        .topbanner .inner {
            width: 100%;
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
        }    
    }
</style>
<?php assetEnd('headStyle');?>
<section class="col-xs-12 topbanner<?=showIf($full, ' fullheight');?>">
    <div class="row inner">
        <div class="col-xs-12">
            <div class="topbanner-content">
                <?=$content;?>
            </div>
        </div>
    </div>
</section>
