<?php $img = s()->drycore_util_image_convertor->getVersionUrl($img, 'big');?>
<?php assetStart();?>
<style>
    .topbanner,
    .topslider,
    .fill {
        -webkit-background-size: cover;
        -moz-background-size: cover;
        background-size: cover;
        -o-background-size: cover;
        background-repeat: no-repeat; 
        background-attachment: scroll;
        background-position: center center;
        position: inherit;
    }
    .topbanner.full,
    .fullheight {
        height: 50vh;
        max-height: 250px;
    }
    @media all and (min-width: 992px) {
        .fullheight {
            height: 100vh;
            max-height: 100vh;
        }
    }
    .topbanner a .logo {
        margin: 10px auto;
    }
    .topbanner .header {
        text-align: center;
        text-shadow: 0 0 10px #000;
        color: #fff;
    }    
    .topslider {
        <?=showIf($height, 'min-height: '.$height.'px;');?>
        <?=showIf($img, 'background-image: url("'.$img.'");');?>
    }
    .fill {
        background-image:url('<?=$slide->imgVariant('sliderimg', 'big');?>');
        <?=showIf($height, 'min-height: '.$height.'px;');?>
    }
</style>
<?php assetEnd('headStyle');?>
<?php $slider = box('drystyle/basic/slider')->findAll();?>
<?php
$cssClass = showIf($full, ' fullheight').showIf($hideSmBanner, ' hidden-xs hidden-sm')
?>
<section data-ride="carousel" id="slider"
    class="carousel slide col-xs-12 topslider <?=$cssClass?>">
  <ol class="carousel-indicators">
    <?php $i = 0;foreach ($slider as $slide) :?>
      <li data-target="#slider" data-slide-to="<?=$i;?>" <?=showIf(($i==0), 'class="active"');?>></li>
    <?php $i++;
    endforeach;?>
  </ol>
  <div class="carousel-inner<?=showIf($full, ' fullheight');?>" role="listbox">
    <?php $i = 0;foreach ($slider as $slide) :?>
    <div class="item <?=showIf(($i==0), ' active');?>">
        <?php if ($slide->type !='notFound') :?>
        <a href="<?=$slide->pageurl;?>">
        <?php endif;?>
          <div class="fill<?=showIf($full, ' fullheight');?>">
          </div>
          <div class="carousel-caption">
            <?=showIf($slide->slidertitle, '<h3>'.$slide->slidertitle.'</h3>');?>
            <?=showIf($slide->slidertext, '<p>'.$slide->slidertext.'</p>');?>
          </div>
        <?php if ($slide->type !='notFound') :?>
        </a>
        <?php endif;?>
    </div>
    <?php $i++;
    endforeach;?>
  </div>
  <a class="left carousel-control" href="#slider" role="button" data-slide="prev">
    <span class="icon-prev" aria-hidden="true"></span>
  </a>
  <a class="right carousel-control" href="#slider" role="button" data-slide="next">
    <span class="icon-next" aria-hidden="true"></span>
  </a>
</section>
