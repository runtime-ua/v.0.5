<?php if ($alertText) : ?>
<div class="alert alert-warning" role="alert">
    <?=$alertText;?>
    <?php if ($alertUrl) : ?>
    <a href="<?=$alertUrl;?>" class="alert-link"><?=$_->more;?></a>
    <?php endif;?>
</div>
<?php endif;?>
