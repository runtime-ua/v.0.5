<?php
/**
 * Набор блоков через один, левый-правый, левый, правый...
 * Старый вариант.
 */
?>
<?php foreach ($articles as $key => $line) :?>
<?php if (($key % 2) == 0) : // четное ?> 
<?= view('app/land/miniArticleLeft', ['model'=>$line]);?>
<?php endif;?>
<?php if (($key % 2) == 1) : // нечетное ?> 
<?= view('app/land/miniArticleRight', ['model'=>$line]);?>
<?php endif;?>
<?php endforeach;
