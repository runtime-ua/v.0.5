<?php assetStart();?>
    <script>
        $('.metaMenuMetacat').click(function () {
            var id = $(this).attr('id');
            if ($('#subcat_' + id).hasClass('hidden')) {
                $('#subcat_' + id).toggleClass('hidden');
                return false;
            }
        });
    </script>
<?php assetEnd('footerScript');?>
<?php
$box = box($boxName)->limit(false);
$menu = s()->drycore_util_widget_menuHelper->go($box->findAll());
?>
<div class="panel-group" id="<?=$id;?>">
    <?php if (isset($needTitle) and $needTitle) :?>
    <div class="h3"><?=$_->title;?></div>
    <?php endif;?>
    <?php foreach ($menu as $line) :?>
    <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a id="<?=$id;?>_metacat_<?=$line['id'];?>" class="no-link metaMenuMetacat" href="<?=$line['url'];?>">
                <?=$line['ancor'];?>
            </a>
          </h4>
        </div>
        <ul class="list-group hidden" id="subcat_<?=$id;?>_metacat_<?=$line['id'];?>">
            <?php if (isset($line['menu'])) :?>
            <?php foreach ($line['menu'] as $subline) : ?>
            <li class="list-group-item">
                <a class="no-link no-color" href="<?=$subline['url'];?>"><?=$subline['ancor'];?></a>
            </li>
            <?php endforeach;?>
            <?php endif;?>
            <?php if (!isset($line['menu'])) :?>
            <li class="list-group-item">
                <?=$_->none;?>
            </li>
            <?php endif;?>
        </ul>
    </div>
    <?php endforeach;?>
</div>
