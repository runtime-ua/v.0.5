<?php
$content = findModel($modelName)->$fieldName;
?>
<?php if ($content) :?>
<div class="panel panel-default hidden-sm hidden-xs">
  <div class="panel-body">
        <?=$content;?>
  </div>
</div>
<?php endif;?>
