<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><?=$_->title;?></h3>
  </div>
  <div class="panel-body">
    <?=showModels($itemView, box($boxName)->limit($limit)->findAll())?>
    <?php if (isset($moreUrl) and $moreUrl) :?>
    <a href="<?=url($moreUrl);?>" class="pull-right no-link no-color"><?=$_->all;?></a><br>
    <?php endif;?>
  </div>
</div>
