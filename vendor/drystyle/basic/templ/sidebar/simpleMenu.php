<div class="panel panel-default">
    <?php if (isset($needTitle) and $needTitle) : ?>
    <div class="panel-heading"><?=$_->title;?></div>
    <?php endif;?>
    <ul class="list-group">
        <?php foreach (box($boxName)->limit(false)->findAll() as $line) : ?>
        <li class="list-group-item">
            <a class="no-link no-color" href="<?=$line->pageurl;?>"><?=$line->menutitle;?></a>
        </li>
        <?php endforeach;?>
    </ul>
</div>