<?php
/**
 * Простое меню у которого нет прозрачности, не закрепленное, нет строки контактов... ничего нет.
 */
if ($dark) {
    $navbarType = 'navbar-inverse';
} else {
    $navbarType = 'navbar-default';
}
?>
<div class="col-xs-12">
<nav data-spy="affix" data-offset-top="100"
     class="navbar <?=$navbarType;?> navbar-custom navbar-fixed-top topnav topmenu affix-menu affix-top"
     role="navigation">
    <?php include 'inc/container.php';?>
</nav>
</div>
<div class="col-xs-12 hidden-lg hidden-md"><br></div>
<?php assetStart();?>
<style>
    .navbar-toggle {
        margin-top: 5px;
        margin-bottom: 5px;
    }
    .navbar-custom .navbar-toggle {
        border-color: inherit;
    }
    .navbar-custom .navbar-toggle .icon-bar {
        background-color: inherit !important;
    }
    @media (min-width: 992px) {
        .affix-menu.affix-top {
            background-color: transparent;
            border-color: transparent;
            box-shadow: none !important;
            color: inherit;
        }
    }
</style>
<?php assetEnd('headStyle');?>
