<?php
/* 
 * Закрепленное, со строкой контактов, без прозрачности
 */
if ($dark) {
    $navbarType = 'navbar-inverse';
} else {
    $navbarType = 'navbar-default';
}
//
$info = findModel('contact@registry');
?>
<div class="col-xs-12 hidden-sm hidden-xs">
<nav class="navbar topline navbar-fixed-top <?=$navbarType;?>" itemscope itemtype="http://schema.org/Organization">
    <?php include 'inc/toplineContainer.php';?>
</nav>    
</div>
<div class="col-xs-12">
<nav class="navbar topline-padding navbar-fixed-top <?=$navbarType;?> topmenu" role="navigation">
        <?php include 'inc/container.php';?>
</nav>
</div>
<div class="col-xs-12 hidden-lg hidden-md"><br><br><br></div>
<?php assetStart();?>
<style>
    body {
        padding-top: 70px;
    }
    .topmenu {
        margin-bottom: 0px;
    }
    .topmenu, .topline {
        margin-top: 10px;
    }
    @media all and (min-width: 992px) {
        .topmenu.navbar-fixed-top.topline-padding {
            top: 30px;
        }
    }
    .topline {
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 30px;
        padding-left: 0px;
        margin-bottom: 0px;
        font-size: 75%;
        height: 30px;
        min-height: 30px;
    }
    .topline .navbar-text {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .navbar-fixed-top {
        margin-top: 0px;
    }
    .container .row .navbar-fixed-top {
        margin-left: auto;
        margin-right: auto;
    }
    @media all and (min-width: 768px) {
        .container .row .navbar-fixed-top {
            max-width: 750px;
        }
    }
    @media all and (min-width: 992px) {
        .container .row .navbar-fixed-top {
            max-width: 970px;
        }
    }
    @media all and (min-width: 1200px) {
        .container .row .navbar-fixed-top {
            max-width: 1170px;
        }
    }
    @media (min-width: 768px) {
        .affix-menu.affix-top {
            background-color: transparent;
            border-color: transparent;
        }
    }
    .skin-yeti .main-header .topmenu form input.form-control {
        font-size: 18px;
    }
</style>
<?php assetEnd('headStyle');?>

