<?php
/* 
 * Простое меню закрепленное вверху. Нет прозрачности, нет строки контактов...
 */
if ($dark) {
    $navbarType = 'navbar-inverse';
} else {
    $navbarType = 'navbar-default';
}
?>
<div class="col-xs-12">
<nav class="navbar navbar-fixed-top <?=$navbarType;?> topmenu" role="navigation">
        <?php include 'inc/container.php';?>
</nav>
</div>
<div class="col-xs-12 hidden-lg hidden-md"><br><br><br></div>
<?php assetStart();?>
<style>
    body {
        padding-top: 50px;
    }
    .topmenu {
        margin-bottom: 0px;
    }
    .topmenu {
        margin-top: 10px;
    }
    .navbar-fixed-top {
        margin-top: 0px;
    }
    .container .row .navbar-fixed-top {
        margin-left: auto;
        margin-right: auto;
    }
    @media all and (min-width: 768px) {
        .container .row .navbar-fixed-top {
            max-width: 750px;
        }
    }
    @media all and (min-width: 992px) {
        .container .row .navbar-fixed-top {
            max-width: 970px;
        }
    }
    @media all and (min-width: 1200px) {
        .container .row .navbar-fixed-top {
            max-width: 1170px;
        }
    }
    .skin-yeti .main-header .topmenu form input.form-control {
        font-size: 18px;
    }
</style>
<?php assetEnd('headStyle');?>
