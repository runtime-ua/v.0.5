<?php
/* 
 * Простое меню у которого нет прозрачности, не закрепленное, нет строки контактов... ничего нет.
 */
if ($dark) {
    $navbarType = 'navbar-inverse';
} else {
    $navbarType = 'navbar-default';
}
?>
<div class="col-xs-12">
<nav class="navbar <?=$navbarType;?> topmenu" role="navigation">
        <?php include 'inc/container.php';?>
</nav>
</div>
<?php assetStart();?>
<style>
    .topmenu {
        margin-bottom: 0px;
    }
    .topmenu {
        margin-top: 10px;
    }
    .skin-yeti .main-header .topmenu form input.form-control {
        font-size: 18px;
    }
</style>
<?php assetEnd('headStyle');?>
