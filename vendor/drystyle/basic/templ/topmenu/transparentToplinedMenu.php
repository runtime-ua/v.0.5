<?php
/* 
 * Простое меню у которого нет прозрачности, не закрепленное, нет строки контактов... ничего нет.
 */
if ($dark) {
    $navbarTypeCSS = 'navbar-inverse';
} else {
    $navbarTypeCSS = 'navbar-default';
}
//
$info = findModel('contact@registry');
?>
<div class="col-xs-12 hidden-sm hidden-xs">
<nav class="navbar transparent navbar-fixed-top <?=$navbarType;?>" itemscope itemtype="http://schema.org/Organization">
    <?php include 'inc/toplineContainer.php';?>
</nav>    
</div>
<div class="col-xs-12">
<nav class="navbar topline-padding transparent navbar-fixed-top <?=$navbarType;?> topmenu" role="navigation">
    <?php include 'inc/container.php';?>
</nav>
</div>
<?php assetStart();?>
<style>
    .topmenu {
        margin-bottom: 0px;
    }
    .topmenu, .topline {
        margin-top: 10px;
    }
    .topline.transparent,
    .topmenu.transparent {
        opacity: 0.9;
    }
    @media all and (min-width: 992px) {
        .topmenu.navbar-fixed-top.topline-padding {
            top: 30px;
        }
    }
    .topline {
        padding-top: 5px;
        padding-bottom: 5px;
        padding-right: 30px;
        padding-left: 0px;
        margin-bottom: 0px;
        font-size: 75%;
        height: 30px;
        min-height: 30px;
    }
    .topline .navbar-text {
        margin-top: 0px;
        margin-bottom: 0px;
    }
    .navbar-fixed-top {
        margin-top: 0px;
    }
    .container .row .navbar-fixed-top {
        margin-left: auto;
        margin-right: auto;
    }
    @media all and (min-width: 768px) {
        .container .row .navbar-fixed-top {
            max-width: 750px;
        }
    }
    @media all and (min-width: 992px) {
        .container .row .navbar-fixed-top {
            max-width: 970px;
        }
    }
    @media all and (min-width: 1200px) {
        .container .row .navbar-fixed-top {
            max-width: 1170px;
        }
    }
    .skin-yeti .main-header .topmenu form input.form-control {
        font-size: 18px;
    }
</style>
<?php assetEnd('headStyle');?>
