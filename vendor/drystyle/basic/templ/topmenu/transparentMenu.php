<?php
/* 
 * Полупрозрачное, закрепленное, без строки контактов
 */
if ($dark) {
    $navbarType = 'navbar-inverse';
} else {
    $navbarType = 'navbar-default';
}
?>
<div class="col-xs-12">
<nav class="navbar transparent navbar-fixed-top <?=$navbarType;?> topmenu" role="navigation">
        <?php include 'inc/container.php';?>
</nav>
</div>
<?php assetStart();?>
<style>
    .topmenu {
        margin-bottom: 0px;
    }
    .topmenu.transparent {
        opacity: 0.9;
    }
    .navbar-fixed-top {
        margin-top: 0px;
    }
    .container .row .navbar-fixed-top {
        margin-left: auto;
        margin-right: auto;
    }
    @media all and (min-width: 768px) {
        .container .row .navbar-fixed-top {
            max-width: 750px;
        }
    }
    @media all and (min-width: 992px) {
        .container .row .navbar-fixed-top {
            max-width: 970px;
        }
    }
    @media all and (min-width: 1200px) {
        .container .row .navbar-fixed-top {
            max-width: 1170px;
        }
    }
    .skin-yeti .main-header .topmenu form input.form-control {
        font-size: 18px;
    }
</style>
<?php assetEnd('headStyle');?>
