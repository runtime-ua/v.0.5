<?php
$menu = s()->drycore_util_widget_menuHelper->go(box($boxName)->findAll());
?>
<?php foreach ($menu as $line) : ?>
    <?php if (!isset($line['menu'])) :?>
        <li <?=showIf(isset($line['active']), ' class="active"');?>>
            <a href="<?=$line['url'];?>"><?=$line['ancor'];?></a>
        </li>
    <?php endif;?>
    <?php if (isset($line['menu'])) :?>
       <li class="dropdown <?=showIf(isset($line['active']), ' class="active"');?>">
          <a href="<?=$line['url'];?>" class="dropdown-toggle"
             data-toggle="dropdown"
             role="button"
             aria-haspopup="true"
             aria-expanded="false"
          >
            <?=$line['ancor'];?>
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <?php foreach ($line['menu'] as $subline) : ?>
            <li><a href="<?=$subline['url'];?>"><?=$subline['ancor'];?></a></li>
            <?php endforeach;?>
          </ul>
        </li>
    <?php endif;?>
<?php endforeach;?>
