<?php
$menu = [];
foreach ($landingList as $id) {
    // получим модельку
    $model = findModel($id.'@drystyle/basic/unit');
    $menu[] = ['url'=>'#'.$model->id,'ancor'=>$model->name];
}
?>
<?php foreach ($menu as $line) : ?>
    <li <?=showIf(isset($line['active']), ' class="active"');?>>
        <a href="<?=$line['url'];?>" class="page-scroll"><?=$line['ancor'];?></a>
    </li>
<?php endforeach;?>
<?php assetStart();?>
<script>
    (function($) {
        "use strict"; // Start of use strict
        // jQuery for page scrolling feature - requires jQuery Easing plugin
        $('a.page-scroll').bind('click', function(event) {
            var $anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: ($($anchor.attr('href')).offset().top - 60)
            }, 1250, 'easeInOutExpo');
            event.preventDefault();
        });
        // Closes the Responsive Menu on Menu Item Click
        $('.navbar-collapse ul li a:not(.dropdown-toggle)').click(function() {
            $('.navbar-toggle:visible').click();
        });
    })(jQuery); // End of use strict
</script>
<?php assetEnd('footerScript');?>
<?php asset('footerJs', '//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js');?>
