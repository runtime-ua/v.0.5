<div class="container<?=showIf(!$boxed, '-fluid');?>">
    <p class="navbar-text navbar-left">
        <i class="fa fa-fw fa-globe fa-lg"></i>
        <span itemprop="name"><?=$info->name;?></span>,
        <span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
            <span itemprop="addressCountry"><?=$info->country;?></span>,
            <span itemprop="postalCode"><?=$info->postalCode;?></span>,
            <span itemprop="addressLocality"><?=$info->city;?></span>, 
            <span itemprop="streetAddress"><?=$info->adress;?></span>
        </span>
    </p>
    <p class="navbar-text navbar-right" itemprop="email">
        <i class="fa fa-envelope"></i> <?=$info->emailText;?>
    </p>
    <p class="navbar-text navbar-right" itemprop="telephone">
        <i class="fa fa-phone"></i> <?=$info->phone;?>
    </p>
    <p class="navbar-text navbar-right">
        <i class="fa fa-clock-o"></i> <?=$info->time;?>
    </p>
</div>