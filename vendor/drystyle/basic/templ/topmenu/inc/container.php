<?php if (!isset($nav2right)) {
    $nav2right=false;
}?>
    <div class="container<?=showIf(!$boxed, '-fluid');?>">
        <a class="navbar-brand" href="/">
        <?php if ($logo) :
    ?>
            <img class="_hidden-sm _hidden-xs" src="<?=$logo;?>">
        <?php
        endif;?>
        <?php if (!$logo) :
?>
            <?=findModel('config@registry')->siteName;?>
        <?php
        endif;?>
        </a>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#topmenu-collapse">
                <i class="fa fa-bars"></i>
            </button>
        </div>
        <div class="collapse navbar-collapse  <?=showIf($nav2right, 'navbar-right');?>" id="topmenu-collapse">
            <ul class="nav navbar-nav">
                <?php
                if ($boxName) {
                    include 'siteMenu.php';
                } else {
                    include 'landingMenu.php';
                }
                ?>
            </ul>
            <?=showBlocks($addonBlocks, 'drystyle/basic/unit'); ?>
        </div>
        <!-- /.navbar-collapse -->
    </div>
