<?php if (!$skin) {
    $skin = 'default';
}?>
<?php asset('headCss', '/files/css/'.$skin.'.first.min.css');?>
<?php asset('headCss', '/files/css/'.$skin.'.last.min.css');?>
<?php asset('mainCss', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');?>
<?php asset('mainJs', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js');?>
<?php asset('mainJs', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js');?>
<?php assetStart();?>
<style>
    html, body {
        height: 100%;
        background-attachment: fixed;
        background-size: <?=$bgSize;?>;
        <?php if ($bgImg) :
    ?>
        background-image: url("<?=$bgImg;?>");
        <?php
        endif;?>
    }
    .row-flex {
      display: -webkit-box; 
      display: -webkit-flex;
      display: flex;
      -webkit-flex-wrap: wrap; 
      flex-wrap: wrap;
    }
    
    
    a.no-link {text-decoration: none;}
    a.no-color {color: inherit;}
    
    p {
        text-align: justify;
        line-height: 1.5;
    }
    .modal {
      text-align: center;
    }
    @media screen and (min-width: 768px) { 
      .modal:before {
        display: inline-block;
        vertical-align: middle;
        content: " ";
        height: 100%;
      }
    }
    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
    .main-section {
        padding-top: 20px;
        padding-bottom: 50px;
        background-color: inherit;
    }
    .main-header {
        background-color: inherit;
    }
    .main-footer, .main-footer .well {
        background-color: <?=$footerBgColor;?>;
        color: <?=$footerColor;?>;
    }
    .main-footer  a, .main-footer a:visited {
        color: <?=$footerColor;?>;
    }
    .main-footer {
      padding: 30px;
    }
    .main-footer .links {
        font-size: small;
    }
</style>
<?php assetEnd('headStyle');?>

<header class="main-header container<?=showIf(!$boxedHeader, '-fluid');?>">
    <div class="row">
        <?=showBlocks($headerBlocks, 'drystyle/basic/unit'); ?>
    </div>
</header>
<section class="main-section container<?=showIf(!$boxed, '-fluid');?>">
    <div class="row">
        <div class="col-md-<?=$contentSize;?> <?=showIf($leftSidebarBlocks, 'col-md-push-'.$leftSize);?> col-xs-12">
            <?=showBlocks($contentBlocks, 'drystyle/basic/unit'); ?>
        </div>
        <?php if ($leftSidebarBlocks) :
?>
        <aside class="col-md-<?=$leftSize;?> col-md-pull-<?=$contentSize;?> col-xs-12">
            <?=showBlocks($leftSidebarBlocks, 'drystyle/basic/unit'); ?>
        </aside>
        <?php
        endif;?>
        <?php if ($rightSidebarBlocks) :
?>
        <aside class="col-md-<?=$rightSize;?> col-xs-12">
            <?=showBlocks($rightSidebarBlocks, 'drystyle/basic/unit'); ?>
        </aside>
        <?php
        endif;?>
    </div>
</section>
<footer class="main-footer container<?=showIf(!$boxedFooter, '-fluid');?>">
    <div class="row">
        <?=showBlocks($footerBlocks, 'drystyle/basic/unit'); ?>
    </div>
</footer>
<?=showBlocks($otherBlocks, 'drystyle/basic/unit'); ?>
