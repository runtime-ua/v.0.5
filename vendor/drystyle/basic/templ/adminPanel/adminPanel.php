<?php if (!s()->user->current()->isNew()) :?>
<?php
$info = s()->route->fields;
$pageId = $info['__pageID'];
$pageType = $info['__pageType'];
if ($pageId and $pageType) {
    $page = box($pageType)->findOne($pageId);
} else {
    $page = false;
}
// потом куда-то вынести в конфиги
$allowedChilds = [
    'article'=>['parent_id'=>'id'],
];
?>
<div class="hidden-sm hidden-xs"
     style="position: fixed; top: 50px;left: 0px;z-index: 10000;opacity: 0.8;width: 60px;">
    <?=view('drystyle/basic/adminPanel/infoPanel');?>
    <div class="list-group" style="background-color: #fff; border-radius: 0 30px 30px 0;">
      <a class="list-group-item"
         style="border-radius: 0 30px 0 0;"
         href="#"
         data-toggle="modal"
         data-target="#infoModal"
      >
          <i class="fa fa-info-circle fa-lg" aria-hidden="true"></i>
      </a>
      <a class="list-group-item" href="<?=url([
            'drycart/main/all:selectCreate',
            'default'=>['parent_id'=>$page->id],
            'filterParent'=>$page->childs->selfType()
        ]);?>">
          <i class="fa fa-plus fa-fw fa-lg" aria-hidden="true"></i>
      </a>
      <a class="list-group-item" href="<?=url(['drycart/main/all:copy','id'=>$page->id]);?>">
          <i class="fa fa-clone fa-fw fa-lg" aria-hidden="true"></i>
      </a>
      <a class="list-group-item" href="<?=url(['drycart/main/all:update','id'=>$page->id]);?>">
          <i class="fa fa-pencil fa-fw fa-lg" aria-hidden="true"></i>
      </a>
      <a class="list-group-item list-group-item-danger"
         style="border-radius: 0 0 30px 0;"
         href="<?=urlXsrf(['drycart/main/all:delete','id'=>$page->id]);?>"
         onClick="return window.confirm('Действительно удалить?');">
          <i class="fa fa-trash fa-fw fa-lg" aria-hidden="true"></i>
      </a>
    </div>
</div>
<?=view('drystyle/basic/adminPanel/infoModal', ['page'=>$page]);?>
<?php endif;?>
