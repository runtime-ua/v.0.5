<div class="modal fade" tabindex="-1" role="dialog" id="infoModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title"><?=$_->title;?></h4>
      </div>
      <div class="modal-body">
        <?=$_->desc;?>
        <?php
        if ($page) {
            $data = [
                'ID'=>$page->id,
                $_->type =>$page->selfName(),
                'route' => $page->route,
                $_->created => s()->locale->asDate($page->created),
                $_->updated =>s()->locale->asDate($page->updated),
                $_->childsCount =>$page->childs->count(),
                $_->phpVersion => phpversion(),
            ];
        } else {
            $data = [
                $_->type =>$_->none,
            ];
        }
        ?>
        <?=view('drycore/util/widget/array', ['data'=>$data]);?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
