<a class="list-group-item" href="<?=url($url);?>">
    <?php $count = box($boxName)->count($where);?>
    <?php if ($count) : ?>
    <span class="badge" style="margin-bottom: -15px;margin-right: -10px;font-size: 60%;"><?=$count;?></span>
    <?php endif; ?>
    <i class="<?=$icoClass;?>" aria-hidden="true"></i>
</a>
