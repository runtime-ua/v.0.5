<div class="list-group" style="border-radius: 0 30px 30px 0;">
    <a class="list-group-item" style="border-radius: 0 30px 0 0;" href="<?=url(['drycart/main/adminIndex:default']);?>">
      <i class="fa fa-dashboard fa-lg" aria-hidden="true"></i>
    </a>
    <?php
    // выведем все дочерние вьювы от drystyle/basic/adminPanel/infoUnit
    $views = s()->viewFactory->factoryChildsList('drystyle/basic/adminPanel/infoUnit', false);
    foreach ($views as $viewName) {
        echo view($viewName);
    }
    ?>
    <a class="list-group-item" style="border-radius: 0 0 30px 0;" href="<?=url(['auth:logout']);?>">
      <i class="fa fa-close" aria-hidden="true"></i>
    </a>
</div>
