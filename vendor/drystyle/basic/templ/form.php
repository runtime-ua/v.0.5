<article>
    <h1><?=$title;?></h1>
    <?=$article;?>
</article>
<?php if (!$model->isBoxed()) : ?>
<form role="form"  class="form-horizontal" method="post">
    <?=view('drycore/util/form/form', ['model'=>$model,'noHelp'=>true]);?>
    <div class="form-group">
        <div class="col-sm-9 col-sm-offset-3">
            <input type="submit" value="<?=$_->submit;?>" class="btn btn-default btn-block">
            <?= s()->xsrf->field(); ?>
        </div>
    </div>
</form>
<?php endif;?>
