<style>
    body, h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6 {
        font-family: Arial, Helvetica, sans-serif;
    }
    /****** Shop index carousel CSS ****/
    .brand-carousel img
    {
      width: 100%;
      height: 100px
    }

    .brand-carousel .carousel-control.left, 
    .brand-carousel .carousel-control.right 
    {
      margin-top: 40px;
    }
    .product-carousel .carousel-control.left, 
    .product-carousel .carousel-control.right 
    {
      margin-top: 170px;
    }
</style>