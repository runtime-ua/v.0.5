<?php
namespace drystyle\basic;

/**
 * Description of LayoutView
 *
 * @author Maks
 */
class LayoutView extends \drystyle\minimal\AbstractLayoutView
{
    public function init()
    {
        $this->fields['content'] = (string) showBlocks([$this->fields['structureId']], 'drystyle/basic/unit');
//        varDie(asset('headStyle'));
        parent::init();
    }
}
