<?php
/**
 * Умрем с данными обернутыми в pre
 * @param type $data
 */
function varDie($data)
{
    varPre($data);
    die();
}

/**
 * Умрем с данными в json обернутыми в pre
 * @param type $data
 */
function jsonDie($data)
{
    jsonPre($data);
    die();
}

/**
 * var_dump в pre, без умирания
 * @param type $data
 */
function varPre($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

/**
 * выводим json в pre без умирания
 * @param type $data
 */
function jsonPre($data)
{
    echo '<pre>';
    echo coolJson($data);
    echo '</pre>';
}

/**
 * Выполнить запрос из построителя и вывести результат
 * @param type $query
 */
function queryGoAndShow($query)
{
    $result = $query->go();
    echo view('drycore/util/widget/sqlResult', ['result'=>$result]);
}

/**
 * Выполнить запрос (SQL) и вывести результат
 * @param type $sql
 */
function sqlGoAndShow($sql)
{
    $result = s()->db->go($sql);
    echo view('drycore/util/widget/sqlResult', ['result'=>$result]);
}

/**
 * Транслитерация по правилам Яндекс.Мани (пока только русских букв)
 * 2DO: сделать хелпер а не вот так...
 * @param string $str
 * @return string
 */
function translit($str)
{
    $converter = [
        'а' => 'a',   'б' => 'b',   'в' => 'v',
        'г' => 'g',   'д' => 'd',   'е' => 'e',
        'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
        'и' => 'i',   'й' => 'i',   'к' => 'k',
        'л' => 'l',   'м' => 'm',   'н' => 'n',
        'о' => 'o',   'п' => 'p',   'р' => 'r',
        'с' => 's',   'т' => 't',   'у' => 'u',
        'ф' => 'f',   'х' => 'kh',  'ц' => 'ts',
        'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
        'ь' => '',    'ы' => 'y',   'ъ' => '',
        'э' => 'e',   'ю' => 'yu',  'я' => 'ya',
        'і' => 'i', 'ї' => 'i', 'є' => 'e'
    ];
    // Все буквы маленькие
    $str = mb_strtolower($str);
    // заменим все неизвестные символы на минус, потом заменим цепочки минусов на единичный минус
    $str = preg_replace(['/[^а-яa-z0-9\.]/u', '/[-]+/'], '-', $str);
    // Уберем минусы по краям
    $str = trim($str, '-');
    // А теперь собственно транслитерация
    return strtr($str, $converter);
}
