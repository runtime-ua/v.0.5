<?php
namespace drycore\util\form;

/**
 * Правило требуещее чтобы поле было обязательно заполнено,
 * расширяет стандартное правило тем что передает факт обязательности форме
 * чтобы по возможности еще в браузере сообщить что поле таки надо заполнить
 */
class RequiredRule extends \drycore\core\otherRules\RequiredRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['required'] = true;
        $this->master->value = $config;
    }
}
