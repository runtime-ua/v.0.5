<?php
namespace drycore\util\form;

/**
 * форма поиска, фильтрует данные по совпадению с требованиями из формы
 */
class SearchFormView extends \proto\View
{
    public function init()
    {
        if (!isset($this->fields['colapsed'])) {
            $this->fields['filtered'] = false;
        }
        $this->fields['filtered'] = false; // если ничего еще не отфильтровано, то можно форму скрыть
        $box = $this->fields['box'];
        // **********************************************************************
        $model = model($box->selfType());
        $model->scenario('search'); // чтобы если в конфиге уже со сценарием, то не было лажи
        s()->get->update($model, 'search');
        $this->fields['model'] = $model;
        // Применим фильтр если есть
        foreach ($model->iteratorFields() as $key) {
            $where = $model->searchFormWhere($key);
            if ($where) {
                $box->where($where);
                $this->fields['filtered'] = true;
            }
        }
        //
        $route = s()->route->fields;
        if (isset($route['search'])) {
            unset($route['search']);
        }
        $this->cancelUrl = url($route);
        // Выполним родителя
        parent::init();
    }
}
