<?php
namespace drycore\util\form;

/**
 * Активформ
 */
class FormView extends \proto\View
{
    protected $namespace = 'drycore/util/form';

    public function init()
    {
        if (!isset($this->fields['varName'])) {
            $this->fields['varName'] = str_replace('/', '_', $this->model->selfType());
        }
        if (!isset($this->fields['cols'])) {
            $this->fields['cols'] = $this->model->iteratorFields();
        }
        //
        $this->fields['inputs'] = [];
        foreach ($this->cols as $col) {
            if ($this->model->offsetExists($col)) {
                $this->doField($col);
            }
        }
        parent::init();
    }
    
    protected function doField($col)
    {
        if (!isset($this->fields['noHelp']) or !$this->fields['noHelp']) {
            $help = $this->model->lang($col.'#help');
        } else {
            $help = null;
        }
        //
        if (isset($this->fields['style'][$col])) {
            $style = $this->fields['style'][$col];
        } else {
            $style = ['inputClass'=>''];
        }
        //
        $data = [
            'id' => $this->varName.'-'.$col,
            'varName'=>$this->varName,
            'error' =>$this->model->error($col),
            'colName' =>$col,
            'value' => $this->model->forEdit($col),
            'label' => $this->model->lang($col),
            'model' => $this->model,
            'help' => $help,
            'style'=>$style,
            'required' => false,
            'type'=>'basic/string'
        ];
        $data = $this->model->activeFormConfig($col, $data);
        $this->fields['inputs'][] = view($this->namespace.'/'.$data['type'], $data);
    }
}
