<?php
namespace drycore\util\form\select;

/**
 * Правило множественного выбора
 */
class MultiSelectRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/multiSelect';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function set()
    {
        if ($this->master->value =='') {
            $this->master->value = [];
        }
    }
    //2DO: сделать OR или in
    protected function callSearchFormWhere()
    {
        throw new \Exception('Cant use multi in search, sorry');
//        $fieldName = $this->master->fieldName;
//        $value = $this->master->model->$fieldName;
//        //
//        if($value) {
//            $this->master->value = [$fieldName=>$value];
//        }
    }
}
