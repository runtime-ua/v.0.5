<?php
namespace drycore\util\form\select;

/**
 * Выбор из списка View-ов
 */
class ReflectionViewRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/reflectionView';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $this->master->value = s()->lang->getString('#self', 'view/'.$value);
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
