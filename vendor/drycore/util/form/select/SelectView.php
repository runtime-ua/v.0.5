<?php
namespace drycore\util\form\select;

/**
 * Select по связи
 */
class SelectView extends \proto\View
{
    public function init()
    {
        $currentValue = $this->fields['value'];
        if (!$currentValue and isset($this->fields['default'])) {
            $currentValue = $this->fields['default'];
        }
        $options = $this->getOptions();
        if ($currentValue) {
            $currentExist = false;
            foreach ($options as $key => $line) {
                $optionValue = $line['value'];
                $options[$key]['selected'] = ($currentValue==$optionValue);
                if ($currentValue==$optionValue) {
                    $currentExist = true;
                }
            }
            if (!$currentExist) {
                $options[$currentValue] = ['selected'=>true,'value'=>$currentValue,'desc'=>'**** current ****'];
            }
            $this->fields['noValue'] = false;
        } else {
            $this->fields['noValue'] = true;
        }
        $this->fields['options'] = $options;
        parent::init();
    }
    
    protected function getOptions()
    {
        $boxName = $this->param['box'];
        $box = box($boxName);
        $box->limit(false);
        if (isset($this->param['valueField'])) {
            $valueField = $this->param['valueField'];
        } else {
            $valueField = 'id';
        }
        //
        if (isset($this->param['descField'])) {
            $descField = $this->param['descField'];
        }
        $options = [];
        foreach ($box->findAll() as $line) {
            $value = $line->$valueField;
            if (isset($descField)) {
                $desc = $line->$descField;
            } else {
                $desc = $line->title();
            }
            $options[] = ['value'=>$value, 'desc'=>$desc];
        }
        return $options;
    }
}
