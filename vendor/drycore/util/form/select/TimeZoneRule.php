<?php
namespace drycore\util\form\select;

/**
 * Правило для поля выбора временной зоны
 */
class TimeZoneRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/timeZone';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
}
