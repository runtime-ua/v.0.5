<?php
namespace drycore\util\form\select;

/**
 * Правило для поля выбора по связи
 */
class SelectRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/select';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
