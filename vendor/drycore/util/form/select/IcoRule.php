<?php
namespace drycore\util\form\select;

/**
 * Правило для выбора иконок
 */
class IcoRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/ico';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
}
