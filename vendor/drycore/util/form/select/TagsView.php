<?php
namespace drycore\util\form\select;

/**
 * Ввод тегов
 */
class TagsView extends \proto\View
{
    public function init()
    {
        $currentValue = $this->fields['value'];
        $options = $this->getOptions();
        if ($currentValue) {
            foreach ($options as $key => $line) {
                $optionValue = $line['value'];
                if (in_array($optionValue, $currentValue)) {
                    $options[$key]['selected'] = true;
                    unset($currentValue[array_search($currentValue, $optionValue)]);
                } else {
                    $options[$key]['selected'] = false;
                }
            }
            foreach ($currentValue as $value) {
                $options[] = ['value'=>$value, 'desc'=>$value,'selected'=>true];
            }
            $this->fields['noValue'] = false;
        } else {
            $this->fields['noValue'] = true;
        }
        $this->fields['options'] = $options;
        parent::init();
    }
    
    protected function getOptions()
    {
        $options = [];
        $boxName = $this->param['box'];
        if ($boxName) {
            $box = box($boxName);
            $box->limit(false);
            if (isset($this->param['valueField'])) {
                $valueField = $this->param['valueField'];
            } else {
                $valueField = 'id';
            }
            //
            foreach ($box->findAll() as $line) {
                $value = $line->$valueField;
                $options[] = ['value'=>$value, 'desc'=>$value];
            }
        }
        return $options;
    }
}
