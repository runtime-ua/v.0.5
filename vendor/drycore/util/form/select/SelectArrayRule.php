<?php
namespace drycore\util\form\select;

/**
 * Выбор из списка
 */
class SelectArrayRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/selectArray';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $fieldName = $this->master->fieldName;
        $id = $fieldName.'_option_'.$value;
        $this->master->value = $this->master->model->lang($id);
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
