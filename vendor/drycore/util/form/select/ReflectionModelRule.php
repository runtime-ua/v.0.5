<?php
namespace drycore\util\form\select;

/**
 * Выбор из списка Model-ов
 */
class ReflectionModelRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'select/reflectionModel';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $this->master->value = s()->lang->getString('#self', 'model/'.$value);
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
