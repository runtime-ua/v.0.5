<?php
namespace drycore\util\form\select;

/**
 * Выбор для часового пояса
 */
class TimeZoneView extends \proto\View
{
    public function init()
    {
        $currentValue = $this->fields['value'];
        if (!$currentValue and isset($this->fields['default'])) {
            $currentValue = $this->fields['default'];
        }
        $options = $this->getOptions($currentValue);
        if ($currentValue) {
            $this->fields['noValue'] = false;
        } else {
            $this->fields['noValue'] = true;
        }
        $this->fields['options'] = $options;
        parent::init();
    }
    
    protected function getOptions($currentValue)
    {
        $options = [];
        foreach (timezone_identifiers_list(DateTimeZone::ALL_WITH_BC) as $value) {
            $options[] = [
            'value'=>$value,
            'desc'=>$value,
            'selected' => ($currentValue==$value)
            ];
        }
        return $options;
    }
}
