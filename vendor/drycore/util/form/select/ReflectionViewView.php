<?php
namespace drycore\util\form\select;

/**
 * Элемент управления (формы) Выбор из списка View-ов
 */
class ReflectionViewView extends \proto\View
{
    public function init()
    {
        $currentValue = $this->fields['value'];
        if (!$currentValue and isset($this->fields['default'])) {
            $currentValue = $this->fields['default'];
        }
        $options = $this->getOptions();
        if ($currentValue) {
            foreach ($options as $key => $line) {
                $optionValue = $line['value'];
                $options[$key]['selected'] = ($currentValue==$optionValue);
            }
            $this->fields['noValue'] = false;
        } else {
            $this->fields['noValue'] = true;
        }
        $this->fields['options'] = $options;
        parent::init();
    }
    
    protected function getOptions()
    {
        $options = [];
        if (!isset($this->param['parent'])) {
            $this->param['parent'] = null;
        }
        if (!isset($this->param['abstract'])) {
            $this->param['abstract'] = null;
        }
        foreach (s()->viewFactory->factoryChildsList($this->param['parent'], $this->param['abstract']) as $value) {
            $desc = s()->lang->getString('#self', 'view/'.$value);
            $options[] = ['value'=>$value, 'desc'=>$desc];
        }
        return $options;
    }
}
