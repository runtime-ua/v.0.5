<?php
namespace drycore\util\form\select;

/**
 * Элемент управления (формы) Выбор из списка вариантов
 */
class SelectArrayView extends \proto\View
{
    public function init()
    {
        $currentValue = $this->fields['value'];
        if (!$currentValue and isset($this->fields['default'])) {
            $currentValue = $this->fields['default'];
        }
        $options = $this->getOptions();
        if ($currentValue) {
            foreach ($options as $key => $line) {
                $optionValue = $line['value'];
                $options[$key]['selected'] = ($currentValue==$optionValue);
            }
            $this->fields['noValue'] = false;
        } else {
            $this->fields['noValue'] = true;
        }
        $this->fields['options'] = $options;
        parent::init();
    }
    
    protected function getOptions()
    {
        $options = [];
        foreach (tags2array($this->param['options']) as $value) {
            $desc = $this->model->lang($this->colName.'_option_'.$value);
            $options[] = ['value'=>$value, 'desc'=>$desc];
        }
        return $options;
    }
}
