<?php
namespace drycore\util\form;

/**
 * Форма с блоками/закладками
 * Разделяем форму на блоки, и заодно им названия готовим
 */
class FormBlockView extends \proto\View
{
    public function init()
    {
        if (!isset($this->fields['noHelp'])) {
            $this->fields['noHelp'] = false;
        }
        // Получим информацию о том кто в каком блоке
        $blockList = [];
        foreach ($this->model->fieldsList() as $field) {
            $value = $this->model->block($field, null);
            if (!is_null($value)) {
                if (!isset($blockList[$value])) {
                    $blockList[$value] = [];
                }
                $blockList[$value][] = $field;
            }
        }
        // Сделаем содержимое вкладок
        $blocks = [];
        foreach ($blockList as $name => $cols) {
            $blocks[$name] = [
                'title'=>$this->model->lang($name.'#Tab'),
                'content'=>view('drycore/util/form/form', [
                    'model'=>$this->model,
                    'cols'=>$cols,
                    'alert'=>$this->lang('alert'),
                    'noHelp'=>$this->fields['noHelp']
                ]),
            ];
        }
        $this->fields['blocks'] = $blocks;
        // Добавим дефолты
        if (!isset($this->fields['actions'])) {
            $this->fields['actions'] = [];
        }
        if (!isset($this->fields['btnText'])) {
            $this->fields['btnText'] = $this->lang('save');
        }
        parent::init();
    }
}
