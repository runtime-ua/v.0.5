<?php
namespace drycore\util\form\basic;

  /**
   * Скрытое поле (форма)
   */
class HiddenRule extends \proto\Rule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/hidden';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
}
