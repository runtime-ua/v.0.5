<?php
namespace drycore\util\form\basic;

  /**
   * Правило наследуется от правила Строка, и ведет себя как Строка в небезопасном режиме
   * В Активформ выводит блок для текста.
   */
class ArticleRule extends \drycore\sys\typeRules\StringRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/article';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function get()
    {
        $this->param['unsafe'] = true;
        parent::get();
    }
    protected function set()
    {
        $this->param['unsafe'] = true;
        parent::get();
    }
    protected function validate()
    {
        $this->param['unsafe'] = true;
        parent::get();
    }
    protected function callNeedUploader()
    {
        $this->master->value = true;
    }
}
