<?php
namespace drycore\util\form\basic;

/**
 * Расширение правила целого, для активформ
 */
class IntRule extends \drycore\sys\typeRules\IntRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/string';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
