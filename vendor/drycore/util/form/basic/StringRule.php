<?php
namespace drycore\util\form\basic;

/**
 * Расширение правила строки, для активформ
 */
class StringRule extends \drycore\sys\typeRules\StringRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/string';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = ['`'.$fieldName.'` LIKE :value', 'value'=>'%'.$value.'%'];
        }
    }
}
