<?php
namespace drycore\util\form\basic;

  /**
   * Правило для пароля.
   * Правило String для строки. Очищает от всех HTML-тегов, кодирует все сущности и т.п.
   * Параметры:
   * minlen - минимальная длина строки
   * maxlen - максимальная длина строки
   * encode - если присутствует данный параметр, то теги не удаляются а кодируются
   * unsafe - если присутствует, то не кодировать и не удалять теги, а оставлять как есть,
   *      без изменений, только (возможно) проверять размер.
   */
class PasswordRule extends \drycore\sys\typeRules\StringRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/password';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
}
