<?php
namespace drycore\util\form\basic;

/**
 * Булево поле в форме, т.е. галочка
 */
class BooleanRule extends \drycore\sys\typeRules\BooleanRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/boolean';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = '<i class="fa fa-check"></i>';
        } else {
            $this->master->value ='<i class="fa fa-square-o"></i>';
        }
    }
}
