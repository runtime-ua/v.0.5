<?php
namespace drycore\util\form\basic;

/**
 * Правило имейла для формы
 */
class EmailRule extends \drycore\sys\typeRules\EmailRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/email';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
