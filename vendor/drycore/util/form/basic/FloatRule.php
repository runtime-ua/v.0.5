<?php
namespace drycore\util\form\basic;

/**
 * Правило числа с плавающей точкой для формы
 */
class FloatRule extends \drycore\sys\typeRules\FloatRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/string';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function callSearchFormWhere()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if ($value) {
            $this->master->value = [$fieldName=>$value];
        }
    }
}
