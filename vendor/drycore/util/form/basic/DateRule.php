<?php
namespace drycore\util\form\basic;

  /**
   * Правило для дат
   */
class DateRule extends \drycore\sys\typeRules\IntRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/date';
        $config['param'] = $this->param;
        $this->master->value = $config;
    }
    protected function set()
    {
        $value = $this->master->value;
        if ($value and !is_int($value)) {
            $this->master->value = strtotime($value);
        }
    }
    
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $format = s()->locale->dateFormat;
        if ($value) {
            $this->master->value = gmdate($format, $value);
        }
    }
    
    protected function callForEdit()
    {
        $value = $this->master->value;
        $format = s()->locale->dateFormat;
        if ($value) {
            $this->master->value = gmdate($format, $value);
        }
    }
}
