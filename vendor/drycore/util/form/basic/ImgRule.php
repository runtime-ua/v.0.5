<?php
namespace drycore\util\form\basic;

  /**
   * Правило для выбора картинок в активформ
   */
class ImgRule extends \drycore\util\image\ImgRule
{
    protected function callActiveFormConfig()
    {
        $config = $this->master->value;
        $config['type'] = 'basic/img';
        $config['param'] = $this->param;
        
        $fieldName = $this->master->fieldName;
        $id = $config['varName'].'-'.$fieldName;
        s()->drycore_util_uploader_uploader->btns[$id] = $this->master->model->lang($fieldName);
        
        $this->master->value = $config;
    }
    protected function callNeedUploader()
    {
        $this->master->value = true;
    }
}
