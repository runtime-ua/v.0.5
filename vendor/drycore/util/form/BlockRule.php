<?php
namespace drycore\util\form;

/**
 * Вернем значение параметра для работы блоков
 * @author Mendel
 */
class BlockRule extends \proto\Rule
{
    /**
     * Возвращаем значение параметра. Нужно для разделения формы на блоки
     */
    public function callBlock()
    {
        $this->master->value = $this->param['name'];
    }
}
