<?php
namespace drycore\util;

/**
 * Description of importHelper
 *
 * @author Maks
 */
class Import extends \proto\Service
{
    /**
     * Импортируем содержимое json-файла и возвращаем массив
     * @param string $filename
     * @return array
     */
    public function json($filename)
    {
        return loadJson($filename);
    }
    
    /**
     * Читаем CSV-файл, возвращаем массив
     * @param type $filename
     * @param type $delimiter
     * @return boolean
     */
    public function readCsv($filename, $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }
        //
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 0, $delimiter)) !== false) {
                $data[] = $row;
            }
            fclose($handle);
        }
        return $data;
    }

    /**
     * Читает файл CSV, разбирает его в массив, где каждая строка это ассоциативный
     *  массив в соответствии с массивом полей
     * В качестве ключей наших данных используется md5 от входных данных и самого файла,
     * что позволяет избежать дублей файлов при дальнейшем мержинге и обработке (если нужно).
     * @param string $filename имя файла
     * @param array $fields массив с ключами выходного ассоциативного массива.
     *  Ключ это номер поля в CSV, значение - ключ результата
     * @param boolean $deleteFirst Признак того надо ли удалять первую строку (обычно описания полей)
     * @param string $delimiter разделитель в CSV
     * @return array
     */
    public function csv($filename, $fields, $deleteFirst = true, $delimiter = ',')
    {
        $data = $this->readCsv($filename, $delimiter);
        $fileHash = md5_file($filename);
        if ($data and $deleteFirst) {
            unset($data[0]); // Удалим первую колонку
        }
        $out = [];
        foreach ($data as $lineKey => $line) {
            $row = [];
            foreach ($fields as $key => $value) {
                $row[$value] = $line[$key];
            }
            $id = md5(json_encode($line).$fileHash.$lineKey);
            $out[$id] = $row;
        }
        return $out;
    }
}
