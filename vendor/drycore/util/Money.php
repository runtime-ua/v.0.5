<?php
namespace drycore\util;

class Money extends \proto\Service
{
    protected $rate = 1;
    protected $digits = 2;
    protected $point = '.';
    protected $thousand = ' ';
    protected $prefix = '$';
    protected $suffix = '';
    protected $isoCurrency = 'USD';
    public function go($money, $nomoney = false, $needSeo = false)
    {
        if ($money) {
            $money = $this->prepare($money);
            $result = $this->prefix.$money.$this->suffix;
        } else {
            $result = $nomoney;
        }
        if ($money and $needSeo) {
            $result = $result . '<meta itemprop="price" content="'.$money.'">';
            $result = $result . '<meta itemprop="priceCurrency" content="'.$this->isoCurrency.'">';
        }
        if (!$money and $needSeo) {
            $result = $result . '<meta itemprop="price" content="0">';
            $result = $result . '<meta itemprop="priceCurrency" content="'.$this->isoCurrency.'">';
            $result = $result . '    <link itemprop="availability" href="http://schema.org/OutStock">';
        }
        //
        return $result;
    }
    public function prepare($money)
    {
        $money = $this->rate * $money;
        $digits = $this->digits;
        $money = round($money, $digits);
        if ($digits < 0) {
            $digits = 0;
        }
        return number_format($money, $digits, $this->point, $this->thousand);
    }
}
