<?php
namespace drycore\util\uploader;

/**
 * Контроллер обрабтывающий аяксзапросы от аплоадеров
 */
class AdminUploaderController extends \proto\WebController
{
    // бокс в котором ищем/добавляем страницы при инсталяции
    protected $installPageBox = 'adminPage';
    //
    protected $serverFolder = SERVER_UPLOADER_FOLDER;
    protected $siteFolder = SITE_UPLOADER_FOLDER;
    public function __construct($config = null)
    {
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
    
    public function defaultAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        // Проверим что есть параметр папки и не содержит слешей
        $folder = $this->param['folder'];
        if (!$folder) {
            throw new \proto\ForbiddenException();
        }
        if (haveStr($folder, '/') or haveStr($folder, '\\')) {
            throw new \proto\ForbiddenException();
        }
        //
        $serverFolder = $this->serverFolder. '/' . $folder. '/';
        $siteFolder = $this->siteFolder. '/' . $folder. '/';
        $selfUrl =  urlXsrf([$this->selfType(),'folder'=>$folder], true);
        //
        s()->drycore_util_uploader_uploader->config($serverFolder, $siteFolder, $selfUrl);
    }
}
