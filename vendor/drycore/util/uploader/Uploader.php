<?php
namespace drycore\util\uploader;

/*
 * jQuery File Upload Plugin PHP Class
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */


class Uploader extends \proto\Service
{
    public $btns = [];
    protected $mkdirMode = 0755; // Права на создаваемые каталоги (если нет каталога куда надо сохранить)
    
    protected $options = [
        'param_name' => 'files',
        // Set the following option to 'POST', if your server does not support
        // DELETE requests. This is a parameter sent to the client:
        'delete_type' => 'DELETE',
        'access_control_allow_origin' => '*',
        'access_control_allow_credentials' => false,
        'access_control_allow_methods' => ['OPTIONS','HEAD','GET','POST','PUT','PATCH','DELETE'],
        'access_control_allow_headers' =>['Content-Type','Content-Range','Content-Disposition'],
        // Defines which files can be displayed inline when downloaded:
        'inline_file_types' => '/\.(gif|jpe?g|png)$/i',
        // Defines which files (based on their names) are accepted for upload:
        'accept_file_types' => '/.+$/i',
        // The php.ini settings upload_max_filesize and post_max_size
        // take precedence over the following max_file_size setting:
        'max_file_size' => null,
        'min_file_size' => 1,
        // Set the following option to false to enable resumable uploads:
        'discard_aborted_uploads' => true,
        'print_response' => true
    ];

    // PHP File Upload error message codes:
    // http://php.net/manual/en/features.file-upload.errors.php
    protected $errorMessages = [
        1 => 'Загружаемый файл больше чем разрешено upload_max_filesize в php.ini',
        2 => 'Загружаемый файл больше MAX_FILE_SIZE в форме',
        3 => 'Файл поврежден',
        4 => 'Нет загружаемого файла',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk',
        8 => 'A PHP extension stopped the file upload',
        'post_max_size' => 'Загружаемый файл больше чем разрешено upload_max_filesize в php.ini',
        'max_file_size' => 'Файл слишком большой',
        'min_file_size' => 'Файл слишком маленький',
        'accept_file_types' => 'Неразрешенный тип файла',
        'abort' => 'Загрузка прервана'
    ];
    

    public function __construct()
    {
        $this->response = [];
        // By default, allow redirects to the referer protocol+host:
        $this->options['redirect_allow_target'] = '/^'.preg_quote(
            parse_url($this->getServerVar('HTTP_REFERER'), PHP_URL_SCHEME)
            .'://'.parse_url($this->getServerVar('HTTP_REFERER'), PHP_URL_HOST)
            .'/', // Trailing slash to not match subdomains by mistake
            '/' // preg_quote delimiter param
        ).'/';
    }
    
    public function config($upload_dir, $upload_url, $script_url)
    {
        $this->options['upload_dir'] = $upload_dir;
        $this->options['upload_url'] = $upload_url;
        $this->options['script_url'] = $script_url;
        $this->initialize();
    }

    protected function initialize()
    {
        switch ($this->getServerVar('REQUEST_METHOD')) {
            case 'OPTIONS':
            case 'HEAD':
                $this->head();
                break;
            case 'GET':
                $this->get($this->options['print_response']);
                break;
            case 'PATCH':
            case 'PUT':
            case 'POST':
                $this->post($this->options['print_response']);
                break;
            case 'DELETE':
                $this->delete($this->options['print_response']);
                break;
            default:
                $this->header('HTTP/1.1 405 Method Not Allowed');
        }
    }

    protected function getUploadPath($file_name = null)
    {
        $file_name = $file_name ? $file_name : '';
        return $this->options['upload_dir'].$file_name;
    }

    protected function getQuerySeparator($url)
    {
        return strpos($url, '?') === false ? '?' : '&';
    }

    protected function getDownloadUrl($file_name)
    {
        return $this->options['upload_url'].rawurlencode($file_name);
    }
    
    // Путь файла на сервере в путь к файлу на сайте (урл САЙТА)
    public function path2Url($filePath)
    {
        if (haveStr($filePath, SERVER_UPLOADER_FOLDER)) {
            list(,$shortFileName) = explode(SERVER_UPLOADER_FOLDER, $filePath, 2);
            return SITE_UPLOADER_FOLDER . $shortFileName;
        } else {
            list(,$shortFileName) = explode(HOME, $filePath, 2);
            return $shortFileName;
        }
    }
    
    // Из ссылки на файл в путь к файлу на сервере
    public function url2Path($url)
    {
        if (haveStr($url, SITE_UPLOADER_FOLDER)) {
            list(,$shortFileName) = explode(SITE_UPLOADER_FOLDER, $url, 2);
            return SERVER_UPLOADER_FOLDER . $shortFileName;
        } else {
            return HOME.$url;
        }
    }

    protected function getErrorMessage($error)
    {
        return isset($this->errorMessages[$error]) ?
            $this->errorMessages[$error] : $error;
    }

    protected function getConfigBytes($val)
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        switch ($last) {
            case 'g':
                $val *= 1024;
                break;
            case 'm':
                $val *= 1024;
                break;
            case 'k':
                $val *= 1024;
                break;
        }
        return $val;
    }

    protected function validate($uploaded_file, $file, $error, $index)
    {
        if ($error) {
            $file->error = $this->getErrorMessage($error);
            return false;
        }
        $content_length = (int)$this->getServerVar('CONTENT_LENGTH');
        $post_max_size = $this->getConfigBytes(ini_get('post_max_size'));
        if ($post_max_size && ($content_length > $post_max_size)) {
            $file->error = $this->getErrorMessage('post_max_size');
            return false;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            $file->error = $this->getErrorMessage('accept_file_types');
            return false;
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = $this->getFileSize($uploaded_file);
        } else {
            $file_size = $content_length;
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
            ) {
            $file->error = $this->getErrorMessage('max_file_size');
            return false;
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            $file->error = $this->getErrorMessage('min_file_size');
            return false;
        }
        return true;
    }

    protected function upcountNameCallback($matches)
    {
        $index = isset($matches[1]) ? ((int)$matches[1]) + 1 : 1;
        $ext = isset($matches[2]) ? $matches[2] : '';
        return ' ('.$index.')'.$ext;
    }

    protected function upcountName($name)
    {
        return preg_replace_callback(
            '/(?:(?: \(([\d]+)\))?(\.[^.]+))?$/',
            array($this, 'upcountNameCallback'),
            $name,
            1
        );
    }

    protected function handleFileUpload($uploaded_file, $name, $size, $type, $error, $index, $content_range)
    {
        $file = new \stdClass();
        $file->name = $this->getFileName(
            $uploaded_file,
            $name,
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
        $file->size = (int) $size;
        $file->type = $type;
        if ($this->validate($uploaded_file, $file, $error, $index)) {
            $upload_dir = $this->getUploadPath();
            if (!is_dir($upload_dir)) {
                mkdir($upload_dir, $this->mkdirMode, true);
            }
            $file_path = $this->getUploadPath($file->name);
            $append_file = $content_range && is_file($file_path) &&
                $file->size > $this->getFileSize($file_path);
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }
            $file_size = $this->getFileSize($file_path, $append_file);
            if ($file_size === $file->size) {
                $file->url = $this->getDownloadUrl($file->name);
                $versions = s()->drycore_util_image_convertor->tryCreateVersions($file_path);
                foreach ($versions as $version => $versionUrl) {
                    $file->{$version.'Url'} = $versionUrl;
                }
                $file->size = $this->getFileSize($file_path, true);
            } else {
                $file->size = $file_size;
                if (!$content_range && $this->options['discard_aborted_uploads']) {
                    unlink($file_path);
                    $file->error = $this->getErrorMessage('abort');
                }
            }
            $this->setAdditionalFileProperties($file);
        }
        return $file;
    }

    protected function header($str)
    {
        header($str);
    }

    protected function getUploadData($id)
    {
        if (!empty($_FILES[$id])) {
            return $_FILES[$id];
        }
    }

    protected function getPostParam($id)
    {
        if (!empty($_POST[$id])) {
            return $_POST[$id];
        }
    }

    protected function getQueryParam($id)
    {
        if (!empty($_GET[$id])) {
            return $_GET[$id];
        }
    }

    protected function getServerVar($id)
    {
        if (!empty($_SERVER[$id])) {
            return $_SERVER[$id];
        }
    }

    protected function getSingularParamName()
    {
        return substr($this->options['param_name'], 0, -1);
    }

    protected function getFileNameParam()
    {
        $name = $this->getSingularParamName();
        return $this->basename(stripslashes($this->getQueryParam($name)));
    }

    protected function getFileNamesParams()
    {
        $params = $this->getQueryParam($this->options['param_name']);
        if (!$params) {
            return null;
        }
        foreach ($params as $key => $value) {
            $params[$key] = $this->basename(stripslashes($value));
        }
        return $params;
    }

//    protected function getFileType($file_path) {
//        switch (strtolower(pathinfo($file_path, PATHINFO_EXTENSION))) {
//            case 'jpeg':
//            case 'jpg':
//                return 'image/jpeg';
//            case 'png':
//                return 'image/png';
//            case 'gif':
//                return 'image/gif';
//            default:
//                return '';
//        }
//    }

    protected function sendContentTypeHeader()
    {
        $this->header('Vary: Accept');
        if (strpos($this->getServerVar('HTTP_ACCEPT'), 'application/json') !== false) {
            $this->header('Content-type: application/json');
        } else {
            $this->header('Content-type: text/plain');
        }
    }

    protected function sendAccessControlHeaders()
    {
        $this->header('Access-Control-Allow-Origin: '.$this->options['access_control_allow_origin']);
        $this->header('Access-Control-Allow-Credentials: '
            .($this->options['access_control_allow_credentials'] ? 'true' : 'false'));
        $this->header('Access-Control-Allow-Methods: '
            .implode(', ', $this->options['access_control_allow_methods']));
        $this->header('Access-Control-Allow-Headers: '
            .implode(', ', $this->options['access_control_allow_headers']));
    }

    public function generateResponse($content, $print_response = true)
    {
        $this->response = $content;
        if ($print_response) {
            $json = json_encode($content);
            $redirect = stripslashes($this->getPostParam('redirect'));
            if ($redirect && preg_match($this->options['redirect_allow_target'], $redirect)) {
                $this->header('Location: '.sprintf($redirect, rawurlencode($json)));
                return;
            }
            $this->head();
            if ($this->getServerVar('HTTP_CONTENT_RANGE')) {
                $files = isset($content[$this->options['param_name']]) ?
                    $content[$this->options['param_name']] : null;
                if ($files && is_array($files) && is_object($files[0]) && $files[0]->size) {
                    $this->header('Range: 0-'.(
                        ((int)$files[0]->size) - 1
                    ));
                }
            }
            echo $json;
        }
        return $content;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function head()
    {
        $this->header('Pragma: no-cache');
        $this->header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->header('Content-Disposition: inline; filename="files.json"');
        // Prevent Internet Explorer from MIME-sniffing the content-type:
        $this->header('X-Content-Type-Options: nosniff');
        if ($this->options['access_control_allow_origin']) {
            $this->sendAccessControlHeaders();
        }
        $this->sendContentTypeHeader();
    }

    public function get($print_response = true)
    {
        $file_name = $this->getFileNameParam();
        if ($file_name) {
            $response = array(
                $this->getSingularParamName() => $this->getFileObject($file_name)
            );
        } else {
            $response = array(
                $this->options['param_name'] => $this->getFileObjects()
            );
        }
        return $this->generateResponse($response, $print_response);
    }

    public function post($print_response = true)
    {
        if ($this->getQueryParam('_method') === 'DELETE') {
            return $this->delete($print_response);
        }
        $upload = $this->getUploadData($this->options['param_name']);
        // Parse the Content-Disposition header, if available:
        $content_disposition_header = $this->getServerVar('HTTP_CONTENT_DISPOSITION');
        $file_name = $content_disposition_header ?
            rawurldecode(preg_replace(
                '/(^[^"]+")|("$)/',
                '',
                $content_disposition_header
            )) : null;
        // Parse the Content-Range header, which has the following form:
        // Content-Range: bytes 0-524287/2000000
        $content_range_header = $this->getServerVar('HTTP_CONTENT_RANGE');
        $content_range = $content_range_header ?
            preg_split('/[^0-9]+/', $content_range_header) : null;
        $size =  $content_range ? $content_range[3] : null;
        $files = array();
        if ($upload) {
            if (is_array($upload['tmp_name'])) {
                // param_name is an array identifier like "files[]",
                // $upload is a multi-dimensional array:
                foreach ($upload['tmp_name'] as $index => $value) {
                    $files[] = $this->handleFileUpload(
                        $upload['tmp_name'][$index],
                        $file_name ? $file_name : $upload['name'][$index],
                        $size ? $size : $upload['size'][$index],
                        $upload['type'][$index],
                        $upload['error'][$index],
                        $index,
                        $content_range
                    );
                }
            } else {
                // param_name is a single object identifier like "file",
                // $upload is a one-dimensional array:
                $files[] = $this->handleFileUpload(
                    isset($upload['tmp_name']) ? $upload['tmp_name'] : null,
                    $file_name ? $file_name : (isset($upload['name']) ?
                            $upload['name'] : null),
                    $size ? $size : (isset($upload['size']) ?
                            $upload['size'] : $this->getServerVar('CONTENT_LENGTH')),
                    isset($upload['type']) ?
                            $upload['type'] : $this->getServerVar('CONTENT_TYPE'),
                    isset($upload['error']) ? $upload['error'] : null,
                    null,
                    $content_range
                );
            }
        }
        $response = array($this->options['param_name'] => $files);
        return $this->generateResponse($response, $print_response);
    }

    private function basename($filepath, $suffix = null)
    {
        $splited = preg_split('/\//', rtrim($filepath, '/ '));
        return substr(basename('X'.$splited[count($splited)-1], $suffix), 1);
    }
    
    protected function setAdditionalFileProperties($file)
    {
        $file->deleteUrl = $this->options['script_url']
            .$this->getQuerySeparator($this->options['script_url'])
            .$this->getSingularParamName()
            .'='.rawurlencode($file->name);
        $file->deleteType = $this->options['delete_type'];
        if ($file->deleteType !== 'DELETE') {
            $file->deleteUrl .= '&_method=DELETE';
        }
        if ($this->options['access_control_allow_credentials']) {
            $file->deleteWithCredentials = true;
        }
    }

    protected function getFileSize($file_path, $clear_stat_cache = false)
    {
        if ($clear_stat_cache) {
            clearstatcache(true, $file_path);
        }
        return filesize($file_path);
    }

    protected function isValidFileObject($file_name)
    {
        $file_path = $this->getUploadPath($file_name);
        if (is_file($file_path) && $file_name[0] !== '.') {
            return true;
        }
        return false;
    }

    public function delete($print_response = true)
    {
        $file_names = $this->getFileNamesParams();
        if (empty($file_names)) {
            $file_names = array($this->getFileNameParam());
        }
        $response = [];
        foreach ($file_names as $file_name) {
            $file_path = $this->getUploadPath($file_name);
            $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
            if ($success) {
                s()->drycore_util_image_convertor->deleteVersions($file_path);
            }
            $response[$file_name] = $success;
        }
        return $this->generateResponse($response, $print_response);
    }
    
    protected function getFileObject($file_name)
    {
        if ($this->isValidFileObject($file_name)) {
            $file = new \stdClass();
            $file->name = $file_name;
            $file_path = $this->getUploadPath($file_name);
            $file->size = $this->getFileSize($file_path);
            $file->url = $this->getDownloadUrl($file->name);
            //
            $versions = s()->drycore_util_image_convertor->getVersionsList($file_path);
            foreach ($versions as $version => $versionUrl) {
                $file->{$version.'Url'} = $versionUrl;
            }
            $this->setAdditionalFileProperties($file);
            return $file;
        }
        return null;
    }

    protected function getFileObjects($iteration_method = 'getFileObject')
    {
        $upload_dir = $this->getUploadPath();
        if (!is_dir($upload_dir)) {
            return array();
        }
        return array_values(array_filter(array_map(
            array($this, $iteration_method),
            scandir($upload_dir)
        )));
    }

//    protected function countFileObjects() {
//        return count($this->getFileObjects('isValidFileObject'));
//    }

    protected function getUniqueFilename($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        while (is_dir($this->getUploadPath($name))) {
            $name = $this->upcountName($name);
        }
        // Keep an existing filename if this is part of a chunked upload:
        $uploaded_bytes = (int) $content_range[1];
        while (is_file($this->getUploadPath($name))) {
            if ($uploaded_bytes === $this->getFileSize(
                $this->getUploadPath($name)
            )) {
                break;
            }
            $name = $this->upcountName($name);
        }
        return $name;
    }

    protected function fixFileExtension($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        // Add missing file extension for known image types:
        if (strpos($name, '.') === false &&
                preg_match('/^image\/(gif|jpe?g|png)/', $type, $matches)) {
            $name .= '.'.$matches[1];
        }
        return $name;
    }

    protected function trimFileName($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        $name = trim($this->basename(stripslashes($name)), ".\x00..\x20");
        // Use a timestamp for empty filenames:
        if (!$name) {
            $name = str_replace('.', '-', microtime(true));
        }
        return $name;
    }

    protected function getFileName($file_path, $name, $size, $type, $error, $index, $content_range)
    {
        $name = translit($name);
        $name = $this->trimFileName(
            $file_path,
            $name,
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
        return $this->getUniqueFilename(
            $file_path,
            $this->fixFileExtension(
                $file_path,
                $name,
                $size,
                $type,
                $error,
                $index,
                $content_range
            ),
            $size,
            $type,
            $error,
            $index,
            $content_range
        );
    }
}
