<a class="btn btn-danger btn-sm"
   href="<?=urlXsrf([$controllerName.':clear']);?>"
   onClick="return window.confirm('<?=$_->confirm;?>');">
    <i class="fa fa-trash"></i> <?=$_->actionName;?>
</a>
