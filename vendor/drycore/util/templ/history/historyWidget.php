<?php
$box = $box->fields(['transaction_id','controller','action','created','createdBy'])->distinct();
$pager = view(
    'drycore/util/widget/pager',
    ['box'=>$box,'param'=>'history-page','cssClass'=>'pagination pagination-sm no-margin pull-right']
);
?>
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title"><?=$_->title;?></h3>
    </div><!-- /.box-header -->
    <table class="table table-hover table-striped table-bordered">
      <tbody>
        <?php foreach ($box->findAll() as $model) : ?>
        <tr>
            <td>
                <img src="<?=$model->creator->avatar;?>"
                     class="img-rounded"
                     style="width:45px; margin-right: 5px;" align="left">
                <?=s()->locale->asDateTime($model->created);?><br>
                <b><?=$model->creator->login;?></b>
            </td>
            <td>
                <?php if (controller()->selfType() != $model->controller) :?>
                <?=s()->lang->getString('#self', 'controller/'.$model->controller);?>
                ->
                <?php endif;?>
                <?=s()->lang->getString($model->action.'#action', 'controller/'.$model->controller);?>
            </td>
<!--            
            <td>
                <a class="btn btn-default"
                    onClick="return window.confirm('<?=$_->confirm;?>');"
                    href="<?//url(['restore:restore','id'=>$model->transaction_id]);?>">
                    <i class="fa fa-undo"></i>
                </a>
            </td>
-->
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <div class="box-footer clearfix">
        <?=$pager->render();?>
    </div>
</div><!--/.direct-chat -->
