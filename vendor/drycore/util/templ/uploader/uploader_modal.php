<?php if (!isset($url)) {
    $url = urlXsrf(['drycart/main/uploader','folder'=>$folder]);
}?>

<?php asset('headCss', '//blueimp.github.io/Gallery/css/blueimp-gallery.min.css');?>
<?php asset('headCss', '/files/myupload/css/jquery.fileupload.css');?>
<?php asset('headCss', '/files/myupload/css/jquery.fileupload-ui.css');?>
<?php asset('footerJs', '/files/myupload/js/vendor/jquery.ui.widget.js');?>
<!-- The Templates plugin is included to render the upload/download listings -->
<?php asset('footerJs', '//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js');?>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<?php asset('footerJs', '//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js');?>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<?php asset('footerJs', '//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js');?>
<!-- blueimp Gallery script -->
<?php asset('footerJs', '//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js');?>
<!-- The basic File Upload plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload.js');?>
<!-- The File Upload processing plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload-process.js');?>
<!-- The File Upload image preview & resize plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload-image.js');?>
<!-- The File Upload audio preview plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload-audio.js');?>
<!-- The File Upload video preview plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload-video.js');?>
<!-- The File Upload validation plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload-validate.js');?>
<!-- The File Upload user interface plugin -->
<?php asset('footerJs', '/files/myupload/js/jquery.fileupload-ui.js');?>

<?php assetStart();?>
<script>
function imgSet(name, file) {
    $('#thumb_'+name).attr('src',file);
    $('#'+name).val(file);
    $('#uploadModal').modal('hide');
    return false;
}    

function imgReset(name, placeholder) {
    $('#thumb_'+name).attr('src',placeholder);
    $('#'+name).val('');
    return false;
}

function addPic(file) {
    editor = CKEDITOR.currentInstance;
    if(editor !== undefined) {
        if ( editor.mode == 'wysiwyg' ) {
            editor.insertHtml('<img class="img-responsive" src="'+file+'">');
        }
        else alert( 'В режиме кода вставка недоступна');
    } else {
        alert('Установите курсор в редакторе на то место, куда вы хотите добавить картинку');
    }
    return false;
}

document.addEventListener('DOMContentLoaded', function () {
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '<?=urlXsrf(['adminUploader','folder'=>$folder]);?>'
    });
    // Load existing files:
    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
        $(this).fileupload('option', 'done')
            .call(this, $.Event('done'), {result: result});
    });
});
</script>
<?php assetEnd('footerScript');?>

<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="UploadModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="UploadModalLabel"><?=$_->title;?></h4>
      </div>
      <div class="modal-body">
        <!-- The file upload form used as target for the file upload widget -->
        <form id="fileupload" method="POST" enctype="multipart/form-data">
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="row fileupload-buttonbar">
                <div class="col-lg-9">
                    <!-- The fileinput-button span is used to style the file input field as button -->
                    <span class="btn btn-success fileinput-button">
                        <i class="fa fa-plus"></i>
                        <span><?=$_->addBtn;?></span>
                        <input type="file" name="files[]" multiple>
                    </span>
                    <button type="submit" class="btn btn-primary start">
                        <i class="fa fa-upload"></i>
                        <span><?=$_->startAllBtn;?></span>
                    </button>
                    <button type="reset" class="btn btn-warning cancel">
                        <i class="fa fa-ban"></i>
                        <span><?=$_->cancelAllBtn;?></span>
                    </button>
                    <button type="button" class="btn btn-danger delete">
                        <i class="fa fa-trash"></i>
                        <span><?=$_->delSelectedBtn;?></span>
                    </button>
                    <input type="checkbox" class="toggle">
                    <!-- The global file processing state -->
                    <span class="fileupload-process"></span>
                </div>
                <!-- The global progress state -->
                <div class="col-lg-3 fileupload-progress fade">
                    <!-- The global progress bar -->
                    <div class="progress progress-striped active"
                         role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                    <!-- The extended global progress state -->
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            <!-- The table listing the files available for upload/download -->
            <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- The blueimp Gallery widget -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0"
                aria-valuemax="100" aria-valuenow="0">
                <div class="progress-bar progress-bar-success" style="width:0%;"></div>
            </div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="fa fa-upload"></i>
                    <span><?=$_->uploadBtn;?></span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="fa fa-ban"></i>
                    <span><?=$_->cancelBtn;?></span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>
                         <img src="{%=file.thumbnailUrl%}">
                    </a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}"
                        download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>
                        {%=file.name%}
                    </a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <input type="checkbox" name="delete" value="1" class="toggle">
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}"
                    data-url="{%=file.deleteUrl%}"
                    {% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="fa fa-trash"></i>
                </button>
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="fa fa-ban"></i>
                    <span>Отменить</span>
                </button>
            {% } %}
            {% if (file.url) { %}
<?php $fields = s()->drycore_util_uploader_uploader->btns;?>
<?php if ($fields) :
    ?>
<?php foreach ($fields as $name => $desc) :
    ?>
                <button class="btn btn-default" onclick="imgSet('<?=$name?>','{%=file.url%}');return false;">
                    <span><?=$desc;?></span>
                </button>
<?php
endforeach; ?>
<?php
endif;?>
<?php if (isset($addEditor) and $addEditor) :
?>
                <button class="btn btn-default" onclick="addPic('{%=file.url%}');return false;">
                    <span>Добавить в редактор</span>
                </button>
<?php
endif;?>
<!--             
                <input value="{%=file.url%}"  style="width:300px;font-size:10px;">
-->
            {% } %}
        </td>
    </tr>
{% } %}
</script>
