<?php if (!isset($active)) {
    $active = false;
}?>
<div class="btn-group" role="group">
<?php foreach ($links as $key => $line) :
    ?>
    <?php if ($key!==$active) :
    ?>
    <a href="<?=$line['url'];?>" class="btn btn-default"><?=$line['title'];?></a>
    <?php
    endif;?>
    <?php if ($key===$active) :
?>
    <a href="<?=$line['url'];?>" class="btn btn-primary"><?=$line['title'];?></a>
    <?php
    endif;?>
<?php
endforeach;?>
</div>
