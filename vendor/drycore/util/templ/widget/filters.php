<ul class="nav nav-pills nav-stacked">
    <?php foreach ($filters as $key => $line) :?>
    <?php if (isset($line['active']) and $line['active']) :?>
    <li class="active">
        <a href="<?=$line['url'];?>">
            <i class="<?=$line['ico'];?>"></i>
            <?=$this->box->lang($key.'_filter');?>
            <?php if ($line['count']) :?>
            <span class="label label-info pull-right"><?=$line['count'];?></span>
            <?php endif;?>
        </a>
    </li>
    <?php endif;?>
    <?php if (!isset($line['active']) or !$line['active']) :?>
    <li>
        <a href="<?=$line['url'];?>">
            <i class="<?=$line['ico'];?>"></i>
            <?=$this->box->lang($key.'_filter');?>
            <?php if ($line['count']) :?>
            <span class="label label-default pull-right"><?=$line['count'];?></span>
            <?php endif;?>
        </a>
    </li>
    <?php endif;?>
    <?php endforeach;?>
</ul>
