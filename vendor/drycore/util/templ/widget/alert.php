<?php foreach ($alerts as $alert) : ?>
<div class="alert alert-<?=$alert['type'];?> alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <?=$alert['content'];?>
</div>
<?php endforeach;?>
