<form class="form-inline">
    <div class="input-group">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default btn-sm"><?=$_->orderBy;?></button>
        </span>
        <select class="form-control input-sm" name="order">
            <option value="" <?=showIf((null==$order), 'selected');?>>
                <?=$_->default;?>
            </option>
        <?php
        foreach ($box->orderList() as $orderName) :?>
            <option value="<?=$orderName?>" <?=showIf(($orderName==$order), 'selected');?>>
                <?=$box->lang($orderName);?> A-Z &uarr;
            </option>
            <option value="!<?=$orderName?>" <?=showIf(('!'.$orderName==$order), 'selected');?>>
                <?=$box->lang($orderName);?> Z-A &darr;
            </option>
        <?php endforeach; ?>
        </select>
    </div>
</form>                    
