<?php if ($alerts) : ?>
    <?php asset('footerJs', 'https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js');?>
    <?php assetStart();?>
    <script>
        var alertData = [];
        <?php foreach ($alerts as $alert) : ?>
        alertData.push('<?=$alert['content'];?>');
        <?php endforeach;?>
        (function($) {
            alertData.forEach(function(item, i, arr) {
                bootbox.alert(item);
            });
        })(jQuery); // End of use strict            
    </script>
    <?php assetEnd('footerScript');?>
<?php endif;?>



