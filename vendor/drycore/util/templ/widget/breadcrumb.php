<?php if ($show) : ?>
<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="/" class="no-link" itemscope itemtype="http://schema.org/Thing" itemprop="item">
            <i class="fa fa-home"></i> 
        </a>
    </li>
    <?php foreach ($parents as $line) : ?>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <a href="<?=$line->pageurl;?>" class="no-link" itemscope itemtype="http://schema.org/Thing" itemprop="item">
            <span itemprop="name"><?=$line->breadtitle;?></span>
        </a>
    </li>
    <?php endforeach;?>
    <li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
        <span itemprop="name"><?=$currentTitle;?></span>
    </li>
</ol>
<?php endif;?>
