<table class="table table-bordered">
    <thead>
        <tr>
        <?php foreach ($columns as $col) : ?>
            <th>
                <?=$box->lang($col);?>
                <?=view('drycore/util/widget/help', ['text'=>$box->lang($col.'#help')]);?>
            </th>
        <?php endforeach;?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($box->findAll() as $model) : ?>
        <tr>
        <?php foreach ($columns as $col) : ?>
            <td><?=$model->$col;?></td>
        <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
