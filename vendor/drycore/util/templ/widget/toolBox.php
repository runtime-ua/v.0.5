<?php foreach ($actions as $block) : ?>
    <div class="btn-group">
        <?php foreach ($block as $action) : ?>
            <?=view($action.'Action', $actionsData);?>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>
