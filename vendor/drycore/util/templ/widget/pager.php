<!-- pagination start -->
<nav>
    <ul class="<?=$cssClass;?>">
        <?php if (isset($previous)) : ?>
        <li><a href="<?=$previous;?>" aria-label="Previous">
            <i class="fa fa-arrow-left"></i>
        </a></li>
        <?php endif;?>
        <?php foreach ($pages as $page) : ?>
        <li <?=showIf($page['active'], 'class="active"');?>><a href="<?=$page['url'];?>"><?=$page['id'];?></a></li>
        <?php endforeach;?>
        <?php if (isset($next)) : ?>
        <li><a href="<?=$next;?>" aria-label="Next">
            <i class="fa fa-arrow-right"></i>
        </a></li>
        <?php endif;?>
    </ul>
</nav>
<!-- pagination end -->
