<?php
$columns = [];
$data = [];
foreach ($result as $line) {
    $data[] = $line;
    foreach ($line as $key => $value) {
        $columns[$key] = $key;
    }
}
// Скажем если нифига нет
if (!$data) {
    echo $_->none;
}
?>
<table class="table table-bordered">
    <thead>
        <tr>
        <?php foreach ($columns as $col) : ?>
            <th><?=$col;?></th>
        <?php endforeach;?>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $line) : ?>
        <tr>
        <?php foreach ($line as $cell) : ?>
            <td><?=$cell;?></td>
        <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
