<?php
$controllerName = controller()->selfType();
if (!isset($defaultAction)) {
    $defaultAction = 'view';
}
?>
<table class="table table-bordered table-hover table-striped">
    <tbody>
        <tr>
        <?php foreach ($columns as $col) : ?>
            <th>
                <?=$box->lang($col);?>
                <?=view('drycore/util/widget/help', ['text'=>$box->lang($col.'#help')]);?>
            </th>
        <?php endforeach;?>
            <th>
            <?php if ($topActions) : ?>
            <?php foreach ($topActions as $block) : ?>
                <div class="btn-group">
                    <?php foreach ($block as $action) : ?>
                        <?=view($action.'Action', ['controllerName'=>$controllerName]);?>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
            <?php endif; ?>
            <?php if (!$topActions) : ?>
                <?=$_->actions;?>
            <?php endif; ?>
            </th>
        </tr>
    <?php foreach ($box->findAll() as $model) : ?>
        <tr>
        <?php foreach ($columns as $col) : ?>
            <td>
                <a href="<?=url([$controllerName.':'.$defaultAction,'id'=>$model->id]);?>" style="color: black;">
                    <?=$model->pretty($col);?>
                </a>
            </td>
        <?php endforeach; ?>
            <td>
            <?php if (isset($actions)) : ?>
            <?php foreach ($actions as $block) : ?>
                <div class="btn-group">
                    <?php foreach ($block as $action) : ?>
                        <?=view($action.'Action', ['model'=>$model,'controllerName'=>$controllerName]);?>
                    <?php endforeach; ?>
                </div>
                <br>
            <?php endforeach; ?>
            <?php endif;?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>