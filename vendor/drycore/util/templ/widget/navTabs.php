<?php
if (!isset($active)) {
    $active = key($tabs);
}
if (isset($id)) {
    $id = $id.'_';
} else {
    $id = '';
}
?>
<ul class="nav nav-tabs hidden-print">
    <?php foreach ($tabs as $key => $line) :?>
    <li<?= showIf(($key==$active), ' class="active"');?>>
        <a href="#<?=$id.$key;?>" data-toggle="tab">
            <?=$line['title'];?>
        </a>
    </li>
    <?php endforeach;?>
    <?php if (isset($actions) and $actions) : ?>
    <li class="pull-right">
        <?=view('drycore/util/widget/toolBox', ['actions'=>$actions,'actionsData'=>$actionsData]);?>
    </li>
    <?php endif; ?>
</ul>
<div class="tab-content">
    <?php foreach ($tabs as $key => $line) :?>
    <div class="tab-pane<?= showIf(($key==$active), ' active');?>" id="<?=$id.$key;?>">
        <?=$line['content'];?>
    </div>
    <?php endforeach;?>
</div>
