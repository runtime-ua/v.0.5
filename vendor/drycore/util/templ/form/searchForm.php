<?php if (!isset($colapsed)) {
    $colapsed = false;
}?>
<button class="btn btn-default btn-sm" type="button" data-toggle="collapse"
    data-target="#searchForm" aria-expanded="false" aria-controls="searchForm">
        <i class="fa fa-search"></i>
</button>    
<div class="collapse <?=showIf($filtered and !$colapsed, 'in') ?>" id="searchForm">
    <div class="well">
        <form role="form" method="get" class="form-horizontal">
            <?=view('drycore/util/form/form', ['model'=>$model,'varName'=>'search']);?>
            <div class="row">
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                        <i class="fa fa-search"></i>
                        <?=$_->search;?>
                    </button>
                </div>
                <div class="col-sm-3">
                    <a class="btn btn-danger btn-lg btn-block" href="<?=$cancelUrl;?>">
                        <i class="fa fa-ban"></i>
                        <?=$_->cancel;?>
                    </a>
                </div>
            </div>
        </form>                
    </div>
</div>                
