<?php
if (!isset($action)) {
    $action = '#';
}
?>
<form role="form"  class="form-horizontal" method="post" action="<?=url($action);?>">
    <?=s()->xsrf->field(); ?>
    <div class="nav-tabs-custom">
        <?=view('drycore/util/widget/navTabs', [
            'tabs'=>$blocks,
            'actions'=>$actions,
            'actionsData'=>['model'=>$model,'controllerName'=>controller()->selfType()]
        ]);?>
        <button class="btn btn-primary btn-block" type="submit">
            <i class="fa fa-save"></i> <?=$btnText;?>
        </button>
    </div>
</form>                
