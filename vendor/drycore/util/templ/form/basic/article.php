<?php asset('footerJs', 'https://cdn.ckeditor.com/4.4.3/full/ckeditor.js');?>
<?php assetStart();?>
<script>
$(function () {
    // https://habrahabr.ru/post/204176/ - статья по созданию плагина, в идеале для вставки картинок...
    CKEDITOR.config.toolbar =
    [
        { name: 'clipboard',   items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
        { name: 'editing',     items : [ 'Print','-','Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
//            '/',
        { name: 'basicstyles', items : [ 
                'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat'
            ] },
        { name: 'paragraph',   items : [ 
                'NumberedList','BulletedList','-',
                'Outdent','Indent','-',
                'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-',
                'BidiLtr','BidiRtl' 
            ] },
        { name: 'links',       items : [ 'Link','Unlink','Anchor' ] },
        { name: 'insert',      items : [ 'Table','HorizontalRule','SpecialChar','PageBreak',,'Blockquote'] },
//            '/',
        { name: 'styles',      items : [ 'Styles','Format','Font','FontSize' ] },
        { name: 'colors',      items : [ 'TextColor','BGColor' ] },
        { name: 'tools',       items : [ 'Maximize', 'ShowBlocks','-','Source' ] }
    ];
    CKEDITOR.config.allowedContent = true;
    // Replace all <textarea class="articleEditor"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replaceAll('articleEditor');
});
</script>
<?php assetEnd('footerScript');?>

<div class="form-group <?=showIf($error, 'has-warning');?>">
    <div class="col-sm-12">
        <label for="<?=$id;?>" class=" control-label">
            <?=$label;?><?=showIf($required, ' *');?>
            <?=view('drycore/util/widget/help', ['text'=>$help]);?>
        </label>
<!-- Версия для модального аплоадера
<button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#uploadModal">
    <i class="fa fa-image"></i>
</button>
-->
        <textarea class="form-control articleEditor"
            rows="9"
            id="<?=$id;?>"
            name="<?=$varName;?>[<?=$colName;?>]"
            placeholder="<?=$placeholder;?>"
            <?=showIf($required, 'required');?>><?=$value;?></textarea>
    <?php if ($error) : ?>
    <span class="help-block  has-warning"><?=$error;?></span>
    <?php endif; ?>
    </div>
</div>
