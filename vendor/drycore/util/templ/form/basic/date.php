<?php asset('headCss', '/files/datepicker/css/bootstrap-datepicker3.min.css');?>
<?php asset('footerJs', '/files/datepicker/js/bootstrap-datepicker.min.js');?>
<?php asset('footerJs', '/files/datepicker/js/locales/bootstrap-datepicker.'.s()->locale->shortLang.'.min.js');?>
<?php assetStart();?>
<script>
$(function () {
    $('.date').datepicker({
        autoclose: true,
        todayBtn: "linked",
        language: "<?=s()->locale->shortLang;?>",
        format: "<?=s()->locale->dateFormatPretty;?>"
    });
});
</script>
<?php assetEnd('footerScript');?>




<?php
if (!isset($style['labelSize'])) {
    $style['labelSize'] = 3;
}
$mainSize = 12 - $style['labelSize'];
//
if (isset($style['placeholder'])) {
    $placeholder = $style['placeholder'];
} else {
    $placeholder = $model->lang($colName);
}
//
if (!isset($style['beforeIco'])) {
    $style['beforeIco'] = 'fa fa-fw fa-calendar';
}
?>
<div class="form-group <?=showIf($error, 'has-warning');?>">
    <label for="<?=$id;?>" class="col-sm-<?=$style['labelSize'];?> control-label">
        <?=$label;?><?=showIf($required, ' *');?>
            <?=view('drycore/util/widget/help', ['text'=>$help]);?>
    </label>
    <div class="col-sm-<?=$mainSize;?>">
        <div class="input-group date">
            <?php if (isset($style['beforeIco']) or isset($style['beforeTxt'])) :?>
            <div class="input-group-addon">
                <?=showIf(isset($style['beforeIco']), '<i class="'.$style['beforeIco'].'"></i>');?>
                <?=showIf(isset($style['beforeTxt']), $style['beforeTxt']);?>
            </div>
            <?php endif;?>
            <input type="text" class="form-control <?=showIf(isset($style['inputClass']), $style['inputClass']);?>"
               id="<?=$id;?>"
               name="<?=$varName;?>[<?=$colName;?>]"
                autocomplete="off"
               placeholder="<?=$placeholder;?>"
               value="<?=$value;?>"
                <?=showIf($required, 'required');?>>
            <?php if (isset($style['afterIco']) or isset($style['afterTxt'])) :?>
            <div class="input-group-addon">
                <?=showIf(isset($style['afterIco']), '<i class="'.$style['afterIco'].'"></i>');?>
                <?=showIf(isset($style['afterTxt']), $style['afterTxt']);?>
            </div>
            <?php endif;?>
        </div>
    <?php if ($error) : ?>
    <span class="help-block has-warning"><?=$error;?></span>
    <?php endif; ?>
    </div>
</div>
