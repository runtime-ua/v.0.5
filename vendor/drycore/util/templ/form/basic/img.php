<?php
if (!$value) {
    $value = '/files/img/rt-logo.png';
}
?>
<div class="form-group <?=showIf($error, 'has-warning');?>">
    <label for="<?=$id;?>" class="col-sm-3 control-label">
        <?=$label;?><?=showIf($required, ' *');?>
            <?=view('drycore/util/widget/help', ['text'=>$help]);?>
    </label>
    <div class="col-sm-1">
        <button class="btn btn-default" onclick="imgReset('<?=$id;?>','/files/img/rt-logo.png');return false;">
            <i class="fa fa-trash"></i>
        </button>
    </div>
    <div class="col-sm-8">
        <input type="hidden" class="form-control"
           id="<?=$id;?>"
           name="<?=$varName;?>[<?=$colName;?>]"
           placeholder="<?=$placeholder;?>"
           value="<?=$value;?>"
            <?=showIf($required, 'required');?>
           style="font-size:10px;">
            <img src="<?=$value;?>" id="thumb_<?=$id;?>" style="width: 300px;height: 200px;">
<!-- Версия для модального аплоадера
        <a href="#" class="no-link" data-toggle="modal" data-target="#uploadModal">
            <img src="<?=$value;?>" id="thumb_<?=$id;?>" style="width: 300px;height: 200px;">
        </a>
-->
        <br>
    <?php if ($error) : ?>
    <span class="help-block  has-warning"><?=$error;?></span>
    <?php endif; ?>
    </div>
</div>
