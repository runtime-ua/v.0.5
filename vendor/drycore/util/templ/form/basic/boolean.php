<div class="form-group <?=showIf($error, 'has-warning');?>">
    <div class="checkbox col-sm-offset-3 col-sm-9">
    <label>
        <input type="hidden"
            id="<?=$id;?>_hidden"
            name="<?=$varName;?>[<?=$colName;?>]"
            value="0">
        
        <input type="checkbox"
            id="<?=$id;?>"
            name="<?=$varName;?>[<?=$colName;?>]"
            <?=showIf($required, 'required');?>
            <?=showIf($value, 'checked');?>>
        <?=showIf($required, '* ');?><?=$label;?>
            <?=view('drycore/util/widget/help', ['text'=>$help]);?>
    </label>
    <?php if ($error) : ?>
    <span class="help-block  has-warning"><?=$error;?></span>
    <?php endif; ?>
    </div>
</div>
