<?php
if (!isset($style['labelSize'])) {
    $style['labelSize'] = 3;
}
$mainSize = 12 - $style['labelSize'];
//
if (isset($style['placeholder'])) {
    $placeholder = $style['placeholder'];
} else {
    $placeholder = $model->lang($colName);
}
//
$needGroup =
    (
        isset($style['beforeIco'])
        or isset($style['afterIco'])
        or isset($style['beforeTxt'])
        or isset($style['afterTxt'])
    );
?>
<div class="form-group <?=showIf($error, 'has-warning');?>">
    <label for="<?=$id;?>" class="col-sm-<?=$style['labelSize'];?> control-label">
        <?=$label;?><?=showIf($required, ' *');?>
            <?=view('drycore/util/widget/help', ['text'=>$help]);?>
    </label>
    <div class="col-sm-<?=$mainSize;?>">
        <?=showIf($needGroup, '<div class="input-group">');?>
            <?php if (isset($style['beforeIco']) or isset($style['beforeTxt'])) :?>
            <div class="input-group-addon">
                <?=showIf(isset($style['beforeIco']), '<i class="'.$style['beforeIco'].'"></i>');?>
                <?=showIf(isset($style['beforeTxt']), $style['beforeTxt']);?>
            </div>
            <?php endif;?>
            <input type="password" class="form-control <?=showIf(isset($style['inputClass']), $style['inputClass']);?>"
               id="<?=$id;?>"
               name="<?=$varName;?>[<?=$colName;?>]"
               placeholder="<?=$placeholder;?>"
               value="<?=$value;?>"
                <?=showIf($required, 'required');?>>
            <?php if (isset($style['afterIco']) or isset($style['afterTxt'])) :?>
            <div class="input-group-addon">
                <?=showIf(isset($style['afterIco']), '<i class="'.$style['afterIco'].'"></i>');?>
                <?=showIf(isset($style['afterTxt']), $style['afterTxt']);?>
            </div>
            <?php endif;?>
        <?=showIf($needGroup, '</div>');?>
    <?php if ($error) : ?>
    <span class="help-block has-warning"><?=$error;?></span>
    <?php endif; ?>
    </div>
</div>
