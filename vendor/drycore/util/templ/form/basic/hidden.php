<?php if (!is_null($value)) : ?>    
<input type="hidden" id="<?=$id;?>" name="<?=$varName;?>[<?=$colName;?>]" value="<?=$value;?>">
<?php endif; ?>
<?php if ($error) : ?>
<div class="form-group <?=showIf($error, 'has-warning');?>">
    <div class="col-sm-offset-3 col-sm-9">
        <span class="help-block  has-warning">hidden:<?=$colName;?>:<?=$error;?></span>
    </div>
</div>
<?php endif; ?>
