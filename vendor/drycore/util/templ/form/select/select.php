<?php asset('headCss', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css');?>
<?php asset('footerJs', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js');?>
<?php assetStart();?>
<script>
$(function () {
    $('.myselect').select2({ width: '100%' });
});
</script>
<?php assetEnd('footerScript');?>

<?php
if (!isset($style['labelSize'])) {
    $style['labelSize'] = 3;
}
$mainSize = 12 - $style['labelSize'];
//
if (isset($style['placeholder'])) {
    $placeholder = $style['placeholder'];
} else {
    $placeholder = $model->lang($colName);
}
//
$needGroup =
    (
        isset($style['beforeIco'])
        or isset($style['afterIco'])
        or isset($style['beforeTxt'])
        or isset($style['afterTxt'])
    );
?>
<div class="form-group <?=showIf($error, 'has-warning');?>">
    <label for="<?=$id;?>" class="col-sm-<?=$style['labelSize'];?> control-label">
        <?=$label;?><?=showIf($required, ' *');?>
            <?=view('drycore/util/widget/help', ['text'=>$help]);?>
    </label>
    <div class="col-sm-<?=$mainSize;?>">
        <?=showIf($needGroup, '<div class="input-group">');?>
            <?php if (isset($style['beforeIco']) or isset($style['beforeTxt'])) :?>
            <div class="input-group-addon">
                <?=showIf(isset($style['beforeIco']), '<i class="'.$style['beforeIco'].'"></i>');?>
                <?=showIf(isset($style['beforeTxt']), $style['beforeTxt']);?>
            </div>
            <?php endif;?>
            <select
                class="form-control <?=showIf(isset($style['inputClass']), $style['inputClass']);?> myselect"
                id="<?=$id;?>"
                name="<?=$varName;?>[<?=$colName;?>]"
                <?=showIf($required, 'required');?>>
                <?php if (!isset($style['noNull'])) :?>
                    <option value="" <?=showIf($noValue, 'selected');?>>
                        <?=$_->undefined;?>
                    </option>
                <?php endif;?>
                <?php foreach ($options as $line) : ?>
                    <option value="<?=$line['value']?>"
                    <?=showIf(isset($line['selected']) and $line['selected'], 'selected');?> >
                        <?=$line['desc']?>
                    </option>
                <?php endforeach;?>
            </select>        
            <?php if (isset($style['afterIco']) or isset($style['afterTxt'])) :?>
            <div class="input-group-addon">
                <?=showIf(isset($style['afterIco']), '<i class="'.$style['afterIco'].'"></i>');?>
                <?=showIf(isset($style['afterTxt']), $style['afterTxt']);?>
            </div>
            <?php endif;?>
        <?=showIf($needGroup, '</div>');?>
    <?php if ($error) : ?>
    <span class="help-block has-warning"><?=$error;?></span>
    <?php endif; ?>
    </div>
</div>
