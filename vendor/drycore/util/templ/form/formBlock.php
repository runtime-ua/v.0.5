<?php
if (!isset($action)) {
    $action = '#';
}
?>
<form role="form"  class="form-horizontal" method="post" action="<?=url($action);?>">
    <?=s()->xsrf->field(); ?>
    <div class="row">
    <?php foreach ($blocks as $line) :?>
        <div class="col-md-<?=$size;?>">
            <?=$line['content'];?>
        </div>
    <?php endforeach;?>
        <div class="col-md-12">
            <button class="btn btn-primary btn-block" type="submit">
                <i class="fa fa-save"></i> <?=$btnText;?>
            </button>
        </div>
    </div>
</form>                
