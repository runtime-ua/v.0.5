<?php if (isset($alert) and !$this->model->isValid()) : ?>
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
  </button>
    <?=$alert;?>
</div>
<?php endif; ?>
<?php foreach ($inputs as $line) {
    echo $line;
}?>
