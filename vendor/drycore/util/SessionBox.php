<?php
namespace drycore\util;

/**
 * Description of SessionBox
 *
 * @author Maks
 */
class SessionBox extends \drycore\sys\arrayBox\AbstractArrayBox
{
    protected $varName;
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        if (!$this->varName) {
            $this->varName = $this->selfType();
        }
    }

    protected function loadArr()
    {
        $varName = $this->varName;
        $arr = s()->session->$varName;
        if (!$arr) {
            $arr = [];
        }
        return $arr;
    }

    protected function saveArr($arr)
    {
        $varName = $this->varName;
        s()->session->$varName = $arr;
    }
}
