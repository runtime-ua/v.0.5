<?php
namespace drycore\util\widget;

/**
 * Description of BreadcrumbView
 *
 * @author Mendel
 */
class BreadcrumbView extends \proto\View
{
    public function init()
    {
        $path = s()->route->path();
        unset($path[0]);
        //
        if ($path) {
            $last = end($path);
            unset($path[key($path)]);
            //
            $this->fields['show'] = true;
            $this->fields['parents'] = $path;
            $this->fields['currentTitle'] = $last->breadtitle;
        } else {
            $this->fields['show'] = false;
        }
        parent::init();
    }
}
