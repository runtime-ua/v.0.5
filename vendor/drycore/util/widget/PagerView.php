<?php
namespace drycore\util\widget;

/**
 * Пагинация, выводим список страниц. дробим по страницам.
 * Динамика потому что... хз, исторически динамика, пусть пока будет динамика
 */
class PagerView extends \proto\View
{
    public $box;
    public $param = 'page';
    public $page = 1;
    public $pageCount;
    

    public function init()
    {
        $box = $this->box;
        // Если в ссылке есть номер страницы то сохраним номер и применим его на боксе
        $route = s()->route->fields;
        $paramName = $this->param;
        if (isset($route[$paramName])) {
            $this->page = $route[$paramName];
        }
        $box->page($this->page);
        // Вычислим общее колво страниц
        $count = $box->count();
        if ($box->getLimit()) {
            $this->pageCount = ceil($count/$box->getLimit());
        } else {
            $this->pageCount = 1;
        }
        // Если не первая - назад
        if ($this->page != 1) {
            $this->fields['previous'] = $this->url($this->page - 1);
        }
        // если не последняя - вперед
        if ($this->page != $this->pageCount) {
            $this->fields['next'] = $this->url($this->page + 1);
        }
        // в цикле все страницы
        $this->fields['pages'] = [];
        for ($i=1; $i<=$this->pageCount; $i++) {
            $this->fields['pages'][] = [
                'id'=>$i,
                'active' =>($i==$this->page),
                'url'=>$this->url($i)
            ];
        }
        // Если класс не указан, то выберем дефолт
        if (!isset($this->fields['cssClass'])) {
            $this->fields['cssClass'] = 'pagination';
        }
        parent::init();
    }

    /**
     * 2DO: Унести в шаблон
     * @return string
     */
    public function render()
    {
        if ($this->pageCount > 1) {
            return parent::render();
        } else {
            return '';
        }
    }

    protected function url($page)
    {
        $route = s()->route->fields;
        $route[$this->param] = $page;
        if ($page == 1) {
            unset($route[$this->param]); // патамушта там может быть не только наш, но и чужой, присланный извне
        }
        return url($route);
    }
}
