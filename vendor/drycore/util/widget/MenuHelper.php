<?php
namespace drycore\util\widget;

class MenuHelper extends \proto\Service
{
    protected $ancorField='menutitle';
    protected $urlField='pageurl';
    protected $parentField='parent_id';
    protected $icoField = 'ico';
    //
    protected $pages = [];
    protected $menu = [];

    public function go($pages = [], $ancorField = 'menutitle', $urlField = 'pageurl', $parentField = 'parent_id')
    {
        $this->pages = $pages;
        $this->ancorField = $ancorField;
        $this->urlField = $urlField;
        $this->parentField = $parentField;
        //
        $this->prepareMenu();
        if ($this->parentField) {
            $this->moveChilds();
        }
        return $this->menu;
    }
    
    protected function prepareMenu()
    {
        $ancorField = $this->ancorField;
        $urlField = $this->urlField;
        $parentField = $this->parentField;
        $icoField = $this->icoField;
        //
        $this->menu = [];
        foreach ($this->pages as $line) {
            $arr = new \ArrayObject([], \ArrayObject::ARRAY_AS_PROPS);
            $arr->id = $line->id;
            $arr->ancor = $line->$ancorField;
            $arr->url = $line->$urlField;
            if ($parentField) {
                $arr->parent = $line->$parentField;
            }
            if ($icoField) {
                $arr->ico = $line->$icoField;
            }
            $this->menu[$line->id] = $arr;
        }
    }
    
    protected function moveChilds()
    {
        foreach ($this->menu as $key => $line) {
            if (isset($this->menu[$line->parent])) {
                $parent = $this->menu[$line->parent];
                if (isset($parent->menu)) {
                    $brothers = $parent->menu;
                } else {
                    $brothers = [];
                }
                $brothers[$key] = $line;
                $parent->menu = $brothers;
                unset($this->menu[$key]);
            }
        }
    }
}
