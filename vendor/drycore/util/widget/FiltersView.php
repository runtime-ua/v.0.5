<?php
namespace drycore\util\widget;

/**
 * Фильтры, они же папки - отбираем по условиям из конфига
 * Статический ибо выводит общие данные, и от дальнейшей фильтрации/пагинации ничего не изменится
 */
class FiltersView extends \proto\View
{
    public $box;
    public $defaultFilter;
    public $filter;
    
    public function init()
    {
        $this->prepareFilters();
        // Вызовем родительский фильтр
        parent::init();
    }
    
    protected function prepareRoute()
    {
        $route = s()->route->fields;
        return $route;
    }
    
    protected function prepareFilters()
    {
        $filters = $this->fields['filters'];
        // Получим ТЕКУЩИЙ фильтр
        $route = $this->prepareRoute();
        if (isset($route['filter'])) {
            $this->filter = $route['filter'];
        }
        if (!isset($this->filter) or !isset($filters[$this->filter])) {
            $this->filter = $this->defaultFilter;
        }
        // Отметим кто текущий
        $filters[$this->filter]['active'] = true;
        // Применим фильтр к боксу
        $this->box->where($filters[$this->filter]['where']);
        // Подготовим данные для фильтров
        $boxName = $this->box->selfType();
        foreach ($filters as $key => $line) {
            // вычислим счетчик
            $countBox = box($boxName);
            $countBox->where($line['where']);
            $countBox->where($line['countWhere']);
            $filters[$key]['count'] = $countBox->count();
            // Сделаем ссылки
            $route = $this->prepareRoute();
            // Если фильтр по умолчанию, то нефиг плодить копии страниц
            if ($key!=$this->defaultFilter) {
                $route['filter'] = $key;
            } else {
                unset($route['filter']);
            }
            $filters[$key]['url'] = url($route);
        }
        // Сохраним данные по фильтрам
        $this->fields['filters'] = $filters;
    }
}
