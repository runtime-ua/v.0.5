<?php
namespace drycore\util\widget;

/**
 * Таблица
 */
class TableView extends \proto\View
{
    public function init()
    {
        $box = $this->fields['box'];
        if (!isset($this->fields['columns'])) {
            $this->fields['columns'] = $box->iteratorFields();
        }
        parent::init();
    }
}
