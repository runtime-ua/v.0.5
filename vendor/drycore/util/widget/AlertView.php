<?php
namespace drycore\util\widget;

/**
 * Для алертов из сессии
 */
class AlertView extends \proto\View
{
    public function init()
    {
        $alerts = s()->session->alerts;
        if ($alerts) {
            $this->fields['alerts'] = $alerts;
            unset(s()->session->alerts);
        } else {
            $this->fields['alerts'] = [];
        }
        parent::init();
    }
}
