<?php
namespace drycore\util\widget;

class Alert extends \proto\Service
{
    /**
     * Добавляет сообщение с типом success в сессию для дальнейшего
     * отображения, обычно на след странице
     * @param type $content
     */
    public function success($content)
    {
        if (isset(s()->session->alerts)) {
            $alerts = s()->session->alerts;
        } else {
            $alerts = [];
        }
        //
        $alerts[] = ['content'=>$content,'type'=>'success'];
        s()->session->alerts = $alerts;
    }

    /**
     * Добавляет сообщение с типом info в сессию для дальнейшего
     * отображения, обычно на след странице
     * @param type $content
     */
    public function info($content)
    {
        if (isset(s()->session->alerts)) {
            $alerts = s()->session->alerts;
        } else {
            $alerts = [];
        }
        //
        $alerts[] = ['content'=>$content,'type'=>'info'];
        s()->session->alerts = $alerts;
    }

    /**
     * Добавляет сообщение с типом warning в сессию для дальнейшего
     * отображения, обычно на след странице
     * @param type $content
     */
    public function warning($content)
    {
        if (isset(s()->session->alerts)) {
            $alerts = s()->session->alerts;
        } else {
            $alerts = [];
        }
        //
        $alerts[] = ['content'=>$content,'type'=>'warning'];
        s()->session->alerts = $alerts;
    }

    /**
     * Добавляет сообщение с типом danger в сессию для дальнейшего
     * отображения, обычно на след странице
     * @param type $content
     */
    public function danger($content)
    {
        if (isset(s()->session->alerts)) {
            $alerts = s()->session->alerts;
        } else {
            $alerts = [];
        }
        //
        $alerts[] = ['content'=>$content,'type'=>'danger'];
        s()->session->alerts = $alerts;
    }
}
