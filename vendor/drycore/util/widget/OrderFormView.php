<?php
namespace drycore\util\widget;

/**
 * Выведем список сортировок, и применим сортировку к боксу
 * Статический потому что это метаданные, и они не поменяются
 */
class OrderFormView extends \proto\View
{
    public $orderParam = 'order';
    
    public function init()
    {
        $box = $this->fields['box'];
        // Если в ссылке есть сортировка то применим на боксе
        $orderParam = $this->orderParam;
        $route = s()->route->fields;
        if (isset($route[$orderParam]) and $route[$orderParam]) {
            $order = $route[$orderParam];
            $this->fields['order'] = $order;
            $box->order($order);
        } else {
            $this->fields['order'] = null;
        }
        
        parent::init();
    }
}
