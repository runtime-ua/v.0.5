<?php
namespace drycore\util\widget;

/**
 * Список разрешенных вариантов сортировок для виджетов сортировки
 * @author Maks
 */
class OrderedRule extends \proto\Rule
{
    public function callOrderList()
    {
        $value = $this->master->value;
        if (is_null($value)) {
            $value = [];
        }
        $value[] = $this->master->fieldName;
        $this->master->value = $value;
    }
}
