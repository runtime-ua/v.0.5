<?php
namespace drycore\util\widget;

/**
 * Выведем красиво модельку
 *
 * @author Mendel
 */
class ModelView extends \proto\View
{
    public function init()
    {
        if (isset($this->fields['scenario'])) {
            $currentScenario = $this->model->scenario();
            $this->model->scenario($this->fields['scenario']);
        }
        if (isset($this->fields['columns'])) {
            $columns = $this->fields['columns'];
        } else {
            $columns = $this->model->iteratorFields();
        }
        //
        $this->fields['data'] = [];
        foreach ($columns as $key) {
            $value = $this->model->pretty($key);
            $key = $this->model->lang($key);
            $this->fields['data'][$key] = $value;
        }
        if (isset($this->fields['scenario'])) {
            $this->model->scenario($currentScenario);
        }
        parent::init();
    }
}
