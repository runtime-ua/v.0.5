<?php
namespace drycore\util;

/**
 * Сортировка по полю, кнопки сортировки и все что с этим связано
 */
class SortRule extends \proto\Rule
{
    /**
     * После сохранения, если у нас это поле пустое, то установим его равным
     * ид, поскольку ид у нас автоинкрементарен и соответственно уникален
     */
    protected function afterSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $id = $this->master->model->id;
            // Напрямую в модель чтобы сразу сохранять
            $fieldName = $this->master->fieldName;
            $this->master->model->$fieldName = $id;
            $this->master->model->save();
        }
    }
    /**
     * Отметим текущее поле как поле для сортировки по умолчанию
     */
    protected function boxDefaultOrder()
    {
        $this->master->value = true;
    }
    
    /**
     * Уберем наше поле из списка отображаемых при итерациях полей
     */
    protected function listed()
    {
        $this->master->value = false;
    }
    
    /**
     * При клонировании обнулим значение
     */
    protected function afterClone()
    {
        $this->master->value = null;
    }
    
    /**
     * Вспомогательная, делает правильный бокс
     * Добавляем условия совпадения тех полей что указаны в параметре whereFields
     * @return type
     */
    protected function subBox()
    {
        if (isset($this->param['whereFields'])) {
            $sortWhereFields = tags2array($this->param['whereFields']);
        } else {
            $sortWhereFields = [];
        }
        $box = $this->master->model->box();
        foreach ($sortWhereFields as $fieldName) {
            $box->where([$fieldName=>$this->master->model->$fieldName]);
        }
        return $box;
    }
    
    /**
     * Поднимем нашу модель в сортировке вверх если это возможно
     */
    public function callSortUp()
    {
        $fieldName = $this->master->fieldName;
        
        $sort1 = $this->master->model->$fieldName;
        $model2 = $this->subBox()->order('!'.$fieldName)->findOne(['`'.$fieldName.'` < :sort', 'sort'=>$sort1]);
        if ($model2) {
            $sort2 = $model2->$fieldName;
            $this->master->model->$fieldName = $sort2;
            $model2->$fieldName = $sort1;
            $this->master->model->save();
            $model2->save();
        }
    }

    /**
     * Опустим нашу модель вниз, если это возможно
     */
    public function callSortDown()
    {
        $fieldName = $this->master->fieldName;
        
        $sort1 = $this->master->model->$fieldName;
        $model2 = $this->subBox()->order($fieldName)->findOne(['`'.$fieldName.'` > :sort', 'sort'=>$sort1]);
        if ($model2) {
            $sort2 = $model2->$fieldName;
            $this->master->model->$fieldName = $sort2;
            $model2->$fieldName = $sort1;
            $this->master->model->save();
            $model2->save();
        }
    }
}
