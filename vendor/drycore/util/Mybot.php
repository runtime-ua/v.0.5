<?php
namespace drycore\util;

class Mybot extends \proto\Service
{
    protected $options = [
        CURLOPT_RETURNTRANSFER => true,     // return into a variable
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 YaBr", // useragent
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 5,      // timeout on connect
        CURLOPT_TIMEOUT        => 5,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false,    // dont verify SSL
        CURLOPT_FAILONERROR    =>  false,   // Fail on errors
//        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
//        CURLOPT_HTTPHEADER     => [],
//        CURLOPT_COOKIEFILE     => '',
//        CURLOPT_COOKIEJAR      => '',
    ];
    public function go($url)
    {
        $this->options[CURLOPT_URL]= $url;
        //
        $ch = curl_init();    // initialize curl handle
        curl_setopt_array($ch, $this->options);
        //
        $content = curl_exec($ch);
        $err     = curl_errno($ch);
        $errmsg  = curl_error($ch);
        $header  = curl_getinfo($ch);
        curl_close($ch);
        //
        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }
}
