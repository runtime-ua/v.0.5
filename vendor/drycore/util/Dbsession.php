<?php
namespace drycore\util;

/**
 * Неблокирующее хранение сессии в базе
 *
 * @author Mendel
 */
class Dbsession extends \proto\Service implements \SessionHandlerInterface
{
    protected $boxName = 'drycore/util/session';

    public function __construct($config = null)
    {
        parent::__construct($config);
        session_set_save_handler($this, true);
    }

    public function close()
    {
        return true;
    }
    
    public function open($save_path, $name)
    {
        return true;
    }
    
    public function read($sessid)
    {
        $data = $this->getData($sessid);
        //
        if ($data) {
            $this->touch($sessid);
        }
        return $data;
    }
    
    protected function model($sessid)
    {
        $box = box($this->boxName);
        $model = $box->findOne(['sessid'=>$sessid]);
        if (!$model) {
            $model = model($this->boxName);
            $model->sessid = $sessid;
        }
        return $model;
    }
    
    protected function getData($sessid)
    {
        $model = $this->model($sessid);
        if ($model) {
            $data = $model->data;
            if (!$data) {
                $data = serialize([]);
            }
            return $data;
        } else {
            return serialize([]);
        }
    }
    
    protected function touch($sessid)
    {
        $model = $this->model($sessid);
        $model->save();
    }

    public function write($sessid, $data)
    {
        $model = $this->model($sessid);
        $model->data = $data;
        $model->user_id = s()->user->current()->id;
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
    
    public function destroy($sessid)
    {
        $model = $this->model($sessid);
        $model->delete();
        return true;
    }
    
    public function gc($maxlifetime)
    {
        $box = box($this->boxName);
        $box->delete(['`updated` + :maxlifetime < :time','maxlifetime'=>$maxlifetime, 'time'=>time()]);
        return true;
//        $sql = 'DELETE FROM `'.DB_PREFIX.$this->table.'`
//            WHERE date_touched + '.$maxlifetime.' < '.time();
    }
}

/**
 CREATE TABLE IF NOT EXISTS `session` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `sessid` VARCHAR( 255 ) NOT NULL,
    `updated` int(11) NOT NULL,
    `data` TEXT NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE ( `sessid` )
) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
 */
