<?php
namespace drycore\util\user;

/**
 * Бокс в котором лежат юзеры. Добавляем метод поиска по паролю
 * @author Mendel
 */
class UserBox extends \proto\Box
{
    /**
     * Ищем пользователя с таким логином и паролем
     * 2DO: переделать параметр логин на условие, вынести в настройки имя поля с хешем
     * @param string $login
     * @param string $password
     * @return mixed
     */
    public function findByPassword($login, $password)
    {
        $user = $this->findOne(['login'=>$login]);
        if ($user and $user->compareHash([$password, $user->passHash])) {
            return $user;
        } else {
            return null;
        }
    }
}
