<?php
namespace drycore\util\user;

  /**
   * Хеширует пароль из указанного поля
   * Параметр field: имя поля с паролем
   */
class PassHashRule extends \proto\Rule
{
    /**
     * Метод хеширования паролей
     */
    protected function beforeSave()
    {
        $field = $this->param['field'];
        $password = $this->master->model->$field;
        if ($password) {
            $this->master->value = password_hash($password, PASSWORD_DEFAULT);
        }
    }
    /**
     * Метод сравнения паролей
     * Проверка необходимости рехеша и сохранение если нужно
     */
    public function callCompareHash()
    {
        list($password, $hash) = $this->master->value;
        $status = password_verify($password, $hash);
        if ($status and password_needs_rehash($hash, PASSWORD_DEFAULT)) {
            $hash = password_hash($password, PASSWORD_DEFAULT);
            // Напрямую в модель чтобы сразу сохранять
            $fieldName = $this->master->fieldName;
            $this->master->model->$fieldName = $hash;
            $this->master->model->save();
        }
        $this->master->value = $status;
    }
}
