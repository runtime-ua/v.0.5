<?php
namespace drycore\util\user;

/**
 * Информация по текущему пользователю
 *
 * @author Mendel
 */
class User extends \proto\Service
{
    /**
     * Имя модели пользователя
     * @var string
     */
    protected $modelName = 'user';
    
    /**
     * Здесь храним пользователя если уже находили
     * @var \proto\Model
     */
    protected $user = null;
    
    /**
     * Залогиним пользователя
     * @param \proto\Model $user
     */
    public function login($user)
    {
        $this->user = $user;
        s()->session->regenerate();
        s()->session->userId = $user->id;
    }
        
    /**
     * Разлогиним пользователя
     */
    public function logout()
    {
        s()->session->destroy();
        $this->user = null;
    }

    /**
     * Вернем текущего пользователя, или новую модель если неавторизован
     * @return \proto\Model
     */
    public function current()
    {
        // Если еще не запрашивали у нас, то найдем юзера
        if (!isset($this->user)) {
            if (isset(s()->session->userId)) {
                $userId = s()->session->userId;
                $user = box($this->modelName)->findOne($userId);
                $this->user = $user;
            } else {
                $this->user = model($this->modelName);
            }
        }
        return $this->user;
    }
}
