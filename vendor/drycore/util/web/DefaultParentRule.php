<?php
namespace drycore\util\web;

/**
 * Правило подставляющее родителем - первый попавшийся страницу у которой указанный роут.
 * 2DO: По идее должно наследоваться от DefaultBoxFieldRule
 */
class DefaultParentRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $unit = box('drycore/web/path')->findOne(['route'=>$this->param['route']]);
            if ($unit) {
                $this->master->value = $unit->id;
            }
        }
    }
    protected function beforeSave()
    {
        $this->get();
    }
    protected function callForEdit()
    {
    }
}
