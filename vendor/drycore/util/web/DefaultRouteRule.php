<?php
namespace drycore\util\web;

/**
 * Description of RouteViewRule
 *
 * @author Maks
 */
class DefaultRouteRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $this->master->model->selfType().':'.$this->param['action'];
        }
    }
    protected function beforeSave()
    {
        $this->get();
    }
}
