<?php
namespace drycore\util\web;

/**
 * Description of RouteViewRule
 *
 * @author Maks
 */
class DefaultSelfRootRouteRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $unit = box('drycore/web/path')->findOne(["route"=>$this->master->model->selfType().':default']);
            if ($unit) {
                $this->master->value = $unit->id;
            }
        }
    }
    protected function beforeSave()
    {
        $this->get();
    }
}
