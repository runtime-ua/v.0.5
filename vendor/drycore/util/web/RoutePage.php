<?php
namespace drycore\util\web;

/**
 * Роутер для контент-сайта
 *
 * @author Mendel
 */
class RoutePage extends \drycore\core\route\Route
{
    protected $cachedPages = [];
    
    protected $boxName = 'drycore/util/web/path';
    protected $path = [];
    protected $indexPage;
    protected $maxLevel = 20; // Максимальный уровень вложенности ссылки (колво "слешей" в урл)
    
    /**
     * @var string контроллер для главной страницы
     */
    protected $defaultRoute = 'index:default';

    protected function box()
    {
        return box($this->boxName);
    }
    
    protected function indexPage()
    {
        if (is_null($this->indexPage)) {
            // Найдем главную страницу
            $this->indexPage = $this->box()->findOne(['route'=>$this->defaultRoute]);
            if (!$this->indexPage) {
                throw new \proto\NotFoundException();
            }
        }
        return $this->indexPage;
    }

    // Правила разбора роута. Возвращает роут. Опционально может добавить гет-параметры и т.п.
    protected function url2Route()
    {
        $page = $this->url2page($this->url);
        $this->fields[0] = $page->route;
        $this->fields['__pageID'] = $page->id;
        $this->fields['__pageType'] = $page->type;
    }

    // Ищет страницу соответствующую ссылке
    protected function url2page($url)
    {
        if ($url=='/') {
            return $this->indexPage();
        }
        // Удалим первый слеш
        $url = substr($url, 1);
        // Разберем ссылку на части
        $slugs = explode('/', $url);
        // Добавим корень сайта в путь
        $page = $this->indexPage();
        $this->path[] = $page;
        $parent_id = $page->id;
        // Найдем в цикле все страницы из нашего пути
        foreach ($slugs as $slug) {
            $page = $this->box()->findOne(['slug'=>$slug,'parent_id'=>$parent_id]);
            if (!$page) {
                throw new \proto\NotFoundException();
            }
            $this->path[] = $page;
            $parent_id = $page->id;
        }
        return $page;
    }
    
    //Делает из роута путь
    public function route2url($route)
    {
        // Если нет экшена, то дефолт
        if (!haveStr($route[0], ':')) {
            $route[0] = $route[0].':default';
        }
        // Пробуем найти страницу
        list($page,$param) = $this->route2page($route);
        // Если не получилось, то пробуем найти родителя для виртуальной
        if (!$page) {
            list($page,$param) = $this->virtualRoute2page($route);
        }
        // Получим урл страницы если есть страница
        if ($page) {
            $url = $page->pageurl;
        } else {
            // Если не нашли, то вернем хоть что-то, ибо падать с ошибкой не всегда уместно
            $url = $route[0];
        }
        // Добавим параметры если остались
        $param = http_build_query($param);
        if ($param) {
            $url = $url. '?'. $param;
        }
        // И вернем результат
        return $url;
    }

    // Ищет страницу по роуту, возвращает массив
    // Первым идет страница, второй параметры за вычетом "использованных"
    protected function route2page($route)
    {
        // Если это корень (главная страница), то вернем ее
        if ($route[0]==$this->defaultRoute) {
            unset($route[0]);
            return [$this->indexPage(),$route];
        }
        // Если указан ИД страницы, то ищем с учетом ИД, если нет, то без него
        if (isset($route['__pageID'])) {
            $page = $this->cachedPage(['route'=>$route[0], 'id'=>$route['__pageID']]);
            unset($route['__pageID']);
        } else {
            $page = $this->cachedPage(['route'=>$route[0]]);
        }
        if (isset($route['__pageType'])) {
            unset($route['__pageType']);
        }
        unset($route[0]);
        return [$page,$route];
    }
    
    // Находит страницу и кеширует, если есть в кеше то возвращает из кеша
    protected function cachedPage($where)
    {
        $id  = http_build_query($where);
        if (isset($this->cachedPages[$id])) {
            return $this->cachedPages[$id];
        } else {
            $page = $this->box()->findOne($where);
            $this->cachedPages[$id] = $page;
            return $page;
        }
    }


    // Ищет страницу по роуту, возвращает массив
    // Первым идет страница, второй параметры за вычетом "использованных"
    protected function virtualRoute2page($route)
    {
        // Получим экшн
        list(, $action) = explode(':', $route[0], 2);
        // Поменяем экшн на дефолтный
        $route[0] = $this->changeAction($route[0], 'default');
        // Найдем родительскую страницу с дефолтным экшеном
        list($page,$param) = $this->route2page($route);
        // Передадим искомый экшн в параметрах
        $param['__action'] = $action;
        return [$page,$param];
    }
    
    /**
     * Получим текущий путь (для хлебных крошек и т.п.)
     * @return type
     */
    public function path()
    {
        return $this->path;
    }
}
