<?php
namespace drycore\util\web;

  /**
   * Правило устанавливает наше поле равным транслитерированнму полю
   * имя которого указано в параметре. Действует только если поле пустое.
   * Параметр field: имя поля значение которое нужно установить
   */
class SlugRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $field = $this->param['field'];
            $value = $this->master->model->$field;
            $value = translit($value);
            $this->master->value = $value;
        }
    }
    protected function set()
    {
        // Отметим как нулл, чтобы сработал дефолт
        $this->master->model->pageurl = null;
    }

    protected function beforeSave()
    {
        $this->get();
    }
    /**
     * При клонировании обнулим значение
     */
    protected function afterClone()
    {
        $this->master->value = null;
    }
}
