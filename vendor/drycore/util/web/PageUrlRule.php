<?php
namespace drycore\util\web;

class PageUrlRule extends \drycore\core\otherRules\VirtualRule
{
    protected function afterLoad()
    {
        // Сохраним старое значение
        $this->master->model->internalSet('pageurl#old', $this->master->value);
    }
    
    protected function afterSave()
    {
        // если значение изменилось, то изменим и всем дочерним страницам
        if ($this->master->value !== $this->master->model->internalGet('pageurl#old')) {
            foreach ($this->master->model->childs->findAll() as $child) {
                // главная в качестве дочки не катит
                if ($child->route != 'index:default') {
                    // Отметим как нулл, чтобы сработал дефолт
                    $child->pageurl = null;
                    $child->save();
                }
            }
        }
    }

    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            // Если главная, то вернем УРЛ главной
            if ($this->master->model->route == 'index:default') {
                $this->master->value = '/';
                return;
            }
            // Если наш родитель - главная, то вернем слуг со слешом впереди
            if ($this->master->model->parent->route == 'index:default') {
                $this->master->value = '/'.$this->master->model->slug;
                return;
            }
            // Если дошли сюда, то значит минимум второй уровень и пилим добавлением через слеш
            $this->master->value = $this->master->model->parent->pageurl. '/'.$this->master->model->slug;
        }
    }
    protected function beforeSave()
    {
        $this->get();
    }
}
