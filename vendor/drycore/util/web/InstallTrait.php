<?php
namespace drycore\util\web;

/**
 * Description of InstallTrait
 *
 * @author Maks
 */
trait InstallTrait
{
    /**
     * @var string бокс в котором ищем/добавляем страницы при инсталяции
     */
    protected $installPageBox = 'singleton';
    /**
     *
     * @var string роут родитель для страниц
     */
    protected $installParentRoute = null;
    /**
     * Ключи - список экшенов страницы для которых нужно добавить
     * значения - данные для этих страниц
     * @var array
     */
    protected $installPages = [
        'default'=>[],
    ];
    /**
     * Ключи - список экшенов страницы для которых нужно добавить как подчиненные для дефолтной
     * значения - данные для этих страниц
     * @var array
     */
    protected $installChildPages = [];

    /**
     * Действие при инсталяции - проверяет есть ли дефолтная страница, и если нет, то создает,
     * Создает необходимый список дочерних страниц
     */
    public function installAction()
    {
        foreach ($this->installPages as $action => $data) {
            // Если есть роут родителя, то ищем родителя и ставим его
            if (!is_null($this->installParentRoute)) {
                $data['parent_id'] = $this->idByRoute($this->installParentRoute);
            }
            // Если есть ид родителя, то ставим его
            if (isset($this->param['parentId'])) {
                $data['parent_id'] = $this->param['parentId'];
            }
            // Добавим страницу
            $this->addSinglton($this->installPageBox, $action, $data, $this->installParentRoute);
        }
        foreach ($this->installChildPages as $action => $data) {
            $data['parent_id'] = $this->idByRoute($this->selfType().':default');
            $this->addSinglton($this->installPageBox, $action, $data);
        }
    }
    
    /**
     * Ищем ид первого у кого такой роут
     * @param type $route
     * @return type
     * @throws \Exception
     */
    protected function idByRoute($route)
    {
        $parent = box('drycore/web/path')->findOne(['route'=>$route]);
        if (!$parent) {
            throw new \Exception('Bad parent '.$route.' at '.$this->selfType());
        }
        return $parent->id;
    }
    
    /**
     * Если нет страницы с нашим контроллером и указанным экшеном (роут), то создаем ее
     * с указанными параметрами
     * @param string $boxName имя бокса в который добавляем страницу
     * @param string $action имя экшена
     * @param array $data данные для страницы
     * @return int ID искомой страницы (существующей или новой)
     * @throws \Exception
     */
    protected function addSinglton($boxName, $action, $data = [])
    {
        $box = box($boxName);
        $route = $this->selfType().':'.$action;
        // Если страница не существует
        if ($box->count(['route'=>$route]) == 0) {
            // Создадим
            $model = model($boxName);
            $model->route = $route;
            // Обзовем страницу - по имени действия
            $model->title = $this->lang($action.'#action');
            // Установим все атрибуты если есть
            foreach ($data as $key => $value) {
                $model->$key = $value;
            }
            // Сохраним
            $model->save();
            if (!$model->isValid()) {
                $msg = 'Invalid page '.$route.PHP_EOL;
                if (isset($parentRoute)) {
                    $msg .= 'Parent: '.$parentRoute.PHP_EOL;
                }
                foreach ($model->error() as $key => $value) {
                    $msg .=$key.' : '.$value.PHP_EOL;
                }
                throw new \Exception($msg);
            }
        }
    }
}
