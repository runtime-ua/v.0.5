<?php
namespace drycore\util\web;

/**
 *
 * @author Mendel
 */
abstract class AbstractWebController extends \drycore\core\controller\AbstractWebController
{
    use InstallTrait;
    protected $pageBoxName = 'singleton';
    
    public function action()
    {
        parent::action();
    }

    public function pageModel($action = null)
    {
        // Узнаем текущий роут
        $route = s()->route->fields[0];
        // Если нам указали от какого роута взять страничку, то поменяем роут и возьмем нужную
        if (!is_null($action)) {
            $route = s()->route->changeAction($route, $action);
        }
        //
        $data = box($this->pageBoxName)->findOne(['route'=>$route]);
        if (!$data) {
            throw new \proto\NotFoundException();
        }
        return $data;
    }

    protected function initShow($pageData = [])
    {
        if (!$pageData) {
            $pageData = $this->pageModel();
        }
        parent::initShow($pageData);
    }
}
