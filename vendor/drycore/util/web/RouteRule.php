<?php
namespace drycore\util\web;

/**
 * Description of RouteRule
 *
 * @author Maks
 */
class RouteRule extends \proto\Rule
{
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        list($controller,$action) = explode(':', $value, 2);
        $controllerName = s()->lang->getString('#self', 'controller/'.$controller);
        $actionName = s()->lang->getString($action.'#action', 'controller/'.$controller);
        $this->master->value = $controllerName.' -> '.$actionName;
    }
}
