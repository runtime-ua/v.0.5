<?php
namespace drycore\util\email;

/**
 * Обертка для PHPMailer
 */
class Email extends \proto\Service
{
    /**
     * Адреса на которые шлется письмо, если не указан адрес получателя
     * для уведомления админа.
     * @var type
     */
    protected $adminEmail=[];
    
    /**
     * Если установлено, то используем SMTP
     * иначе - mail()
     * @var type
     */
    protected $needSmtp = false;
    
    /**
     * Хост SMTP
     * @var type
     */
    protected $host; // smtp1.example.com;smtp2.example.com
    
    /**
     * Порт SMTP
     * @var type
     */
    protected $port = 557; //
    
    /**
     * Имя пользователя для SMTP
     * @var type
     */
    protected $username; // 'user@example.com'
    
    /**
     * Пароль для SMTP
     * @var type
     */
    protected $password; //
    
    /**
     * Вид шифрования. Варианты - FALSE без шифрования, tls, ssl
     * @var type
     */
    protected $secure = 'tls'; // tls, ssl
    
    /**
     * email отправителя
     * @var type
     */
    protected $fromEmail; // from@example.com
    
    /**
     * Имя отправителя (Читаемое, не мыло)
     * @var type
     */
    protected $fromName = ''; // пустая строка если не указано (важно!)

    /**
     * Layout для HTML-писем
     * @var type
     */
    protected $layout = 'drystyle/basic/email/main';
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        require_once  HOME.'/vendor/PHPMailer/PHPMailer/PHPMailerAutoload.php';
    }

        /**
     * Настраивает нашими настройками экземляр PHPMailer
     * в который мы сами можем что-то подобавлять в чистом виде, и отправить
     * $mail->addAddress('joe@example.net', 'Joe User');     // Add a recipient
     * $mail->addAddress('ellen@example.com');               // Name is optional
     * $mail->addReplyTo('info@example.com', 'Information');
     * $mail->addCC('cc@example.com');
     * $mail->addBCC('bcc@example.com');
     * $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
     * $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
     * $mail->isHTML(true);                                  // Set email format to HTML
     * $mail->Subject = 'Here is the subject';
     * @return \PHPMailer
     */
    public function newMail()
    {
        $mail = new \PHPMailer;
        $mail->CharSet = 'UTF-8';
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output
        // Если указан смтп то смтп, если нет, то mail()
        if ($this->needSmtp) {
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = $this->host;                            // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = $this->username;                    // SMTP username
            $mail->Password = $this->password;                    // SMTP password
            if ($this->secure) {
                $mail->SMTPSecure = $this->secure;  // Enable TLS encryption, `ssl` also accepted
            }
            $mail->Port = $this->port;                            // TCP port to connect to
        }
        //
        return $mail;
    }

    /**
     * Главный метод отправки письма, подготавливает и отправляет
     * @param type $to Получатель, если массив, то получателИ
     * @param type $subj Тема
     * @param type $data Данные (переменные передаваемые на рендеринг)
     * @param type $template Шаблон
     * @param type $fromEmail Адрес отправителя. Если не указан, то берем из конфига
     * @param type $fromName Имя отправителя, если не указан, то берем из конфига
     * @throws EmailException
     */
    public function send($to, $subj, $data, $template, $fromEmail, $fromName)
    {
        if (empty($template)) {
            $template = 'drystyle/basic/article/email';
        }
        $mail = $this->newMail();
        // Если не указан получатель, то отправляем админу/админам
        if (is_null($to) and $this->adminEmail) {
            $to = $this->adminEmail;
        }
        if (is_null($to)) {
            throw new EmailException('Unknown TO adress');
        }
        // Подставим отправителя если задан, по умолчанию из конфига (Если задан)
        if (is_null($fromEmail) and $this->fromEmail) {
            $fromEmail = $this->fromEmail;
        }
        if (!$fromName and $this->fromName) {
            $fromName = $this->fromName;
        }
        if (!$fromName) {
            $fromName = findModel('config@registry')->siteName;
        }
        if (!is_null($fromEmail)) {
            $mail->setFrom($fromEmail, $fromName);
        }
        //
        if (!is_array($to)) {
            $to = [$to];
        }
        foreach ($to as $line) {
            $mail->addAddress($line);
        }
        //
        $mail->Subject = $subj;
        $mail->isHTML(true);
        //
        $layout = view($this->layout, [
            'subject'=>$subj,
            'content'=>view($template, $data)
        ]);
        //
        $mail->Body    = (string) $layout;
        $mail->AltBody = (string) view($template.'Alt', $data);
        //
        $status = $mail->send();
        if (!$status) {
            throw new EmailException($mail->ErrorInfo);
        }
    }
}
