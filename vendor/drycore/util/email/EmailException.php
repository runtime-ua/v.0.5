<?php
namespace drycore\util\email;

/**
 * Исключение при отправке писем
 */
class EmailException extends \Exception
{
}
