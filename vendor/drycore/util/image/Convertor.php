<?php
namespace drycore\util\image;

/**
 * Сервис работы с версиями картинок
 * Ресайзит, находит пути и т.п.
 * Частично взято отсюда: https://github.com/blueimp/jQuery-File-Upload
 *
 * @author Maks
 */
class Convertor extends \proto\Service
{
    /**
     * Массив настроек разных версий
     * @var type 
     */
    protected $versions = [];
    
    /**
     * Список версий которые создаем при сохранении картинки
     * @var type 
     */
    protected $defaultVersions = ['#original','thumbnail','mini','small','medium','big'];
    
    use ImagickTrait, GDTrait;
    
    /**
     * Временное хранилище для объектов картинок
     * @var type 
     */
    protected $imageObjects = [];
    
    /**
     * Права с которыми создаем каталоги
     * @var type 
     */
    protected $mkdirMode = 0755; // Права на создаваемые каталоги (если нет каталога куда надо сохранить)

    /**
     * Создадим версию картинки по указанному пути и указанным типом версии
     * @param type $oldFileName
     * @param type $newFilename
     * @param type $version
     * @return type
     */
    public function createVersion($oldFileName, $newFilename, $version)
    {
        $options = $this->versions[$version];
        return $this->createScaledImage($oldFileName, $newFilename, $options);
    }
    
    /**
     * Получим имя файла версии по имени основного файла
     * @param type $oldFileName
     * @param type $version
     * @return string
     */
    public function getVersionFilename($oldFileName, $version = '#original')
    {
        if ($version == '#original') {
            return $oldFileName;
        }
        $dir = dirname($oldFileName);
        $filename = basename($oldFileName);
        $newFilename = $dir .'/'.$version.'/'.$filename;
        return $newFilename;
    }
    
    /**
     * Получим УРЛ версии из УРЛ-а основного файла
     * @param type $url
     * @param type $version
     * @return type
     */
    public function getVersionUrl($url, $version = '#original')
    {
        $oldFileName = s()->drycore_util_uploader_uploader->url2Path($url);
        $newFilename = $this->getVersionFilename($oldFileName, $version);
        if (!file_exists($newFilename)) {
            $ok = $this->createVersion($oldFileName, $newFilename, $version);
            if (!$ok) {
                $newFilename = $oldFileName;
            }
        }
        return s()->drycore_util_uploader_uploader->path2Url($newFilename);
    }
    
    /**
     * Удалим все версии основного файла
     * @param type $fileName
     */
    public function deleteVersions($fileName)
    {
        foreach (array_keys($this->versions) as $version) {
            $file = $this->getVersionFilename($fileName, $version);
            if (($version != '#original') and is_file($file)) {
                unlink($file);
            }
        }
    }
    
    /**
     * Получим список версий файла по имени файла (основных)
     * @param type $file_path
     * @return type
     */
    public function getVersionsList($file_path)
    {
        $urls = [];
        $url = s()->drycore_util_uploader_uploader->path2Url($file_path);
        foreach ($this->defaultVersions as $version) {
            $versionPath = $this->getVersionFilename($file_path, $version);
            if (is_file($versionPath)) {
                $urls[$version] = $this->getVersionUrl($url, $version);
            }
        }
        return $urls;
    }


    /**
     * Если это актуально, т.е. это картинка,
     * то создает альтернативные версии файла (картинки)
     * 
     * @param type $file_path
     * @return type
     */
    public function tryCreateVersions($file_path)
    {
        // Если это не изображение, то пропустим обработку
        if (!$this->isValidImageFile($file_path)) {
            return;
        }
        //
        $urls = [];
        $url = s()->drycore_util_uploader_uploader->path2Url($file_path);
        foreach ($this->defaultVersions as $version) {
            $new_file_path = $this->getVersionFilename($file_path, $version);
            $ok = $this->createVersion($file_path, $new_file_path, $version);
            if ($ok and ($version != '#original')) {
                $urls[$version] = $this->getVersionUrl($url, $version);
            }
        }
        // Free memory:
        $this->destroyImageObject($file_path);
        //
        return $urls;
    }
    
    /**
     * Проверим а картинка ли это
     * 
     * @param type $file_path
     * @return boolean
     */
    public function isValidImageFile($file_path)
    {
        if (!preg_match('/\.(gif|jpe?g|png)$/i', $file_path)) {
            return false;
        }
        $image_info = $this->imageSize($file_path);
        return $image_info && $image_info[0] && $image_info[1];
    }
    
    /**
     * Создадим изображение для версии
     * @param type $oldFileName
     * @param type $newFilename
     * @param type $options
     * @return type
     * @throws \Exception
     */
    public function createScaledImage($oldFileName, $newFilename, $options)
    {
        $newDir = dirname($newFilename);
        if (!file_exists($newDir)) {
            mkdir($newDir, $this->mkdirMode, true);
        }
        if (!is_dir($newDir)) {
            throw new \Exception('Bad directory '.$newDir);
        }
        if (extension_loaded('imagick')) {
            return $this->imagickCreateScaledImage($oldFileName, $newFilename, $options);
        } else {
            return $this->gdCreateScaledImage($oldFileName, $newFilename, $options);
        }
    }

    /**
     * Получим размер изображения
     * @param type $file_path
     * @return type
     */
    public function imageSize($file_path)
    {
        if (extension_loaded('imagick')) {
            return $this->imagicGetImageSize($file_path);
        } else {
            return $this->gdGetImageSize($file_path);
        }
    }
    
    /**
     * Разрушим временный объект для картинки
     * @param type $fileName
     * @return type
     */
    public function destroyImageObject($fileName)
    {
        if (extension_loaded('imagick')) {
            return $this->imagickDestroyImageObject($fileName);
        }
    }
}
