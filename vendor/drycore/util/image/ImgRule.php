<?php
namespace drycore\util\image;

/**
 * Правило базовой работы с картинками
 */
class ImgRule extends \proto\Rule
{
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        if (isset($this->param['prettyVersion'])) {
            $version = $this->param['prettyVersion'];
        } else {
            $version = 'thumbnail';
        }
        //
        $src = s()->drycore_util_image_convertor->getVersionUrl($value, $version);
        // 2DO: сделать создание ассетсов для размера картинок из данных вариантов...
        $this->master->value = '<img src="'.$src.'">';
    }
    
    protected function callImgVariant()
    {
        // Значение поля, с дефолтами
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        // версия передана вторым параметром
        $version = $this->master->value;
//        jsonDie([$value,$version]);
        //
        $this->master->value = s()->drycore_util_image_convertor->getVersionUrl($value, $version);
    }
}
