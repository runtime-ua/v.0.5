<?php
namespace drycore\util\history;

  /**
   * Правило устанавливает в поле ID текущего юзера
   */
class UpdaterIdRule extends \proto\Rule
{
    protected function beforeSave()
    {
        $this->master->value = s()->user->current()->id;
    }
}
