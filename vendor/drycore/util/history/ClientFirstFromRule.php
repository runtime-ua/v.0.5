<?php
namespace drycore\util\history;

/**
 * Устанавливает Ссылку ОТКУДА впервые пришел пользователь (не сегодня, а впервые)
 */
class ClientFirstFromRule extends \proto\Rule
{

    protected function beforeSave()
    {
        $this->get();
    }
    
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $data = s()->http->get();
            $this->master->value = $data['firstFrom'];
        }
    }
}
