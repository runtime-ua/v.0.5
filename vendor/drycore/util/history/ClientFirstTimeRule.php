<?php
namespace drycore\util\history;

/**
 * Устанавливает Время первого захода пользователя (или текущее если неизвестно)
 */
class ClientFirstTimeRule extends \proto\Rule
{

    protected function beforeSave()
    {
        $this->get();
    }
    
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $data = s()->http->get();
            $this->master->value = $data['firstTime'];
        }
    }
}
