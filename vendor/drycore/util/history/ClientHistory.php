<?php
namespace drycore\util\history;

/**
 * Сервис логинга в куках первого захода пользователя
 *
 * @author Mendel
 *
 */
class ClientHistory extends \proto\Service
{
    /**
     * @var type имя куки где храним инфу о первом приходе
     */
    protected $firstVisitInfoId = '__firstVisitInfo';
    /**
     * @var type срок жизни куки
     */
    protected $firstVisitInfoTtl = 31536000; // 1 год
    /**
     * @var type имя куки где храним инфу о приходе в текущую сессию
     */
    protected $currentVisitInfoId = '__currentVisitInfo';
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        //
        $firstVisitInfoId = $this->firstVisitInfoId;
        $currentVisitInfoId = $this->currentVisitInfoId;
        if (is_null(s()->cookie->$firstVisitInfoId)) {
            // base64 от json
            $value = base64_encode(json_encode($this->data()));
            s()->cookie->addCookie($firstVisitInfoId, $value, ['ttl'=>$this->firstVisitInfoTtl]);
        }
        if (is_null(s()->cookie->$currentVisitInfoId)) {
            // base64 от json
            $value = base64_encode(json_encode($this->data()));
            s()->cookie->addCookie($currentVisitInfoId, $value);
        }
    }
    
    /**
     * Вернем текущее время, реферер и точку входа
     * @return type
     */
    private function data()
    {
        return [
            'id'=> randId(),
            'time' => time(),
            'from' => $_SERVER['HTTP_REFERER'],
            'to' => $_SERVER['REQUEST_URI']
        ];
    }
    
    /**
     * Получим сохраненные данные если есть, или текущие если нет сохраненных
     * @return type
     */
    public function get()
    {
        $firstVisitInfoId = $this->firstVisitInfoId;
        $currentVisitInfoId = $this->currentVisitInfoId;
        // Получим инфу по первому посещению
        if (!is_null(s()->cookie->$firstVisitInfoId)) {
            // base64 от json
            $first = json_decode(base64_decode(s()->cookie->$firstVisitInfoId), true);
        } else {
            $first = $this->data();
        }
        // получим инфу по текущей сессии
        if (!is_null(s()->cookie->$currentVisitInfoId)) {
            // base64 от json
            $current = json_decode(base64_decode(s()->cookie->$currentVisitInfoId), true);
        } else {
            $current = $this->data();
        }
        // Сформируем ответ
        return [
            'firstId'=>$first['id'],
            'firstTime'=>$first['time'],
            'firstFrom'=>$first['from'],
            'firstTo'=>$first['to'],
            'currentId'=>$current['id'],
            'currentTime'=>$current['time'],
            'currentFrom'=>$current['from'],
            'currentTo'=>$current['to'],
        ];
    }
}
