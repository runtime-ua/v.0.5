<?php
namespace drycore\util\history;

class UpdateHistoryRule extends \drycore\core\otherRules\VirtualRule
{
    
    protected function afterLoad()
    {
        // Сохраним себе старое значение
        $old = $this->current();
        $this->master->value = $old;
    }
    
    protected function afterSave()
    {
        // если изменились поля описанные как требующие "слежки", то
        // сохраним себе текущее значение, а в бокс - разницу с прошлым
        $model = $this->master->model;
        $old = $this->old();
        $current = $this->current();
        $diff = array_udiff_assoc($old, $current, [$this,'arrayDiff']);
        $this->save('update', $model, $diff);
    }
    
    /**
     * Сравнивает две переменые JSON-сериализуя их, а потом сравнивая
     * Вспомогательная для получения разницы между массивами
     * @see self::afterSave()
     * @param mixed $val_1
     * @param mixed $val_2
     * @return int
     */
    private function arrayDiff($val_1, $val_2)
    {
        $j_1 = json_encode($val_1);
        $j_2 = json_encode($val_2);
        if ($j_1 === $j_2) {
            return 0;
        }
        return ($j_1 > $j_2)? 1:-1;
    }
    
    
    protected function afterDelete()
    {
        // сохраним всё прошлое значение как удаленное
        $model = $this->master->model;
        $old = $this->old();
        $this->save('delete', $model, $old);
    }
    
    protected function old()
    {
        $old = $this->master->value;
        if (is_null($old)) {
            return [];
        }
        $fieldName = $this->master->fieldName;
        if (isset($old[$fieldName])) {
            unset($old[$fieldName]);
        }
        return $old;
    }

    protected function current()
    {
        $current = $this->master->model->internalGetAllFields();
        if (is_null($current)) {
            return [];
        }
        $fieldName = $this->master->fieldName;
        if (isset($current[$fieldName])) {
            unset($current[$fieldName]);
        }
        return $current;
    }
    
    // Сохраним подготовленные данные
    protected function save($modelMethod, $model, $data = [])
    {
        $history = model('drycore/util/history/updateHistory');
        $history->modelName = $model->selfType();
        $history->model_id = $model->id;
        $history->modelMethod = $modelMethod;
        $history->controller = controller()->selfType();
        $history->action = controller()->action;
        $history->data = $data;
        $history->transaction_id = s()->db->transactionId;
        $history->save();
    }
}
