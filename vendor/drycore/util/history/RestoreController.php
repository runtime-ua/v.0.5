<?php
namespace drycore\util\history;

/**
 * Заготовка контроллера отката транзакций из истории
 * Пока не дописан поскольку я пока не просчитал как оно с безопасностью будет дружить
 */
class RestoreController extends \drycore\util\standartController\AdminOnepageController
{
    public function restoreAction()
    {
        $transaction = $this->getTransaction();
        foreach ($transaction as $line) {
            switch ($line->modelMethod) {
                case 'delete':
                    break;
                case 'update':
                    break;
                case 'create':
                    break;
                default:
                    throw new \Exception('wrong restore modelMethod');
            }
        }
        $this->deleteHistory($transaction);
        $this->redirect();
    }
    
    public function deleteHistory($transaction)
    {
        foreach ($transaction as $line) {
            $whereId = ['`id`>=:id', 'id'=>$line->id];
            $where = ['modelName'=>$line->modelName,'model_id'=>$line->model_id];
            box('drycore/util/history/updateHistory')->where($whereId)->delete($where);
        }
    }
    
    protected function getTransaction()
    {
        $box = box('drycore/util/history/updateHistory')->limit(false)->where(['transaction_id'=>$this->param['id']]);
        return $box->findAll();
    }
}
