<?php
namespace drycore\util\history;

/**
 * Устанавливает страницу КУДА зашел пользователь в начале сессии
 * (нашу страницу с параметрами, метками, партнерами и т.п.)
 */
class ClientCurrentToRule extends \proto\Rule
{

    protected function beforeSave()
    {
        $this->get();
    }
    
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $data = s()->clientHistory->get();
            $this->master->value = $data['currentTo'];
        }
    }
}
