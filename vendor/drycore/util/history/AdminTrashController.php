<?php
namespace drycore\util\history;

/**
 * Контроллер корзины
 */
class AdminTrashController extends \drycore\util\standartController\AdminCrudController
{
    /**
     * Список страниц при инсталяции
     * @var array 
     */
    protected $installPages = [
        'default'=>[],
    ];
    /**
     * Ключи - список экшенов страницы для которых нужно добавить как подчиненные для дефолтной
     * значения - данные для этих страниц
     * @var array 
     */
    protected $installChildPages = [
        'view'=>[]
    ];
    
    /**
     * Очистка корзины, удаляем все
     */
    public function clearAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $box = $this->box();
        // Жертва ООП - иначе связи не хочет удалять.
        foreach ($box->findAll() as $model) {
            $model->scenario('trashActions');
            $model->delete(true);
        }
        $this->redirect();
    }
    
    /**
     * Удаляем конкретный объект в корзине
     */
    public function deleteAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->scenario('trashActions');
        $model->delete(true);
        $this->redirect();
    }
    
    /**
     * Восстанавливаем элемент из корзины
     */
    public function repairAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->scenario('trashActions');
        $model->trash = false;
        $model->save();
        $this->redirect();
    }
}
