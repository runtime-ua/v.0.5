<?php
namespace drycore\util\history;

  /**
   * Правило устанавливает в поле текущий таймштамп если в поле ничего не указано.
   */
class TimeCreateRule extends \proto\Rule
{
    protected function beforeSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = time();
        }
    }
    /**
     * При клонировании обнулим значение
     */
    protected function afterClone()
    {
        $this->master->value = null;
    }
}
