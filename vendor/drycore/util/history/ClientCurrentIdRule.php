<?php
namespace drycore\util\history;

/**
 * Устанавливает ID текущей сессии (не сессионИД системы сессий, а рекламный)
 */
class ClientCurrentIdRule extends \proto\Rule
{

    protected function beforeSave()
    {
        $this->get();
    }
    
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $data = s()->http->get();
            $this->master->value = $data['currentId'];
        }
    }
}
