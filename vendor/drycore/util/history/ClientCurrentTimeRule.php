<?php
namespace drycore\util\history;

/**
 * Устанавливает Время захода пользователя (Начала сессии)
 */
class ClientCurrentTimeRule extends \proto\Rule
{

    protected function beforeSave()
    {
        $this->get();
    }
    
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $data = s()->http->get();
            $this->master->value = $data['currentTime'];
        }
    }
}
