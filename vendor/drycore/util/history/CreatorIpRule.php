<?php
namespace drycore\util\history;

/**
 * Устанавливает IP текущего пользователя
 */
class CreatorIpRule extends \proto\Rule
{
    protected function beforeSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $_SERVER['REMOTE_ADDR'];
        }
    }
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $_SERVER['REMOTE_ADDR'];
        }
    }
}
