<?php
namespace drycore\util\history;

/**
 * Устанавливает useragent текущего пользователя
 */
class CreatorAgentRule extends \proto\Rule
{
    protected function beforeSave()
    {
        $this->get();
    }
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $_SERVER['HTTP_USER_AGENT'];
        }
    }
}
