<?php
namespace drycore\util\history;

/**
 * Уникальный ид выдаваемый юзерагенту при первом входе (или совсем случайный)
 */
class ClientFirstIdRule extends \proto\Rule
{

    protected function beforeSave()
    {
        $this->get();
    }
    
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $data = s()->http->get();
            $this->master->value = $data['firstId'];
        }
    }
}
