<?php
namespace drycore\util\history;

class TrashRule extends \proto\Rule
{
    protected function beforeDelete()
    {
        $fieldName = $this->master->fieldName;
        if (!$this->master->model->$fieldName == true) {
            $this->master->model->$fieldName = true;
            $this->master->model->save();
            $this->master->hidden = false;
        }
    }
    protected function boxFilter()
    {
        $fieldName = $this->master->fieldName;
        if ($this->master->box->scenario() != 'trashActions') {
            $this->master->value = [$fieldName => false];
        }
    }
    protected function beforeSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            if ($this->master->model->scenario() != 'trashActions') {
                $this->master->value = false;
            }
        }
    }
}
