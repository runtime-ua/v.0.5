<?php
namespace drycore\util\history;

  /**
   * Правило устанавливает в поле текущий таймштамп
   */
class TimeUpdateRule extends \proto\Rule
{
    protected function beforeSave()
    {
        $this->master->value = time();
    }
}
