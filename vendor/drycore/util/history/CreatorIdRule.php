<?php
namespace drycore\util\history;

  /**
   * Правило устанавливает в поле ID текущего пользователя если в поле ничего не указано.
   */
class CreatorIdRule extends \proto\Rule
{
    protected function beforeSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = s()->user->current()->id;
        }
    }
    /**
     * При клонировании обнулим значение
     */
    protected function afterClone()
    {
        $this->master->value = null;
    }
}
