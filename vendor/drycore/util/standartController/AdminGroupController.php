<?php
namespace drycore\util\standartController;

class AdminGroupController extends \drycore\util\standartController\GroupController
{
    public function __construct($config = null)
    {
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
}
