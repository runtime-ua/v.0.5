<?php
namespace drycore\util\standartController;

class AdminOneModelController extends \proto\WebController
{
    protected $modelName;
    public $updateActions=[['drystyle/admin/actions/save']];

    public function __construct($config = null)
    {
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
        parent::__construct($config);
    }

    public function defaultAction()
    {
        $model = findModel($this->modelName);
        // Пробуем обновить данные из POST, если обновили и данные валидны, то сохраним
        if (s()->post->update($model) and $model->isValid()) {
            $model->save();
        }
        $this->initShow();
        $this->showVar('model', $model);
        $this->show();
    }
}
