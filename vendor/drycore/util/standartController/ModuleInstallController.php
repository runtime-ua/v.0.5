<?php
namespace drycore\util\standartController;

/**
 * Просто выполним все инсталяции из указанных модулей
 * Сахар, чтобы вынести инсталяции модуля в конфиг модуля, а не все в одном месте
 * @author Maks
 */
class ModuleInstallController extends \proto\Controller
{
    protected $pages = [];
    protected $controllers = [];

    public function installAction()
    {
        // Создадим все страницы и сохраним их
        $pages = $this->addPages();
        // Инициируем инсталяцию всех контроллеров и передаем им соответствующие данные о родителях
        foreach ($this->controllers as $controller => $parentName) {
            $installRoute = [$controller.':install'];
            if (!is_null($parentName) and isset($pages[$parentName])) {
                $parentId = $pages[$parentName]->id;
                $installRoute = [$controller.':install',"parentId"=>$parentId];
            }
            startController($installRoute);
        }
    }
    protected function addPages()
    {
        $pages = [];
        foreach ($this->pages as $pageName => $data) {
            $page = model($data['type']);
            unset($data['type']);
            foreach ($data as $key => $value) {
                $page->$key = $value;
            }
            $page->save();
            if ($page->isValid()) {
                $pages[$pageName] = $page;
            } else {
                $msg = 'Invalid page (category):'.$this->selfType().PHP_EOL;
                foreach ($page->error() as $key => $value) {
                    $msg .=$key.' : '.$value.PHP_EOL;
                }
                throw new \Exception($msg);
            }
        }
        return $pages;
    }
}
