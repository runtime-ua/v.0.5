<?php
namespace drycore\util\standartController;

/**
 * Description of modelController
 *
 * @author Администратор
 */
abstract class AbstractModelController extends \proto\WebController
{
    // Храним тут модель на случай повторного запроса
    private $model;
    // Имя бокса который используем в контроллере
    protected $boxName;
    // Параметр (из ссылки, роута и т.п.) по которому ищется моделька
    protected $findModelParam = 'id';
    // Поле по которому ищется моделька, т.е. это поле должно быть равно параметру
    protected $findModelField = 'id';
    
    public function __construct($config = null)
    {
        // Сделаем по умолчанию значение бокса совпадающим с именем контроллера
        parent::__construct($config);
        if (!$this->boxName) {
            $this->boxName = $this->selfType();
        }
    }

    // Получим наш бокс (каждый раз новый, чтобы условия не накапливались)
    protected function box()
    {
        return box($this->boxName);
    }
    
    /**
     * Вернем текущую модельку
     * Если передали нам модельку, то назначим ее текущей (для перехвата редиректа после создания и т.п.
     * @return type
     * @throws routeNotFoundException
     */
    protected function model($model = null)
    {
        if (!is_null($model)) {
            $this->model = $model;
        }
        // Если есть, то вернем
        if (isset($this->model)) {
            return $this->model;
        }
        // Иначе ищем
        if (isset($this->param[$this->findModelParam])) {
            $param = $this->param[$this->findModelParam];
            $model = $this->box()->findOne([$this->findModelField=>$param]);
            if (!$model) {
                throw new \proto\NotFoundException();
            }
            $this->model = $model;
            return $model;
        } else {
            throw new \proto\NotFoundException();
        }
    }
}
