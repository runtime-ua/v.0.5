<?php
namespace drycore\util\standartController;

class CrudRedirectController extends \drycore\util\standartController\AbstractModelController
{
    protected $redirectBack = true;
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }

    public function defaultAction()
    {
        $this->initShow();
        $this->showVar('box', $this->box());
        $this->show();
    }
    
    public function createAction()
    {
        $model = s()->post->getModel($this->boxName);
        if (!$model->isNew() and $model->isValid()) {
            $model->save();
            //
            s()->alert->success($this->lang('createdAlert'));
            $this->redirect();
        } else {
            throw new \proto\ForbiddenException();
        }
    }
    
    public function updateAction()
    {
        $model = $this->model();
        // **********************************************************************
        // Пробуем обновить данные из POST, если обновили и данные валидны, то сохраним
        if (s()->post->update($model) and $model->isValid()) {
            $model->save();
            s()->alert->success($this->lang('updatedAlert'));
            $this->redirect();
        } else {
            throw new \proto\ForbiddenException();
        }
    }
    
    public function deleteAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->delete();
        s()->alert->danger($this->lang('deletedAlert'));
        $this->redirect();
    }
}
