<?php
namespace drycore\util\standartController;

class AdminCrudController extends \drycore\util\standartController\AbstractModelController
{
    // бокс в котором ищем/добавляем страницы при инсталяции
    protected $installPageBox = 'adminPage';
    /**
     * Ключи - список экшенов страницы для которых нужно добавить
     * значения - данные для этих страниц
     * @var array
     */
    protected $installPages = [
        'default'=>['mainmenu'=>true],
    ];
    // Ключи - список экшенов страницы для которых нужно добавить как подчиненные для дефолтной
    // значения - данные для этих страниц
    protected $installChildPages = [
        'view'=>[],
        'create'=>[],
        'update'=>[],
    ];
    
    protected $findModelParam = 'id';
    
    public $listScenario='list';
    public $viewScenario = 'view';
    public $listActions;
    public $listTopActions = [['add']];
    public $viewActions=false;
    public $createActions=[['drystyle/admin/actions/save']];
    public $updateActions=[['drystyle/admin/actions/save','drystyle/admin/actions/bigDelete']];
    public $filters=[];
    public $defaultFilter;

    public function __construct($config = null)
    {
        parent::__construct($config);
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
    
    public function defaultAction()
    {
        $this->initShow();
        $box = $this->box();
        $box->scenario($this->listScenario);
        $this->showVar('box', $box);
        $this->show();
    }
    
    public function viewAction()
    {
        $model = $this->model();
        // **********************************************************************
        $pageData = $this->pageModel();
        $pageData->metatitle = $pageData->metatitle.' - '.$model->title();
        $pageData->title = $pageData->title.' - '.$model->title();
        // **********************************************************************
        $this->initShow($pageData);
        $model->scenario($this->viewScenario);
        $this->showVar('model', $model);
        $this->show();
    }

    // Выбираем какую модель добавлять, если есть дочерние типы
    // Используем страницу создания, отдельную не добавляем
    public function selectCreateAction()
    {
        $childsData = $this->selectCreateChilds();
        if (!s()->modelFactory->isAbstract($this->boxName)) {
            $parentData = [
                'url'=>$this->selectCreateUrl($this->boxName),
                'ancor'=>s()->lang->getString('#self', 'model/'.$this->boxName)
            ];
        } else {
            $parentData = null;
        }
        // Если детей нет, то чем морочить голову, стразу редиректим на основное создание
        // Если конечно оно не абстрактное)
        if (!$childsData and $parentData) {
            redirect(UrlXsrf($parentData['url']));
        }
        //
        $pageData = $this->pageModel('create');
        $this->initShow($pageData);
        $this->showVar('parentData', $parentData);
        $this->showVar('childsData', $childsData);
        $this->show();
    }
    
    /**
     * Подготовим данные по дочерним типам
     * @return type
     */
    protected function selectCreateChilds()
    {
        $childs = s()->modelFactory->factoryChildsList($this->boxName, false);
        // Если есть тип который также должен быть родителем, то установим его, если нет, то основной по умолчанию
        if (isset($this->param['filterParent'])) {
            $filterParent = $this->param['filterParent'];
        } else {
            $filterParent = $this->boxName;
        }
        //
        $childsData = [];
        foreach ($childs as $child) {
            if (s()->modelFactory->isChild($child, $filterParent)) {
                $childsData[] = [
                    'url'=>$this->selectCreateUrl($child),
                    'ancor'=>s()->lang->getString('#self', 'model/'.$child)
                ];
            }
        }
        return $childsData;
    }
    
    protected function selectCreateUrl($modelName)
    {
        $result = [$this->selfType().':create','modelName'=>$modelName];
        if (isset($this->param['default'])) {
            $result['default'] = $this->param['default'];
        }
        return $result;
    }
    
    public function createAction()
    {
        $model = $this->modelForCreate();
        if (!$model->isNew() and $model->isValid()) {
            $model->save();
            $this->model($model); // Сохраним, вдруг редиректу нужна, или афтерэкшн....
            s()->alert->success($this->lang('createdAlert'));
            $this->redirect();
        } else {
            $this->model($model); // Сохраним, вдруг редиректу нужна, или афтерэкшн....
            $this->initShow();
            $this->showVar('model', $model);
            $this->show();
        }
    }

    /**
     * Вспомагательная для create. Берет пост-модель, или если пост не заполнен, то
     * берет ее же в гет, проверив что ссылка не поддельная.
     * @return type
     */
    protected function modelForCreate()
    {
        $boxName = $this->boxName;
        // Попробуем получить имя дочерней модели, если была передана.
        if (isset($this->param['modelName'])) {
            s()->xsrf->validateGet(); // Защита от невнимательности+XSRF)
            // Проверим что выбранная модель является нашим потомком
            if (!s()->modelFactory->isChild($this->param['modelName'], $boxName)) {
                throw new \proto\ForbiddenException();
            }
            $boxName = $this->param['modelName'];
        }
        // Пробуем пост
        $model = s()->post->getModel($boxName);
        // Если нет поста, т.е. модель осталась новой, без данных, то смотрим в гет
        if ($model->isNew()) {
            if (isset($this->param['default'])) {
                s()->xsrf->validateGet();
                // Вот так в цикле и вручную ибо нам не нужно валидировать
                foreach ($this->param['default'] as $key => $value) {
                    $model->$key = $value;
                }
            }
            // Сделаем модель новой, ведь по сути то это еще не данные, а лишь дефолтные значения.
            $model->isNew(true);
        }
        return $model;
    }
    
    public function updateAction()
    {
        $model = $this->model();
        $pageData = $this->pageModel();
        $pageData->metatitle = $pageData->metatitle.' - '.$model->title();
        $pageData->title = $pageData->title.' - '.$model->title();
        // Пробуем обновить данные из POST, если обновили и данные валидны, то сохраним
        if (s()->post->update($model) and $model->isValid()) {
            $model->save();
            s()->alert->success($this->lang('updatedAlert'));
            $this->redirect();
        } else {
            $this->initShow($pageData);
            $this->showVar('model', $model);
            $this->show();
        }
    }

    // Копирование - как создание, только на основе чужой копии
    public function copyAction()
    {
        $model = clone $this->model();
        if (s()->post->update($model) and $model->isValid()) {
            $model->save();
            s()->alert->success($this->lang('createdAlert'));
            $this->model($model); // Сохраним, вдруг редиректу нужна, или афтерэкшн....
            $this->redirect();
        } else {
            $pageData = $this->pageModel('create');
            $this->initShow($pageData);
            $this->showVar('model', $model);
            $this->show();
        }
    }
    
    public function deleteAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->delete();
        s()->alert->warning($this->lang('deletedAlert'));
        $this->redirect();
    }
}
