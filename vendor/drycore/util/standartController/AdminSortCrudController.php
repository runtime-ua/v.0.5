<?php
namespace drycore\util\standartController;

class AdminSortCrudController extends \drycore\util\standartController\AdminCrudController
{
    public function sortUpAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->sortUp();
        $this->redirect();
    }
    public function sortDownAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->sortDown();
        $this->redirect();
    }
}
