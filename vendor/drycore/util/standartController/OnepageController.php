<?php
namespace drycore\util\standartController;

class OnepageController extends \proto\WebController
{
    public function defaultAction()
    {
        $this->initShow();
        $this->show();
    }
}
