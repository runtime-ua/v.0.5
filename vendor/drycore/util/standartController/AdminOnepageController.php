<?php
namespace drycore\util\standartController;

class AdminOnepageController extends \drycore\util\standartController\OnepageController
{
    // бокс в котором ищем/добавляем страницы при инсталяции
    protected $installPageBox = 'adminPage';
    public function __construct($config = null)
    {
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
}
