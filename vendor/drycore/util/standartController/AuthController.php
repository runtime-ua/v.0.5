<?php
namespace drycore\util\standartController;

class AuthController extends \proto\WebController
{

    /**
     *
     * @var string роут родитель для страниц
     */
    protected $installParentRoute = 'index:default';
    /**
     * @var string бокс в котором ищем/добавляем страницы при инсталяции
     */
    protected $installPageBox = 'adminPage';
    
    protected $pageBoxName = 'adminPage';
    
    protected $redirect = ['index:default'];

    public function __construct($config = null)
    {
        parent::__construct($config);
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
    }
    
    public function loginAction()
    {
        $this->initShow();
        // **********************************************************************
        $model = s()->post->getModel('drycart/main/login', null, true);
        if (!$model->isNew() and $model->isValid()) {
            $user = box('user')->findByPassword($model->login, $model->password);
            if (!is_null($user)) {
                s()->user->login($user);
                $this->redirect();
                return;
            } else {
                $this->showVar('loginError', true);
            }
        } else {
            $this->showVar('loginError', false);
        }
        // **********************************************************************
        $this->showVar('login', $model);
        $this->show();
    }
    
    public function registerAction()
    {
        $model = model('user:profile');
        if (s()->post->update($model) and $model->isValid()) {
            $model->scenario('default');
            $model->save();
            $this->redirect();
            return;
        }
        $this->initShow();
        $this->showVar('user', $model);
        $this->show();
    }
    
    public function logoutAction()
    {
        s()->user->logout();
        $this->redirect();
    }
    
    public function profileAction()
    {
        if (s()->user->current()->isNew()) {
            throw new \proto\ForbiddenException();
        }
        // **********************************************************************
        $id = s()->user->current()->id;
        $model = box('user')->findOne($id);
        $model->scenario('profile');
        // Пробуем обновить данные из POST, если обновили и данные валидны, то сохраним
        if (s()->post->update($model) and $model->isValid()) {
            $model->scenario('default');
            // Не уверен что это действительно нужно, но пусть будет!
            if ($id != $model->id) {
                throw new \proto\ForbiddenException();
            }
            $model->save();
            $this->redirect();
        } else {
            $this->initShow();
            $this->showVar('model', $model);
            $this->show();
        }
    }
}
