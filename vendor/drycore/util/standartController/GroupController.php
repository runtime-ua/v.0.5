<?php
namespace drycore\util\standartController;

class GroupController extends \drycore\util\standartController\AbstractModelController
{
    protected $findModelParam = '__pageID';
    /**
     * Ключи - список экшенов страницы для которых нужно добавить
     * значения - данные для этих страниц
     * @var array
     */
    protected $installPages = [
        'default'=>['mainmenu'=>true],
    ];
    
    public function defaultAction()
    {
        $this->initShow();
        $this->showVar('box', $this->box());
        $this->show();
    }
    
    public function viewAction()
    {
        $model = $this->model();
        $this->initShow($model);
        $this->showVar('model', $model);
        $this->show();
    }
}
