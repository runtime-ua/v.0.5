<?php
namespace drycore\util\standartController;

/**
 * Description of IndexController
 *
 * @author Maks
 */
class IndexController extends \drycore\util\standartController\OnepageController
{
    /**
     * Переопределяем инсталятор, чтобы создать независимо от родителя
     */
    public function installAction()
    {
        $model = model($this->pageBoxName);
        $model->route = $this->selfType().':default';
        $model->title = $this->selfName();
        $model->save();
    }
}
