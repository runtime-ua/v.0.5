<?php
namespace drycore\util\standartController;

class FormController extends \proto\WebController
{
    /**
     * Ключи - список экшенов страницы для которых нужно добавить
     * значения - данные для этих страниц
     * @var array
     */
    protected $installPages = [
        'default'=>['mainmenu'=>true],
    ];
    protected $needEmail = false;
    protected $adminEmail = null;
    protected $boxName;
    protected $emailTemplate;
    protected $save = true;
    protected $redirect = false;
    protected $needRedirectBack = false;

    public function __construct($config = null)
    {
        // Сделаем по умолчанию значение бокса совпадающим с именем контроллера
        parent::__construct($config);
        if (!$this->boxName) {
            $this->boxName = $this->selfType();
        }
        s()->http->ttl = false; // Запретим кешировать наш вывод ибо он динамичный
        if (isset($this->param['back'])) {
            $this->needRedirectBack = (bool) $this->param['back'];
        }
    }
    
    public function defaultAction()
    {
        $model = s()->post->getModel($this->boxName);
        if (!$model->isNew() and $model->isValid()) {
            // Вызовем основное действие
            $this->go($model);
            // выведем плашку что всё ок
            s()->alert->success($this->lang('success'));
        }
        // Если в форме ошибка
        if (!$model->isNew() and !$model->isValid()) {
            // выведем плашку что ошибка заполнения формы
            s()->alert->warning($this->lang('formError'));
        }
        // **********************************************************************
        if ($this->redirect or $this->needRedirectBack) {
            redirect($this->redirect, $this->needRedirectBack);
        } else {
            $model->scenario('form');
            $this->initShow();
            $this->showVar('model', $model);
            $this->show();
        }
    }
    
    
    // Основной блок - вынесен для простоты наследования :)
    protected function go($model)
    {
        // Если надо сохранять
        if ($this->save) {
            $model->save();
            // Попробуем отправить письмо админу
            $this->tryAdminEmail($model);
        }
    }

    protected function tryAdminEmail($model)
    {
        if ($this->needEmail) {
            s()->email->send(
                $this->adminEmail,
                $this->lang('subj'),
                ['model'=>$model],
                $this->emailTemplate
            );
        }
    }
}
