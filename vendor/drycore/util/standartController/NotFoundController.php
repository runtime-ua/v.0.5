<?php
namespace drycore\util\standartController;

/**
 * Когда страница должна быть, но выводить ее не стоит, например родитель какой, или скрытая страница...
 */
class NotFoundController extends \drycore\util\standartController\AbstractModelController
{
    protected $findModelParam = '__pageID';
    
    public function defaultAction()
    {
        throw new \proto\NotFoundException();
    }
    
    public function viewAction()
    {
        throw new \proto\NotFoundException();
    }
}
