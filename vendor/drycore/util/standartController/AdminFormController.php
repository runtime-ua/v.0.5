<?php
namespace drycore\util\standartController;

class AdminFormController extends \drycore\util\standartController\AdminCrudController
{
    public function toNewAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->new = true;
        $model->save();
        $this->redirect();
    }
    public function toOldAction()
    {
        // Проверяем что в ссылке есть валидный маркер
        s()->xsrf->validateGet();
        //
        $model = $this->model();
        $model->new = false;
        $model->save();
        $this->redirect();
    }
}
