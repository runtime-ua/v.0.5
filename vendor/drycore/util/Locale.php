<?php
namespace drycore\util;

/**
 * Основные настройки локального отображения, формат даты, текущий язык и т.п.
 * 2DO: Унести все эти параметры в юзера, а дефолтные в дефолт модели (гость)
 * @author Mendel
 */
class Locale extends \proto\Service
{
    /**
     * Временная зона
     * @var string
     */
    public $timeZone = 'UTC';
    
    /**
     * Формат даты
     * @var string
     */
    public $dateFormat = 'd.m.Y';
    /**
     * Красивый формат для виджета дат
     *    d, dd: Numeric date, no leading zero and leading zero, respectively. Eg, 5, 05.
     *    D, DD: Abbreviated and full weekday names, respectively. Eg, Mon, Monday.
     *    m, mm: Numeric month, no leading zero and leading zero, respectively. Eg, 7, 07.
     *    M, MM: Abbreviated and full month names, respectively. Eg, Jan, January
     *    yy, yyyy: 2- and 4-digit years, respectively. Eg, 12, 2012.
     *
     * @var string
     */
    public $dateFormatPretty = 'dd.mm.yyyy';
    
    /**
     * Формат времени
     * @var string
     */
    public $timeFormat = 'G:i';
    
    /**
     * Формат время и дата
     * @var string
     */
    public $dateTimeFormat = 'd.m.Y - G:i';
    
    /**
     * Текущий язык (трехбуквенный)
     * @var string
     */
    public $lang;
    
    /**
     * Двубуквенный формат языка
     * @var string
     */
    public $shortLang;

    /**
     * Конструктор
     * @param array $config
     */
    public function __construct($config = null)
    {
        parent::__construct($config);
        date_default_timezone_set($this->timeZone);
        s()->lang->setLang($this->lang);
    }
    
    /**
     * Таймштамп в дату
     * @param int $timestamp
     * @return string
     */
    public function asDate($timestamp)
    {
        return date($this->dateFormat, $timestamp);
    }
    
    /**
     * Таймштамп в время
     * @param int $timestamp
     * @return string
     */
    public function asTime($timestamp)
    {
        return date($this->timeFormat, $timestamp);
    }
    
    /**
     * Таймштамп в дату и время
     * @param int $timestamp
     * @return string
     */
    public function asDateTime($timestamp)
    {
        return date($this->dateTimeFormat, $timestamp);
    }
}
