<?php
namespace drycore\util\migration;

/**
 * Функционал миграции моделей (боксов)
 */
trait MigrateModelTrait
{
    /**
     * Меняем для бокса всем у кого есть старый набор полей, на новый
     * @param type $boxName
     * @param type $old
     * @param type $new
     */
    protected function migrateModel($boxName, $old, $new)
    {
        $box = box($boxName);
        if (defined('MIGRATE_START')) {
            $box->update($new, $old);
        } else {
            foreach ($box->findAll($old) as $model) {
                jsonPre([$model->internalGetAllFields(),$new]);
            }
        }
//        foreach ($box->findAll($old) as $model) {
//            if(defined('MIGRATE_START')) {
//                foreach($new as $key=>$value) $model->$key = $value;
//                $model->save();
//            } else {
//                $data = [];
//                foreach($old as $key=>$value) $data[$key] = [$model->$key, $value];
//                foreach($new as $key=>$value) $data[$key] = [$model->$key, $value];
//                foreach($info as $key) $data[$key] = [$model->$key];
//                jsonPre($data);
//            }
//        }
    }
    /**
     * Выполним миграцию моделей по конфигу
     * @param type $config
     */
    protected function modelConfigUp($config)
    {
        foreach ($config as $line) {
            $info = [];
            if (isset($line['info'])) {
                $info = $line['info'];
            }
            $this->migrateModel($line['boxName'], $line['old'], $line['new'], $info);
        }
    }

    /**
     * Выполним ОТКАТ миграции моделей по конфигу
     * @param type $config
     */
    protected function modelConfigDown($config)
    {
        foreach ($config as $line) {
            $info = [];
            if (isset($line['info'])) {
                $info = $line['info'];
            }
            $this->migrateModel($line['boxName'], $line['new'], $line['old'], $info);
        }
    }
}
