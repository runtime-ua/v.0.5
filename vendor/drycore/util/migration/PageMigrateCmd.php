<?php
namespace drycore\util\migration;

/**
 * Типовой контроллер для миграций страниц и отдельных боксов
 */
class PageMigrateCmd extends \proto\Controller
{
    use CoreTrait, MigratePageTrait, MigrateModelTrait;
    
    /**
     * Список видов страниц которые нужно изменять
     * @var type 
     */
    protected $pageMigrate = [];
    
    /**
     * Список видов моделей которые надо изменять
     * @var type 
     */
    protected $modelMigrate = [];
    
    /**
     * Список таблиц которые переименовываются
     * @var type 
     */
    protected $renameTable = [];
    
    /**
     * Список условий на контроллеры, которые не должны находиться,
     * а если находятся то вывести, значит что-то не мигрировали
     * @var type 
     */
    protected $likeController = [];
    
    /**
     * Список типов страниц которых быть не должно (лайк-условия)
     * Проверяется, после миграции и если находятся то что-то пропутстили
     * @var type 
     */
    protected $likeType = [];
    
    /**
     * Выполним все миграции и проверки
     */
    public function migrateUpAction()
    {
        $this->pageConfigUp($this->pageMigrate); // 2do: еще бы историю патчить
        $this->modelConfigUp($this->modelMigrate);
        foreach ($this->renameTable as $line) {
            $this->renameTable($line['old'], $line['new']);
        }
        $controllers = $types = [];
        // Проверим что больше не осталось контроллеров из нашей папки, и если остались то выведем их
        foreach ($this->likeController as $like) {
            $controllers = array_merge($controllers, $this->findLikeControllers($like));
        }
        if ($controllers) {
            jsonPre($controllers);
        }
        // Проверим что больше не осталось типов из нашей папки, и если остались то выведем их
        foreach ($this->likeType as $like) {
            $types = array_merge($types, $this->findLikeTypes($like));
        }
        if ($types) {
            jsonPre($types);
        }
    }
    
    /**
     * Выполним все миграции обратно
     */
    public function migrateDownAction()
    {
        $this->pageConfigDown($this->config);
        $this->modelConfigDown($this->modelMigrate);
        foreach ($this->renameTable as $line) {
            $this->renameTable($line['new'], $line['old']);
        }
    }
}
