<?php
namespace drycore\util\migration;

/**
 * Функционал миграции страниц
 */
trait MigratePageTrait
{
    /**
     * Выполним миграцию страниц по конфигу
     * @param type $config
     */
    protected function pageConfigUp($config)
    {
        foreach ($config as $line) {
            $oldController = $newController = $oldType = $newType = null;
            if (isset($line['oldController'])) {
                $oldController = $line['oldController'];
            }
            if (isset($line['newController'])) {
                $newController = $line['newController'];
            }
            if (isset($line['oldType'])) {
                $oldType = $line['oldType'];
            }
            if (isset($line['newType'])) {
                $newType = $line['newType'];
            }
            $this->migratePage($oldController, $newController, $oldType, $newType);
        }
    }

    /**
     * Выполним миграцию страниц по конфигу
     * только меняем новое на старое а не наоборот
     * @param type $config
     */
    protected function pageConfigDown($config)
    {
        foreach ($config as $line) {
            $oldController = $newController = $oldType = $newType = null;
            if (isset($line['oldController'])) {
                $oldController = $line['oldController'];
            }
            if (isset($line['newController'])) {
                $newController = $line['newController'];
            }
            if (isset($line['oldType'])) {
                $oldType = $line['oldType'];
            }
            if (isset($line['newType'])) {
                $newType = $line['newType'];
            }
            $this->migratePage($newController, $oldController, $newType, $oldType);
        }
    }
    
    /**
     * Меняем во всех роутах всех страниц у которых встречается наш контроллер
     * на новое значение
     * Меняем у страниц с роутом связанным с искомым контроллером - тип на нужный
     * @param string $oldController
     * @param string $newController
     * @param string $oldType
     * @param string $newType
     */
    protected function migratePage($oldController, $newController = null, $oldType = null, $newType = null)
    {
        // Для всех роутов относящихся к указанному контроллеру
        foreach ($this->findLike('page', 'route', $oldController.':%') as $oldRoute) {
            // Если указан новый тип, то поменяем его на новый
            if (!is_null($newType)) {
                $this->updateTable('page', ['route'=>$oldRoute,'type'=>$oldType], ['type'=>$newType]);
            }
            // Если указан новый контроллер, то поменяем старый на новый
            if (!is_null($newController)) {
                $newRoute = str_ireplace($oldController.':', $newController.':', $oldRoute);
                $this->updateTable('page', ['route'=>$oldRoute], ['route'=>$newRoute]);
            }
        }
    }
    
    /**
     * Ищем в базе контроллеры которые соответствуют лайк-условию
     * Находит все роуты с такими условиями, отбрасывает у них экшины
     * и возвращает уникальный список.
     * По большому счету - сахар для помощи в написании миграции, и в проверке что все ок
     * а не в самой миграции.
     * @param type $like
     * @param type $where
     * @return type
     */
    protected function findLikeControllers($like, $where = true)
    {
        $result = $this->findLike('page', 'route', $like, $where);
        $out = [];
        foreach ($result as $line) {
            list($controller,) = explode(':', $line);
            $out[$controller] = $controller;
        }
        return array_keys($out);
    }

    /**
     * Получим список всех типов по маске.
     * Используем для создания миграции и для проверки. Аналогично как с контроллером
     * @param type $like
     * @param type $where
     * @return type
     */
    protected function findLikeTypes($like, $where = true)
    {
        return $this->findLike('page', 'type', $like, $where);
    }
}
