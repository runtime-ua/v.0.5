<?php
namespace drycore\util\migration;

/**
 * Основной функционал для миграций
 */
trait CoreTrait
{
    /**
     * Найдем всех у кого соответствующее поле похоже на искомое
     * Вернем массив значений этого поля
     * @param type $table
     * @param type $field
     * @param type $like
     * @param type $where
     * @return type
     */
    protected function findLike($table, $field, $like, $where = true)
    {
        $query = s()->db->query(['table'=>$table]);
        $query->where($where);
        $query->where(['`'.$field.'` LIKE "'.$like.'"']);
        $query->select([$field], true);
        $result = $query->go();
        return array_column(iterator_to_array($result), $field);
    }

    /**
     * Меняем в таблице поле со старого значения на новое
     * @param type $table
     * @param type $where
     * @param type $data
     */
    protected function updateTable($table, $where, $data)
    {
        $query = s()->db->query(['table'=>$table]);
        $query->where($where);
        $query->update($data);
        if (defined('MIGRATE_START')) {
            $query->go();
        } else {
            jsonPre([$query->sql,$query->param]);
        }
    }
    
    /**
     * Переименуем таблицу
     * @param type $old
     * @param type $new
     */
    protected function renameTable($old, $new)
    {
        $query = s()->db->query(['table'=>$old]);
        $query->renameTable($new);
        if (defined('MIGRATE_START')) {
            $query->go();
        }
    }
}
