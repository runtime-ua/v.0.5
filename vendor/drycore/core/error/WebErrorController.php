<?php
namespace drycore\core\error;

/**
 * Контроллер для вывода сообщений об ошибках в браузер
 */
class WebErrorController extends \proto\WebController
{
    /**
     * Отключим транзакции в обработке экшена
     * @var boolean
     */
    protected $needTransaction = false;

    /**
     * Екшн для случая когда не найдена страница/екшн
     */
    public function notFoundAction()
    {
        s()->http->notFound();
        $pageData = [
            'title' =>$this->lang('err404Title'),
            'metatitle' =>$this->lang('err404Title'),
            'article' =>$this->lang('err404Text')
        ];
        $this->initShow($pageData);
        $this->showVar('exception', $this->param['exception']);
        $this->show();
    }

    /**
     * Экшн если доступ запрещен
     */
    public function forbiddenAction()
    {
        s()->http->forbidden();
        $pageData = [
            'title' =>$this->lang('err403Title'),
            'metatitle' =>$this->lang('err403Title'),
            'article' =>$this->lang('err403Text')
        ];
        $this->initShow($pageData);
        $this->showVar('exception', $this->param['exception']);
        $this->show();
    }

    /**
     * Внутренняя ошибка
     */
    public function internalAction()
    {
        s()->http->internal();
        s()->db->rollBack(); // откатим транзакцию раз что-то пошло не так
        $pageData = [
            'title' =>$this->lang('err500Title'),
            'metatitle' =>$this->lang('err500Title'),
            'article' =>$this->lang('err500Text')
        ];
        $this->initShow($pageData);
        $this->showVar('exception', $this->param['exception']);
        $this->show();
    }
}
