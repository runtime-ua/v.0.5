<?php
namespace drycore\core\error;

/**
 * Контроллер для вывода сообщений об ошибках в консоль
 * Теоретически можно и в браузер, но вообще в консоль
 */
class ErrorController extends \proto\WebController
{
    /**
     * Отключим транзакции в обработке экшена
     * @var boolean
     */
    protected $needTransaction = false;

    /**
     * Екшн для случая когда не найдена страница/екшн
     */
    public function notFoundAction()
    {
        echo $this->lang('err404Title').PHP_EOL.PHP_EOL;
    }

    /**
     * Экшн если доступ запрещен
     */
    public function forbiddenAction()
    {
        echo $this->lang('err403Title').PHP_EOL.PHP_EOL;
    }

    /**
     * Внутренняя ошибка
     */
    public function internalAction()
    {
        s()->db->rollBack(); // откатим транзакцию раз что-то пошло не так
        echo $this->lang('err500Title').PHP_EOL.PHP_EOL;
        if (defined('DEVMODE')) {
            var_dump($this->param['error']);
        }
    }
}
