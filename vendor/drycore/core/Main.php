<?php
namespace drycore\core;

/**
 * Основной "цикл".
 * Вызываем роут, контроллер, контроллер ошибок и все такое
 *
 * @author Mendel
 */
class Main extends \proto\Service
{
    /**
     * @var string Имя контроллера для обработки ошибок (права доступа, 404 и т.п.)
     */
    protected $errorController = 'error';

    /**
     * Инитим перехваты ошибок и т.п. чтобы сделать это как можно раньше
     * (сразу после инициализации сервислокатора)
     */
    public function init()
    {
        // Включаем перехват ошибок
        if (!defined('DEBUG')) {
            ini_set('display_errors', 'Off');
        }
        set_error_handler([$this, 'errHandler']);
        set_exception_handler([$this, 'catchException']);
    }

    /**
     * Обработчик ошибок - тяжелые ошибки превращаем в исключения, мелкие записываем в лог
     * @param string $severity
     * @param string $message
     * @param string $file
     * @param string $line
     * @throws \ErrorException
     */
    public function errHandler($severity, $message, $file, $line)
    {
        // Этот код ошибки включен в error_reporting то исклюение, иначе пишем в нотис
        if (error_reporting() & $severity) {
            $exception = new \ErrorException($message, 0, $severity, $file, $line);
            $this->catchException($exception);
        } else {
            s()->log->notice($file, $line, $message);
        }
    }

    /**
     * Запускаем основной цикл
     */
    public function start()
    {
        // start controller and resolve route exceptions
        $controllerFactory = s()->controllerFactory;
        // выполним основной код.
        try {
            s()->route->init();
            $controllerFactory->startController();
        } catch (\Exception $exception) {
            $this->catchException($exception);
        }
    }

    /**
     * Обработчик исключений - неизвестный роут (экшн/страница и т.п.), запрет доступа и т.п.
     * @param \Exception $exception
     */
    public function catchException($exception)
    {
        switch (get_class($exception)) {
            case 'proto\NotFoundException':
                startController([$this->errorController.':notFound','exception'=>$exception]);
                break;
            case 'proto\ForbiddenException':
                startController([$this->errorController.':forbidden','exception'=>$exception]);
                break;
            default:
                s()->log->exception($exception);
                startController([$this->errorController.':internal','exception'=>$exception]);
                break;
        }
    }
}
