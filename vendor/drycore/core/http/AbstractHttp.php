<?php
namespace drycore\core\http;

/**
 * Вспомогательный класс для HTTP, содержит различную логику
 */
abstract class AbstractHttp extends \proto\Service
{
    /**
     * Время кеширования страницы (браузером)
     * @var int
     */
    public $ttl = 60;    // Кэшируем контент на минуту по умолчанию
    
    /**
     * Время (таймштамп) когда было последнее изменение страницы (LastModify)
     * @var int
     */
    public $editTime = false; // Таймстамп когда страница была изменена
            // если не указан, то не учитывается
    
    /**
     * Выводит заголовки кеширования если разрешено, и запрета кеширования если нельзя
     */
    protected function cacheTtl()
    {
        if ($this->ttl) {
            header("cache-control: public, max-age = $this->ttl");
            header("pragma: public");
            $expireTime = time() + $this->ttl;
            header("Expires: " . gmdate("D, d M Y H:i:s", $expireTime) ." GMT");
        } else {
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("pragma: no-cache");
            header("Expires: Thu, 19 Nov 1981 08:52:00 GMT");
        }
    }
    
    /**
     * Преобразует дату в таймштамп
     * вспомогалка для If-Modified-Since
     * по идее говнокод, должно быть где-то готовое, но пока оставлю
     * DEPRECATED!!!!!
     * @param string $s
     * @return int
     */
    private function date2unixstamp($s)
    {
        $months = [
            'Jan' => 1,
            'Feb' => 2,
            'Mar' => 3,
            'Apr' => 4,
            'May' => 5,
            'Jun' => 6,
            'Jul' => 7,
            'Aug' => 8,
            'Sep' => 9,
            'Oct' => 10,
            'Nov' => 11,
            'Dec' => 12
        ];
        $a = explode(' ', $s);
        $b = explode(':', $a[4]);
        return gmmktime($b[0], $b[1], $b[2], $months[$a[2]], $a[1], $a[3]);
    }


    /**
     * Сообщаем что контент меняется от кук, и меняем куку в зависимости от залогинености...
     * переделать под зависимость от сессии или другого признака, не юзера а чего-то более нормального
     * DEPRECATED!!!!!
     */
    protected function varyCookie()
    {
        header("Vary: Cookie");
        // идентификатор пользователя в виде хеша от всех данных
        // SID используется для усложнения определения ИД юзера, хотя обычно это и не секрет, но лучше не рисковать
        $hashId = md5(s()->user->current()->id . session_id());
        if (!isset($_COOKIE['_uid']) or ($_COOKIE['_uid'] != $hashId)) {
            setcookie("_uid", $hashId, time()+3600, "/");
        }
    }

    /**
     * Кеш браузера валидный и узнали мы это по дате их тега и дате создания контента
     * @return type
     */
    protected function validByDate()
    {
        if (isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
            return false; // Проваливаем проверку по дате если есть Ётаг
        }
        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
            $cacheTime = $this->date2unixstamp($_SERVER['HTTP_IF_MODIFIED_SINCE']);
        } else {
            $cacheTime = 0;
        }
        return ($cacheTime === $this->editTime);
    }

    /**
     * Если ётег совпадает, то страница актуальная
     * @param string $etag
     * @return boolean
     */
    protected function validByEtag($etag)
    {
        return (isset($_SERVER['HTTP_IF_NONE_MATCH']) and $etag==$_SERVER['HTTP_IF_NONE_MATCH']);
    }
}
