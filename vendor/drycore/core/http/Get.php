<?php
namespace drycore\core\http;

/**
 * Компонента для работы с GET-данными
 * Возвращает готовые модельки с нужным классом и нужными данными
 */
class Get extends \proto\Service
{
    /**
     * Проверка того, что нужные данные были отправлены
     * @param mixed $fieldName имя искомого параметра, или пусто если любые данные
     * @return boolean Истина если есть данные, Ложь в противном случае
     */
    public function posted($fieldName = null)
    {
        // Если нет массива с пост-данными
        if (!isset($_GET) or !$_GET) {
            return false;
        }
        // если он не массив
        if (!is_array($_GET)) {
            return false;
        }
        // если нам не нужно проверять конкретное поле, то проверка пройдена
        if (is_null($fieldName)) {
            return true;
        }
        // Если мы дошли сюда, то запрошенное поле не пустое, и проверяем, что
        // искомое поле существует
        if (!isset($_GET[$fieldName]) or !is_array($_GET[$fieldName])) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Получаем модель с данными или пустую модель если данных нет
     * @param string $modelName класс модели который наполнять данными
     * @param string $fieldName имя поля из которого брать данные или пусто если весь массив
     * @return mixed искомая модель, с данными или без
     */
    public function getModel($modelName, $fieldName = null)
    {
        $model = model($modelName);
        $this->update($model, $fieldName);
        return $model;
    }
    
    /**
     * Обновим нашу модель данными полученными из GET
     * @param string $model модель которая обновляется/наполняется данными
     * @param string $fieldName имя поля из которого брать данные или пусто если весь массив
     * @return mixed Истинна если исходные данные менялись, Ложь если не менялись
     */
    public function update($model, $fieldName = null)
    {
        if (is_null($fieldName)) {
            $fieldName = str_replace('/', '_', $model->selfType());
        }
        // Если данные есть, то получим данные, если данных нет, то вернем пустую модель
        if ($this->posted($fieldName)) {
            $data = $this->getData($fieldName);
            // в цикле добавим данные
            foreach ($data as $key => $value) {
                $model->$key = $value;
            }
            // выполним валидацию модели
            $model->validate();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Возвращает данные для обработки. Основной массив если не задано поле
     * или конкретное поле, если задано. Проверки на валидность проводить
     * до вызова этого метода
     * @param mixed $fieldName имя искомого поля
     * @return array искомые данные
     */
    protected function getData($fieldName)
    {
        if (is_null($fieldName)) {
            return $_GET;
        } else {
            return $_GET[$fieldName];
        }
    }
}
