<?php
namespace drycore\core\http;

/**
 * Работа с куками
 */
class Cookie extends \proto\Service
{
    /**
     * Данные по кукам которые нам нужно установить
     * @var type
     */
    protected $data = [];

    /**
     * Отправим все куки в браузер
     */
    public function sendCookie()
    {
        foreach ($this->data as $name => $data) {
            if (isset($data['ttl'])) {
                $data['expire'] = time() + $data['ttl'];
            }
            setcookie(
                $name,
                $data['value'],
                $data['expire'],
                $data['path'],
                $data['domain'],
                $data['secure'],
                $data['httpOnly']
            );
        }
    }

    /**
     * Сохраним куки для того чтобы их отправить при выводе контента
     * expire - таймштамп когда истекает срок жизни куки (лучше ttl)
     * ttl - через сколько секунд истекает срок жизни, затирает expire
     * path - папка в которой живет кука, по умолчанию - корень сайта
     * domain - домен, по умолчанию текущий
     * secure - безопасная (запрет передавать без https) по умолчанию ВКЛЮЧЕНО
     * httpOnly - кука видна полько серверу, в жс не видно, по умолчанию ВКЛЮЧЕНО
     *
     * @param string $name имя куки которую ставим
     * @param string $value значение куки которое устанавливаем
     * @param array $param список параметров
     */
    public function addCookie($name, $value, $param = [])
    {
        $data = ['value'=>$value,'expire'=>0,'path'=>'/','domain'=>'','secure'=>true,'httpOnly'=>true];
        $data = array_merge($data, $param);
        $this->data[$name] = $data;
    }

    /**
     * Магический метод, получает переменную из кук
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        } else {
            return null;
        }
    }
}
