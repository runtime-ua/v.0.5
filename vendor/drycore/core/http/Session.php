<?php
namespace drycore\core\http;

/**
 * Работа с сессией (хранение переменных в сессии)
 */
class Session extends AbstractSession
{
    /**
     * @var string Имя куки для идентификатора сессии
     */
    protected $sessionName = 'session';
    
    /**
     * @var integer Срок жизни сессии
     */
    protected $maxLifeTime = 86400; // 24 hour
    
    /**
     * Признак того запущена ли уже сессия или нет
     * @var boolean
     */
    public $isStarted = false;

    /**
     * Магический метод, сохраняет переменную в сессию
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if (!$this->isStarted) {
            $this->start();
        }
        $_SESSION[$name] = $value;
    }

    /**
     * Магический метод, получает переменную из сессии
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if ($this->isStarted and isset($_SESSION[$name])) {
            return $_SESSION[$name];
        } else {
            return null;
        }
    }

    /**
     * Проверяем наличие переменной в сессии
     * @param string $name
     * @return boolean
     */
    public function __isset($name)
    {
        if ($this->isStarted) {
            return isset($_SESSION[$name]);
        } else {
            return false;
        }
    }

    /**
     * Удаляем переменную из сессии
     * @param string $name
     */
    public function __unset($name)
    {
        if ($this->isStarted) {
            unset($_SESSION[$name]);
        }
    }
 
    /**
     * Разрушаем сессию и все ее данные
     */
    public function destroy()
    {
        if ($this->isStarted) {
            session_destroy();
            $this->isStarted = false;
        }
    }

    /**
     * "обновляем" сессию
     */
    public function regenerate()
    {
        if ($this->isStarted) {
            session_regenerate_id(true);
        }
    }
}
