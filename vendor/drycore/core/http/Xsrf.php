<?php
namespace drycore\core\http;

/**
 * Компонент создающий маркер безопасности
 * Создает новый маркер и сохраняет его в сессию если у пользователя открыта сессия
 * Атака на неавторизованого пользователя бессмысленна.
 * Менять токен при каждом запросе - можно. Но поскольку бывает открыто несколько вкладок,
 * то это качественно усложняет задачу, при незначительном профите.
 */
class Xsrf extends \proto\Service
{
    /**
     * Хранит текущий маркер
     * @var string
     */
    public $marker;
    
    /**
     * Имя поля в котором мы передаем/ищем маркер
     * @var string
     */
    public $fieldName = '__xsrf';

    /**
     * Конструктор, инициируем маркер если открыта сессия
     * @param type $config
     */
    public function __construct($config = null)
    {
        parent::__construct($config);
        // Если есть сессия
        if (s()->session->isStarted) {
            // Если уже есть маркер
            if (isset(s()->session->xsrfMarker)) {
                $this->marker = s()->session->xsrfMarker;
            } else { // Если маркера еще нет
                $this->marker = md5(rand());
                s()->session->xsrfMarker = $this->marker;
            }
        }
    }

    /**
     * Проверяем что маркер передали в GET-данных
     * @throws XsrfException
     */
    public function validateGet()
    {
        // если сессия не начата, то можно не проверять - всё ок.
        if (!s()->session->isStarted) {
            return;
        }
        // если мы дошли сюда, то должен быть маркер, и он должен быть равен текущему маркеру
        if (!isset($_GET[$this->fieldName]) or ($_GET[$this->fieldName] != $this->marker)) {
            throw new XsrfException();
        }
    }

    /**
     * Проверяем что маркер передали в POST-данных
     * @throws XsrfException
     */
    public function validatePost()
    {
        // если сессия не начата, то можно не проверять - всё ок.
        // если запрос не пост, то можно не проверять - всё ок.
        if (!s()->session->isStarted or !isset($_POST)) {
            return;
        }
        // если мы дошли сюда, то должен быть маркер, и он должен быть равен текущему маркеру
        if (!isset($_POST[$this->fieldName]) or ($_POST[$this->fieldName] != $this->marker)) {
            throw new XsrfException();
        }
    }
    
    /**
     * Генерируем код скрытого поля с маркером
     * @return string
     */
    public function field()
    {
        if (!is_null($this->marker)) {
            return '<input type="hidden" name="'.$this->fieldName.'" value="'.$this->marker.'">';
        } else {
            return '';
        }
    }
}
