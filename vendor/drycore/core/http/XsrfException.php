<?php
namespace drycore\core\http;

/**
 * Исключение бросается если маркер безопасности устарел/неверен, или отсутствует
 */
class XsrfException extends \Exception
{
}
