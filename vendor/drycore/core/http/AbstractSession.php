<?php
namespace drycore\core\http;

/**
 * Сервисный функционал для Session
 *
 * @author Mendel
 */
abstract class AbstractSession extends \proto\Service
{
    /**
     * Конструктор, стартуем сессию если есть кука
     * @param type $config
     */
    public function __construct($config = null)
    {
        parent::__construct($config);
        $this->safeConfig();
        session_name($this->sessionName);
        if (isset($_COOKIE[$this->sessionName])) {
            $this->start();
        }
    }
    
    /**
     * Стартуем сессию если еще не запущена
     */
    protected function start()
    {
        if (!$this->isStarted) {
            if (session_status()!=PHP_SESSION_ACTIVE) {
                session_start();
            }
            $this->isStarted = true;
        }
        // 2DO: add check useragent
        $this->checkIp();
    }
    
    /**
     * Проверка что IP пользователя совпадает с IP из сессии
     */
    protected function checkIp()
    {
        $clientIp = $_SERVER['REMOTE_ADDR'];
        if (!isset($_SESSION['clientIp'])) {
            $_SESSION['clientIp'] = $clientIp;
        }
        if ($clientIp != $_SESSION['clientIp']) {
            $this->destroy();
        }
    }
  
    protected function safeConfig()
    {
        ini_set('session.cookie_lifetime', 0);
        ini_set('session.use_cookies', true);
        ini_set('session.use_only_cookies', true);
        if (version_compare(PHP_VERSION, '5.5.2', '>=')) {
            ini_set('session.use_strict_mode', true);
        }
        ini_set('session.cookie_httponly', true);
        ini_set('session.gc_maxlifetime', $this->maxLifeTime);
        ini_set('session.use_trans_sid', false);
        ini_set('session.cache_limiter', 'nocache');
        ini_set('session.hash_function', 'sha256');
    }
}
