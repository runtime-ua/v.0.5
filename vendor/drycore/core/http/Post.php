<?php
namespace drycore\core\http;

/**
 * Компонента для работы с пост-данными
 * Возвращает готовые модельки с нужным классом и нужными данными
 */
class Post extends \proto\Service
{
    /**
     * Проверка того, что нужные данные были отправлены
     * @param mixed $fieldName имя искомого параметра, или пусто если любые данные
     * @return boolean Истина если есть данные, Ложь в противном случае
     */
    public function posted($fieldName = null)
    {
        // Если нет массива с пост-данными
        if (!isset($_POST) or !$_POST) {
            return false;
        }
        // если он не массив
        if (!is_array($_POST)) {
            return false;
        }
        // если нам не нужно проверять конкретное поле, то проверка пройдена
        if (is_null($fieldName)) {
            return true;
        }
        // Если мы дошли сюда, то запрошенное поле не пустое, и проверяем, что
        // искомое поле существует
        if (!isset($_POST[$fieldName]) or !is_array($_POST[$fieldName])) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Получаем модель с данными или пустую модель если данных нет
     * @param string $modelName класс модели который наполнять данными
     * @param string $fieldName имя поля из которого брать данные или пусто если весь массив
     * @param boolean $danger если истина, то не проверять на XSRF. Может быть опасно
     * @return mixed искомая модель, с данными или без
     */
    public function getModel($modelName, $fieldName = null, $danger = false)
    {
        // Получим данные
        $model = model($modelName);
        $this->update($model, $fieldName, $danger);
        return $model;
    }
    
    /**
     * Обновим нашу модель данными полученными из POST
     * @param string $model модель которая обновляется/наполняется данными
     * @param string $fieldName имя поля из которого брать данные или пусто если весь массив
     * @param boolean $danger если истина, то не проверять на XSRF. Может быть опасно
     * @return boolean Истина если данные менялись, Ложь если не менялись
     */
    public function update($model, $fieldName = null, $danger = false)
    {
        if (is_null($fieldName)) {
            $fieldName = str_replace('/', '_', $model->selfType());
        }
        // Если данные есть, то получим данные, если данных нет, то вернем пустую модель
        if ($this->posted($fieldName)) {
            // Проверим на XSRF если нет маркера что можно опасные данные
            if (!$danger) {
                s()->xsrf->validatePost();
            }
            //
            $data = $this->getData($fieldName);
            $this->updateModel($model, $data);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Обновление готовой модельки готовыми данными
     * @param type $model
     * @param type $data
     */
    protected function updateModel($model, $data)
    {
        // в цикле добавим данные
        foreach ($data as $key => $value) {
            $model->$key = $value;
        }
        // выполним валидацию модели
        $model->validate();
    }


    /**
     * Возвращает данные для обработки. Основной массив если не задано поле
     * или конкретное поле, если задано. Проверки на валидность проводить
     * до вызова этого метода
     * @param mixed $fieldName имя искомого поля
     * @return array искомые данные
     */
    protected function getData($fieldName)
    {
        if (is_null($fieldName)) {
            return $_POST;
        } else {
            return $_POST[$fieldName];
        }
    }
}
