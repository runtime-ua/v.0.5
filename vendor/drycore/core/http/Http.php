<?php
namespace drycore\core\http;

/**
 * Выводим контент, с разными заголовками и т.п., переадресации, ошибки и т.п.
 */
class Http extends AbstractHttp
{
    /**
     * Выведем обычную страницу с тегами кеширования и прочими прелестями
     * @param string $content
     */
    public function show($content)
    {
        s()->cookie->sendCookie();
        $this->cacheTtl();
        $this->varyCookie();
        $etag=md5($content);
        if ($this->validByEtag($etag) or $this->validByDate()) {
            header('HTTP/1.1 304 Not Modified');
            die();
        } else {
            header('Etag: '.$etag);
            if ($this->editTime) {
                header("Last-Modified: ".gmdate("D, d M Y H:i:s", $this->editTime)." GMT");
            }
            echo $content;
        }
    }
    
    /**
     * Выведем заголовок ошибки 404
     */
    public function notFound()
    {
        header("HTTP/1.0 404 Not Found");
    }

    /**
     * Выведем заголовок ошибки прав доступа 403
     */
    public function forbidden()
    {
        header("HTTP/1.0 403 Forbidden");
    }

    /**
     * Выведем заголвок внутренней ошибки и очистим буфер вывода если актуально
     */
    public function internal()
    {
        header("HTTP/1.0 500 Internal Server Error");
        // Если уже что-то выведено в буфер, то очистим его
        if (ob_get_level()) {
            @ob_end_clean();
        }
    }

    /**
     * Переадресует по ссылке/роуту, если установлен параметр Back то пробует переадресовать
     * назад по рефереру
     * @param type $url
     * @param type $back
     * @throws Exception
     */
    public function redirect($url = null, $back = false)
    {
        s()->cookie->sendCookie();
        if ($back and isset($_SERVER['HTTP_REFERER'])) {
            $redirect = $_SERVER['HTTP_REFERER'];
        } else {
            $redirect = $this->toAbsUrl(url($url));
        }
        //
        $current = $this->toAbsUrl(url(s()->route->fields));
        //
        if ($redirect == $current) {
            $redirect = $this->toAbsUrl(url($url));
        }
        if (!is_null($redirect)) {
            header('Location: '.$redirect);
        } else {
            throw new \Exception('Unknown url redirect');
        }
    }
  
    /**
     * Приводим ссылку к абсолютному виду (для редиректа)
     * @param string $url
     * @return string
     */
    protected function toAbsUrl($url)
    {
        if (!haveStr($url, '://')) {
            if (isset($_SERVER[ 'HTTPS']) and $_SERVER[ 'HTTPS']) {
                $protocol = 'https://';
            } else {
                $protocol = 'http://';
            }
            $url = $protocol . $_SERVER['HTTP_HOST'] . $url;
        }
        return $url;
    }
}
