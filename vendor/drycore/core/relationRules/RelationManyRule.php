<?php
namespace drycore\core\relationRules;

class RelationManyRule extends \drycore\core\otherRules\VirtualRule
{
    protected function get()
    {
        $box = $this->prepareBox();
        $this->master->value = $box;
    }
    protected function callForEdit()
    {
    }
    
    // Выведем колво записей по связи, для красоты
    protected function callPretty()
    {
        $fieldName = $this->master->fieldName;
        $box = $this->master->model->$fieldName;
        $this->master->value = $box->count();
    }
    
    //
    protected function beforeSave()
    {
        $this->master->hidden = true;
    }
    
    /**
     * Если установлен параметр clone то клонируем всех из этой связи
     */
    protected function afterSave()
    {
        if (isset($this->param['clone']) and $this->master->model->isClone()) {
            // получим наще поле и его значение
            if (isset($this->param['modelField'])) {
                $modelField = $this->param['modelField'];
            } else {
                $modelField = 'id';
            }
            $value = $this->master->model->$modelField;
            // получим имя удаленного поля
            if (isset($this->param['boxField'])) {
                $boxField = $this->param['boxField'];
            } else {
                $boxField = 'id';
            }
            // Получим бокс предка из которого нужно брать объекты для клона
            $parent = box($this->master->model->selfType())->findOne($this->master->model->clonedId());
            $fieldName = $this->master->fieldName;
            $box = $parent->$fieldName;
            // Клонируем все объекты из связанного бокса
            foreach ($box->limit(false)->findAll() as $unit) {
                $child = clone $unit;
                $child->$boxField = $value;
                $child->save();
            }
        }
    }
    
    /**
     * Если есть параметр delete то при удалении удалим сначала из связи
     */
    protected function beforeDelete()
    {
        if (isset($this->param['delete']) and !$this->master->hidden) {
            $box = $this->prepareBox();
            // удалим все объекты из связанного бокса
            foreach ($box->limit(false)->findAll() as $unit) {
                $ok = $unit->delete();
//                if(!$ok) {
//                    $this->master->valid = FALSE;
//                    break; // выйти из цикла если такая печалька что не удалилось
//                }
            }
        }
    }
    
    protected function prepareBox()
    {
        // имя бокса с которым связываемся
        $boxName = $this->param['boxName'];
        // получим наще поле и его значение
        if (isset($this->param['modelField'])) {
            $modelField = $this->param['modelField'];
        } else {
            $modelField = 'id';
        }
        $value = $this->master->model->$modelField;
        //
        // получим имя удаленного поля
        if (isset($this->param['boxField'])) {
            $boxField = $this->param['boxField'];
        } else {
            $boxField = 'id';
        }
        $where = [$boxField=>$value];
        // Создадим наш бокс и настроим его соответственно
        $box = box($boxName);
        $box->where($where);
        //
        return $box;
    }
}
