<?php
namespace drycore\core\relationRules;

/**
 * Description of RuleDefaultChild
 *
 * @author Maks
 */
class NeedChildRule extends \proto\Rule
{
    /**
     * Если у нашей записи нет дочерних, то добавим
     */
    protected function afterSave()
    {
        // Узнаем сколько "детей" у нашей модели
        $model = $this->master->model;
        $fieldName = $this->master->fieldName;
        $box = $model->$fieldName;
        $count = $box->count();
        // Если меньше 1, то добавим
        if ($count < 1) {
            // получим наще поле и его значение
            if (isset($this->param['modelField'])) {
                $modelField = $this->param['modelField'];
            } else {
                $modelField = 'id';
            }
            $value = $this->master->model->$modelField;
            // получим имя удаленного поля
            if (isset($this->param['boxField'])) {
                $boxField = $this->param['boxField'];
            } else {
                $boxField = 'id';
            }
            // Получим дефолтного потомка
            // и установим его поле в наше значение чтобы указать связь
            $box = $model->$fieldName;
            $child = $box->model();
            $child->$boxField = $value;
            $child->save();
        }
    }
}
