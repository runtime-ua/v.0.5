<?php
namespace drycore\core\relationRules;

class JsonArrayRule extends \proto\Rule
{
    protected function afterLoad()
    {
        $this->master->value = json_decode($this->master->value, true);
    }

    protected function beforeSave()
    {
        $this->master->value = coolJson($this->master->value);
    }
}
