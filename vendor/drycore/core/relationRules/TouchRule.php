<?php
namespace drycore\core\relationRules;

/**
 * При сохранении дочерней записи пересохраняем родителя
 * ибо часто изменение дочерних влияет на родителей, дату их изменения,
 *
 * @author Maks
 */
class TouchRule extends \proto\Rule
{
    protected function afterSave()
    {
        $model = $this->master->model;
        $fieldName = $this->master->fieldName;
        $parent = $model->$fieldName;
        $parent->save();
    }
    protected function afterDelete()
    {
        $model = $this->master->model;
        $fieldName = $this->master->fieldName;
        $parent = $model->$fieldName;
        if ($parent) {
            $parent->save();
        }
    }
}
