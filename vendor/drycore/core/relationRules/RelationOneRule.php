<?php
namespace drycore\core\relationRules;

class RelationOneRule extends \drycore\core\relationRules\RelationManyRule
{
    protected function get()
    {
        $box = $this->prepareBox();
        $this->master->value = $box->findOne();
    }
    
    // Выведем заголовок (название) объекта по связи
    protected function callPretty()
    {
        $fieldName = $this->master->fieldName;
        $model = $this->master->model->$fieldName;
        if ($model) {
            $this->master->value = $model->title();
        }
    }
}
