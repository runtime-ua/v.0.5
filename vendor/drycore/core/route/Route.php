<?php
namespace drycore\core\route;

/**
 * Сервис отвечающий за разбор URL запроса, выбор из нее страницы, параметров и т.п.
 * Роутер для веба.
 */
class Route extends AbstractRoute
{
    /**
     * @var string контроллер для главной страницы
     */
    protected $defaultRoute = 'index:default';
    /**
     * Имя сайта добавляемое для абсолютных путей
     * @var type
     */
    protected $siteUrl;

    /**
     * Хранит текущий адрес страницы
     * @var string
     */
    protected $url;

    /**
     * Определяем текущий сайт и протокол, если не указано
     * @return string
     */
    protected function defaultSiteUrl()
    {
        $isSecure = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $isSecure = true;
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
            $isSecure = true;
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
            $isSecure = true;
        }
        $proto = $isSecure ? 'https://' : 'http://';
        return $proto . $_SERVER['HTTP_HOST'];
    }

    /**
     * Вернем текущий адрес сайта
     * @return string
     */
    public function siteUrl()
    {
        return $this->siteUrl;
    }
    
    /**
     * Инициализация модуля
     * Не вызываем из конструктора, потому что исключения в конструкторе
     * проваливают конструктор, а обработка нашего исключения потом
     * хочет живой роут.
     */
    public function init()
    {
        $this->siteUrl = $this->defaultSiteUrl();
        $this->url = $_SERVER['REQUEST_URI'];
        // Отделим URL и GET-параметры
        if (haveStr($this->url, '?')) {
            list($this->url, $param) = explode('?', $this->url, 2);
        } else {
            $param = '';
        }
        // Разберем параметры
        parse_str($param, $this->fields);
        // Если не установлен роут, то сделаем его из ссылки
        if (!isset($this->fields[0])) {
            $this->url2Route();
        }
        // Магическое изменение экшена, если указан в ссылке
        if (isset($this->fields['__action'])) {
            $this->fields[0] = $this->changeAction($this->fields[0], $this->fields['__action']);
            unset($this->fields['__action']);
        }
    }
      
    /**
     * Правила разбора роута. Возвращает роут. Опционально может добавить гет-параметры и т.п.
     */
    protected function url2Route()
    {
        if ($this->url=='/') {
            $this->fields[0] = $this->defaultRoute;
        } else {
            $this->fields[0] = substr($this->url, 1); // delete first slash
        }
    }
  
    /**
     * Делает из роута путь
     * @param array $route
     * @return string
     */
    public function route2url($route)
    {
        $url = '/'.$route[0];
        unset($route[0]);
        $param = http_build_query($route);
        if ($param) {
            $url = $url. '?'. $param;
        }
        return $url;
    }
}
