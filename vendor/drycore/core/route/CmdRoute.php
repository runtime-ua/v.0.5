<?php
namespace drycore\core\route;

/**
 * Роут для командной строки
 *
 * @author Mendel
 */
class CmdRoute extends AbstractRoute
{
    /**
     * Хранит параметры полученные из командной строки
     * @var array
     */
    protected $commandArgs;
    
    /**
     * Инициализация роута из командной строки
     * Первый параметр - роут, остальные - параметры роута
     * @throws RouteNotFoundException
     */
    public function init()
    {
        $this->commandArgs = $_SERVER['argv'];
        // Первый параметр имя скрипта
        unset($this->commandArgs[0]);
        $this->prepareRout();
        $this->prepareParam();
    }
    
    /**
     * Первый параметр - имя роута который вызываем
     * Разбираем этот параметр
     * @throw \proto\NotFoundException
     */
    protected function prepareRout()
    {
        if (isset($this->commandArgs[1])) {
            $route = $this->commandArgs[1];
            unset($this->commandArgs[1]);
            if (!haveStr($route, ':')) {
                $route = $route.':default';
            }
            $this->fields[0] = $route;
        } else {
            throw new \proto\NotFoundException();
        }
    }
    
    /**
     * Все остальные параметры это параметры роута
     * Если у параметра есть знак равно, то литерал до равно это имя параметра
     * а литерал после - значение
     * Если равно отсутствует, то значит это имя параметра, а значение - истина
     */
    protected function prepareParam()
    {
        foreach ($this->commandArgs as $arg) {
            if (haveStr($arg, '=')) {
                list($key,$value) = explode('=', $arg);
                $this->fields[$key] = $value;
            } else {
                $this->fields[$arg] = true;
            }
        }
    }
}
