<?php
namespace drycore\core\route;

/**
 * Абстрактный роут со всей неспецифичной логикой
 *
 * @author Mendel
 */
abstract class AbstractRoute extends \proto\Service
{
    /**
     * Параметры, роут
     * @var array
     */
    public $fields = [];
    
    /**
     * Инициализация модуля
     * Не вызываем из конструктора, потому что исключения в конструкторе
     * проваливают конструктор, а обработка нашего исключения потом
     * хочет живой роут.
     */
    abstract public function init();
    
    /**
     * Меняет в роуте экшн на указанный. Нужно для виртуальных действий/страниц
     * @param array $route
     * @param string $action
     */
    public function changeAction($route, $action)
    {
        list($controller,) = explode(':', $route, 2);
        return $route = $controller.':'.$action;
    }
}
