<?php
namespace drycore\core\dao\query;

/**
 * Класс генерации основных запросов
 */
class MysqlQuery extends AbstractMysqlQuery
{
    /**
     * Генерируем запрос на Инсерт, т.е. добавление новой записи в таблицу
     * @param array $data
     * @return \mysqlQuery
     */
    public function insert($data)
    {
        $this->param = []; // Чтобы дефолтные условия не мешались
        $data = $this->filterField($data);
        $set = $this->fields2set($data, 'fields_');
        $dataParam = $this->prefixParam($data, 'fields_');
        $this->param = array_merge($this->param, $dataParam);
        $sql = 'INSERT '.$this->table().' SET '.$set;
        $this->sql = $sql;
        return $this;
    }

    /**
     * Генерируем запрос на Апдейт, т.е. редактирование уже существующей записи (или записЕЙ)
     * удовлетворяющих условию  (указанному ранее)
     * @param array $data
     * @return \mysqlQuery
     */
    public function update($data)
    {
        if (!$this->where) {
            $this->where(true);
        }
        $data = $this->filterField($data);
        $set = $this->fields2set($data, 'fields_');
        $dataParam = $this->prefixParam($data, 'fields_');
        $this->param = array_merge($this->param, $dataParam);
        $where = $this->where2string();
        $sql = 'UPDATE '.$this->table().' SET '.$set.' WHERE '.$where;
        $this->sql = $sql;
        return $this;
    }

    /**
     * Генерируем запрос на удаление записи соответственно условию переданному ранее.
     * @return \mysqlQuery
     */
    public function delete()
    {
        if (!$this->where) {
            $this->where(false); // чтобы случайно не убить всех
        }
        $where = $this->where2string();
        $sql = 'DELETE FROM '.$this->table().' WHERE '.$where;
        $this->sql = $sql;
        return $this;
    }
    
    /**
     * Генерируем запрос на переименование текущей таблицы
     * @param type $newName
     * @return \drycore\dao\MysqlQuery
     */
    public function renameTable($newName)
    {
        $sql = 'RENAME TABLE '.$this->table().' TO '.$newName;
        $this->sql = $sql;
        return $this;
    }

    /**
     * Генерируем запрос на выбор всех записей удовлетворяющих условию,
     * выбирает только те поля что указаны
     * @param array $data
     * @param boolean $distinct
     * @return \mysqlQuery
     */
    public function select($data, $distinct = false)
    {
        if (!$this->where) {
            $this->where(true);
        }
        if (is_array($data)) {
            $data = $this->quoteFieldList($data);
        }
        if ($distinct) {
            $data = ' DISTINCT '.$data;
        }
        $where = $this->where2string();
        $sql = 'SELECT '.$data.' FROM '.$this->table().' WHERE '.$where;
        $this->sql = $sql;
        return $this;
    }

    /**
     * Генерирует запрос на вычисление количества записей удовлетворяющих условию
     * переданному ранее
     * @return type
     */
    public function count()
    {
        if (!$this->where) {
            $this->where(true);
        }
        $data = 'COUNT(*) as cnt';
        return $this->select($data);
    }

    /**
     * Меняем колво записей ожидаемых запросом, и номер с записи с которой начинаем данные
     * @param int $limit
     * @param int $offset
     * @return \mysqlQuery
     */
    public function limit($limit, $offset = null)
    {
        $this->sql = $this->sql . $this->limitSql($limit, $offset);
        return $this;
    }

    /**
     * Передает порядок сортировки
     * Если на входе строка, то она оборачивается в массив,
     * а в целом ожидается массив полей по которым сортируем
     * @param mixed $order
     * @return \mysqlQuery
     */
    public function order($order)
    {
        $orderSql = $this->orderSql($order);
        if ($orderSql) {
            $this->sql = $this->sql .' ORDER BY '. $orderSql;
        }
        return $this;
    }
    /**
     * Передает правила группировки
     * Если на входе строка, то она оборачивается в массив,
     * а в целом ожидается массив полей по которым группируем
     * @param mixed $group
     * @return \mysqlQuery
     */
    public function group($group)
    {
        $this->sql = $this->sql .' GROUP BY '. $this->orderSql($group);
        return $this;
    }
}
