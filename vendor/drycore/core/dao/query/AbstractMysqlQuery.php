<?php
namespace drycore\core\dao\query;

/**
 * Базовый класс для генератора запросов.
 */
class AbstractMysqlQuery extends \proto\Component
{
    /**
     * Сервис DAO который создал этот запрос, и который будет его выполнять
     * @var type
     */
    protected $db;
    /**
     * Имя таблицы
     * @var type
     */
    protected $table;
    /**
     * Список разрешенных полей
     * @var type
     */
    protected $allowedFields;
    
    /**
     * Хранит условия которые добавлялись в построитель
     * @var type
     */
    protected $where = [];

    /**
     * Хранит условия HAVING которые добавлялись в построитель
     * @var type
     */
    protected $having = [];
    
    /**
     * Параметры передаваемые с запросом (в ДАО)
     * @var type
     */
    public $param=[];
    /**
     * Уже сформированный запрос
     * @var type
     */
    public $sql='';

    /**
     * Выполним запрос и вернем результат
     * @return type
     */
    public function go()
    {
        return $this->db->go(
            $this->sql,
            $this->param
        );
    }

    /**
     * Проверим разрешенное ли поле
     * Если список разрешенных не передавался, то это ручной запрос и разрешены все
     * @param type $field
     * @return boolean
     */
    public function isAllowed($field)
    {
        if (is_null($this->allowedFields)) {
            return true;
        }
        return in_array($field, $this->allowedFields);
    }
    
    
    /**
     * Обработка условий. Условия разбираются и сохраняются в параметрах и условии
     *
     * не массив - ид
     * есть [0] - параметрицированный
     * нет - обрабатываем массив
     * Истина - меняем на всегда истинное условие
     *
     * @param string $data
     */
    public function where($data)
    {
        // Если истина, значит дадим всегда истинное условие
        if (true === $data) {
            $data = ['1=1'];
        }
        // Если ложь, значит дадим всегда ложное условие
        if (false === $data) {
            $data = ['1=2'];
        }
        // Если скаляр а не массив, то это ИД записи, и сделаем условие по ИД
        if (!is_array($data)) {
            $data = [$this->quoteId('id') .'=:id', 'id' =>$data];
        }
        // Если у массива нет записи с условием, то делаем условие из массива
        if (!isset($data[0])) {
            $data = $this->array2where($data);
        }
        // Префикс у условий содержит порядковй номер условия нумеруя с нуля
        $prefix = 'c'.count($this->where).'_';
        $this->where[] = $this->addWhere($data, $prefix);
        //
        return $this;
    }

    
    /**
     * Обработка условий HAVING . Условия разбираются и сохраняются в параметрах и условии
     *
     * не массив - ид
     * есть [0] - параметрицированный
     * нет - обрабатываем массив
     * Истина - меняем на всегда истинное условие
     *
     * @param string $data
     */
    public function having($data)
    {
        // Если истина, значит дадим всегда истинное условие
        if (true === $data) {
            $data = ['1=1'];
        }
        // Если ложь, значит дадим всегда ложное условие
        if (false === $data) {
            $data = ['1=2'];
        }
        // Если у массива нет записи с условием, то делаем условие из массива
        if (!isset($data[0])) {
            $data = $this->array2where($data);
        }
        // Префикс у условий содержит порядковй номер условия нумеруя с нуля
        $prefix = 'h'.count($this->having).'_';
        $this->having[] = $this->addWhere($data, $prefix);
        //
        return $this;
    }
    
    /**
     * Добавим условие в наш список услвоий, предварительно добавив префикс параметрам
     * @param type $data
     * @param type $prefix
     * @return type
     */
    protected function addWhere($data, $prefix)
    {
        $where = $data[0];
        unset($data[0]);
        // Перекодируем префиксы под уникальность.
        //2ДУ: переписать под замену по массиву что быстрее(?)
        $param = [];
        foreach ($data as $key => $value) {
            $param[$prefix.$key] = $value;
            $where = str_replace(':'.$key, ':'.$prefix.$key, $where);
        }
        //
        $this->param = array_merge($this->param, $param);
        return $where;
    }
    
    /**
     * Поклеим все условия в одно
     * @return type
     */
    protected function where2string()
    {
        if (!$this->where) {
            $this->where(false);
        }
        return '('.implode(') AND (', $this->where). ')';
    }

    /**
     * Делаем условия из массива.
     * @param type $data
     * @return type
     */
    protected function array2where($data)
    {
        $mylist = [];
        foreach ($data as $key => $value) {
            $mylist[] = $this->quoteId($key) .'=:'.$key;
        }
        $data[0] = implode(' AND ', $mylist);
        return $data;
    }

    /**
     * Разбираем условие сортировки в SQL.
     * @param type $orders
     * @return string
     */
    public function orderSql($orders)
    {
        if (!is_array($orders)) {
            $orders = [$orders];
        }
        $result = [];
        foreach ($orders as $line) {
            $result[] = $this->subOrder($line);
        }
        return  implode(', ', $result);
    }
    
    /**
     * Обрабатывает один элемент сортировки и по нашим правилам добавляет ему указание направления
     * @param type $order
     * @return string
     */
    protected function subOrder($order)
    {
        if ($order{0}=='!') {
            $field = substr($order, 1);
            // Если поле не разрешенное, то нафиг...
            if (!$this->isAllowed($field)) {
                return '';
            }
            $field = $this->quoteId($field);
            $result = $field.' DESC';
        } else {
            $field = $order;
            // Если поле не разрешенное, то нафиг...
            if (!$this->isAllowed($field)) {
                return '';
            }
            $field = $this->quoteId($field);
            $result = $field.' ASC';
        }
        return $result;
    }

    /**
     * Разбираем лимит и отступ в SQL
     * @param type $limit
     * @param type $offset
     * @return type
     */
    public function limitSql($limit, $offset = null)
    {
        $result = ' LIMIT ';
        if ($offset) {
            $result .= intval($offset).','.intval($limit);
        } else {
            $result .= intval($limit);
        }
        return $result;
    }

    /**
     * Отбираем из полученных полей только разрешенные и делаем
     * из них список предварительно обернув кавычками
     * @param type $fields
     * @return type
     */
    public function quoteFieldList($fields)
    {
        $mylist = [];
        foreach ($fields as $value) {
            if ($this->isAllowed($value)) {
                $mylist[] = $this->quoteId($value);
            }
        }
        return implode(' , ', $mylist);
    }

    
    /**
     * Делаем из массива подготовленный список полей с именами параметров вместо значений
     * для передачи потом в запрос.
     * $fields = ['name'=>'namedata','value'=>'valuedata']
     * return: `name` = :prefixname, `value` = :prefixvalue
     *
     * @param type $fields
     * @param type $prefix
     * @return type
     */
    public function fields2set($fields, $prefix = '')
    {
        $mylist = [];
        foreach ($fields as $key => $value) {
            $mylist[] = $this->quoteId($key) .'=:'.$prefix.$key;
        }
        return implode(' , ', $mylist);
    }

    /**
     * Добавляет к именам параметров указанный префикс
     * @param array $data
     * @param string $prefix
     * @return array
     */
    public function prefixParam($data, $prefix)
    {
        $out = [];
        foreach ($data as $key => $value) {
            $out[$prefix.$key] = $value;
        }
        return $out;
    }
    
    /**
     * Оборачиваем имя поля в кавычки.
     * @param type $id
     * @return type
     */
    public function quoteId($id)
    {
        return "`".$id."`";
    }

    /**
     * Фильтруем список данных на предмет того чтобы только разрешенные поля были
     * @param type $data
     * @return boolean
     */
    public function filterField($data)
    {
        if (is_object($data) or is_array($data)) {
            $result = [];
            foreach ($data as $key => $value) {
                if ($this->isAllowed($key)) {
                    $result[$key] = $value;
                }
            }
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Обернутое кавычками название таблицы :)
     * @return type
     */
    public function table()
    {
        return $this->quoteId($this->table);
    }
}
