<?php
namespace drycore\core\dao;

/**
 * Итератор по результатам запроса.
 * публичное свойство ОК содержит булево значение удачности запроса.
 * Метод one - сахар для запросов когда ожидается только один резульат.
 */
class DbResult extends \proto\Component implements \Iterator
{
    /**
     * Ключ нашего итератора
     * @var type
     */
    protected $key;
    
    /**
     * Текущее значение
     * @var type
     */
    protected $current;

    /**
     * Результат запроса переданный нам при инициализации
     * @var type
     */
    protected $statement;
    
    /**
     * Результат ок, или не ок)
     * @var type
     */
    protected $ok;
    
    /**
     * Имя модели, если возвращаем модели
     * Если FALSE, то возвращается как массив
     * @var type
     */
    public $modelName = false;

    /**
     * Возвращает текущее поле
     * @return type
     */
    public function current()
    {
        return $this->current;
    }

    /**
     * Перемотка на следующую запись
     */
    public function next()
    {
        $this->key++;
    }

    /**
     * Получим текущий ключ
     * @return type
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * Проверяем не вылезли ли за пределы
     * @return boolean
     */
    public function valid()
    {
        $this->current = $this->getRecord($this->key);
        if (false === $this->current) { // (!array()) == True
            return false;
        } else {
            return true;
        }
    }

    /**
     * Получаем отдельную запись, и если это уместно - выполняем манипуляции после загрузки
     * @param type $key
     * @return type
     */
    protected function getRecord($key)
    {
        $record = $this->statement->fetch(
            \PDO::FETCH_ASSOC,
            \PDO::FETCH_ORI_ABS,
            $key
        );
        if ($record and $this->modelName) {
            $model = model($this->modelName);
            $model->load($record);
            return $model;
        } else {
            return $record;
        }
    }

    /**
     * Перемотка в начало
     */
    public function rewind()
    {
        $this->key = 0;
    }

    /**
     * Получим одну запись (в идеале единственную)
     * @return type
     */
    public function one()
    {
        $this->rewind();
        $this->valid();
        $obj = $this->current();
        $this->statement->closeCursor();
        return $obj;
    }
    
    /**
     * Возвращает статус запроса
     * @return type
     */
    public function ok()
    {
        return $this->ok;
    }
}
