<?php
namespace drycore\core\dao;

/**
 * Description of box
 *
 * @author Admin
 */
class DaoBox extends \drycore\sys\box\AbstractBox
{
    protected $table;
    protected $dbServiceName = 'db';
    protected $db;
    public $lastResult;
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        $dbServiceName = $this->dbServiceName;
        $this->db = s()->$dbServiceName;
    }
    
    public function query()
    {
        if (!$this->table) {
            $this->table = $this->defaultTable();
        }
        $query =  $this->db->query(['table'=>$this->table, 'allowedFields'=>$this->fieldsList()]);
        foreach ($this->where as $where) {
            $query->where($where);
        }
        return $query;
    }

    private function defaultTable()
    {
        $name = $this->selfType();
        $name = str_replace('/', '_', $name);
        return $name;
    }

    public function insert($data)
    {
        $query = $this->query();
        $query->insert($data);
        $result = $this->lastResult = $query->go();
        if ($result->ok()) {
            return $this->db->insId();
        } else {
            return false;
        }
    }
    
    public function update($data, $where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $query = $this->query();
        $query->update($data);
        $result = $this->lastResult = $query->go();
        return $result->ok();
    }

    public function delete($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $query = $this->query();
        $query->delete();
        $result = $this->lastResult = $query->go();
        return $result->ok();
    }

    public function select($data, $where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $query = $this->query();
        $query->select($data, $this->distinct);
        if ($this->group) {
            $query->group($this->group);
        }
        if ($this->having) {
            $query->having($this->having);
        }
        if (is_null($this->order)) {
            $this->defaultOrder();
        }
        if ($this->order) {
            $query->order($this->order);
        }
        if (!($this->limit === false)) {
            $query->limit($this->limit, $this->offset);
        }
        $result = $this->lastResult = $query->go();
        //
        if ($result->ok()) {
            return $result;
        } else {
            return false;
        }
    }

    public function count($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $query = $this->query();
        $query->count();
        $result = $this->lastResult = $query->go();
        $result = $result->one();
        return (int) $result['cnt'];
    }
    
    public function findOne($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $this->limit = 1;
        $result = $this->select($this->fields);
        if ($result) {
            $result->modelName = $this->selfType();
            return $result->one();
        } else {
            return false;
        }
    }
    
    public function findAll($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $result = $this->select($this->fields);
        $result->modelName = $this->selfType();
        $result = iterator_to_array($result);
        return $result;
    }
}
