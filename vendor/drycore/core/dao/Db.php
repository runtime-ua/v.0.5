<?php
namespace drycore\core\dao;

/**
 * Инициирует ПДО, выполняет запрос с параметрами.
 * Результат возвращает в виде DbResult.
 * DbResult - итератор
 */
class Db extends \proto\Service
{
    /**
     * Определяет использовать ли механизм транзакций
     * @var type
     */
    protected $needTransaction = true;
    
    /**
     * ID транзакции для логирования и т.п.
     * @var type
     */
    public $transactionId = null;
    /**
     * Хранит экземпляр ПДО инициированный при создании
     * @var type
     */
    protected $pdo;
    
    /**
     * Строка доступа к базе (параметр для ПДО)
     * @var type
     */
    protected $dsn;
    
    /**
     * Имя пользователя базы, если уместно
     * @var type
     */
    protected $user = 'root';
    
    /**
     * Имя хоста
     * @var type
     */
    protected $host = 'localhost';

    /**
     * Имя базы
     * @var type
     */
    protected $dbname = 'test';

    /**
     * Пароль доступа к базе если уместно
     * @var type
     */
    protected $password = '';
    
    /**
     * Настройки передаваемые в ПДО
     * @var type
     */
    protected $options = [\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''];

    /**
     * Имя класса построителя запросов для нашей базы
     * @var type
     */
    protected $queryClass = '\drycore\core\dao\query\MysqlQuery';

    /**
     * Конструктор, создает и инициирует экземпляр ПДО-соединения
     * @param type $config
     */
    public function __construct($config = null)
    {
        parent::__construct($config);
        if (is_null($this->dsn)) {
            $this->dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname.';charset=UTF8';
        }
        $this->pdo = new \PDO($this->dsn, $this->user, $this->password, $this->options);
        // бросаем исключения при проблемах
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Возвращает ИД последней вставки если драйвер поддерживает
     * В идеале наверное унести в Query
     * @return type
     */
    public function insId()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * Вспомогательный метод для GO. Добавляет к именам параметров двоеточие)
     * @param type $param
     * @return type
     */
    protected function prepareParam($param)
    {
        $result = [];
        foreach ($param as $key => $value) {
            $key = ':' . (string) $key;
            $value = (string) $value;
            $result[$key] = $value;
        }
        return $result;
    }

    /**
     * Основной метод. Получает запрос с параметрами, возвращает результат.
     * @param type $sql
     * @param type $param
     * @return \DbResult
     */
    public function go($sql, $param = null)
    {
        if (defined('DEBUG')) {
            s()->log->sql($sql, $param);
        }
        $statement = $this->pdo->prepare($sql, [\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL]);
        $statement->setFetchMode(\PDO::FETCH_ASSOC);
        if ($param) {
            $param = $this->prepareParam($param);
            $executeResult = $statement->execute($param);
        } else {
            $executeResult = $statement->execute();
        }
        return new DbResult([
            'statement'=>$statement,
            'ok'=>$executeResult
        ]);
    }

    /**
     * Вернем построитель запросов характерный для данной базы
     * @param type $config
     * @return \Component
     */
    public function query($config)
    {
        $config['db'] = $this;
        $class = $this->queryClass;
        return new $class($config);
    }
    
    /**
     * Начнем транзакцию
     * @return boolean
     */
    public function beginTransaction()
    {
        if (!is_null($this->transactionId)) {
            return false;
        }
        $this->transactionId = randId();
        if ($this->needTransaction) {
            $result = $this->pdo->beginTransaction();
        } else {
            $result = true;
        }
        return $result;
    }
    
    /**
     * Завершим транзакцию и сохраним все изменения
     * @return boolean
     */
    public function commit()
    {
        if (is_null($this->transactionId)) {
            return false;
        }
        $this->transactionId = null;
        if ($this->needTransaction) {
            $result = $this->pdo->commit();
        } else {
            $result = true;
        }
        return $result;
    }
    
    /**
     * Прервем транзакцию и откатим все изменения
     * @return boolean
     */
    public function rollBack()
    {
        if (is_null($this->transactionId)) {
            return false;
        }
        $this->transactionId = null;
        if ($this->needTransaction) {
            $result = $this->pdo->rollBack();
        } else {
            $result = true;
        }
        return $result;
    }
}
