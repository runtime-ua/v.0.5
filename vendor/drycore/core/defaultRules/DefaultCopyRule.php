<?php
namespace drycore\core\defaultRules;

  /**
   * Правило устанавливающее значение по умолчанию равное другому полю
   * Если значение равно NULL то устанавливаем значение по умолчанию
   * Параметр field: имя поля значение которое нужно установить
   */
class DefaultCopyRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $field = $this->param['field'];
            $this->master->value = $this->master->model->$field;
        }
    }
    protected function callForEdit()
    {
    }
}
