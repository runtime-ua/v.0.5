<?php
namespace drycore\core\defaultRules;

  /**
   * Правило устанавливающее значение по умолчанию равное другому полю
   * Если значение равно NULL то устанавливаем значение по умолчанию
   * Параметр field: имя поля значение которое нужно установить
   * При копировании поле очищается от тегов и урезается до максимальной длины
   */
class DefaultCopyStrictRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $field = $this->param['field'];
            $value = $this->master->model->$field;
            $value = strip_tags($value);
            $value = trim($value);
            $maxlen = $this->param['maxlen'];
            $value = mb_substr($value, 0, $maxlen);
            $this->master->value = $value;
        }
    }
    protected function callForEdit()
    {
    }
}
