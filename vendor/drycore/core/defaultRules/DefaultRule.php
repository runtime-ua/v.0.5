<?php
namespace drycore\core\defaultRules;

  /**
   * Правило устанавливающее значение по умолчанию
   * Если значение равно NULL то устанавливаем значение по умолчанию
   * Параметр value: значение которое нужно установить
   */
class DefaultRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $this->param['value'];
        }
    }
    protected function callForEdit()
    {
    }
}
