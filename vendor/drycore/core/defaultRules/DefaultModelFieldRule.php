<?php
namespace drycore\core\defaultRules;

  /**
   * Правило устанавливающее значение по умолчанию полученное из "Реестра"
   * Если значение равно NULL то устанавливаем значение по умолчанию
   * Параметр modelName: имя модели из которой берем поле
   * Параметр fieldName: имя поля модели реестра значение которое нужно установить
   */
class DefaultModelFieldRule extends \proto\Rule
{

    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $model = findModel($this->param['modelName']);
            $fieldName = $this->param['fieldName'];
            $this->master->value = $model->$fieldName;
        }
    }
    protected function callForEdit()
    {
    }
}
