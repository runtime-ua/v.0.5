<?php
namespace drycore\core\defaultRules;

  /**
   * Правило устанавливающее значение по умолчанию ПРИ РЕДАКТИРОВАНИИ
   * мы не сохраняем его, не используем его, только при редактировании
   * Параметр value: значение которое нужно установить
   */
class DefaultEditRule extends \proto\Rule
{
    protected function callForEdit()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $this->param['value'];
        }
    }
}
