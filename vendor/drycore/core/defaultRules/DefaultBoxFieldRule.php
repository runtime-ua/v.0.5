<?php
namespace drycore\core\defaultRules;

/**
 * Правило.
 * При отсутствии значения - находит запись удовлетворяющюю условию и возвращает ее поле
 * Параметры:
 * fieldName - имя поля которое берем. По умолчанию id
 * boxName - имя бокса в котором ищем, по умолчанию path
 * where - условие по которому ищем
 */
class DefaultBoxFieldRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            if (isset($this->param['boxName'])) {
                $boxName = $this->param['boxName'];
            } else {
                $boxName = 'drycore/web/path';
            }
            //
            $box = box($boxName);
            $unit = $box->findOne($this->param['where']);
            //
            if (isset($this->param['fieldName'])) {
                $field = $this->param['fieldName'];
            } else {
                $field = 'id';
            }
            //
            if ($unit) {
                $this->master->value = $unit->$field;
            }
        }
    }
    protected function beforeSave()
    {
        $this->get();
    }
    protected function callForEdit()
    {
    }
}
