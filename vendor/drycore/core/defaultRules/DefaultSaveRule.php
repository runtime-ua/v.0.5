<?php
namespace drycore\core\defaultRules;

  /**
   * Правило устанавливающее значение по умолчанию и СОХРАНЯЮЩЕЕ его
   * Если значение равно NULL то устанавливаем значение по умолчанию
   * Параметр value: значение которое нужно установить
   */
class DefaultSaveRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $this->param['value'];
        }
    }
    protected function beforeSave()
    {
        $this->get();
    }
    protected function callForEdit()
    {
    }
}
