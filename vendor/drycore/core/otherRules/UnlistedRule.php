<?php
namespace drycore\core\otherRules;

/**
 * Сообщим что поле хоть и разрешенное, но не нужно его проходить в итераторах
 */
class UnlistedRule extends \proto\Rule
{
    protected function listed()
    {
        $this->master->value = false;
    }
}
