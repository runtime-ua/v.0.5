<?php
namespace drycore\core\otherRules;

class ModelTypeRule extends \proto\Rule
{
    protected function callPretty()
    {
        $this->master->value = $this->master->model->selfName();
    }
    protected function afterLoad()
    {
        $this->master->model->selfType($this->master->value);
    }
    protected function beforeSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $this->master->model->selfType();
        }
    }
}
