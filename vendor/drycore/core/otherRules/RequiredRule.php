<?php
namespace drycore\core\otherRules;

  /**
   * Правило Required требующее чтобы поле обязательно было заполнено (не NULL)
   * параметры не предусмотренны
   */
class RequiredRule extends \proto\Rule
{
    protected function validate()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            if (isset($this->param['errMsg'])) {
                $error = $this->param['errMsg'];
            } else {
                $error = $this->lang('validateRequiredError');
            }
            $this->master->error[] = $error;
            $this->master->valid = false;
        }
    }
}
