<?php
namespace drycore\core\otherRules;

/**
 * Устанавливает случайный ИД
 */
class RandIdRule extends \proto\Rule
{
    protected function get()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $value = randId();
            $field = $this->master->fieldName;
            $this->master->value = $value;
            $this->master->model->$field = $value;
        }
    }
    /**
     * При клонировании обнулим значение
     */
    protected function afterClone()
    {
        $value = randId();
        $field = $this->master->fieldName;
        $this->master->value = $value;
        $this->master->model->$field = $value;
    }
}
