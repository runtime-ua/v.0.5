<?php
namespace drycore\core\otherRules;

  /**
   * Правило устанавливающее значение равное другому полю.
   * ДЕЙСТВУЕТ ВСЕГДА А НЕ ТОЛЬКО ПРИ ДЕФОЛТЕ
   * Параметр field: имя поля значение которое нужно установить
   */
class CopyRule extends \proto\Rule
{
    protected function beforeSave()
    {
        $field = $this->param['field'];
        $this->master->value = $this->master->model->$field;
    }
}
