<?php
namespace drycore\core\otherRules;

/**
 * Description of FilteredModelTypeRule
 *
 * @author Maks
 */
class FilteredModelTypeRule extends ModelTypeRule
{
    protected function boxFilter()
    {
        $fieldName = $this->master->fieldName;
        $value = $this->master->box->selfType();
        $this->master->value = [$fieldName => $value];
    }
    protected function beforeSave()
    {
        if (is_null($this->master->value) or $this->master->value ==='') {
            $this->master->value = $this->master->model->selfType();
        }
    }
}
