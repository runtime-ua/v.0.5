<?php
namespace drycore\core\otherRules;

/**
 * Правило устанавливает в поле значение
 * Параметр value: значение которое нужно установить
 */
class SetValueRule extends \proto\Rule
{
    protected function beforeSave()
    {
        $this->master->value = $this->param['value'];
    }
}
