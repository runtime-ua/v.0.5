<?php
namespace drycore\core\otherRules;

  /**
   * Правило - признак виртуальности поля, которое не сохраняется и т.п.
   */
class VirtualRule extends \proto\Rule
{
    protected function beforeSave()
    {
        $this->master->hidden = true;
    }
}
