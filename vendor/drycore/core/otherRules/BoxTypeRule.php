<?php
namespace drycore\core\otherRules;

class BoxTypeRule extends \proto\Rule
{
    protected function boxType()
    {
        $value = $this->param['value'];
        $this->master->value = $value;
    }
}
