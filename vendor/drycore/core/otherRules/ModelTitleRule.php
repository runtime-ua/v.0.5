<?php
namespace drycore\core\otherRules;

/**
 * Имя экземпляра модели назначаем текущее поле
 * @author Maks
 */
class ModelTitleRule extends \proto\Rule
{
    public function callTitle()
    {
        $this->master->value = $this->master->model[$this->master->fieldName];
    }
}
