<?php
namespace drycore\core\otherRules;

  /**
   * Правило проверяет уникальность поля в базе
   * param: fields - список полей которые тоже должны совпадать чтобы считать совпадением
   */
class UniqueRule extends \proto\Rule
{
    protected function validate()
    {
        $box = $this->master->model->box();
        // Если есть список дополнительных полей, то добавим их в условие
        if (isset($this->param['fields'])) {
            $fields = tags2array($this->param['fields']);
            $where = [];
            foreach ($fields as $f) {
                $where[$f] = $this->master->model->$f;
            }
            $box->where($where);
        }
        // Если мы уже сохранялись, то совпадение с самим собой не считается
        if ($this->master->model->isBoxed()) {
            $box = $box->where(['id<>:id', 'id'=>$this->master->model->id]);
        }
        // Добавим в проверку основное поле и посчитаем таки сколько записей
        $fieldName = $this->master->fieldName;
        $count = $box->count([$fieldName=>$this->master->value]);
        if ($count > 0) {
            if (isset($this->param['errMsg'])) {
                $error = $this->param['errMsg'];
            } else {
                $error = $this->lang('validateUniqueError');
            }
            $this->master->error[] = $error;
            //
            $this->master->valid = false;
        }
    }
}
