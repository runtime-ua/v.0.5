<?php
namespace drycore\core\otherRules;

  /**
   * Правило проверяющее равенство поля другому полю той же модели
   * Параметр field: имя поля с которым сравниваем
   */
class CompareRule extends \proto\Rule
{
    protected function validate()
    {
        $field = $this->param['field'];
        $valid = ($this->master->value == $this->master->model->$field);
        if (!$valid) {
            if (isset($this->param['errMsg'])) {
                $error = $this->param['errMsg'];
            } else {
                $error = $this->lang('validateCompareError');
            }
            $this->master->error[] = $error. $field;
            $this->master->valid = false;
        }
    }
}
