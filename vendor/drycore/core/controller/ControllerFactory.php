<?php
namespace drycore\core\controller;

/**
 * Класс отвечающий за вызов контроллера
 * Находит нужный класс, передает ему все настройки и т.п.
 */
class ControllerFactory extends \proto\ConfigurableService
{
    /**
     * Здесь у нас указывается контекст в котором мы запустились
     * @var string
     */
    protected $contextFieldName = 'actions';
    
    /**
     * Текущий контроллер
     * @var type
     */
    public $lastController;

    /**
    * Вызов контроллера
    */
    public function startController($param = null)
    {
        // Если нам не передавали параметры/роут, то берем из роутера
        if (is_null($param)) {
            $param = s()->route->fields;
        }
        // Если передали строкой то обернем в массив
        if (!is_array($param)) {
            $param = [$param];
        }
        // получим имя роута чтобы на его основании вызвать контроллер
        $route = $param[0];
        // Если нет экшена, то добавим дефолтный
        if (!haveStr($route, ':')) {
            $route = $route .':default';
        }
        // Разберем на контроллер и экшн
        list($name, $action) = explode(':', $route, 2);
        // получим конфиг с учетом контекста
        $config = $this->config($name);
        $context = $this->context($name, $action);
        $config = array_merge($config, $context);
        // Узнаем какой класс вызывать
        if (isset($config['#class'])) {
            $class = $config['#class'];
            unset($config['#class']);
        } else {
            throw new ControllerNotFoundException($route);
        }
        // Добавим параметры, экшн и т.п. в конфигурацию
        $config['param'] = $param;
        $config['action'] = $action;
        // Вызываем наш класс и его экшн
        $controller = new $class($config);
        $this->lastController = $controller;
        $controller->action();
    }
}
