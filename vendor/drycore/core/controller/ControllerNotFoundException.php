<?php
namespace drycore\core\controller;

/**
 * Исключение выбрасываемое если:
 * нет класса в конфиге,
 * нет класса имя которого указано
 * в нашем классе нет экшена который нужен
 */
class ControllerNotFoundException extends \Exception
{
}
