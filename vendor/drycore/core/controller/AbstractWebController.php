<?php
namespace drycore\core\controller;

/**
 * Базовый контроллер для WEB, содержит сахар для вывода страниц и т.п.
 *
 * @author Mendel
 */
class AbstractWebController extends \proto\Controller
{
    use RedirectTrait;
    
    /**
     * @var array Внутреннее хранилище для переменных выводимых в браузер
     */
    private $viewVar = [];
    
    /**
     * @var type Основное содержимое страницы для вывода в layout
     */
    public $layoutContent;
    
    /**
     * Текущий layout
     * @var type
     */
    protected $layoutName;
    
    /**
     * Шаблон страницы для экшена
     * @var type
     */
    protected $viewName;
    
    /**
     * Инициируем вывод на страницу переданными данными
     * @param array $pageData
     */
    protected function initShow($pageData = [])
    {
        if (is_a($pageData, '\proto\Model')) {
            $pageData = $pageData->modelToArray();
        }
        $this->viewVar = array_merge($this->viewVar, $pageData);
    }
    
    /**
     * Выведем переменную во вьюв
     * @param type $name
     * @param type $value
     */
    protected function showVar($name, $value)
    {
        $this->viewVar[$name] = $value;
    }
    
    /**
     * Заканчиваем работу и выводим все что у нас подготовленно в браузер
     */
    protected function show()
    {
        $content = view($this->viewName, $this->viewVar);
        $this->layoutContent = $content;
        $layout = view($this->layoutName, $this->viewVar);
        s()->http->show($layout);
    }
}
