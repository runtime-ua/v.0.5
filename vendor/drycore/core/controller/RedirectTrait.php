<?php
namespace drycore\core\controller;

/**
 * Сахар для удобства редиректа из контроллера
 *
 * @author Mendel
 */
trait RedirectTrait
{
    /**
     * Конфиг с правилами редиректа
     * @var type
     */
    protected $redirect = false;
    /**
     * Конфиг с дефолтными правилами редиректа, если назад не получилось
     * @var type
     */
    protected $redirectBack = false;

    /**
     * Редиректим куда в конфиге указано, а если не указано, то в индекс или бек в индекс...
     */
    protected function redirect()
    {
        $route = $this->redirectBack;
        if ($route) {
            // Если это банальный TRUE то слать надо, но к себе в корень
            if ($route === true) {
                redirect([$this->selfType()], true);
            }
            redirect($route, true);
            return;
        }
        $route = $this->redirect;
        if ($route) {
            redirect($route);
            return;
        }
        // Если нет ни одного правила, то шлем на свой индекс
        redirect([$this->selfType()]);
    }
}
