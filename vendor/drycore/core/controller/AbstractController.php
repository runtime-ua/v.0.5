<?php
namespace drycore\core\controller;

/**
 * Базовый класс для контроллеров
 * @author Admin
 */
abstract class AbstractController extends \proto\Component
{
    /**
     *  Параметры адресной/командной строки или сгенеренные роутом
     * @var array
     */
    protected $param;
    
    /**
     * Имя вызванного экшена
     * @var type
     */
    public $action;
    
    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'controller';
    }
    
    /**
     * Выполним наш экшн
     */
    public function action()
    {
        $method = $this->action . 'Action';
        // Если у нас нет такого метода, то грохнем исключение
        if (!method_exists($this, $method)) {
            throw new ControllerNotFoundException($this->selfType().':'.$this->action);
        }
        $this->$method();
    }

    /**
     * Возвращает параметр специфичный для соответствующего экшена, или общее значение если специфичного нет
     * Если не массив, то распространяется на всех
     * Если ничего не указано, то возвращает дефолт
     * 2DO: Переделать под более универсальный формат конфига
     * @param type $name имя конфиг-массива
     * @param type $default значение по умолчанию, если конфига нет
     * @return type
     */
    protected function actionConfig($name, $default = null)
    {
        $conf = $this->$name;
        if (!is_array($conf)) {
            return $conf;
        }
        if (isset($conf[$this->action]) and !is_null($conf[$this->action])) {
            return $conf[$this->action];
        }
        if (isset($conf[0])) {
            return $conf[0];
        }
        return $default;
    }
}
