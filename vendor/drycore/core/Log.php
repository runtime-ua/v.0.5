<?php
namespace drycore\core;

/**
 * Сохраняем логи и отладочную информацию. Жуткий легаси,
 * переделать нафиг, даже документировать не буду, но лучше так чем вообще
 * без логов.
 */
class Log extends \proto\Service
{
    /**
     * Имя файла лога ошибок
     * @var string 
     */
    protected $errLog = RUNTIME_FOLDER.'/err.log';
    
    /**
     * Имя файла лога предупреждений
     * @var string 
     */
    protected $noticeLog = RUNTIME_FOLDER.'/notice.log';
    
    /**
     * Имя файла лога SQL-запросов
     * @var string 
     */
    protected $sqlLog = RUNTIME_FOLDER.'/sql.log';
    
    /**
     * Конструктор
     * @param array $config
     */
    public function __construct($config = null)
    {
        parent::__construct($config);
        // удалим "отладочные" логи
        if (defined('DEBUG')) {
            if (file_exists($this->sqlLog)) {
                unlink($this->sqlLog);
            }
        }
    }

    /**
     * Сохраним предупреждение
     * @param string $file
     * @param string $line
     * @param string $message
     */
    public function notice($file, $line, $message)
    {
        $file = str_replace(HOME, 'home:', $file);
        $msg = $file.' : '.$line .' : '.$message;
        $this->writeDated($this->noticeLog, $msg);
    }
    
    /**
     * Сохраним исключение
     * @param \Exception $data
     */
    public function exception($data)
    {
        $msg = 'EXEPTION: '
                .  get_class($data).':'
                .'('. $data->getFile().':'.$data->getLine() .')'
                .' '.$data->getMessage();
        $this->writeDated($this->errLog, $msg);
    }
    
    /**
     * Сохраним SQL-запрос
     * @param string $sql
     * @param array $param
     */
    public function sql($sql, $param)
    {
        $msg = $sql. ' **** '. json_encode($param, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        $this->writeLog($this->sqlLog, $msg);
    }

    /**
     * Запишем сообщение в файл, префиксировав датой и временем
     * @param string $fileName
     * @param string $msg
     */
    protected function writeDated($fileName, $msg)
    {
        $datetime = strftime("%d/%m/%Y %H:%M:%S", time());
        $msg = "$datetime'".": $msg";
        $this->writeLog($fileName, $msg);
    }
    
    /**
     * Сохраняем уже готовое сообщение в файл
     * @param string $fileName
     * @param string $msg
     */
    protected function writeLog($fileName, $msg)
    {
        $save_path = $fileName;
        $fp = @fopen($save_path, 'a'); // open or create the file for writing and append info
        fputs($fp, "$msg\n"); // write the data in the opened file
        fclose($fp); // close the file
    }
}
