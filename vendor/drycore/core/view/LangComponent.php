<?php
namespace drycore\core\view;

/**
 * Сахар для вьюва. Возвращает языковые данные объекта (переданного вьюва)
 *
 * @author Mendel
 */
class LangComponent extends \proto\Component
{
    /**
     * Основной объект от которого мы берем языковые данные
     * @var \proto\View
     */
    protected $mainObj;

    /**
     * Возвращаем языковую переменную по имени поля)
     * @param string $name
     * @return string
     */
    public function __get($name)
    {
        return $this->mainObj->lang($name);
    }
}
