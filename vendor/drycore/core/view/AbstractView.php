<?php
namespace drycore\core\view;

/**
 * Основной "типовой" Вьюв. Для голых шаблонов подставляют его, или переопределяют
 */
abstract class AbstractView extends \proto\Component
{
    use TraitGetSet, TraitRender;
    
    /**
     * Конструктор
     * @param array $config
     */
    public function __construct($config = [])
    {
        // Если указан шаблон, то отразим его
        if (isset($config['#template'])) {
            $this->template($config['#template']);
            unset($config['#template']);
        };
        parent::__construct($config);
    }
    
    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'view';
    }
    
    /**
     * Вернем готовый (или отрендерим, если еще не готовый) контент вьюва
     */
    public function __toString()
    {
        return $this->contentStr;
    }
    
    /**
     * Действие выполняемое когда данные уже передали
     * вызывается из хелпера view(). Переопределять с целью выполнения манипуляций с данными которые
     * могут понадобится кому-то другому, вне нашего класса, во время после инициализации
     * но до рендеринга (см. пагинацию для примера)
     */
    public function init()
    {
        $this->contentStr = $this->render();
    }
}
