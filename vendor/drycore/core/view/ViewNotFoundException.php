<?php
namespace drycore\core\view;

/**
 * Исключение выбрасываемое при отсутствии искомого вьюва
 * (точнее шаблона, ибо при отсутствии вьюва мы подставляем дефолтный с одноименным шаблоном)
 */
class ViewNotFoundException extends \Exception
{
}
