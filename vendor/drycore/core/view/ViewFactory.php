<?php
namespace drycore\core\view;

/**
 * Фабрика вьювов
 */
class ViewFactory extends \proto\ConfigurableService
{
    /**
     * Создает новый вьюв соответствующего типа
     * @param string $name
     * @return \proto\View
     * @throws ViewNotFoundException
     */
    public function factory($name)
    {
        $config = $this->config($name);
        // Узнаем какой класс вызывать
        if (isset($config['#class'])) {
            $class = $config['#class'];
            unset($config['#class']);
        } else {
            throw new ViewNotFoundException($name);
        }
        //
        $view = new $class($config);
        return $view;
    }
}
