<?php
/**
 * возвращает подготовленный вьюв, если передали данные, то "впихивает" их во вьюв
 * @param string $name
 * @param mixed $data
 * @param boolean $needInit
 * @return \proto\View
 * @throws \drycore\core\view\ViewNotFoundException
 */
function view($name, $data = [], $needInit = true)
{
    if (!$name) {
        throw new \drycore\core\view\ViewNotFoundException('Bad view name');
    }
    $view = s()->viewFactory->factory($name);
    if (is_array($data)) {
        foreach ($data as $key => $value) {
            $view->$key = $value;
        }
    }
    if (is_a($data, '\proto\Model')) {
        foreach ($data->fieldsList() as $key) {
            $value = $data->$key;
            $view->$key = $value;
        }
    }
    if ($needInit) {
        $view->init();
    }
    return $view;
}

/**
 * Если массив, то приводим к строке каждый элемент и конкатенируем результаты,
 * если не массив - тупо возвращаем значение приведенное к строке
 * @param mixed $data
 * @return string
 */
function show($data = null)
{
    $result = '';
    if (is_array($data)) {
        foreach ($data as $line) {
            $result = $result . $line;
        }
    } else {
        $result = (string) $data;
    }
    return $result;
}

/**
 * На входе получает массив ИМЕН вьювов, на выходе возвращает результат рендеринга их всех
 * @param array $data
 * @return string
 */
function showViews($data = [])
{
    $result = '';
    foreach ($data as $viewName) {
        $view = view($viewName);
        $result = $result . $view;
    }
    return $result;
}

/**
 * Создает по вьюву для каждого элемента переданного массива, обрабатывает через него и возвращает
 * все результаты. Т.е. применяет указанный вьюв для всех элементов массива.
 * Передает данные как ОДНУ ПЕРЕМЕННУЮ $model
 * @param string $viewName
 * @param \proto\Model $data
 * @return string
 */
function showModels($viewName, $data)
{
    $result = '';
    //
    foreach ($data as $model) {
        $result = $result . view($viewName, ['model'=>$model]);
    }
    return $result;
}

/**
 * Сахар для всяких Актив, селектед и т.п. Если условие верно, то выводим текст, или пусто если неверно
 * @param boolean $cond
 * @param string $text
 * @return string
 */
function showIf($cond, $text)
{
    return $cond ? $text : '';
}

/**
 * Выводит блоки описанные в моделях
 * Находит все модели из соответствующей "коробки", чьи ИД присутствуют в массиве
 * Создается вьюв тип которого указан в свойстве viewName
 * Вся остальная модель передается вьюву в качестве параметров
 * @param array $idsList
 * @param string $boxName
 */
function showBlocks($idsList, $boxName)
{
    $result = '';
    //
    foreach ($idsList as $id) {
        // получим модельку
        $model = findModel($id.'@'.$boxName);
        // применим все массивы ассетсов
        $assetTypes = [
            "mainCss","headCss","footerCss","mainJs","headJs","footerJs"
        ];
        foreach ($assetTypes as $assetType) {
            if ($model->$assetType) {
                foreach ($model->$assetType as $value) {
                    asset($assetType, $value);
                }
            }
        }
        // Применим все текстовые скалярные ассетсы
        $assetTypes = [
            "headStyle","footerStyle","headScript","footerScript"
        ];
        foreach ($assetTypes as $assetType) {
            asset($assetType, $model->$assetType);
        }
        // выведем соответствующий вьюв
        $result = $result . view($model->viewName, $model);
    }
    return $result;
}

//************************************************************************************
// Функции "имуществ", т.е. всяких зависимостей, файлов, ресурсов и т.п.
// 2DO: Переделать в класс из статики
//************************************************************************************

/**
 * Начинаем сохранять код "имущества". Отдельная функция чисто для красоты.
 */
function assetStart()
{
    ob_start();
}

/**
 * Сохраним буферизированное "имущество" в соответствующий типу набор и очистим буфер.
 * @param string $type
 */
function assetEnd($type)
{
    $result = ob_get_contents();
    ob_end_clean();
    asset($type, $result);
}

/**
 * Сохраняет "имущество" соответствующего типа себе в статике,
 * или возвращает весь набор если вызвать без значения
 * Если добавляемые данные у нас уже есть, то дубля не будет.
 * @staticvar array $data хранит все "имущества" (накопительно)
 * @param string $type тип имущества который мы добавляем
 * @param string $value значение имущества
 * @return array массив всех "имуществ"
 */
function asset($type, $value = null)
{
    static $data = [];
    if (!isset($data[$type])) {
        $data[$type] = [];
    }
    if (is_null($value)) {
        return $data[$type];
    }
    $key  = md5($value);
    if (!isset($data[$type][$key])) {
        $data[$type][$key] = $value;
    }
}
