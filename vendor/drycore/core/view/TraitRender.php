<?php
namespace drycore\core\view;

/**
 * Функционал связанный с шаблоном и его рендерингом
 *
 * @author Mendel
 */
trait TraitRender
{


    /**
     * Имя шаблона (именно шаблона, файла шаблона и т.п.)
     * @var type
     */
    private $template;
    
    /**
     * Установим/получим ИМЯ шаблона
     * @param string $template
     * @return string
     */
    public function template($template = null)
    {
        if (!is_null($template)) {
            $this->template = $template;
        }
        return $this->template;
    }

    /**
     * Рендерим наш вьюв в строку
     * 2DO: Финализировать метод, пусть переопределяют только инит
     * @return string
     */
    public function render()
    {
        // Сахар для языковых строк
        $_ = new LangComponent(['mainObj'=>$this]);
        extract($this->fields);
        ob_start();
        // не использую $filename чтобы не похерить имя переменной
        require $this->findTemplateFile();
        $contentStr = ob_get_contents();
        ob_end_clean();
        return $contentStr;
    }
    
    /**
     * Получим имя файла шаблона
     * @return string
     * @throws ViewNotFoundException
     */
    protected function findTemplateFile()
    {
        // Пробуем установленный шаблон, если нет, пробуем шаблон одноименный вьюву
        $template = $this->template();
        if (!$template) {
            $template = $this->selfType();
        }
        // Сделаем из имени шаблона - имя файла
        $filename = $this->templateFile($template);
        if (file_exists($filename)) {
            return $filename;
        }
        // Если дошли сюда, то по умолчанию не сработало, и пробуем родителей
        foreach ($this->parentsList() as $parent) {
            $filename = $this->templateFile($parent);
            if (file_exists($filename)) {
                return $filename;
            }
        }
        // Если мы дошли сюда, то ни одного шаблона нет, а значит умрем
        throw new ViewNotFoundException($template);
    }

    /**
     * Сделаем из имени шаблона имя файла шаблона
     * @return string
     */
    protected function templateFile($template)
    {
        list($type,$module,$name) = explode('/', $template, 3);
        if ($type =='app') {
            $filename = APP_FOLDER . '/'.$module.'/templ/'.$name.'.php';
        } else {
            $filename = VENDOR_FOLDER . '/'.$type.'/'.$module.'/templ/'.$name.'.php';
        }
        return $filename;
    }
}
