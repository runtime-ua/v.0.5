<?php
namespace drycore\core\view;

/**
 * для виртуальных полей хранимых в отдельном хранилище
 * 2DO: проработать совместимость с геттерами/сеттерами
 * @author Mendel
 */
trait TraitGetSet
{
    /**
     * Хранилище полей
     * 2DO: сделать приватным
     * @var array
     */
    protected $fields = [];
    
    /**
     * Сохраняем переменную в хранилище
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->fields[$name] = $value;
    }

    /**
     * Достаем переменную из хранилища
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (isset($this->fields[$name])) {
            return $this->fields[$name];
        } else {
            return parent::__get($name);
        }
    }
}
