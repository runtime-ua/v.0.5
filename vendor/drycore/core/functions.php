<?php
/**
 * Возвращает текущий контроллер
 * @return type
 */
function controller()
{
    return s()->controllerFactory->lastController;
}

/**
 * Выполним контроллер
 * @param array $route
 */
function startController($route)
{
    s()->controllerFactory->startController($route);
}

/**
 * Переадресует по ссылке/роуту, если установлен параметр Back то пробует переадресовать
 * назад по рефереру
 * @param mixed $url
 * @param boolean $back
 */
function redirect($url = null, $back = false)
{
    s()->http->redirect($url, $back);
}

/**
 * Подготавливает ссылку - если параметр роут, то делает ссылку,
 * если ссылка, то возвращает ее
 * @param mixed $route
 * @param boolean $absolute
 * @return string
 */
function url($route, $absolute = false)
{
    if (is_array($route)) {
        $route = s()->route->route2url($route);
    }
    if ($absolute) {
        $route = s()->route->siteUrl() .$route;
    }
    return $route;
}

/**
 * Как url Только добавляет маркер xsrf к пути, и нет варианта
 * с уже готовым путем ибо на него маркер вешать неуместно
 * @param mixed $route
 * @param boolean $absolute
 * @return string
 */
function urlXsrf($route, $absolute = false)
{
    // Если есть сессия, иначе смысла в маркере нет
    if (s()->session->isStarted) {
        $route[s()->xsrf->fieldName] = s()->xsrf->marker;
    }
    return url($route, $absolute);
}

//************************************************************************************
/**
 * Генерирует случайный ID.
 * 2DO: Подумать о криптонадежности, но надо оставить минимальную (32) длину
 * @return string
 */
function randId()
{
    return md5(uniqid(rand(), true));
}

/**
 * Разбиваем строку на теги
 * @param string $string
 * @return array
 */
function tags2array($string)
{
    $separators = ", \n\r\t";
    $array = [];
    $token = strtok($string, $separators);
    while ($token !== false) {
        $token = trim($token);
        if ($token) {
            $array[] = $token;
        }
        $token = strtok($separators);
    }
    return $array;
}

/**
 * Склеиваем массив через запятую (сахар)
 * @param array $array
 * @return string
 */
function array2tags($array)
{
    return implode(',', $array);
}
