<?php
/**
 * Компилируем конфиги для сервисов
 */
// Складываем все конфиги из модулей в один файл для каждого вида
$make = new \drycore\sys\make\MergeConfig();
$make->go([
    'models'=>RUNTIME_FOLDER.'/config/models.json',
    'controllers'=>RUNTIME_FOLDER.'/config/controllers.json',
    'services'=>RUNTIME_FOLDER.'/config/services.json',
    'rules'=>RUNTIME_FOLDER.'/config/rules.json',
    'views'=>RUNTIME_FOLDER.'/config/views.json',
]);
// Вытащим конфиг бокса из конфига модели, попутно удалив его оттуда
$makeBox = new \drycore\sys\box\MakeBox();
$makeBox->go(
    RUNTIME_FOLDER.'/config/models.json',
    RUNTIME_FOLDER.'/config/boxs.json'
);
// Скомпилируем конфиги, включая боксы
$makeConfig = new \drycore\sys\make\MakeConfig();
$makeConfig->go(
    RUNTIME_FOLDER.'/config/',
    [
        'models'=>'scenarios',
        'controllers'=>'actions',
        'services'=>'#context',
        'rules'=>null,
        'boxs'=>null,
        'views'=>null,
    ]
);

/**
 * Компилируем конфиги для языков
 */
// Подготовим конфиги
$mergeConfig = $compileConfig = [];
foreach(LANG_LIST as $lang) {
    $mergeConfig[$lang] = RUNTIME_FOLDER.'/config/'.$lang.'.json';
    $compileConfig[$lang] = null;
}
// Складываем все конфиги из модулей в один файл для каждого языка
$make = new \drycore\sys\make\MergeConfig();
$make->go($mergeConfig);
// Скомпилируем конфиги, включая языки и боксы
$makeConfig = new \drycore\sys\make\MakeConfig();
$makeConfig->go(
    RUNTIME_FOLDER.'/config/',
    $compileConfig
);
// Подготовим языки, а именно добавим языкам родителей взятых из основных конфигов
// 2DO: Кажись это глупости, и надо брать прямо из конфигов самих компонет (модели, вьювы...)
// по идее у нас у всех есть у их фабрик такая информация, так что нужно брать оттуда,
// а по хорошему вообще вынести в отдельный сервис конфигов, чтобы они там были все едины
$makeLang = new \drycore\sys\lang\MakeLang([
    'folder'=>RUNTIME_FOLDER.'/config/compiled'
]);
$makeLang->loadParents([
    'model',
    'controller',
    'service',
    'rule',
    'view',
]);
$makeLang->makeLang(LANG_LIST);
