<?php
namespace drycore\sys\minimalModel;

/**
 * Чтение/запись полей через правила и т.п.
 *
 * @author Mendel
 */
trait DataTrait
{
    /**
     * Признак того новая ли модель
     * @var boolean
     */
    private $isNew = true;
    
    /**
    * Признак новой модели.
    * Если передали, то ставит указанное значение, или возвращает текущее.
    * @return boolean признак валидности
    */
    public function isNew($status = null)
    {
        if (!is_null($status)) {
            $this->isNew = $status;
        }
        return $this->isNew;
    }
    
    /**
    * Получаем поле по имени, применяем правила если уместно
    * @param string $name имя читаемого поля
    * @return mixed
    */
    public function __get($name)
    {
        // Если поле существует, то применим к нему наши правила
        if ($this->offsetExists($name)) {
            $interpreter = $this->interpreter('get', $name, $this->internalGet($name));
            $value = $interpreter->value;
            return $value;
        }
    }
    
    /**
     * Сохраним поле, если уместно, то применим правила
     * @param type $name
     * @param type $value
     */
    public function __set($name, $value)
    {
        // Если поле существует, то применим к нему наши правила
        if ($this->offsetExists($name)) {
            $interpreter = $this->interpreter('set', $name, $value);
            $value = $interpreter->value;
            $this->internalSet($name, $value);
            $this->isNew(false);
        }
    }

    /**
     * Получим все данные в виде массива
     * Использует все наши правила
     * @return array
     */
    public function modelToArray()
    {
        $data = [];
        foreach ($this->fieldsList() as $key) {
            $value = $this->__get($key);
            $data[$key] = $value;
        }
        return $data;
    }
}
