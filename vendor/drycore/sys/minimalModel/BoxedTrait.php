<?php
namespace drycore\sys\minimalModel;

/**
 * Функционал модели которая может храниться в коробке
 */
trait BoxedTrait
{
    /**
     * Определяет является ли модель полученной из "коробки" и/или уже была сохранена в нее.
     * @var boolean
     */
    private $isBoxed = false;

    /**
     * ID клонированного объекта, потому что может понадобится клонировать связанные таблицы ит.п.
     * @var type
     */
    private $clonedId;
    
    /**
     * Клонируем модельку
     */
    public function __clone()
    {
        $this->isBoxed = false;
        $this->clonedId = $this->__get('id');
        $this->offsetUnset('id');
        /**
         * Выполним правила клонирования
         */
        foreach ($this->fieldsList() as $key) {
            $interpreter = $this->interpreter('afterClone', $key, $this->internalGet($key));
            $this->internalSet($key, $interpreter->value);
        }
    }
    
    /**
     * Вернем приватный ИД донора, а приватный он чтобы не занимать пространство имен
     * @return type
     */
    public function clonedId()
    {
        return $this->clonedId;
    }

    /**
     * Определяет является ли модель полученной из "коробки" и/или уже была сохранена в нее.
     * @return boolean признак валидности
     */
    public function isBoxed($status = null)
    {
        if (!is_null($status)) {
            $this->isBoxed = $status;
        }
        return $this->isBoxed;
    }

    /**
     * Вернем бокс соответствующий модели
     * @return type
     */
    public function box()
    {
        $boxName = null;
        foreach ($this->fieldsList() as $fieldName) {
            $interpreter = $this->interpreter('boxType', $fieldName);
            $value = $interpreter->value;
            if (!is_null($value)) {
                $boxName = $value;
            }
        }
        if (is_null($boxName)) {
            $boxName = $this->selfType().':'.$this->scenario();
        }
        return box($boxName);
    }

    /**
     * Сохраняем модель
     * @return boolean
     */
    public function save()
    {
        // Невадидную не сохраняем
        $this->validate();
        if (!$this->isValid()) {
            return false;
        }
        // Получим данные для сохранения
        $data = [];
        foreach ($this->fieldsList() as $key) {
            $interpreter = $this->interpreter('beforeSave', $key, $this->internalGet($key));
            if (!$interpreter->hidden) {
                $data[$key] = $interpreter->value;
            }
        }
        // Сохраняем как новую или как старую, воизбежание проблем с дублем ид
        if ($this->isBoxed()) {
            $this->saveBoxed($data);
        } else {
            $this->saveNew($data);
        }
        $this->isBoxed = true; // Перед afterSave поскольку если сохранять повторно, то будет дубль если не поставить...
        // Выполним обработку после сохранения
        foreach ($this->fieldsList() as $key) {
            $this->interpreter('afterSave', $key, $this->internalGet($key));
        }
        return true;
    }

    /**
     * Сохраним модель которая уже сохранялась раньше
     * @param type $data
     */
    protected function saveBoxed($data)
    {
        $this->box()->update($data, $data['id']);
    }

    /**
     * Сохраним модель которая еще не сохранялась
     * @param type $data
     */
    protected function saveNew($data)
    {
        $id = $this->box()->insert($data);
        $this->internalSet('id', $id);
    }

    /**
     * Возвращает TRUE если моделька была клонирована (есть ид предка)
     * @return type
     */
    public function isClone()
    {
        return !is_null($this->clonedId);
    }

    /**
     * Удалим модель
     */
    public function delete()
    {
        // Проверим можно ли удалять, или проваливаем удаление
        $ok = true;
        foreach ($this->fieldsList() as $key) {
            $interpreter = $this->interpreter('beforeDelete', $key, $this->internalGet($key));
            if ($interpreter->hidden) {
                $ok = false;
            }
        }
        // Если ок, то удалим, и выполним действия после удаления
        if ($ok) {
            $this->box()->delete($this->__get('id'));
            foreach ($this->fieldsList() as $key) {
                $interpreter = $this->interpreter('afterDelete', $key, $this->internalGet($key));
            }
        }
    }
    
    /**
     * Загружаем данные
     * @param type $fields
     */
    public function load($fields)
    {
        $this->internalSetAllFields($fields);
        $this->isNew(false);
        $this->isBoxed = true;
        // Обрабатываем правила для всех возможных полей
        foreach ($this->fieldsList() as $key) {
            $interpreter = $this->interpreter('afterLoad', $key, $this->internalGet($key));
            if (!$interpreter->hidden) {
                $this->internalSet($key, $interpreter->value);
            }
        }
    }
}
