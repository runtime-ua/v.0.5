<?php
namespace drycore\sys\minimalModel;

/**
 * Модель с самым базовым функционалом
 * Все необходимые системные вещи, поля с правилами и всё
 * @author Mendel
 */
class MinimalModel extends \proto\Component
{
    use InternalDataTrait, ApiTrait, DataTrait, BoxedTrait;
    
    /**
     * Применим сценарий
     * @param type $config
     */
    public function __construct($config = array())
    {
        parent::__construct($config);
        if (isset($config['#scenario'])) {
            $this->scenario($config['#scenario']);
            unset($config['#scenario']);
        }
    }
    

    /**
    * Текущий сценарий
    * @var string
    */
    private $scenario = 'default';

    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'model';
    }


    /**
    * Получаем (устанвливаем) сценарий. Доступ через метод потому, что публичные
    * свойства у нас зарезервированы под имена полей нашей модели.
    * @param string $scenario сценарий, или пусто если чтение
    * @return string текущий сценарий
    */
    public function scenario($scenario = null)
    {
        if (!is_null($scenario)) {
            $this->scenario = $scenario;
        }
        return $this->scenario;
    }
}
