<?php
namespace drycore\sys\minimalModel;

/**
 * Основные механизмы ядра нашей системы моделей
 * Связь с "Божественным" хранилищем настроек, вызов интерпретатора и т.п.
 * @author Mendel
 */
trait ApiTrait
{
    /**
     * Получим результат интерпретации правил в соответствующем режиме
     * @param type $mode
     * @param type $key
     * @param type $value
     * @return \drycore\sys\rule\Interpreter
     */
    protected function interpreter($mode, $key, $value = null)
    {
        $interpreter = new \drycore\sys\rule\Interpreter([
            'value'=>$value,
            'rules'=>$this->fieldRules($key),
            'model'=>$this,
            'mode' =>$mode,
            'fieldName'=>$key]);
        $interpreter->go();
        return $interpreter;
    }

    /**
     * Выполним магический метод
     * Если первый аргумент массив, то вызовем всех передав им этот массив в качестве данных
     * Иначе в зависимости от кол-ва аргументов
     * Если у нас нет аргументов, то просто выполним правила у всех полей
     * Если аргумент один, то считаем что это имя поля, а данными передадим
     *      внутреннее значение поля
     * Если аргумента два, то первый аргумент это имя поля, а второй это значение которое передаем правилу
     * @param type $name
     * @param type $arguments
     */
    public function __call($name, $arguments)
    {
        // Если первый аргумент массив, то вызовем всех передав им этот массив в качестве данных
        if (isset($arguments[0]) and is_array($arguments[0])) {
            // Вернем результат прошедший через все правила всех полей
            $value = $arguments[0];
            foreach ($this->fieldsList() as $fieldName) {
                $interpreter = $this->interpreter('call'.ucfirst($name), $fieldName, $value);
                $value = $interpreter->value;
            }
            return $value;
        }
        // Если дошли сюда, значит исходя из колва аргументов выбираем
        switch (count($arguments)) {
            case 0:
                // Вернем результат прошедший через все правила всех полей
                $value = null;
                foreach ($this->fieldsList() as $fieldName) {
                    $interpreter = $this->interpreter('call'.ucfirst($name), $fieldName, $value);
                    $value = $interpreter->value;
                }
                return $value;
            case 1:
                    $interpreter = $this->interpreter(
                        'call'.ucfirst($name),
                        $arguments[0],
                        $this->internalGet($arguments[0])
                    );
                return $interpreter->value;
            case 2:
                    $interpreter = $this->interpreter('call'.ucfirst($name), $arguments[0], $arguments[1]);
                return $interpreter->value;
            default:
                throw new \Exception('Bad arguments count');
        }
    }

    /**
     * Возвращает список полей по которым доступна итерация
     * по умолчанию любое поле доступно, но можно сообщить что его не отображать
     * @return array
     */
    public function iteratorFields()
    {
        $fields = [];
        foreach ($this->fieldsList() as $fieldName) {
            $interpreter = $this->interpreter('listed', $fieldName, true);
            if ($interpreter->value) {
                $fields[] = $fieldName;
            }
        }
        return $fields;
    }
    
    /**
     * Cписок правил для текущего сценария
     * @return array
     */
    protected function allRules()
    {
        return s()->modelFactory->rules($this->selfType(), $this->scenario());
    }
    
    /**
     * Список доступных полей
     * @return type
     */
    public function fieldsList()
    {
        return s()->modelFactory->fieldsList($this->selfType(), $this->scenario());
    }

    /**
     * Cписок правил одной переменной
     * @param type $name
     * @return type
     */
    public function fieldRules($name)
    {
        $rules = $this->allRules();
        if (empty($rules[$name])) {
            return [];
        }
        // Расшифруем из строки (уйдет после однопараметрового формата, так что хай будет)
        $data = [];
        foreach ($rules[$name] as $rule) {
            $str = '0='.$rule;
            $out = [];
            parse_str($str, $out);
            $data[] = $out;
        }
        return $data;
    }
}
