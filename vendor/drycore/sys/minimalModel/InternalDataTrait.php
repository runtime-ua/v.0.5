<?php
namespace drycore\sys\minimalModel;

/**
 * Работа с внутренним хранилищем только через геттеры/сеттеры
 *
 * @author Mendel
 */
trait InternalDataTrait
{
    /**
     * Здесь мы храним наши поля "массива"
     * @var array
     */
    private $fields = [];
    
    /**
     * Установим все поля в нашем внутреннем массиве
     * @param type $data
     */
    public function internalSetAllFields($data)
    {
        $this->fields = $data;
    }
    
    /**
     * Все внутренние данные всех полей одним массивом (внутренний массив по сути)
     * @return array
     */
    public function internalGetAllFields()
    {
         return $this->fields;
    }
    
    /**
     * Установим ВНУТРЕННЕЕ значение одного поля
     * @param type $name
     * @return type
     */
    public function internalSet($name, $value)
    {
        $this->fields[$name] = $value;
    }

    /**
     * Получим ВНУТРЕННЕЕ значение одного поля
     * @param type $name
     * @return type
     */
    public function internalGet($name)
    {
        if (isset($this->fields[$name])) {
            return $this->fields[$name];
        } else {
            return null;
        }
    }
}
