<?php
namespace drycore\sys\minimalModel;

/**
 * Фабрика моделей + выдача конфигов моделям
 *
 * @author Maks
 */
class ModelFactory extends \proto\ConfigurableService
{
    protected $contextFieldName = 'scenarios';
    
    public function factory($name)
    {
        list($name, $scenario) = explode(':', $name, 2);
        $config = $this->config($name);
        $config['#scenario'] = $scenario;
        // Узнаем какой класс вызывать
        if (isset($config['#class'])) {
            $class = $config['#class'];
            unset($config['#class']);
        } else {
            throw new ModelNotFoundException($name);
        }
        //
        $model = new $class($config);
        return $model;
    }
    
    
    public function fieldsList($modelName, $scenario = 'default')
    {
        return array_keys($this->rules($modelName, $scenario));
    }
        
    public function rules($modelName, $scenario = 'default')
    {
        $rules = $this->context($modelName, $scenario);
        foreach ($rules as $key => $value) {
            // Если это наследование, то имя сценария от которого наследуемся
            if (is_string($value)) {
                $parentRules = $this->rules($modelName, $value);
                if (isset($rules[$key])) {
                    $rules[$key] = $parentRules[$key];
                }
            }
            // если NULL то удаляем нафиг
            if (is_null($rules[$key])) {
                unset($rules[$key]);
            }
        }
        return $rules;
    }
}
