<?php
namespace drycore\sys\standartModel;

/**
 * Стандартная модель - реализуем всякие полезные интерфейсы и прочие типовые бонусы
 * в добавок к минимальной модели
 * @author Mendel
 */
class StandartModel extends \drycore\sys\minimalModel\MinimalModel
implements \Iterator, \ArrayAccess, \Countable, \JsonSerializable
{
    use IteratorTrait, ArrayAccessTrait, ValidatedTrait, MagicGetTrait;
    
    /**
     * Перематываем при создании модели
     * @param type $config
     */
    public function __construct($config = array())
    {
        parent::__construct($config);
        $this->rewind(); // Перемотаем в начало, чтобы если нужно итерировать мы уже готовы
    }

    /**
     * Количество полей, для интерфейса \Countable
     * @return type
     */
    public function count()
    {
        return count($this->fieldsList());
    }
    
    /**
     * Для интерфейса  \JsonSerializable
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->internalGetAllFields();
    }
}
