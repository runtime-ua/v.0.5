<?php
namespace drycore\sys\standartModel;

/**
 * Реализация методов интерфейса ArrayAccess
 *
 * @author Mendel
 */
trait ArrayAccessTrait
{
    /**
     * Проверим что такое поле существует
     * @param string $offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return in_array($offset, $this->fieldsList());
    }

    /**
     * Получим значение по ключу
     * @param string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->__get($offset) : null;
    }

    /**
     * Установим значение по ключу
     * @param string $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            throw new \Exception('Model cant have unindexed value, like $model[] = $value');
        }
        $this->__set($offset, $value);
    }

    /**
     * Удаляем поле (делаем NULL)
     * @param string $offset
     */
    public function offsetUnset($offset)
    {
        $this->internalSet($offset, null);
    }
}
