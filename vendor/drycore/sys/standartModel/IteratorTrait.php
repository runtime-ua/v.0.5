<?php
namespace drycore\sys\standartModel;

/**
 * Методы для реализации интерфейса Итератор
 *
 * @author Mendel
 */
trait IteratorTrait
{
    /**
     * Идентификатор текущего положения при итерации
     * @var type
     */
    private $position;
    
    /**
     * Получить текущий элемент
     * @return type
     */
    public function current()
    {
        return $this->__get($this->position);
    }

    /**
     * Получим текущее значение ключа
     * @return type
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * Перемотка на следующее значение
     */
    public function next()
    {
        // Возьмем список доступных полей
        $fieldsList = $this->iteratorFields();
        // Найдем в нем текущий ИД поля, точнее его номер в списке
        $currentKey = array_search($this->position, $fieldsList);
        // Следующий на единицу больше
        $nextKey = $currentKey + 1;
        // Проверяем а есть ли собственно такой и устанавливаем значение
        if (isset($fieldsList[$nextKey])) {
            $this->position = $fieldsList[$nextKey];
        } else {
            $this->position = null;
        }
    }
    
    /**
     * Перемотка в начало
     */
    public function rewind()
    {
        $fieldsList = $this->fieldsList();
        $this->position = $fieldsList[0];
    }

    /**
     * Проверяем что мы при перемотке не ускакали за пределы данных
     * @return type
     */
    public function valid()
    {
        return $this->offsetExists($this->position);
    }
}
