<?php
namespace drycore\sys\standartModel;

/**
 * Магия вызова вложенных объектов и методов
 * @author Mendel
 */
trait MagicGetTrait
{
    /**
    * Если в имени встречается -> то достаем из дочернего объекта,
    * если в конце скобки, то вызовем метод (без параметров естественно)
    * @param string $name имя читаемого поля
    * @return mixed
    */
    public function __get($name)
    {
        if (haveStr($name, '->')) {
            list($name,$subname) = explode('->', $name, 2);
            $obj = $this->__get($name);
            return $obj->$subname;
        } else {
            // Если это запрос метода, то вызовем метод (например count)
            if (substr($name, -2) == '()') {
                $name = substr($name, 0, -2);
                return $this->$name();
            } else {
                return parent::__get($name);
            }
        }
    }
}
