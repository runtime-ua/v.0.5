<?php
namespace drycore\sys\standartModel;

/**
 * Функционал валидации, ошибок и т.п.
 */
trait ValidatedTrait
{
    /**
    * @var boolean признак того, прошла ли модель валидацию
    */
    private $valid = true;
  
    /**
    * @var array ошибки валидации с соответствующими текстами
    */
    private $error = [];

    /**
    * Выполняем валидацию модели по описанным правилам
    */
    public function validate()
    {
        // сбрасываем прошлые ошибки
        $this->valid = true;
        $this->error = [];
        // Обрабатываем правила для всех возможных полей
        foreach ($this->fieldsList() as $key) {
            $interpreter = $this->interpreter('validate', $key, $this[$key]);
            // Если валидация провалена, то отразим статус валидации и сохраним полученные ошибки
            if (!$interpreter->valid) {
                $this->valid = false;
                $errText = implode(' ', $interpreter->error);
                $this->error[$key] = $errText;
            }
        }
        return $this->valid;
    }
    
    /**
    * Получаем результат валидации хранящийся в свойстве.
    * @return boolean признак валидности
    */
    public function isValid()
    {
        return $this->valid;
    }

    /**
    * Получаем список ошибок валидации
    * Если указано имя поля, то только по нему, если нет, то весь пакет
    * @return array список ошибок валидации
    */
    public function error($name = null)
    {
        if (!is_null($name)) {
            if (isset($this->error[$name])) {
                return $this->error[$name];
            } else {
                return false;
            }
        } else {
            return $this->error;
        }
    }
}
