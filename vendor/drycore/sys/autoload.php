<?php
/**
 * Функция автоматической загрузки классов
 */
function autoload($class)
{
    $parts = explode('\\', $class);
    if ($parts[0] == 'app') {
        unset($parts[0]);
        $filename = APP_FOLDER.'/'.implode('/', $parts).'.php';
    }
    if ($parts[0] == 'proto') {
        unset($parts[0]);
        $filename = PROTO_FOLDER.'/'.implode('/', $parts).'.php';
    }
    if (!isset($filename)) {
        $filename = VENDOR_FOLDER.'/'.implode('/', $parts).'.php';
    }
    if (file_exists($filename)) {
        require_once($filename);
    }
}
