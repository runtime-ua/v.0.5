<?php
namespace drycore\sys\rule;

/**
 * Абстрактный родитель для правил
 */
abstract class AbstractRule extends \proto\Component
{
    protected $param; // список параметров
    protected $master; //объект вызвавший нас, у которого основные данные находятся
    
    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'rule';
    }

    /**
     * Метод вызываемый при выполнении правила
     */
    public function go()
    {
        $mode = $this->master->mode;
        if (method_exists($this, $mode)) {
            $this->$mode();
        }
    }
    
    /**
     * По умолчанию тоже самое что и гет, где надо - переопределяется
     */
    protected function callForEdit()
    {
        if (method_exists($this, 'get')) {
            $this->get();
        }
    }
        
    /**
     * По умолчанию тоже самое что и гет, где надо - переопределяется
     */
    protected function callPretty()
    {
        if (method_exists($this, 'get')) {
            $this->get();
        }
    }
}
