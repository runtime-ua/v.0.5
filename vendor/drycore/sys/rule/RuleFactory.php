<?php
namespace drycore\sys\rule;

/**
 * Фабрика правил
 */
class RuleFactory extends \proto\ConfigurableService
{
    public function factory($name, $ruleConfig)
    {
        $config = $this->config($name);
        // Узнаем какой класс вызывать
        if (isset($config['#class'])) {
            $class = $config['#class'];
            unset($config['#class']);
        } else {
            throw new RuleNotFoundException($name);
        }
        //
        $config = array_merge($config, $ruleConfig);
        $rule = new $class($config);
        return $rule;
    }
}
