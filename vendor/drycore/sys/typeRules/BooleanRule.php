<?php
namespace drycore\sys\typeRules;

  /**
   * Правило Boolean - приводит к Boolean, если не приводится, то проверка
   * проваливается. К TRUE приводятся "1", "true", "on" и "yes".
   * К FALSE приводятся "0", "false", "off", "no" и "".
   * Всё остальное приводит к ошибке валидации.
   */
class BooleanRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) or $this->master->value ==='') {
            $value = filter_var($this->master->value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            if (is_null($value)) {
                if (isset($this->param['errMsg'])) {
                    $error = $this->param['errMsg'];
                } else {
                    $error = $this->lang('validateBooleanError');
                }
                $this->master->error[] = $error;
                $this->master->valid = false;
            }
        }
    }
    protected function afterLoad()
    {
        $this->set();
    }
    protected function set()
    {
        if (!is_null($this->master->value) or $this->master->value ==='') {
            $filtered = filter_var($this->master->value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            if (!is_null($filtered)) {
                $this->master->value = $filtered;
            }
        }
    }
}
