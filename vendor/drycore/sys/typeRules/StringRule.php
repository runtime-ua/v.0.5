<?php
namespace drycore\sys\typeRules;

  /**
   * Правило String для строки. Очищает от всех HTML-тегов, кодирует все сущности и т.п.
   * Параметры:
   * minlen - минимальная длина строки
   * maxlen - максимальная длина строки
   * encode - если присутствует данный параметр, то теги не удаляются а кодируются
   * unsafe - если присутствует, то не кодировать и не удалять теги, а оставлять как есть,
   *      без изменений, только (возможно) проверять размер.
   */
class StringRule extends \proto\Rule
{
    // Вернем обработанную. Теоретически хватит и сет, но мало ли откуда залетит.
    protected function get()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $this->master->value = $this->sanitizeValue();
        }
    }
    // Санитаризация при сохранении ибо нефиг.
    protected function set()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $this->master->value = $this->sanitizeValue();
        }
    }

    // Валидация санитаризированной
    protected function validate()
    {
        if (false and !is_null($this->master->value) and $this->master->value !=='') {
            $len = mb_strlen($this->sanitizeValue());
            if (isset($this->param['minlen']) and $len < $this->param['minlen']) {
                if (isset($this->param['errMinMsg'])) {
                    $error = $this->param['errMinMsg'];
                } else {
                    $error = $this->lang('validateStringMinError');
                }
                $this->master->error[] = $error . $this->param['minlen'];
                $this->master->valid = false;
            }
            if (isset($this->param['maxlen']) and $len > $this->param['maxlen']) {
                if (isset($this->param['errMaxMsg'])) {
                    $error = $this->param['errMaxMsg'];
                } else {
                    $error = $this->lang('validateStringMaxError');
                }
                $this->master->error[] = $error . $this->param['maxlen'];
                $this->master->valid = false;
            }
        }
    }
    
    // санитаризая с учетом параметров
    private function sanitizeValue()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            if (isset($this->param['encode'])) {
                $value = filter_var($this->master->value, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            }
            if (isset($this->param['unsafe'])) {
                $value = $this->master->value;
            }
            if (!isset($this->param['encode']) and !isset($this->param['unsafe'])) {
                $value = filter_var($this->master->value, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
            }
        } else {
            $value = $this->master->value;
        }
//        jsonPre([$this->master->model->selfType(),$this->master->fieldName,$value]);
        return $value;
    }
}
