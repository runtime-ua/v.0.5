<?php
namespace drycore\sys\typeRules;

/**
 * Правило Mixed  - не делает ничего, оставляя данные как есть.
 * Предназначено для того, чтобы сделать поле разрешенным, но ничего не проверять
 * и ничего не менять.
 */
class MixedRule extends \proto\Rule
{
}
