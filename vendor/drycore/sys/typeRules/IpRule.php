<?php
namespace drycore\sys\typeRules;

/**
 * Правило для IP. IPv4 всегда разрешен
 * Доступные параметры:
 * ipv6 - разрешить ipv6
 * public - только публичные адреса (т.е. запрет зарезервированных и внутренних)
 */
class IpRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $flags = FILTER_FLAG_IPV4;
            if (isset($this->param['ipv6'])) {
                $flags = $flags | FILTER_FLAG_IPV6;
            }
            if (isset($this->param['public'])) {
                $flags = $flags | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE;
            }
            $value = filter_var($this->master->value, FILTER_VALIDATE_IP, $flags);
            if (false === $value) {
                if (isset($this->param['errMsg'])) {
                    $error = $this->param['errMsg'];
                } else {
                    $error = $this->lang('validateIpError');
                }
                $this->master->error[] = $error;
                //
                if (isset($this->param['errPublicMsg'])) {
                    $errorPublic = $this->param['errPublicMsg'];
                } else {
                    $errorPublic = $this->lang('validateIpPublicError');
                }
                if (isset($this->param['public'])) {
                    $this->master->error[] = $errorPublic;
                }
                //
                $this->master->valid = false;
            }
        }
    }
}
