<?php
namespace drycore\sys\typeRules;

  /**
   * Правило для целочисленных полей.
   * Доступные параметры:
   * min - минимальное допустимое значение
   * max - максимальное допустимое значение
   * hex - если присутствует, то разрешает шестнадцатиричные числа
   * octal - если присутствует, то разрешает восьмиричные числа
   */
class IntRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $options = [];
            if (isset($this->param['min'])) {
                $options['options']['min_range'] = $this->param['min'];
            }
            if (isset($this->param['max'])) {
                $options['options']['max_range'] = $this->param['max'];
            }
            if (isset($this->param['octal'])) {
                $options['flags'] = FILTER_FLAG_ALLOW_OCTAL;
            }
            if (isset($this->param['hex'])) {
                $options['flags'] = FILTER_FLAG_ALLOW_HEX;
            }
            if (isset($this->param['hex']) and isset($this->param['octal'])) {
                $options['flags'] = FILTER_FLAG_ALLOW_OCTAL | FILTER_FLAG_ALLOW_HEX;
            }
            $value = filter_var($this->master->value, FILTER_VALIDATE_INT, $options);
            if (false === $value) {
                $this->error(); // Добавим сообщения об ошибках
                $this->master->valid = false;
            }
        }
    }
    protected function afterLoad()
    {
        $this->set();
    }
  
    /**
     * Вспомогательный метод вывода ошибок валидации для целых
     */
    protected function error()
    {
        if (isset($this->param['errMsg'])) {
            $error = $this->param['errMsg'];
        } else {
            $error = $this->lang('validateIntError');
        }
        $this->master->error[] = $error;
        //
        if (isset($this->param['min'])) {
            if (isset($this->param['errMinMsg'])) {
                $errorMin = $this->param['errMinMsg'];
            } else {
                $errorMin = $this->lang('validateIntMinError');
            }
            $this->master->error[] = $errorMin. $this->param['min'];
        }
        //
        if (isset($this->param['max'])) {
            if (isset($this->param['errMaxMsg'])) {
                $errorMin = $this->param['errMaxMsg'];
            } else {
                $errorMax = $this->lang('validateIntMaxError');
            }
            $this->master->error[] = $errorMax. $this->param['max'];
        }
    }
  
    protected function set()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $options = [];
            if (isset($this->param['min'])) {
                $options['options']['min_range'] = $this->param['min'];
            }
            if (isset($this->param['max'])) {
                $options['options']['max_range'] = $this->param['max'];
            }
            if (isset($this->param['octal'])) {
                $options['flags'] = FILTER_FLAG_ALLOW_OCTAL;
            }
            if (isset($this->param['hex'])) {
                $options['flags'] = FILTER_FLAG_ALLOW_HEX;
            }
            if (isset($this->param['hex']) and isset($this->param['octal'])) {
                $options['flags'] = FILTER_FLAG_ALLOW_OCTAL | FILTER_FLAG_ALLOW_HEX;
            }
            $filtered = filter_var($this->master->value, FILTER_VALIDATE_INT, $options);
            if (false !== $filtered) {
                $this->master->value = $filtered;
            }
        }
    }
}
