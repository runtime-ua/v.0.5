<?php
namespace drycore\sys\typeRules;

  /**
   * Правило Float проверяющее, что поле - корректное число с плавающей точкой
   */
class FloatRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $value = filter_var($this->master->value, FILTER_VALIDATE_FLOAT);
            if (false === $value) {
                if (isset($this->param['errMsg'])) {
                    $error = $this->param['errMsg'];
                } else {
                    $error = $this->lang('validateFloatError');
                }
                $this->master->error[] = $error;
                $this->master->valid = false;
            }
        }
    }
    protected function afterLoad()
    {
        $this->set();
    }
    protected function set()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $filtered = filter_var($this->master->value, FILTER_VALIDATE_FLOAT);
            if (false !== $filtered) {
                $this->master->value = $filtered;
            }
        }
    }
}
