<?php
namespace drycore\sys\typeRules;

/**
 * Правило проверки email
 * @todo Добавить проверку на резолвинг домена почты
 */
class EmailRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $value = filter_var($this->master->value, FILTER_VALIDATE_EMAIL);
            if (false === $value) {
                if (isset($this->param['errMsg'])) {
                    $error = $this->param['errMsg'];
                } else {
                    $error = $this->lang('validateEmailError');
                }
                $this->master->error[] = $error;
                $this->master->valid = false;
            }
        }
    }
}
