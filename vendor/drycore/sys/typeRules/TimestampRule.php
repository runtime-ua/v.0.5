<?php
namespace drycore\sys\typeRules;

class TimestampRule extends IntRule
{
    protected function callPretty()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $format = s()->locale->dateFormat; // Если сильно отличается, то дата
//        if(date('Y') == date('Y',$value)) $format = 'j M'; // совпадает год
        if (date('Y:M:j') == date('Y:M:j', $value)) {
            $format = s()->locale->timeFormat; // совпадает год, месяц и день
        }
        //
        $this->master->value = date($format, $value);
    }

    protected function callDateTime()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $format = s()->locale->dateTimeFormat;
        if ($value) {
            $this->master->value = date($format, $value);
        }
    }
    protected function callDate()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $format = s()->locale->dateFormat;
        if ($value) {
            $this->master->value = date($format, $value);
        }
    }
    protected function callTime()
    {
        // Возьмем так, чтобы если там умолчание, то было умолчание)
        $fieldName = $this->master->fieldName;
        $value = $this->master->model->$fieldName;
        //
        $format = s()->locale->timeFormat;
        if ($value) {
            $this->master->value = date($format, $value);
        }
    }
}
