<?php
namespace drycore\sys\typeRules;

/**
 * Правило Regexp проверки соответствию регулярному выражению
 * Обязательный параметр regexp содержит соответствующее выражение
 */
class RegexpRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $options = [];
            $options['options']['regexp'] = $this->param['regexp'];
            $value = filter_var($this->value, FILTER_VALIDATE_REGEXP, $options);
            if (false === $value) {
                if (isset($this->param['errMsg'])) {
                    $error = $this->param['errMsg'];
                } else {
                    $error = $this->lang('validateRegexpError');
                }
                $error = $error . $this->param['regexp'];
                $this->master->error[] = $error;
                $this->master->valid = false;
            }
        }
    }
}
