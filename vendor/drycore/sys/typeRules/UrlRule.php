<?php
namespace drycore\sys\typeRules;

  /**
   * Правило Url проверяющее URL на валидность. Допускает внутренние ссылки.
   * @todo добавить параметр для запрета внутренних ссылок, параметр добавления указанного домена
   * для добивания до полной ссылки, добавления/удаления http(s), проверки резолвинга ссылки.
   */
class UrlRule extends \proto\Rule
{
    protected function validate()
    {
        if (!is_null($this->master->value) and $this->master->value !=='') {
            $value = filter_var($this->master->value, FILTER_VALIDATE_URL);
            if (false === $value) {
                if (isset($this->param['errMsg'])) {
                    $error = $this->param['errMsg'];
                } else {
                    $error = $this->lang('validateUrlError');
                }
                $this->master->error[] = $error;
                $this->master->valid = false;
            }
        }
    }
}
