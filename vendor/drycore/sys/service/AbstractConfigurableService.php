<?php
namespace drycore\sys\service;

/**
 * Типовые конфиги из файлов, для стандартных массовых конфигов
 * @author Mendel
 */
abstract class AbstractConfigurableService extends \proto\Service
{
    /**
     * Какой контекст у текущего обрабатываемого конфига
     * @var type
     */
    protected $contextFieldName;

    /**
     * @var array Содержит конфигурацию
     * @see loadConfig()
     */
    private $config = null;
    
    /**
     * Загрузка конфигурации
     * @param array $config конфигурация
     */
    public function loadConfig($config)
    {
        $this->config = $config;
    }
    
    /**
     * Получим контекст
     * @param string $name
     * @param string $context
     */
    protected function context($name, $context)
    {
        if (!$this->isConfigurated()) {
            throw new \Exception('Dont have config!');
        }
        if (isset($this->config[$name][$this->contextFieldName][$context])) {
            return $this->config[$name][$this->contextFieldName][$context];
        }
        if (isset($this->config['#default'][$this->contextFieldName][$context])) {
            return $this->config[$this->contextFieldName][$this->contextFieldName][$context];
        }
        return [];
    }

    /**
     * Получим конфигурацию по имени
     * @param string $name
     */
    protected function config($name)
    {
        if (!$this->isConfigurated()) {
            throw new \Exception('Dont have config!');
        }
        if (isset($this->config[$name])) {
            $config = $this->config[$name];
        } else {
            $config = $this->config['#default'];
        }
        if (isset($config[$this->contextFieldName])) {
            unset($config[$this->contextFieldName]);
        }
        $config['#selfType'] = $name;
        return $config;
    }
    
    /**
     * Проверим что у нас есть конфиг (проверка на NULL)
     * @return boolean
     */
    protected function isConfigurated()
    {
        return !is_null($this->config);
    }
    
    /**
     * Получим список потомков модели
     * @param string $name
     * @param mixed $abstract Только абстракнтые, только не абстрактные или все если NULL
     * @return array
     */
    public function factoryChildsList($name, $abstract = null)
    {
        // Если имя не указано, то вернем все что есть
        // Иначе - возьмем потомков
        if (!is_null($name)) {
            $config = $this->config($name);
            $units = $config['#childsList'];
        } else {
            $units = array_keys($this->config);
        }
        // Если абстрактность не указана, то не фильтруем а возвращаем список
        if (is_null($abstract)) {
            return $units;
        }
        // Если дошли сюда, то разделим на абстрактные и реальные
        $abstractUnits = $realUnits = [];
        foreach ($units as $line) {
            if ($this->isAbstract($line)) {
                $abstractUnits[] = $line;
            } else {
                $realUnits[] = $line;
            }
        }
        // Вернем тех кто нужен. Абстракнтых или реальных
        if ($abstract) {
            return $abstractUnits;
        } else {
            return $realUnits;
        }
    }
    
    /*
     * Получим список родителей модели
     * @param string $name
     * @param mixed $abstract Только абстракнтые, только не абстрактные или все если NULL
     * @return array
     */
    public function factoryParentsList($name, $abstract = null)
    {
        $config = $this->config($name);
        $units = $config['#parentsList'];
        if (is_null($abstract)) {
            return $units;
        }
        $abstractUnits = $realUnits = [];
        foreach ($units as $line) {
            if ($this->isAbstract($line)) {
                $abstractUnits[] = $line;
            } else {
                $realUnits[] = $line;
            }
        }
        if ($abstract) {
            return $abstractUnits;
        } else {
            return $realUnits;
        }
    }
    
    /*
     * Проверяет является ли наш тип абстрактным
     */
    public function isAbstract($name)
    {
        return haveStr($name, '#');
    }
    
    /**
     * Проверим что первый параметр потомок второго параметра
     * @param string $child
     * @param string $parent
     * @return boolean
     */
    public function isChild($child, $parent)
    {
        if ($child == $parent) {
            return true;
        }
        return in_array($child, $this->factoryChildsList($parent));
    }
    
    /**
     * Проверим существует ли такой юнит
     * @param string $name
     * @return boolean
     */
    public function exist($name)
    {
        return(isset($this->config[$name]));
    }
}
