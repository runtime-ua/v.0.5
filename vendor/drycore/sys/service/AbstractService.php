<?php
namespace drycore\sys\service;

/**
 * Абстрактный родитель для всех сервисов
 * @author Mendel
 */
abstract class AbstractService extends \proto\Component
{
    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'service';
    }

    /**
     * Подгружаем настройки из модели получаемой по параметру #modelConfig
     * @param array $config массив конфигурации.
     */
    public function __construct($config = [])
    {
        // Если получили имя модели доп.конфигурации, то применим эту модель
        if (isset($config['#modelConfig'])) {
            $model = findModel($config['#modelConfig']);
            unset($config['#modelConfig']);
            // По списку переменных, чтобы дефолты отработали
            //  2DO: сделать чтобы по ним модель ходила а не по полям
            foreach ($model->fieldsList() as $key) {
                $value = $model->$key;
                $config[$key] = $value;
            }
        }
        parent::__construct($config);
    }
}
