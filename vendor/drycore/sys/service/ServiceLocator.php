<?php
namespace drycore\sys\service;

/**
 * Service Locator - поиск и инициализация сервисов
 * @author Mendel
 */
class ServiceLocator extends \proto\ConfigurableService
{
    /**
     * Здесь у нас указывается контекст в котором мы запустились
     * @var string
     */
    protected $contextFieldName = '#context';
    
    /**
     * @var string Хранит значение контекста в котором мы работаем
     */
    protected $currentContext;
    
    /**
     * @var array Содержит уже инициированные компоненты
     */
    protected $services = [];
    
    /**
     * Установим текущий контекст
     * @param string $context
     */
    public function setContext($context)
    {
        $this->currentContext = $context;
    }

    /**
     * Основной метод - возвращаем сервис по имени сервиса
     * Если уже есть такой инициированный, то вернет его
     * если нет, то инициирует и вернет.
     * @param string $name
     * @return \proto\Service
     */
    public function __get($name)
    {
        // если есть подчеркивание - заменим на слеш
        $name = str_replace('_', '/', $name);
        if (!isset($this->services[$name])) {
            // получим конфиг с учетом контекста
            $config = $this->config($name);
            $context = $this->context($name, $this->currentContext);
            $config = array_merge($config, $context);
            // Узнаем какой класс вызывать
            if (isset($config['#class'])) {
                $class = $config['#class'];
                unset($config['#class']);
            } else {
                $class = null;
            }
            if (is_null($class)) {
                throw new ServiceNotFoundException($name);
            }
            $this->services[$name] = new $class($config);
        }
        return $this->services[$name];
    }
}
