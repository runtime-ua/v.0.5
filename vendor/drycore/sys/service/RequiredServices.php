<?php
namespace drycore\sys\service;

/**
 * requiredServices - расширение для ServiceLocator инициирующее обязательные компоненты
 * @author Mendel
 */
class RequiredServices extends \proto\Service
{
    /**
     * Список обязательных сервисов, инициируемых ДО того как их вызовут
     * @var array
     */
    protected $services = [];
    
    /**
    * Инициируем компоненты которые переданы в качестве параметра
    */
    public function start()
    {
        foreach ($this->services as $service) {
            s()->$service;
        }
    }
}
