<?php
namespace drycore\sys\service;

/**
 * Исключение выбрасываемое если мы не нашли запрошенный сервис
 */
class ServiceNotFoundException extends \Exception
{
}
