<?php
/**
 * Вернем наш СервисЛокатор
 * @return \drycore\core\service\ServiceLocator
 */
function s()
{
    static $serviceLocator = null;
    if (is_null($serviceLocator)) {
        $serviceLocator = new \drycore\sys\service\ServiceLocator();
    }
    return $serviceLocator;
}

/**
 *  Возвращает модель по имени модели
 * @param string $name
 * @return type
 */
function model($name)
{
    if (!haveStr($name, ':')) {
        $name = $name.':default';
    }
    return s()->modelFactory->factory($name);
}

/**
 * Возвращает модель из бокса по ид, или новую модель если нет
 * @param type $name
 * @return type
 */
function findModel($name)
{
    list($id, $modelName) = explode('@', $name, 2);
    $model = box($modelName)->findOne($id);
    if (!$model) {
        $model = model($modelName);
    }
    return $model;
}

/**
 * Возвращает бокс по имени
 * @param string $name
 * @return type
 */
function box($name)
{
    if (!haveStr($name, ':')) {
        $name = $name.':default';
    }
    return s()->boxFactory->factory($name);
}

// ****************************************************************************

/**
 * Сахар. Проверяет содержит ли строка искомую строку
 * @param string $str
 * @param string $needle
 * @return boolean
 */
function haveStr($str, $needle)
{
    return (strpos($str, $needle) !== false);
}

/**
 * Сахар для красивого сохранения JSON. На входе массив, на выходе красивый JSON
 * @param array $data
 * @return string
 */
function coolJson($data)
{
    return json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
}

/**
 * Загрузим данные из файла в формате JSON
 * @param string $filename
 * @throws \Exception
 */
function loadJson($filename)
{
    if (!file_exists($filename) || !is_readable($filename)) {
        throw new \Exception('NotFound: '.$filename);
    }
    $data = file_get_contents($filename);
    return json_decode($data, true);
}
/**
 * Сохраняет файл с данными в JSON
 * @param string $filename
 * @param array $data
 */
function saveJson($filename, $data)
{
    if (file_exists($filename)) {
        unlink($filename);
    }
    file_put_contents($filename, coolJson($data));
}
