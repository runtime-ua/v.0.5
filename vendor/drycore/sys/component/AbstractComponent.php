<?php
namespace drycore\sys\component;

/**
 * Component - базовый класс для всех компонентов
 * @author Mendel
 */
abstract class AbstractComponent
{
    use LangTrait;
    
    /**
     * Внутренний идентификатор компоненты внутри ее типа
     * @var string
     */
    private $selfType = '#unknown';
    /**
     * Содержит массив со всеми родителями
     * @var array
     */
    private $parentsList = [];
    /**
     * Содержит массив со всеми детьми
     * @var array
     */
    private $childsList = [];
    
    /**
     * Конструктор. Загружает конфигурацию в свойства объекта
     * Ключи массива это имена свойств,
     * значения - значения свойств объекта которые присваиваются при инициализации.
     * @param array $config массив конфигурации.
     */
    public function __construct($config = [])
    {
        if (isset($config['#selfType'])) {
            $this->selfType($config['#selfType']);
            unset($config['#selfType']);
        }
        if (isset($config['#parentsList'])) {
            $this->parentsList($config['#parentsList']);
            unset($config['#parentsList']);
        }
        if (isset($config['#childsList'])) {
            $this->childsList($config['#childsList']);
            unset($config['#childsList']);
        }
        // Применим параметры
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }
    /**
     * Вернем список родителей (наших псевдо-классов)
     * или установим другое его значение
     * @param array $parentsList
     * @return array
     */
    public function parentsList($parentsList = null)
    {
        if (!is_null($parentsList)) {
            $this->parentsList = array_reverse($parentsList);
        }
        return $this->parentsList;
    }
    /**
     * Вернем список потомков (наших псевдо-классов)
     * или установим другое его значение
     * @param array $childsList
     * @return array
     */
    public function childsList($childsList = null)
    {
        if (!is_null($childsList)) {
            $this->childsList = $childsList;
        }
        return $this->childsList;
    }

    /**
     * Вернем тип объекта (наш псевдо-класс)
     * или установим другое его значение
     * @param string $type
     * @return string
     */
    public function selfType($type = null)
    {
        if (!is_null($type)) {
            $this->selfType = $type;
        }
        if ($this->selfType == '#unknown') {
            throw new \Exception('Bad #selfType');
        }
        return $this->selfType;
    }
    
    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'component';
    }
}
