<?php
namespace drycore\sys\component;

/**
 * Функции связанные с языковыми строками, самоназвание, языковые строки и т.п.
 *
 * @author Mendel
 */
trait LangTrait
{
    /**
     * Самоназвание данного компонента (компонента данного типа)
     * @return string
     */
    public function selfName()
    {
        return $this->lang('#self');
    }
    
    /**
     * Название ТИПА компонента, например box >>> Бокс
     * @return string
     */
    public function componentName()
    {
        return $this->lang('componentType#'.$this->componentType());
    }
    
    /**
     * Возвращает тип компонента (контроллер, модель и т.п.) используемый для языков,
     * может быть переопределен, чтобы использовать языки в связанных типах компонентов
     * например боксы наследуют языки у моделей
     * @return string
     */
    public function langType()
    {
        return $this->componentType();
    }
    
    /**
     * Получить строку соответствующую нашему langSpace и текущему языку
     * @param string $name
     * @return string
     */
    public function lang($name)
    {
        return s()->lang->getString($name, $this->langType().'/'.$this->selfType());
    }
}
