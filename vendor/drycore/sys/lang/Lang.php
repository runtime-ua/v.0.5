<?php
namespace drycore\sys\lang;

/**
 * Сервис поиска языковых строк для компонент
 */
class Lang extends \proto\Service
{
    /**
     * Путь к папке в которой храним наши конфиги (языки)
     * @var string
     */
    protected $configFolder = RUNTIME_FOLDER.'/config/compiled/';
    
    /**
     * Языковые данные
     */
    protected $langData = [];
    
    /**
     * Язык по умолчанию
     * @var string
     */
    protected $defaultLang = 'eng';
    
    /**
     * Текущий язык
     * @var string
     */
    public $lang = 'eng';

    /**
     * Установим текущим указанный язык
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }
    
    /**
     * Загрузим строки если еще не загружали
     * @param string $lang
     */
    protected function tryConfig($lang = null)
    {
        if (is_null($lang)) {
            $lang = $this->lang;
        }
        if (!isset($this->langData[$lang])) {
            $filename = $this->configFolder.$lang.'Lang.json';
            if (file_exists($filename)) {
                $this->langData[$lang] = loadJson($filename);
            } else {
                $this->langData[$lang] = [];
            }
        }
    }

    /**
     * Возвращает строку для соответствующего лангСпейса
     * @param string $name
     * @param string $langSpace
     * @return string
     */
    public function getString($name, $langSpace = '#default')
    {
        $string = $this->subGetString($this->lang, $langSpace, $name);
        if (!is_null($string)) {
            return $string;
        }
        $string = $this->subGetString($this->defaultLang, $langSpace, $name);
        if (!is_null($string)) {
            return $string;
        }
        // Если не нашли и включена отладка, то пишем полный путь к константе
        if (defined('DEVMODE')) {
            return $langSpace.':'.$name;
        }
        // Если есть решетка, то ничего не возвращаем (для #help)
        if (haveStr($name, '#')) {
            return null;
        }
        // Вернем имя как наиболее читабельное что у нас есть
        return $name;
    }
    
    /**
     * Ищем строку для конкретного языка
     * @param string $lang
     * @param string $langSpace
     * @param string $name
     * @return string
     */
    protected function subGetString($lang, $langSpace, $name)
    {
        $this->tryConfig($lang);
        // Ищем в нужном языке и ланспейсе
        if (isset($this->langData[$lang][$langSpace][$name])) {
            return $this->langData[$lang][$langSpace][$name];
        }
        // Ищем в нужном языке и родительских лангспейсах
        // Обратный порядок чтобы правильно наследовать
        $parents = array_reverse($this->langData[$lang][$langSpace]['#parentsList']);
        foreach ($parents as $parentLangSpace) {
            if (isset($this->langData[$lang][$parentLangSpace][$name])) {
                return $this->langData[$lang][$parentLangSpace][$name];
            }
        }
        // Ищем в нужном языке и дефолтном ланспейсе
        if (isset($this->langData[$lang]['#default'][$name])) {
            return $this->langData[$lang]['#default'][$name];
        }
        return null;
    }
}
