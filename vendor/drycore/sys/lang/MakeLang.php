<?php
namespace drycore\sys\lang;

/**
 * Подготовка языкового конфига с учетом его специфики
 */
class MakeLang extends \proto\Component
{
    protected $parentsData = [];
    protected $folder;

    public function makeLang($langs)
    {
        foreach ($langs as $lang) {
            $this->prepareLang($this->folder.'/'.$lang.'.json');
        }
    }

    protected function prepareLang($filename)
    {
        $config = loadJson($filename);
        foreach ($this->parentsData as $key => $parentsList) {
            if (!isset($config[$key])) {
                $config[$key] = [];
            }
            if (!isset($config[$key]['#parentsList'])) {
                $config[$key]['#parentsList'] = [];
            }
            $config[$key]['#parentsList'] = array_merge($parentsList, $config[$key]['#parentsList']);
        }
        saveJson($filename, $config);
    }
    
    public function loadParents($typeList)
    {
        $parentsData = [];
        foreach ($typeList as $type) {
            $config = loadJson($this->folder.'/'.$type.'s.json');
            foreach ($config as $key => $value) {
                $name = $type.'/'.$key;
                $parentsList = [];
                if (isset($value['#parentsList'])) {
                    foreach ($value['#parentsList'] as $parent) {
                        $parentsList[] = $type.'/'.$parent;
                    }
                }
                $parentsData[$name] = $parentsList;
            }
        }
        $this->parentsData = $parentsData;
    }
}
