<?php
namespace drycore\sys\arrayBox;

/**
 * Description of RegBox
 *
 * @author Maks
 */
class FolderBox extends AbstractArrayBox
{
    protected $folder = null;
    protected $runtimeFolder = null;
    
    public function __construct($config = null)
    {
        parent::__construct($config);
        if (is_null($this->folder)) {
            $this->folder = RUNTIME_FOLDER.'/'.$this->runtimeFolder;
        }
    }

    protected function loadArr()
    {
        $dir = array_diff(scandir($this->folder.'/'), array('..', '.'));
        $arr = [];
        foreach ($dir as $line) {
            $filename = $this->folder.'/' . $line;
            if (!haveStr($line, '.')) {
                $arr[$line] = json_decode(file_get_contents($filename), true);
            }
        }
        return $arr;
    }

    protected function saveArr($arr)
    {
        // Удалим/забекапим текущие файлы
        $dir = array_diff(scandir($this->folder.'/'), array('..', '.'));
        foreach ($dir as $line) {
            $filename = $this->folder.'/' . $line;
            if (!haveStr($line, '.')) {
                if (file_exists($filename.'.bak')) {
                    unlink($filename.'.bak');
                }
                if (file_exists($filename)) {
                    rename($filename, $filename.'.bak');
                }
            }
        }
        // Сохраняем новый вариант
        foreach ($arr as $key => $value) {
            $filename = $this->folder.'/'.$key;
            $data = json_encode($value, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            file_put_contents($filename, $data);
        }
    }
}
