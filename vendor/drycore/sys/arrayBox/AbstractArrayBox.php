<?php
namespace drycore\sys\arrayBox;

/**
 * Description of AbstractArrayBox
 *
 * @author Maks
 */
abstract class AbstractArrayBox extends \drycore\sys\box\AbstractBox
{
    use PrepareArrayTrait;
    
    /**
     * Данная функция должна возвращать ВСЕ данные из бокса для дальнейшей обработки
     */
    abstract protected function loadArr();
    
    /**
     * Метод сохраняет ВСЕ данные которые были переданы, в том числе и не измененные
     * если элемента в массиве нет, то он удаляется
     */
    abstract protected function saveArr($arr);
    
    /**
     * Считаем колво элементов в боксе с учетом условия
     * @param type $where
     * @return boolean
     */
    public function count($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $arr = $this->loadArr();
        $arr = $this->filterArray($arr);
        return count($arr);
    }

    /**
     * Удалим всех удовлетворяющих условию
     * @param type $where
     * @return boolean
     */
    public function delete($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $arr = $this->loadArr();
        $filtered = $this->filterArray($arr);
        // Удаляем все найденные элементы и сохраняем
        foreach (array_keys($filtered) as $key) {
            unset($arr[$key]);
        }
        $this->saveArr($arr);
        return true;
    }
    
    /**
     * Найдем все модели удовлетворяющие условию
     * @param type $where
     * @return type
     */
    public function findAll($where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $arr = $this->loadArr();
        $arr = $this->filterArray($arr);
        $arr = $this->orderArray($arr);
        $arr = $this->limitArray($arr);
        // Сделаем из текущих данных - модели
        $result = [];
        foreach ($arr as $line) {
            $model = $this->model();
            $model->load($line);
            $result[] = $model;
        }
        return $result;
    }

    /**
     * Вернем первую удовлетворяющую условию
     * @param type $where
     * @return boolean
     */
    public function findOne($where = null)
    {
        $arr = $this->findAll($where);
        // Если есть что-то, то вернем первую
        if (isset($arr[0])) {
            return $arr[0];
        } else {
            return false;
        }
    }

    /**
     * Добавим новую запись в наш бокс и сохраним
     * @param type $data
     */
    public function insert($data)
    {
        $arr = $this->loadArr();
        if (!isset($data['id']) or !$data['id']) {
            $data['id'] = count($arr)+1;
        }
        $arr[$data['id']] = $data;
        $this->saveArr($arr);
    }

    /**
     * Обновим данные с учетом условия и новых данных
     * @param type $data
     * @param type $where
     * @return boolean
     */
    public function update($data, $where = null)
    {
        if ($where) {
            $this->where($where);
        }
        $arr = $this->loadArr();
        $filtered = $this->filterArray($arr);
        // обновим тех кого нашли
        foreach (array_keys($filtered) as $key) {
            $arr[$key] = array_merge($arr[$key], $data);
        }
        $this->saveArr($arr);
    }
}
