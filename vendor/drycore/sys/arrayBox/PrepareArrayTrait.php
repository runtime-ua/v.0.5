<?php
namespace drycore\sys\arrayBox;

/**
 * Вспомогательные функции для работы с массивом - сортировка, фильтрация и т.п.
 */
trait PrepareArrayTrait
{
    /**
     * Фильтруем массив по  ОДНОМУ условию
     * @param type $arr
     * @param type $where
     * @return type
     */
    private function oneFilterArray($arr, $where)
    {
        $out = [];
        foreach ($arr as $id => $line) {
            $ok = true;
            foreach ($where as $key => $value) {
                if ($line[$key]!=$value) {
                    $ok = false;
                }
            }
            if ($ok) {
                $out[$id] = $line;
            }
        }
        return $out;
    }
    
    /**
     * Фильтруем массив по нашим условиям
     * @param type $arr
     * @return array
     * @throws \Exception
     */
    protected function filterArray($arr)
    {
        foreach ($this->where as $data) {
            // Если истина, значит дадим всегда истинное условие
            if (true === $data) {
                $data = [];
            }
            // Если ложь, значит дадим всегда ложное условие
            if (false === $data) {
                return [];
            }
            // Если скаляр а не массив, то это ИД записи, и сделаем условие по ИД
            if (!is_array($data)) {
                $data = ['id' => $data];
            }
            // Если у условия есть нулевое поле то это особое условие которое мы не поддерживаем
            if (isset($data[0])) {
                throw new \Exception('Unsupportet where format');
            }
            // Отфильтруем по подготовленному условию
            $arr = $this->oneFilterArray($arr, $data);
        }
        return $arr;
    }
    
    /**
     * Применим к массиву наши лимиты с сдвиги
     * @param type $arr
     * @return type
     */
    protected function limitArray($arr)
    {
        if (!($this->limit === false)) {
            if (is_null($this->offset)) {
                $this->offset = 0;
            }
            if (is_null($this->limit)) {
                $this->limit = count($arr);
            }
            $arr = array_slice($arr, $this->offset, $this->limit, true);
        }
        return $arr;
    }

    /**
     * Сортируем массив по нашим правилам сортировки
     * @param type $arr
     * @return type
     */
    protected function orderArray($arr)
    {
        if (!$this->order) {
            $this->defaultOrder();
        }
        if ($this->order) {
            uasort($arr, [$this,'compareOrder']);
        }
        return $arr;
    }
    
    /**
     * Определяем кто первый из двух записей с учетом текущего правила сортировки
     * Учитывает только первое правило, доделать в цикл
     * Вызывается как колбек из функции сортировки
     * @param type $a
     * @param type $b
     * @return int
     */
    private function compareOrder($a, $b)
    {
        $order = $this->order[0];
        if ($order{0}=='!') {
            $field = substr($order, 1);
            if ($a[$field] == $b[$field]) {
                return 0;
            }
            return ($a[$field] > $b[$field]) ? -1 : 1;
        } else {
            $field = $order;
            if ($a[$field] == $b[$field]) {
                return 0;
            }
            return ($a[$field] < $b[$field]) ? -1 : 1;
        }
    }
}
