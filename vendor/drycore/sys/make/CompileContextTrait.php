<?php
namespace drycore\sys\make;

/**
 * Трейт компиляции контекстов (основной конфиг компилируется отдельно)
 */
trait CompileContextTrait
{
    protected function compileContext($config)
    {
        foreach ($config as $name => $value) {
            if (isset($this->config['#default'][$this->contextFieldName])) {
                $default = $this->config['#default'][$this->contextFieldName];
            } else {
                $default = [];
            }
            //
            $current = $this->config[$name][$this->contextFieldName];
            $current = $this->mergeContext($default, $current);// В обратном порядке)
            //
            $parentsList = array_reverse($value['#parentsList']);
            foreach ($parentsList as $parentName) {
                $parent = $this->config[$parentName][$this->contextFieldName];
                $current = $this->mergeContext($parent, $current);// В обратном порядке)
            }
            $config[$name][$this->contextFieldName] = $current;
        }
        return $config;
    }
    
    /**
     * Рекурсивно дополняем конфиг содержимым конфига "родителя"
     * @param array $oldContext
     * @param array $name
     * @return array
     */
    private function recursiveGetContext($oldContext, $name, $context)
    {
        if (isset($this->config[$name])) {
            $currentConfig = $this->config[$name];
        } else {
            $currentConfig = [];
        }
        if (isset($currentConfig[$this->contextFieldName][$context])) {
            $currentContext = $currentConfig[$this->contextFieldName][$context];
        } else {
            $currentContext = [];
        }
        if (isset($currentConfig['#parent'])) {
            $parents = $currentConfig['#parent'];
            unset($currentConfig['#parent']);
            //
            if (!is_array($parents)) {
                $parents = [$parents];
            }
            foreach ($parents as $parent) {
                $currentContext = $this->recursiveGetContext($currentContext, $parent, $context);
            }
        }
        return $this->mergeContext($currentContext, $oldContext);
    }
    
    /**
     * Объединям массивы конфигов контекста
     * @param array $config1
     * @param array $config2
     * @return array
     */
    private function mergeContext($config1, $config2)
    {
        foreach ($config2 as $key => $value) {
            if (isset($config1[$key])) {
                $config1[$key] = $this->mergeArray($config1[$key], $value);
            } else {
                $config1[$key] = $value;
            }
        }
        return $config1;
    }
}
