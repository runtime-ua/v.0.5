<?php
namespace drycore\sys\make;

/**
 * Класс для компиляции конфигов. Собирает конфиги из актуальных модулей и т.п.
 *
 * @author Mendel
 */
class MakeConfig extends \proto\Component
{
    use CompileConfigTrait, CompileContextTrait;
    
    /**
     * Какой контекст у текущего обрабатываемого конфига
     * @var type
     */
    protected $contextFieldName;
    
    /**
     * Текущий конфиг храним здесь
     * @var array
     */
    protected $config;

//    /**
//     * Путь к папке в которую сохраняем наши конфиги
//     * @var string
//     */
//    protected $configFolder;

    /**
     * Подготовим все основные конфиги
     * @param string $configFolder
     * @param array $configNames
     */
    public function go($configFolder, $configNames)
    {
//        $this->configFolder = $configFolder;
        //
        foreach ($configNames as $name => $contextName) {
            $this->contextFieldName = $contextName;
            $this->config = loadJson($configFolder.$name.'.json');
            $config = $this->compileConfig();
            if (!is_null($contextName)) {
                $config = $this->compileContext($config);
            }
            saveJson($configFolder.'/compiled/'.$name.'.json', $config);
        }
    }
    
    /**
     * Вспомогательная, для склейки массивов
     * @param type $arr1
     * @param type $arr2
     * @return type
     */
    protected function mergeArray($arr1, $arr2)
    {
        // Не рекурсивная потому что нумерные массивы задваиваются(
        return array_replace($arr1, $arr2);
    }
}
