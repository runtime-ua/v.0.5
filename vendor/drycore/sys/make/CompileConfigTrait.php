<?php
namespace drycore\sys\make;

/**
 * Компиляция конфигов без учета контекстов
 */
trait CompileConfigTrait
{
    protected function compileConfig()
    {
        $result = [];
        // Скомпилируем всем конфиги
        foreach (array_keys($this->config) as $name) {
            $current = $this->recursiveGetConfig([], $name);
            if (isset($this->config['#default'])) {
                $default = $this->config['#default'];
            } else {
                $default = [];
            }
            $config = $this->mergeConfig($default, $current);// В обратном порядке
            //
            $result[$name] = $config;
        }
        // Добавим список наследников
        foreach (array_keys($this->config) as $name) {
            // Добавим себе, чтобы если у нас нет детей, то хоть пустой массив был
            if (!isset($result[$name]['#childsList'])) {
                $result[$name]['#childsList'] = [];
            }
            // запишемся всем родителям в дети
            foreach ($result[$name]['#parentsList'] as $parent) {
                if (!isset($result[$parent]['#childsList'])) {
                    $result[$parent]['#childsList'] = [];
                }
                $result[$parent]['#childsList'][] = $name;
            }
        }
        return $result;
    }
    
    /**
     * Рекурсивно дополняем конфиг содержимым конфига "родителя"
     * @param array $oldConfig
     * @param array $name
     * @return array
     */
    private function recursiveGetConfig($oldConfig, $name)
    {
        if (isset($this->config[$name])) {
            $currentConfig = $this->config[$name];
        } else {
            $currentConfig = [];
        }
        if (isset($currentConfig['#parent'])) {
            $parents = $currentConfig['#parent'];
            unset($currentConfig['#parent']);
            //
            if (!is_array($parents)) {
                $parents = [$parents];
            }
            foreach ($parents as $parent) {
                $currentConfig = $this->recursiveGetConfig($currentConfig, $parent);
            }
            //
        }
        if (!isset($currentConfig['#parentsList'])) {
            $currentConfig['#parentsList'] = [];
        }
        if (isset($parents)) {
            foreach ($parents as $parent) {
                $currentConfig['#parentsList'][] = $parent;
            }
        }
        //
        return $this->mergeConfig($currentConfig, $oldConfig);
    }
    
    /**
     * Объединям массивы конфигов, удаляя из них "контекст", для отдельной обработки
     * @param array $config1
     * @param array $config2
     * @return array
     */
    private function mergeConfig($config1, $config2)
    {
        if (!is_null($this->contextFieldName)) {
            if (isset($config1[$this->contextFieldName])) {
                unset($config1[$this->contextFieldName]);
            }
            if (isset($config2[$this->contextFieldName])) {
                unset($config2[$this->contextFieldName]);
            }
        }
        return $this->mergeArray($config1, $config2);
    }
}
