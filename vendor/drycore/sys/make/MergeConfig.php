<?php
namespace drycore\sys\make;

/**
 * Объединим все конфиги
 */
class MergeConfig extends \proto\Component
{
    /**
     * Объединим все конфиги и сохраним их
     * @param type $configs
     */
    public function go($configs)
    {
        $modules = loadJson(RUNTIME_FOLDER.'/modules.json');
        foreach ($configs as $name => $filename) {
            $config = $this->moduleConfig($name, $modules);
            saveJson($filename, $config);
        }
    }
    /**
     * Собирает конфиг из всех модулей
     * @param string $name
     * @param array $modules
     * @return array
     */
    protected function moduleConfig($name, $modules)
    {
        // Загрузим основные конфиги
        $filename = APP_FOLDER . '/'.$name.'.json';
        if (file_exists($filename)) {
            $conf = loadJson($filename);
        } else {
            $conf = [];
        }
        // Загрузим конфиги всех модулей
        foreach ($modules as $module) {
            $filename = $this->moduleFolder($module).'/'.$name.'.json';
            if (file_exists($filename)) {
                $info = loadJson($filename);
                $conf = $this->addConfig($name, $conf, $info);
            }
        }
        return $conf;
    }

    /**
     * По имени модуля возвращает папку модуля
     * @param type $module
     * @return type
     */
    protected function moduleFolder($module)
    {
        list($prefix, $submodule) = explode('/', $module, 2);
        if ($prefix == 'app') {
            return APP_FOLDER.'/'.$submodule;
        } else {
            return VENDOR_FOLDER.'/'.$module;
        }
    }
    
    /**
     * Добавим к текущему общему конфигу полученный конфиг модуля
     * @param array $data
     * @param array $addData
     * @return array
     */
    protected function addConfig($type, $data, $addData)
    {
        // Пройдемся в цикле, чтобы проверить нет ли дублей
        foreach ($addData as $key => $value) {
            if (isset($data[$key])) {
                throw new \Exception('Duplicate config: '.$type.'/'.$key);
            }
            if (!is_array($value)) {
                throw new \Exception('Config not array: '.$type.'/'.$key);
            }
            $data[$key] = $value;
        }
        return $data;
    }
}
