<?php
namespace drycore\sys\box;

/**
 * Абстрактная модель с различными универсальными фичами
 */
abstract class AbstractBox extends CoreBox
{
    /**
     * Выполним правила добавляющие фильтры на бокс
     * @param type $config
     */
    public function __construct($config = null)
    {
        parent::__construct($config);
        foreach ($this->fieldsList() as $fieldName) {
            $interpreter = $this->interpreter('boxFilter', $fieldName);
            $where = $interpreter->value;
            if (!is_null($where)) {
                $this->where($where);
            }
        }
    }
}
