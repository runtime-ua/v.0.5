<?php
namespace drycore\sys\box;

class BoxFactory extends \proto\ConfigurableService
{
    public function factory($name)
    {
        list($name, $scenario) = explode(':', $name, 2);
        $config = $this->config($name);
        // Узнаем какой класс вызывать
        if (isset($config['#class'])) {
            $class = $config['#class'];
            unset($config['#class']);
        } else {
            throw new BoxNotFoundException($name);
        }
        //
        $config['#scenario'] = $scenario;
        $box = new $class($config);
        return $box;
    }
}
