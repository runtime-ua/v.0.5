<?php
namespace drycore\sys\box;

abstract class CoreBox extends \proto\Component
{
    use ApiTrait;
    
    protected $scenario = 'default';
    protected $where = [];
    protected $limit;
    protected $page = 1;
    protected $offset;
    protected $order;
    protected $group = [];
    protected $fields = '*';
    protected $distinct=false;
    
    // Установим нужный сценарий. Здесь, чтобы переопределение позже работало в нужном сценарии сразу
    public function __construct($config = array())
    {
        $this->scenario($config['#scenario']);
        unset($config['#scenario']);
        parent::__construct($config);
    }

        /**
     * Переопределим, поскольку нам нужны строки от моделей
     * @return string
     */
    public function langType()
    {
        return 'model';
    }
    
    /**
    * Получаем (устанвливаем) сценарий. Доступ через метод чтобы
    * при этом менять список разрешенных полей
    * @param string $scenario сценарий, или пусто если чтение
    * @return string текущий сценарий
    */
    public function scenario($scenario = null)
    {
        if (!is_null($scenario)) {
            $this->scenario = $scenario;
        }
        return $this->scenario;
    }

    /**
     * Возвращает тип компонента (контроллер, модель и т.п.)
     * @return string
     */
    public function componentType()
    {
        return 'box';
    }
    
    /**
     * Создаем дефолтную модель для этой "коробки"
     * @return type
     */
    public function model()
    {
        return model($this->selfType().':'. $this->scenario());
    }

    public function where($where)
    {
        $this->where[] = $where;
        return $this;
    }
    
    public function getLimit()
    {
        return $this->limit;
    }
    
    public function limit($limit, $offset = null)
    {
        $this->limit = $limit;
        if (!is_null($offset)) {
            $this->offset = $offset;
        }
        return $this;
    }

    public function page($page = null)
    {
        if ($page) {
            $this->page = $page;
        }
        $offset = ($page - 1)* $this->limit; // минус 1 потому, что первая страница с нуля
        if ($offset > 0) {
            $this->offset = $offset;
        } else {
            $this->offset = null; // потому что в оригинале нулл, и лень проверять что ноль тоже ок.
        }
        //
        return $this;
    }
    
    public function order($order)
    {
        $this->order = $order;
        return $this;
    }
    
    public function group($group)
    {
        $this->group = $group;
        return $this;
    }
    
    public function having($having)
    {
        $this->having[] = $having;
        return $this;
    }
    
    public function fields($fields)
    {
        $this->fields = $fields;
        return $this;
    }
        
    public function distinct($distinct = true)
    {
        $this->distinct = $distinct;
        return $this;
    }
    
    protected function defaultOrder()
    {
        $order = [];
        foreach ($this->fieldsList() as $fieldName) {
            $interpreter = $this->interpreter('boxDefaultOrder', $fieldName);
            if ($interpreter->value) {
                $order[] = $fieldName;
            }
        }
        $this->order = $order;
    }

    abstract public function insert($data);
    
    abstract public function update($data, $where = null);

    abstract public function delete($where = null);
    
    abstract public function findOne($where = null);
    
    abstract public function findAll($where = null);
    
    abstract public function count($where = null);
}
