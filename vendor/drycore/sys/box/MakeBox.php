<?php
namespace drycore\sys\box;

/**
 * Допобработка конфигов боксов, а именно создание оных из конфига
 * моделей, и добавление им списка родителей от моделей
 */
class MakeBox extends \proto\Component
{
    /**
     * 
     * @param type $modelsFilename
     * @param type $boxsFilename
     */
    public function go($modelsFilename, $boxsFilename)
    {
        $models = loadJson($modelsFilename);
        $boxs = [];
        foreach (array_keys($models) as $modelType) {
            if (isset($models[$modelType]['box'])) {
                $box = $models[$modelType]['box'];
                unset($models[$modelType]['box']);
            } else {
                $box = [];
            }
            if (isset($models[$modelType]['#parent'])) {
                $box['#parent'] = $models[$modelType]['#parent'];
            }
            $boxs[$modelType] = $box;
        }
        saveJson($modelsFilename, $models);
        saveJson($boxsFilename, $boxs);
    }
}
