<?php
// initialisation
ini_set('session.auto_start', 0);
ini_set('default_charset', 'UTF-8');
ini_set('auto_detect_line_endings',TRUE);
//mb_internal_encoding("UTF-8");
ini_set('error_reporting', E_ERROR | E_USER_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_RECOVERABLE_ERROR | E_PARSE);
// Включим вывод ошибок в браузер, чтобы выводилось даже если не перехвачено
if(defined('DEVMODE')) {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}
// Подключим функции
require_once VENDOR_FOLDER.'/drycore/sys/functions.php';
require_once VENDOR_FOLDER.'/drycore/core/functions.php';
require_once VENDOR_FOLDER.'/drycore/core/view/functions.php';
require_once VENDOR_FOLDER.'/drycore/util/functions.php';

// Автозагрузка
require_once VENDOR_FOLDER.'/drycore/sys/autoload.php';
spl_autoload_register('autoload');
// Компилируем конфиги если нужно
if(defined('DEVMODE')) require_once VENDOR_FOLDER.'/drycore/sys/make.php';
// Инитим сервисы
s()->loadConfig(loadJson(RUNTIME_FOLDER.'/config/compiled/services.json'));
// Cообщим сервисам контекст
s()->setContext($mode);
// Включим нашу обработку ошибок и т.п.
s()->main->init();
// Инитим модели
s()->modelFactory->loadConfig(loadJson(RUNTIME_FOLDER.'/config/compiled/models.json'));
// Инитим боксы
s()->boxFactory->loadConfig(loadJson(RUNTIME_FOLDER.'/config/compiled/boxs.json'));
// Инитим правила
s()->ruleFactory->loadConfig(loadJson(RUNTIME_FOLDER.'/config/compiled/rules.json'));
// Инициируем контроллеры
s()->controllerFactory->loadConfig(loadJson(RUNTIME_FOLDER.'/config/compiled/controllers.json'));
// Инициируем вьюв
s()->viewFactory->loadConfig(loadJson(RUNTIME_FOLDER.'/config/compiled/views.json'));
// Запустим все обязательные сервисы
s()->requiredServices->start();
// Если нам передали роут (например инсталяция), то установим его
if(isset($route)) s()->route->fields[0] = $route;
// Если существует такой файл, то выполним его 
// (для всяких аварийных вызовов когда ядро уже запущено,
// но основной цикл по каким-то причинам не заводится)
if(file_exists(HOME.'/path.php')) require_once HOME.'/path.php';
// Запустим основной цикл
s()->main->start();
