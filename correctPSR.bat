@set PHP_DIR=c:\OpenServer\modules\php\PHP-5.6\
@set PHP_BIN=%PHP_DIR%php.exe
@if "%PHP_BIN%" == "" set PHP_BIN=php.exe

@echo "PSR-2"
@%PHP_BIN% c:\OpenServer\util\phpcbf.phar --standard=PSR2 app
@%PHP_BIN% c:\OpenServer\util\phpcbf.phar --standard=PSR2 proto
@%PHP_BIN% c:\OpenServer\util\phpcbf.phar --standard=PSR2 vendor\drycore
@%PHP_BIN% c:\OpenServer\util\phpcbf.phar --standard=PSR2 vendor\drycart
@%PHP_BIN% c:\OpenServer\util\phpcbf.phar --standard=PSR2 vendor\drystyle
