<?php
// Список всех поддерживаемых нашим скриптом языком и соответственно набор файлов с их строками
const LANG_LIST = array('rusLang','engLang');
// Объявим пути
define('HOME', dirname(__FILE__));
const VENDOR_FOLDER = HOME. '/vendor';
const APP_FOLDER = HOME. '/app';
const PROTO_FOLDER = HOME. '/proto';
const RUNTIME_FOLDER = HOME. '/runtime';
const SERVER_UPLOADER_FOLDER = HOME. '/uploads';
const SITE_UPLOADER_FOLDER = '/uploads';
// Проверим флаги
if(file_exists(RUNTIME_FOLDER.'/flag/devmode')) define('DEVMODE', TRUE);
if(file_exists(RUNTIME_FOLDER.'/flag/debug')) define('DEBUG', TRUE);
// Сообщим в каком режиме запускаться (т.е. какой конфиг сервисов включать)
$mode = 'cmd';
// Выполним основной запуск ядра
require_once HOME.'/start.php';